program tonaz_bezwodnyTests2;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  Woda in '..\Woda.pas',
  woda_testy in 'woda_testy.pas';

{$R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.


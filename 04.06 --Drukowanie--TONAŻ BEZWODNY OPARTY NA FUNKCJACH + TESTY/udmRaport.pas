unit udmRaport;

interface

uses
  System.SysUtils, System.Classes, frxClass;

type
  TdmRaport = class(TDataModule)
    frxReport1: TfrxReport;
    procedure frxReport1NewGetValue(Sender: TObject; const VarName: string;
      var Value: Variant);
  private
    FTonazSmoly: Double;
    FProcentWody: Double;
    FtonazBezwodny: Double;
    FPoleSAP1:Integer;
    FPoleSAP2:Integer;
    FNumerDostawy:String;
    { Private declarations }
  public
    { Public declarations }
    procedure Podglad;
    property TonazSmoly: Double read FTonazSmoly write FTonazSmoly;
    property ProcentWody: Double read FProcentWody write FProcentWody;
    property TonazBezwodny: Double read FTonazBezwodny write FTonazBezwodny;
    property PoleSAP1: Integer read FPoleSAP1 write FPoleSAP1;
    property PoleSAP2: Integer read FPoleSAP2 write FPoleSAP2;
    property NumerDostawy: string read FNumerDostawy write FNumerDostawy;
  end;

var
  dmRaport: TdmRaport;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmRaport.frxReport1NewGetValue(Sender: TObject;
  const VarName: string; var Value: Variant);
begin
  if VarName = 'TonazSmoly' then
    Value := TonazSmoly;
  if VarName = 'ProcentWody' then
    Value := ProcentWody;
  if VarName = 'TonazBezwodny' then
    Value := TonazBezwodny;
  if VarName = 'PoleSAP1' then
    Value := PoleSAP1;
  if VarName = 'PoleSAP2' then
    Value := PoleSAP2;
  if VarName = 'NumerDostawy' then
    Value := NumerDostawy;

end;

procedure TdmRaport.Podglad;
begin
  frxReport1.ShowReport;
end;

end.

unit U_dop_wielk_formy;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm2 = class(TForm)
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure zmien_wielkosc_formy(Sender: TObject);
    procedure zmien_styl_buttona(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.FormCreate(Sender: TObject);
begin
Edit1.Text:= '500';
Edit2.Text:='600';

end;

procedure TForm2.zmien_wielkosc_formy(Sender: TObject);
begin
 Form2.Height:= StrToInt(Edit1.TEXT);
 Form2.Width:= StrToInt(Edit2.Text);
end;

procedure TForm2.zmien_styl_buttona(Sender: TObject);
begin
Form2.color:= clBlack;
Button1.Font.Style:= [fsBold,fsunderline];

end;

end.

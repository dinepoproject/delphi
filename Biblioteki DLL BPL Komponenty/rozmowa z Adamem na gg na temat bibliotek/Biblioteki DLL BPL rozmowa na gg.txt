13 maja 2017
19:50
Traptak
Biblioteki dll pisze si� dosy� trudno w Delphi. Oczywi�cie da si�, ale ka�da biblioteka ma wkompilowany w�asny zestaw klas podstawowych. To powoduje, �e przekazywanie bardziej z�o�onych struktur cz�sto robi si� dosy� skomplikowane - trzeba wszystko rozbija� na typy proste. Tych ogranicze� nie maj� w Delphi biblioteki bpl, ale te mog� by� u�ywane wy��cznie przez programy napisane w Delphi. Dodatkowo musz� by� skompilowane w dok�adnie tej samej wersji Delphi i komponent�w co u�ywaj�ca je g��wna aplikacja.
Ale je�li ca�o�� systemu powstaje w jednym �rodowisku (tak jak Baza kolejowa) to nie ma przeszk�d by je stosowa�.
19:52
Dzi�ki temu mog� np. uzyskiwa� r�ne dzia�anie tych samych funkcji u r�nych klient�w. U ka�dego z nich jest za�adowana do aplikacji inna biblioteka bpl z r�nymi wersjami kodu. Tak na roboczo biblioteki bpl nazywam pakietami.
19:54
Co do test�w, to musisz na razie robi� jest w starszej wersji biblioteki. Inaczej nie b�d� m�g� ich pod��czy� do modu��w testowych, kt�re u�ywaj� nowej wersji.
Dopiero jak przejdziemy produkcyjnie na nowsze Delphi b�d� aktualizowa� system testowy.
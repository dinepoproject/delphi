object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 365
  ClientWidth = 712
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 0
    Top = 344
    Width = 170
    Height = 25
    Caption = 'CA'#321'A TABLICA CHAR'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 0
    Top = 8
    Width = 185
    Height = 330
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 1
    StyleElements = [seFont, seClient]
  end
  object Button2: TButton
    Left = 224
    Top = 105
    Width = 241
    Height = 25
    Caption = 'NA BUTTONIE POKAZE  ZNAK'
    TabOrder = 2
    OnClick = Button2Click
  end
  object LabeledEdit1: TLabeledEdit
    Left = 216
    Top = 24
    Width = 449
    Height = 21
    EditLabel.Width = 104
    EditLabel.Height = 13
    EditLabel.Caption = 'WPROWADZ ZDANIE '
    TabOrder = 3
  end
  object LabeledEdit2: TLabeledEdit
    Left = 216
    Top = 64
    Width = 97
    Height = 21
    EditLabel.Width = 111
    EditLabel.Height = 13
    EditLabel.Caption = 'PODAJ NUMER CHAR'#39'A'
    TabOrder = 4
  end
  object LabeledEdit3: TLabeledEdit
    Left = 224
    Top = 184
    Width = 161
    Height = 21
    EditLabel.Width = 144
    EditLabel.Height = 13
    EditLabel.Caption = 'WPROWADZ LICZBE 0 D0 255'
    TabOrder = 5
  end
  object Button3: TButton
    Left = 224
    Top = 224
    Width = 187
    Height = 25
    Caption = 'NA BUTTONIE POKAZE ZNAK '
    TabOrder = 6
    OnClick = Button3Click
  end
end

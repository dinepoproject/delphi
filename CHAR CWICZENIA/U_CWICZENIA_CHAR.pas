unit U_CWICZENIA_CHAR;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm2 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
var
i:Integer;
c:array[0..255] of Char;
s:array[0..255] of string;
begin
  for I := 0 to 255 do
  begin
  c[i]:= Char(i);
  s[i]:= c[i];

  Memo1.Lines.add(i.ToString + ' = ' + (s[i]));
   end;
end;

procedure TForm2.Button2Click(Sender: TObject);
//     https://4programmers.net/Forum/Delphi_Pascal/83623-Char_do_string
//    http://pascal.kurs-programowania.pl/turbo_pascal,operacje_na_tekstach.html
//       https://4programmers.net/Delphi/Tablice
VAR
S: string;
I:Integer;
C: Char;
begin
  S:= LabeledEdit1.Text;
  I:= StrToInt(LabeledEdit2.Text);
  C:= S[I];
  Button2.Caption:= C;

end;

procedure TForm2.Button3Click(Sender: TObject);
var
I:INTEGER;
begin
I:= StrToInt(LabeledEdit3.Text);
Button3.Caption:= Chr(I);
end;


end.

object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 483
  ClientWidth = 766
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Edit1: TEdit
    Left = 8
    Top = 24
    Width = 305
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 344
    Top = 24
    Width = 345
    Height = 21
    TabOrder = 1
    Text = 'Edit2'
    OnChange = Edit2Change
  end
  object btn1_odczyt_edit1_do_wlasc: TButton
    Left = 64
    Top = 80
    Width = 217
    Height = 25
    Caption = 'btn1_odczyt_edit1_do_wlasc'
    TabOrder = 2
    OnClick = btn1_odczyt_edit1_do_wlascClick
  end
  object Pokaz_wlasciwosc_w_edit2: TButton
    Left = 400
    Top = 80
    Width = 209
    Height = 25
    Caption = 'Pokaz_wlasciwosc_w_edit2'
    TabOrder = 3
    OnClick = Pokaz_wlasciwosc_w_edit2Click
  end
  object ZMIANA_WLASC_WSKAZNIKIEM: TButton
    Left = 64
    Top = 167
    Width = 249
    Height = 25
    Caption = 'ZMIANA_WLASC_WSKAZNIKIEM'
    TabOrder = 4
    OnClick = ZMIANA_WLASC_WSKAZNIKIEMClick
  end
  object Memo1: TMemo
    Left = 40
    Top = 240
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    TabOrder = 5
  end
  object DODAJ_WART_WLASC_DO_MEMO: TButton
    Left = 288
    Top = 288
    Width = 273
    Height = 25
    Caption = 'DODAJ_WART_WLASC_DO_MEMO'
    TabOrder = 6
    OnClick = DODAJ_WART_WLASC_DO_MEMOClick
  end
end

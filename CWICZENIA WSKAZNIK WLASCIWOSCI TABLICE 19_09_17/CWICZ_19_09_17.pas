unit CWICZ_19_09_17;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

 type
  Tklasa = class (Tobject)

  private
    pole_do_przech_wart_wlasc: string;
    function odczyt: string;
    procedure zapis (const value: string);
    procedure ZMIANA_PROCEDURY_WSKAZNIKIEM;

  public
   property znaki: string read odczyt write zapis;

  end;
type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    btn1_odczyt_edit1_do_wlasc: TButton;
    Pokaz_wlasciwosc_w_edit2: TButton;
    ZMIANA_WLASC_WSKAZNIKIEM: TButton;
    Memo1: TMemo;
    DODAJ_WART_WLASC_DO_MEMO: TButton;
    procedure btn1_odczyt_edit1_do_wlascClick(Sender: TObject);
    procedure DODAJ_WART_WLASC_DO_MEMOClick(Sender: TObject);
    procedure Edit2Change(Sender: TObject);

    procedure Pokaz_wlasciwosc_w_edit2Click(Sender: TObject);
    procedure ZMIANA_WLASC_WSKAZNIKIEMClick(Sender: TObject);

  private
    { Private declarations }
     Uchwyt_Tklasa: tklasa;
     DANE:array OF string;
     ILE: Integer;
     NUMER:Integer;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor destroy; override;

  end;




var
  Form1: TForm1;


implementation

{$R *.dfm}

{ TForm1 }

constructor TForm1.Create(AOwner: TComponent);
begin
  inherited;
  SetLength(DANE,1);
  ILE:= 1;
  NUMER:=0;
 Uchwyt_Tklasa:=tklasa.create;

end;

destructor TForm1.destroy;
begin
 uchwyt_tklasa.free;
  inherited;
end;

procedure TForm1.btn1_odczyt_edit1_do_wlascClick(Sender: TObject);
begin
Uchwyt_Tklasa.znaki:= Edit1.Text;
end;

procedure TForm1.DODAJ_WART_WLASC_DO_MEMOClick(Sender: TObject);
VAR
I:Integer;
begin

 BEGIN
  {for I := 0 to ILE-1 do

   SetLength(DANE,ILE+1);
   NUMER:= ILE;
   DANE[NUMER]:= Edit2.Text;
    ILE:= ILE +1;
   Memo1.Text:= Memo1.TEXT+ DANE[NUMER];   }



 END;
  Memo1.Lines.Add(Uchwyt_Tklasa.znaki);  // teraz naprawde dodaje wlascowoisci :)))
                                         // ale cwiczenia na tablice jest super (zakomentowane jest)
 { var
i:Integer;
begin
begin

    for i  := 0 to rozmiar_tablicy-1 do    // [1]
  SetLength(dane,rozmiar_tablicy +1);    // [0,1] pamietaj to tylko deklaracja!!!
  punkt_tablicy:= rozmiar_tablicy;       //[0]
  dane[punkt_tablicy]:= Edit1.Text;
  rozmiar_tablicy:=rozmiar_tablicy +1;
  Memo1.text := Memo1.Text+ dane[punkt_tablicy];
end;
Memo1.Lines.Add('');}
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
  //Edit2.Text:= DANE[NUMER];
end;



procedure TForm1.Pokaz_wlasciwosc_w_edit2Click(Sender: TObject);
begin
Edit2.Text:= Uchwyt_Tklasa.znaki;
end;

procedure TForm1.ZMIANA_WLASC_WSKAZNIKIEMClick(Sender: TObject);
begin
Uchwyt_Tklasa.ZMIANA_PROCEDURY_WSKAZNIKIEM;
end;

{ Tklasa }

function Tklasa.odczyt: string;
begin
 result:= pole_do_przech_wart_wlasc;
end;

procedure Tklasa.zapis(const value: string);
begin
pole_do_przech_wart_wlasc:= value;
end;

procedure Tklasa.ZMIANA_PROCEDURY_WSKAZNIKIEM;
VAR
X: ^STRING;
begin
 X:= @pole_do_przech_wart_wlasc;
 X^:= 'TRI';
end;

end.


object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 342
  ClientWidth = 734
  Color = clAqua
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 56
    Top = 64
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object SpeedButton1: TSpeedButton
    Left = 392
    Top = 64
    Width = 23
    Height = 22
    Hint = 'Dodaj do listy'
    Caption = '+'
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 464
    Top = 64
    Width = 23
    Height = 22
    Hint = 'Usu'#324' z listy'
    HelpType = htKeyword
    HelpKeyword = '-'
    Caption = '-'
    OnClick = SpeedButton2Click
  end
  object ComboBox1: TComboBox
    Left = 184
    Top = 61
    Width = 145
    Height = 21
    TabOrder = 0
    Text = 'ComboBox1'
    OnKeyDown = ComboBox1KeyDown
  end
end

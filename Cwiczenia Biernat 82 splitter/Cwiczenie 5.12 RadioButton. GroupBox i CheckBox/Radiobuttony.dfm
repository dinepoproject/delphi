object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 417
  ClientWidth = 650
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 56
    Top = 24
    Width = 257
    Height = 177
    Caption = 'Kolor Formatki'
    TabOrder = 0
    object RadioButton1: TRadioButton
      Left = 0
      Top = 40
      Width = 113
      Height = 17
      Caption = 'RadioButton1'
      TabOrder = 0
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Left = 0
      Top = 80
      Width = 113
      Height = 17
      Caption = 'RadioButton2'
      TabOrder = 1
      OnClick = RadioButton1Click
    end
    object RadioButton3: TRadioButton
      Left = 0
      Top = 103
      Width = 113
      Height = 17
      Caption = 'RadioButton3'
      TabOrder = 2
      OnClick = RadioButton1Click
    end
    object RadioButton8: TRadioButton
      Left = 3
      Top = 140
      Width = 113
      Height = 17
      Caption = 'RadioButton8'
      TabOrder = 3
      OnClick = RadioButton1Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 352
    Top = 24
    Width = 257
    Height = 353
    Caption = 'Wy'#347'wietl napis na pasku tytu'#322'owym'
    TabOrder = 1
    object RadioButton4: TRadioButton
      Left = 16
      Top = 40
      Width = 113
      Height = 17
      Caption = 'Delphi'
      TabOrder = 0
      OnClick = RadioButton4Click
    end
    object RadioButton5: TRadioButton
      Left = 14
      Top = 80
      Width = 113
      Height = 17
      Caption = 'Atari'
      TabOrder = 1
      OnClick = RadioButton4Click
    end
    object RadioButton6: TRadioButton
      Left = 16
      Top = 112
      Width = 113
      Height = 17
      Caption = 'Komputer'
      TabOrder = 2
      OnClick = RadioButton4Click
    end
    object RadioButton7: TRadioButton
      Left = 19
      Top = 151
      Width = 113
      Height = 17
      Caption = 'Napis standardowy'
      TabOrder = 3
      OnClick = RadioButton4Click
    end
  end
  object CheckBox1: TCheckBox
    Left = 64
    Top = 312
    Width = 193
    Height = 17
    AllowGrayed = True
    Caption = 'Okna na ca'#322'y ekran'
    TabOrder = 2
    OnClick = CheckBox1Click
  end
end

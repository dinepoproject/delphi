unit Radiobuttony;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CheckBox1: TCheckBox;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton8: TRadioButton;

    procedure RadioButton4Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
if
(CheckBox1.Checked = False) then
begin
  Form1.WindowState := wsNormal;
end
else
begin
  Form1.WindowState := wsMaximized;
end;
end;



procedure TForm1.RadioButton1Click(Sender: TObject);
begin
 if RadioButton1.Checked = True then  Color:= clRed;
if RadioButton2.Checked = True then Color:= clGreen;
if RadioButton3.Checked = True then Color:= clGray;
if RadioButton8.Checked = true then Color := clYellow;

end;

procedure TForm1.RadioButton4Click(Sender: TObject);
begin
if RadioButton4.Checked = True then Caption := 'Delphi';
if RadioButton5.Checked = True then Caption := 'Atari';
if RadioButton6.Checked = True then Caption := 'Komputer';
if RadioButton7.Checked = True then Caption := 'Komponent Radio Button';
end;

end.

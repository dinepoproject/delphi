unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.MPlayer;

type
  TForm1 = class(TForm)
    MediaPlayer1: TMediaPlayer;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
//http://forum.4programmers.net/Newbie/206847-jak_wstawic_opensavedialog_do_zewnetrznego_modulu
{$R *.dfm}
// wyglada na to, ze jak jest T przed np. Topendialog to trzeba robic klase:
procedure TForm1.Button1Click(Sender: TObject);
var
opendialog1 : Topendialog;
begin
opendialog1:=TOpenDialog.Create(nil); //nil znalezione w zielonym,
//spytac Adama o to.
 { Normalnie zamiast nil powinno si� poda� wska�nik do instancji klasy,
  kt�ra b�dzie w�a�cicielem tworzonej instancji.
  Kiedy w�a�ciciel jest zwalniany przy pomocy metody Free automatycznie
  s� zwalniane wszystkie nale��ce do niego instancje klas.
  W tym przypadku nil oznacza brak w�a�ciciela,
  czyli sam musisz zadba� o zwolnienie utworzonej instancji klasy
  }
  //  https://pl.wikipedia.org/wiki/Instancja_(programowanie)
opendialog1.FileName:= '';
opendialog1.Execute;
if (Trim(opendialog1.Filename)<> '') then
 begin
   MediaPlayer1.Close;
   MediaPlayer1.FileName := (opendialog1.filename);
   MediaPlayer1.Open;
   MediaPlayer1.Play;
 end;


end;

end.

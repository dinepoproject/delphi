unit Menu_cwiczenie;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.ComCtrls, Vcl.ToolWin,
  System.ImageList, Vcl.ImgList;

type
  TForm1 = class(TForm)
    mm1_Menu_pierwsze: TMainMenu;
    mm1_Menu_drugie: TMainMenu;
    Elementpierwszy1: TMenuItem;
    Elementdrugi1: TMenuItem;
    Rozwinieciemenu1: TMenuItem;
    Element11: TMenuItem;
    Element21: TMenuItem;
    Drugiemenu1: TMenuItem;
    Wyjcie1: TMenuItem;
    Drugirzad1: TMenuItem;
    Drugiemenu2: TMenuItem;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    Powrtdomenu11: TMenuItem;
    N3podmenu1: TMenuItem;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ImageList1: TImageList;
    submenu1: TMenuItem;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    procedure Elementpierwszy1Click(Sender: TObject);
    procedure Elementdrugi1Click(Sender: TObject);
    procedure Element11Click(Sender: TObject);
    procedure Drugiemenu1Click(Sender: TObject);
    procedure Wyjcie1Click(Sender: TObject);
    procedure Powrtdomenu11Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

//   https://www.youtube.com/watch?v=8b_xv8N72tw
//   https://www.youtube.com/watch?v=RW9RDvpjnCg

procedure TForm1.Drugiemenu1Click(Sender: TObject);
begin
menu:= mm1_Menu_drugie;
end;

procedure TForm1.Element11Click(Sender: TObject);
begin
ShowMessage(Element11.caption);
end;

procedure TForm1.Elementdrugi1Click(Sender: TObject);
begin
ShowMessage(Elementdrugi1.Caption);
end;

procedure TForm1.Elementpierwszy1Click(Sender: TObject);
begin
//ShowMessage(Elementpierwszy1.Caption);
end;

procedure TForm1.Powrtdomenu11Click(Sender: TObject);
begin
 menu:= mm1_Menu_pierwsze;
end;

procedure TForm1.Wyjcie1Click(Sender: TObject);
begin
Close;
end;

end.

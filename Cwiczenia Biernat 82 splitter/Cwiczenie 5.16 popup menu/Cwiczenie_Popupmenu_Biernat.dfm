object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 201
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btn1: TButton
    Left = 106
    Top = 80
    Width = 151
    Height = 25
    Caption = 'Uruchom Popupmenu'
    TabOrder = 0
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 106
    Top = 127
    Width = 160
    Height = 25
    Caption = 'popupmenu 2'
    TabOrder = 1
    OnClick = btn2Click
  end
  object pm1_popup_1: TPopupMenu
    Left = 333
    Top = 79
    object Popupmenu11: TMenuItem
      Caption = 'Popupmenu1'
    end
    object Plik1: TMenuItem
      Caption = 'Plik'
    end
    object zapisz1: TMenuItem
      Caption = 'zapisz'
    end
  end
  object pm1_popup_2: TPopupMenu
    Left = 329
    Top = 136
    object Popupmenu21: TMenuItem
      Caption = 'Popupmenu2'
    end
    object Plik2: TMenuItem
      Caption = 'Plik'
    end
    object Zapisz2: TMenuItem
      Caption = 'Zapisz'
    end
  end
  object pm1exit: TPopupMenu
    Left = 230
    Top = 26
    object WYJSCIE1: TMenuItem
      Caption = 'WYJSCIE'
      OnClick = WYJSCIE1Click
    end
  end
end

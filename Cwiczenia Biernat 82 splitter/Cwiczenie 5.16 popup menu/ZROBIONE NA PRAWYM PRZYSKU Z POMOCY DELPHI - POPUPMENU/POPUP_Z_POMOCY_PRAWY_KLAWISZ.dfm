object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 358
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Edit1: TEdit
    Left = 142
    Top = 97
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 368
    Top = 97
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Edit2'
  end
  object PopupMenu1: TPopupMenu
    Left = 427
    Top = 185
    object CUT1: TMenuItem
      Caption = 'CUT'
      OnClick = CUT1Click
    end
    object COPY1: TMenuItem
      Caption = 'COPY'
      OnClick = COPY1Click
    end
    object PASTE1: TMenuItem
      Caption = 'PASTE'
      OnClick = PASTE1Click
    end
  end
end

unit POPUP_Z_POMOCY_PRAWY_KLAWISZ;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    PopupMenu1: TPopupMenu;
    CUT1: TMenuItem;
    COPY1: TMenuItem;
    PASTE1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure COPY1Click(Sender: TObject);
    procedure CUT1Click(Sender: TObject);
    procedure PASTE1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.COPY1Click(Sender: TObject);

 begin
  if PopupMenu1.PopupComponent = Edit1 then
   Edit1.CopyToClipboard

   //kopiowanie zawartosci edita
  else if PopupMenu1.PopupComponent = Edit2 then
    Edit2.CopyToClipboard
  else
    Beep;


end;

procedure TForm1.CUT1Click(Sender: TObject);

 begin
  if PopupMenu1.PopupComponent = Edit1 then
    Edit1.CutToClipboard
  else if PopupMenu1.PopupComponent = Edit2 then
    Edit2.CutToClipboard
  else
    Beep;
;

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  PopupMenu1.AutoPopup := True;
  // uruchamianie popup prawym przyciskiem
  Edit1.PopupMenu := PopupMenu1;
  //przypisanie do komponentu pop up'a
  Edit2.PopupMenu := PopupMenu1;
end;


procedure TForm1.PASTE1Click(Sender: TObject);
begin
  if PopupMenu1.PopupComponent = Edit1 then
    Edit1.PasteFromClipboard
  else if PopupMenu1.PopupComponent = Edit2 then
    Edit2.PasteFromClipboard
  else
    Beep;
end;

end.

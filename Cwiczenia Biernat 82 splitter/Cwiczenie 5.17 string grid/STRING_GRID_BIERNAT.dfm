object Form1: TForm1
  Left = 195
  Top = 55
  Caption = 'Form1'
  ClientHeight = 468
  ClientWidth = 747
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 8
    Top = 198
    Width = 660
    Height = 270
    TabOrder = 0
  end
  object btn1_DODAJ_WIERSZ: TButton
    Left = 76
    Top = 45
    Width = 134
    Height = 25
    Caption = 'DODAJ WIERSZ'
    TabOrder = 1
    OnClick = btn1_DODAJ_WIERSZClick
  end
  object btn2_DODAJ_KOLUMNE: TButton
    Left = 212
    Top = 45
    Width = 142
    Height = 25
    Caption = 'DODAJ KOLUMN'#280
    TabOrder = 2
    OnClick = btn2_DODAJ_KOLUMNEClick
  end
  object Button3: TButton
    Left = 360
    Top = 45
    Width = 159
    Height = 25
    Caption = 'WYPE'#321'NIJ CA'#321#260' TABEL'#280
    TabOrder = 3
    OnClick = Button3Click
  end
  object btn4_WSTAW_NOWY_WIERSZ: TButton
    Left = 76
    Top = 96
    Width = 134
    Height = 25
    Caption = 'WSTAW NOWY WIERSZ'
    TabOrder = 4
    OnClick = btn4_WSTAW_NOWY_WIERSZClick
  end
  object btn1_WSTAW_NOWA_KOLUMNE: TButton
    Left = 212
    Top = 96
    Width = 142
    Height = 25
    Caption = 'WSTAW NOW'#260' KOLUMN'#280
    TabOrder = 5
    OnClick = btn1_WSTAW_NOWA_KOLUMNEClick
  end
  object btn1_USUN_KOLUMNE: TButton
    Left = 216
    Top = 151
    Width = 138
    Height = 25
    Caption = 'USU'#323' KOLUMN'#280
    TabOrder = 6
    OnClick = btn1_USUN_KOLUMNEClick
  end
  object btn7_USUN_WIERSZ: TButton
    Left = 76
    Top = 151
    Width = 134
    Height = 25
    Caption = 'USU'#323' WIERSZ'
    TabOrder = 7
    OnClick = btn7_USUN_WIERSZClick
  end
  object ZAPISZ: TButton
    Left = 360
    Top = 96
    Width = 159
    Height = 25
    Caption = 'ZAPISZ'
    TabOrder = 8
  end
end

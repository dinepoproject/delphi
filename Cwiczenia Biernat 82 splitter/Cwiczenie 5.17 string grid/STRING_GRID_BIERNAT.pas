unit STRING_GRID_BIERNAT;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    btn1_DODAJ_WIERSZ: TButton;
    btn2_DODAJ_KOLUMNE: TButton;
    Button3: TButton;
    btn4_WSTAW_NOWY_WIERSZ: TButton;
    btn1_WSTAW_NOWA_KOLUMNE: TButton;
    btn1_USUN_KOLUMNE: TButton;
    btn7_USUN_WIERSZ: TButton;
    ZAPISZ: TButton;
    procedure btn1_DODAJ_WIERSZClick(Sender: TObject);
    procedure btn4_WSTAW_NOWY_WIERSZClick(Sender: TObject);
    procedure btn7_USUN_WIERSZClick(Sender: TObject);
    procedure btn2_DODAJ_KOLUMNEClick(Sender: TObject);
    procedure btn1_WSTAW_NOWA_KOLUMNEClick(Sender: TObject);
    procedure btn1_USUN_KOLUMNEClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //function STRING_GRID_ZAPISZ (TXTFILENAME:string; NUMCOL:Integer): ShortInt;

  end;



var
  Form1: TForm1;

implementation

{$R *.dfm}



procedure TForm1.btn1_DODAJ_WIERSZClick(Sender: TObject);
begin
StringGrid1.RowCount:= StringGrid1.RowCount +1;
if StringGrid1.RowCount > 7 then begin
raise exception.Create('ZBYT DU�A LICZBA WIERSZY');
end;
end;

procedure TForm1.btn1_USUN_KOLUMNEClick(Sender: TObject);
var
AA,BB:Integer;

begin
 for AA := 0 to StringGrid1.RowCount-1 do
   StringGrid1.Cells[StringGrid1.Col,AA]:='';
   for AA := 0 to StringGrid1.ColCount-1 do
     for BB := 0 to StringGrid1.RowCount-1 do
       begin
         StringGrid1.Cells[StringGrid1.Col+AA,BB]:= Trim(StringGrid1.Cells[StringGrid1.Col+1+AA,BB]) ;

       end;
       StringGrid1.ColCount:=StringGrid1.ColCount-1;
end;

procedure TForm1.btn1_WSTAW_NOWA_KOLUMNEClick(Sender: TObject);
var
AA,BB:Integer;

begin
for AA := StringGrid1.ColCount -1 downto  StringGrid1.Col do
 for BB := 0 to StringGrid1.RowCount-1 do
   StringGrid1.Cells[1+AA,BB]:=Trim(StringGrid1.Cells[AA,BB]);
   for AA := 0 to StringGrid1.RowCount-1 do
     StringGrid1.Cells[StringGrid1.Col,AA]:='';
     StringGrid1.ColCount:= StringGrid1.ColCount+1;


end;

procedure TForm1.btn2_DODAJ_KOLUMNEClick(Sender: TObject);
begin

 Assert(StringGrid1.colCount <= 7);
StringGrid1.ColCount := StringGrid1.ColCount+1;

 { TRAPTAK
 Cze��
ExpectedException := EassertionFailed; to sprawdzenie czy zostanie wywo�any wyj�tek assercji
Sam� assercj� wywo�uje si� tak:
Assert(Grid.RowCount <= 7);}
end;


procedure TForm1.btn4_WSTAW_NOWY_WIERSZClick(Sender: TObject);
var
AA,BB:Integer;
begin
 for AA := StringGrid1.RowCount-1 downto StringGrid1.Row do
 for BB := 0 to StringGrid1.ColCount -1 do    // kopiowanie kolumny o jedna w d�:
 StringGrid1.Cells [BB, 1+AA]:= Trim(StringGrid1.Cells[BB, AA]);
 // wyczyszczenie calego  wiersza przez co powstaje nowy pusty wiersz
 for AA := 0 to StringGrid1.ColCount -1 do

  StringGrid1.Cells[AA, StringGrid1.Row]:='';
  //dodanie nowego wiersza
  StringGrid1.RowCount:= StringGrid1.RowCount +1;
end;

procedure TForm1.btn7_USUN_WIERSZClick(Sender: TObject);
var
AA,BB:Integer;

begin
 for AA := 0 to StringGrid1.ColCount -1 do  // czysci ca�y wiersz
 StringGrid1.Cells[AA,StringGrid1.Row]:='';
 // przesuniecie przez kopiowanie wierszy o jeden wyzej
 for AA := 0 to StringGrid1.RowCount-1 do
  for BB := 0 to StringGrid1.ColCount -1 do
  begin
    StringGrid1.Cells[BB, StringGrid1.Row + AA]:= Trim(StringGrid1.Cells[BB, StringGrid1.Row +1+AA]);
  end;
  //usuniecie wiersza
  StringGrid1.RowCount:=StringGrid1.RowCount -1;

end;

procedure TForm1.Button3Click(Sender: TObject);
begin
StringGrid1.Cells[0, 0]:= 'NAZWISKO';
StringGrid1.Cells[1, 0]:= 'IMI�';
StringGrid1.Cells[2, 0]:= 'MIASTO';
StringGrid1.Cells[3, 0]:= 'ADRES';
StringGrid1.Cells[4, 0]:= 'TELEFON';
StringGrid1.Cells[0, 1]:= 'KOWALSKI';
StringGrid1.Cells[1, 1]:= 'JAN';
StringGrid1.Cells[2, 1]:= 'GDA�SK';
StringGrid1.Cells[3, 1]:= 'B�OTNA 22';
StringGrid1.Cells[4, 1]:= '811-56-12';

end;

{function TForm1.STRING_GRID_ZAPISZ(TXTFILENAME: string;
  NUMCOL: Integer): ShortInt;
  var
  ACOL, AROW: Integer;
  FT:TextFile;
begin
  STRING_GRID_ZAPISZ:= -1;
  if (Trim(TXTFILENAME)<> '') then
  begin
    AssignFile(FT,Trim(TXTFILENAME));
    FileSetAttr(Trim(TXTFILENAME, FAARCHIVE));
    Rewrite(FT);
    for AROW := 0 to StringGrid1.RowCount -1 do
      for ACOL := 0 to StringGrid1.RowCount -1 do
      if (Trim(StringGrid1.Cells[NUMCOL,AROW])<>'') then
      Writeln(FT,StringGrid1.Cells[ACOL,AROW]);
       CloseFile(FT);
     STRING_GRID_ZAPISZ:= 1;

  end;


end;  }

end.

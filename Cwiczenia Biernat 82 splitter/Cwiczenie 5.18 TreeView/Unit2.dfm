object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 437
  ClientWidth = 626
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 281
    Top = 0
    Height = 437
    ExplicitLeft = 352
    ExplicitTop = 176
    ExplicitHeight = 100
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 281
    Height = 437
    Align = alLeft
    Caption = 'Panel1'
    TabOrder = 0
    ExplicitLeft = 32
    ExplicitTop = 24
    ExplicitHeight = 41
    object TreeView1: TTreeView
      Left = 32
      Top = 48
      Width = 121
      Height = 97
      Indent = 19
      TabOrder = 0
      OnChange = TreeView1Change
    end
  end
  object Panel2: TPanel
    Left = 284
    Top = 0
    Width = 342
    Height = 437
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 1
    ExplicitLeft = 424
    ExplicitTop = 160
    ExplicitWidth = 185
    ExplicitHeight = 41
  end
end

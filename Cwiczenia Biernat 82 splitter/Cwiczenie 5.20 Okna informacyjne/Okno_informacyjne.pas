unit Okno_informacyjne;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    btn1_pokaz_okienko_mowiace_o_bledzie: TButton;
    btn1_pokaz_okienko_informacyjne: TButton;
    btn1_pokaz_okienko_ostrzegawcze: TButton;
    btn1_pokaz_okienko_informacyjne_2: TButton;
    procedure btn1_pokaz_okienko_mowiace_o_bledzieClick(Sender: TObject);
    procedure btn1_pokaz_okienko_informacyjneClick(Sender: TObject);
    procedure btn1_pokaz_okienko_ostrzegawczeClick(Sender: TObject);
    procedure btn1_pokaz_okienko_informacyjne_2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

// ZWROC UWAGE NA APPLICATION, BEZ TEGO NIE DZIA�A

procedure TForm1.btn1_pokaz_okienko_informacyjneClick(Sender: TObject);
begin
Application.MessageBox('OKNO INFORMACYJNE O SKO�CZENIU KOPIOWANIA' , 'INFORMACJA', MB_ICONINFORMATION or MB_OK)
end;

procedure TForm1.btn1_pokaz_okienko_informacyjne_2Click(Sender: TObject);
begin
ShowMessage('OKNO OSTRZEGAWCZE!!!');
end;

procedure TForm1.btn1_pokaz_okienko_mowiace_o_bledzieClick(Sender: TObject);
begin
Application.MessageBox('Okno informacyjne o b��dzie', 'B��d', MB_ICONERROR OR MB_OK)
end;

procedure TForm1.btn1_pokaz_okienko_ostrzegawczeClick(Sender: TObject);
begin
Application.MessageBox('POKAZUJE OKIENKO OSTRZEGAWCZE', 'OSTRZE�ENIE', MB_ICONWARNING OR MB_OK)
end;

end.

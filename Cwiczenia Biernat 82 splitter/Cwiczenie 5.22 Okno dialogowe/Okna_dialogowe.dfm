object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 441
  ClientWidth = 742
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 208
    Top = 52
    Width = 38
    Height = 25
    Caption = 'lbl1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btn1_otworz_plik: TButton
    Left = 208
    Top = 97
    Width = 219
    Height = 25
    Caption = 'btn1_otworz_plik'
    TabOrder = 0
    OnClick = btn1_otworz_plikClick
  end
  object btn1_zapisz_plik: TButton
    Left = 208
    Top = 144
    Width = 219
    Height = 25
    Caption = 'btn1_zapisz_plik'
    TabOrder = 1
    OnClick = btn1_zapisz_plikClick
  end
  object btn1_otworz_plik_graf: TButton
    Left = 208
    Top = 186
    Width = 219
    Height = 25
    Caption = 'btn1_otworz_plik_graf'
    TabOrder = 2
    OnClick = btn1_otworz_plik_grafClick
  end
  object btn1_zapisz_plik_graf: TButton
    Left = 208
    Top = 232
    Width = 219
    Height = 25
    Caption = 'btn1_zapisz_plik_graf'
    TabOrder = 3
    OnClick = btn1_zapisz_plik_grafClick
  end
  object btn1_ustaw_kolor_format: TButton
    Left = 208
    Top = 275
    Width = 219
    Height = 25
    Caption = 'btn1_ustaw_kolor_format'
    TabOrder = 4
    OnClick = btn1_ustaw_kolor_formatClick
  end
  object btn1_ustawienia_wydruku: TButton
    Left = 208
    Top = 317
    Width = 219
    Height = 25
    Caption = 'btn1_ustawienia_wydruku'
    TabOrder = 5
    OnClick = btn1_ustawienia_wydrukuClick
  end
  object OpenDialog1: TOpenDialog
    Left = 569
    Top = 114
  end
  object SaveDialog1: TSaveDialog
    Left = 571
    Top = 169
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 571
    Top = 222
  end
  object SavePictureDialog1: TSavePictureDialog
    Left = 571
    Top = 275
  end
  object ColorDialog1: TColorDialog
    Left = 576
    Top = 326
  end
  object PrinterSetupDialog1: TPrinterSetupDialog
    Left = 567
    Top = 70
  end
end

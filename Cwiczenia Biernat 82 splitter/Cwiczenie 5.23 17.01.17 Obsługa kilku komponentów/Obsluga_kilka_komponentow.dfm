object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 418
  ClientWidth = 716
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClick = btn1_wyczysc_wszystkie_komponenty_EDIT_i_wpisz_slowo_BONDClick
  PixelsPerInch = 96
  TextHeight = 13
  object RadioGroup1: TRadioGroup
    Left = 43
    Top = 20
    Width = 614
    Height = 164
    Caption = 'komponent: EDIT'
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 142
    Top = 59
    Width = 157
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 381
    Top = 59
    Width = 157
    Height = 21
    TabOrder = 2
    Text = 'Edit2'
  end
  object Edit3: TEdit
    Left = 142
    Top = 108
    Width = 157
    Height = 21
    TabOrder = 3
    Text = 'Edit3'
    OnClick = btn1_wyczysc_wszystkie_komponenty_EDIT_i_wpisz_slowo_BONDClick
  end
  object Edit4: TEdit
    Left = 381
    Top = 108
    Width = 157
    Height = 21
    TabOrder = 4
    Text = 'Edit4'
  end
  object btn1_wyczysc_wszystkie_komponenty_EDIT_i_wpisz_slowo_BOND: TButton
    Left = 142
    Top = 150
    Width = 396
    Height = 25
    Caption = 'btn1_wyczysc_wszystkie_komponenty_EDIT_i_wpisz_slowo_BOND'
    TabOrder = 5
    OnClick = btn1_wyczysc_wszystkie_komponenty_EDIT_i_wpisz_slowo_BONDClick
  end
  object GroupBox1: TGroupBox
    Left = 43
    Top = 223
    Width = 256
    Height = 105
    Caption = 'Komponent SpeedButton'
    TabOrder = 6
    object SpeedButton1: TSpeedButton
      Left = 25
      Top = 44
      Width = 23
      Height = 22
      Caption = '1'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 46
      Top = 44
      Width = 23
      Height = 22
      Caption = '2'
      OnClick = btn1_wyczysc_wszystkie_komponenty_EDIT_i_wpisz_slowo_BONDClick
    end
    object SpeedButton3: TSpeedButton
      Left = 68
      Top = 45
      Width = 23
      Height = 22
      Caption = '3'
      OnClick = SpeedButton1Click
    end
    object SpeedButton4: TSpeedButton
      Left = 88
      Top = 44
      Width = 23
      Height = 22
      Caption = '4'
      OnClick = SpeedButton1Click
    end
    object SpeedButton5: TSpeedButton
      Left = 109
      Top = 44
      Width = 23
      Height = 22
      Caption = '5'
      OnClick = SpeedButton1Click
    end
    object SpeedButton6: TSpeedButton
      Left = 130
      Top = 44
      Width = 23
      Height = 22
      Caption = '6'
      OnClick = SpeedButton1Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 305
    Top = 223
    Width = 233
    Height = 105
    Caption = 'Komponent Button'
    TabOrder = 7
    object Button1: TButton
      Tag = 1
      Left = 28
      Top = 47
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Tag = 2
      Left = 128
      Top = 49
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
end

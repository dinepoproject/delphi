unit Scrol_bar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Imaging.jpeg;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    ScrollBar1: TScrollBar;
    ScrollBar2: TScrollBar;
    procedure FormCreate(Sender: TObject);
    procedure ScrollBar1Change(Sender: TObject);
    procedure ScrollBar2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
// przerobiono program do obslugi jpeg, dodano modul: Vcl.Imaging.jpeg
procedure TForm1.FormCreate(Sender: TObject);
begin
//Image1.Picture.LoadFromFile('kawabmp.bmp');
ScrollBar1.Max:= Image1.Picture.Height - Panel1.Height;
ScrollBar2.max:= Image1.Picture.Width-panel1.Width;
end;

procedure TForm1.ScrollBar1Change(Sender: TObject);
begin
Image1.top:= (ScrollBar1.Position*-1);
end;

procedure TForm1.ScrollBar2Change(Sender: TObject);
begin
Image1.left:= (ScrollBar2.Position*-1);
end;

end.

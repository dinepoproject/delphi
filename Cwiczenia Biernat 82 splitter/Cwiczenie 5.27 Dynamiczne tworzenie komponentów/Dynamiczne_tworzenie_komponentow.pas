unit Dynamiczne_tworzenie_komponentow;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, frxDesgnCtrls, Vcl.ExtDlgs,
  Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    SavePictureDialog1: TSavePictureDialog;
    Panel1: TPanel;
    Image1: TImage;
    ScrollBox1: TScrollBox;
    function ListBoxSzukajPlikow(txtPath, txtExt:string):ShortInt;
    procedure dccZapiszJako(Sender:TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    dccpanel:array [1..100] of TPanel;
    dccimage:array [1..100] of TImage;

  public
    { Public declarations }


  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

{ TForm1 }

procedure TForm1.dccZapiszJako(Sender: TObject);
begin
savepicturedialog1.FileName:= '';
SavePictureDialog1.Execute;
if (Trim(SavePictureDialog1.FileName)<> '') then
begin
dccimage [(Sender as TImage).Tag].Picture.SaveToFile
(Trim(SavePictureDialog1.FileName));

end;

end;

procedure TForm1.FormCreate(Sender: TObject);
var
TT, numY:Integer;
begin
 ListBoxSzukajPlikow('','*.ico');
 numY:=0;
 for TT := 0 to listbox1.Items.Count -1 do
 begin
   dccpanel[1+TT]:= TPanel.Create(ScrollBox1);
   dccpanel[1+TT].Top:=9+numY;
   dccpanel[1+TT].Left:=9;
   dccpanel[1+TT].Width:=49;
   dccpanel[1+TT].Height:= 49;
   dccpanel[1+TT].Caption:='';
   scrollbox1.InsertControl(dccpanel[1+TT]);
   dccimage[1+TT]:= TImage.Create(dccpanel[1+TT]);
   dccimage[1+TT].Top:=2;
   dccimage[1+TT].Left:= 3;
   dccimage[1+TT].Width:= dccpanel[1+TT].Width -5;
   dccimage[1+TT].Height:=dccpanel[1+TT].Height -5;
   dccimage[1+TT].Transparent:= False;
   dccimage[1+TT].AutoSize:= False;
   dccimage[1+TT].Center:= True;
   dccimage[1+TT].Picture.LoadFromFile(ListBox1.Items [TT]);
   dccimage[1+TT].OnClick:= dccZapiszJako;
   dccimage[1+TT].ShowHint:= True;
   dccimage[1+TT].Hint:= ListBox1.Items[TT];
   dccpanel[1+TT].InsertControl(dccimage[1+TT]);
   numY:= numY + 55;


 end;
end;


function TForm1.ListBoxSzukajPlikow(txtPath, txtExt: string): ShortInt;
var
SR:TSearchRec;

begin
ListBoxSzukajPlikow:= -1;
ListBox1.Items.Clear;
ListBox1.IntegralHeight:= false;
ListBox1.Sorted:=True;
if (FindFirst(Trim(txtPath + txtExt), faAnyFile, SR) = 0) then
begin
  repeat
    if (SR.Attr<>faDirectory) then
    begin
      ListBox1.Items.Add(Trim(SR.Name));
      ListBoxSzukajPlikow:=1;

    end;
  until (FindNext(SR)<>0);
  FindClose(SR);

end;



end;

end.

unit Wczytanie_czcionek_do_listbox;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    ComboBox1: TComboBox;
    ListBox1: TListBox;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
Label1.Font.Name:= ComboBox1.Text;

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
ComboBox1.Items.Clear;
ComboBox1.Items.Assign(Screen.Fonts);
ComboBox1.Text:='';
ListBox1.Items.Clear;
ListBox1.Items.Assign(Screen.Fonts);

end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
if (ListBox1.ItemIndex >-1) then
begin
  Label1.Font.Name:= ListBox1.Items[ListBox1.ItemIndex];
end;

end;

end.

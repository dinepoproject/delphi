program MDIAPP_aplikacja_wielodokumentowa;

uses
  Forms,
  MAIN in '..\..\..\Embarcadero\Studio\Projects\MAIN.PAS' {MainForm},
  CHILDWIN in '..\..\..\Embarcadero\Studio\Projects\CHILDWIN.PAS' {MDIChild},
  about in '..\..\..\Embarcadero\Studio\Projects\about.pas' {AboutBox};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.Run;
end.

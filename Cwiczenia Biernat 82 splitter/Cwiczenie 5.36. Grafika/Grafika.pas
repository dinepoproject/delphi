unit Grafika;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);

// w ksiazce jest tform1.bCanvasClick(Sender: TObject) ale oni tylko
//zmienili sobie nazwe procedury buttona;
begin
with Canvas do begin
  Brush.Style:=bsSolid;
  Brush.Color:=clWhite;
  FillRect(ClipRect);
  // ko�o
  Canvas.Pen.Color:=clBlack;
  Canvas.Brush.Color:= clRed;
  Canvas.Ellipse(9,9,34,34);
  // kwadrat
  Canvas.Pen.Color:=clBlack;
  Canvas.Pen.Style:= psDot;
  Canvas.Brush.Color:= clRed;
  Canvas.Brush.Style:= bsDiagCross;
  Canvas.Rectangle(39,9,66,36);
  // linia

  Canvas.MoveTo(66,66);
  Canvas.LineTo(366,66);
  //ko�o pochylone

  Canvas.Pen.Color:=clBlack;
  Canvas.Brush.Color:= clRed;
  Canvas.Brush.Style:= bsCross;
  Canvas.Chord(199,9,77,46,8,8,8,8);

  // tekst

  Canvas.Font.Name:= 'Arial';
  Canvas.Font.Color:= clGreen;
  Canvas.Font.Style:= [fsBold,fsItalic,fsUnderline];
  Canvas.Font.Size:= 33;
  Canvas.TextOut(23,88, 'to jest tekst');



end;
end;

end.

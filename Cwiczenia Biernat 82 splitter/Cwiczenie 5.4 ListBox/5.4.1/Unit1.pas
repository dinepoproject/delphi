unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    CZYSC: TButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure CZYSCClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    procedure MyAppException(Sender: TObject; E : Exception);
  end;
 type
  ELowError = class(Exception);
  EMediumError = class(Exception);
  EHighError = class(Exception);
var
  Form1: TForm1;

implementation

{$R *.dfm}
 // W PROGRAMIE SA PRZYKLADY WYJATKOW:  http://4programmers.net/Delphi/Wyj�tki
procedure TForm1.Button1Click(Sender: TObject);  //dodaj
begin
ListBox1.Items.Clear;
  ListBox1.Items.Add('Lenovo 401');
  ListBox1.Items.Add('Lenovo 501');
  ListBox1.Items.Add('Sony Vaio');
  ListBox1.Items.Add('Komputer Wieslaw');
  ListBox1.Items.Add('Komputer Windows XP');
  ListBox1.Sorted:= True;

end;

procedure TForm1.Button2Click(Sender: TObject);  // wczytaj z pliku
begin
 OpenDialog1.FileName:= '';
 OpenDialog1.Execute;
ListBox1.Items.Clear;

//if (FileExists(Trim('listaa2.TXT'))= True) then
begin
if OpenDialog1.FileName <> '' then

ListBox1.Items.LoadFromFile(OpenDialog1.FileName);
end
 //else
//raise EHighError.Create('BRAK LISTY');       // w dniu 15.06.2017 przerob. na opendialog
end;                                           // w ramach cwiczenia

procedure TForm1.Button3Click(Sender: TObject); // zaznacz wszystko
var
TT:Integer;

begin
  ListBox1.MultiSelect:= true;
  for  TT:= 0 to ListBox1.Items.Count-1 do
  ListBox1.Selected[TT]:= TRUE;

end;

procedure TForm1.Button4Click(Sender: TObject);  //odznacz wszystko
var
TT: Integer;

begin
 ListBox1.MultiSelect:= True; // w�acza mozliwosc zaznaczania wiecej niz jednej pozycji.
 for TT:= 0 to ListBox1.Items.Count-1 do
   ListBox1.Selected[TT] := False;


end;

procedure TForm1.Button5Click(Sender: TObject);  // zaznacz odznacz wszystko
var
TT: Integer;
begin
 ListBox1.MultiSelect:= True;
 for TT:= 0 to ListBox1.Items.Count-1 do
   if (ListBox1.Selected[TT] = True) then
   begin
     ListBox1.Selected[TT] := False;

   end
   else
   begin
    ListBox1.Selected[TT] := True;
   end;
end;

procedure TForm1.Button6Click(Sender: TObject); //zapisz do pliku

begin
 SaveDialog1.FileName:= '';
SaveDialog1.Execute;

 if SaveDialog1.FileName <> '' then
  begin


ListBox1.Items.SaveToFile(SaveDialog1.FileName);
  end;
end;

procedure TForm1.Button7Click(Sender: TObject);  //dodaj zawartosc edita
begin
if (Trim(Edit1.Text)= '') then
raise ELowError.Create  ('PUSTY EDIT');

if (Trim(Edit1.Text)<> '') then
ListBox1.Items.Add(Trim(Edit1.text));
ListBox1.Sorted:= True;
Edit1.Text:= '';

end;

procedure TForm1.CZYSCClick(Sender: TObject);
begin
ListBox1.Items.Clear;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
ListBox1.Items.Clear;
Edit1.Text:= '';
 Application.OnException := MyAppException;
end;

procedure TForm1.MyAppException(Sender: TObject; E: Exception);
begin
{ wy�wietlenie komunikat�w wyj�tk�w }
  Application.ShowException(E);

  if E is EHighError then // je�eli wyj�tek to EHighError...
  begin
    if Application.MessageBox('Dalsze dzia�anie programu grozi zawieszeniem systemu. Czy chcesz kontynuowa�?',
    'B��d', MB_YESNO + MB_ICONWARNING) = Id_No then Application.Terminate;
  end;
  if E is ELowError then
    begin
    if Application.MessageBox('Puste pole. Wprowad� dane',
    'B��d', MB_YESNO + MB_ICONWARNING) = Id_No then Application.Terminate;
  end;

end;

end.

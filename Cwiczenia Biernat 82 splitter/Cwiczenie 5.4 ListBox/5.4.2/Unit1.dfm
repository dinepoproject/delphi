object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 461
  ClientWidth = 611
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 88
    Top = 48
    Width = 209
    Height = 265
    ItemHeight = 13
    TabOrder = 0
    OnKeyDown = Edit1KeyDown
    OnKeyPress = ListBox1KeyPress
  end
  object Edit1: TEdit
    Left = 88
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
    OnChange = Edit1Change
    OnClick = Edit1Click
    OnKeyDown = Edit1KeyDown
    OnMouseEnter = Edit1MouseEnter
  end
  object Edit2: TEdit
    Left = 480
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Edit2'
  end
end

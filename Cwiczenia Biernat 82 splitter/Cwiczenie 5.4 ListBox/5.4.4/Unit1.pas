unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    Edit1: TEdit;
    Button1: TButton;

    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ListBoxVerify (txtTextVerify : string):Boolean;
    function ListBoxSelectAdd(txtAddText: String):String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);

begin
 if ListBoxVerify(Edit1.Text)= False and (Trim(Edit1.text)<> '')then
 ListBoxSelectAdd(Edit1.text);
 Edit1.Text:= '';

end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
Button1.Enabled:= False;
if Length(Trim(Edit1.text))>0 then
Button1.Enabled:= True;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
Edit1.Text:= '';
ListBox1.Items.Clear;
end;

function TForm1.ListBoxSelectAdd(txtAddText: String): String;
var
TT:Integer;
begin
  ListBoxSelectAdd:= '';
  ListBox1.MultiSelect:= True;
  ListBox1.IntegralHeight:= True;
  ListBox1.Sorted:=False;
  if (Trim(txtAddText)<>'') then
  begin
    ListBox1.Items.Add(Trim(txtAddText));
    for TT:=0 to ListBox1.Items.Count-1 do
    ListBox1.Selected [TT] := false;  // kasuje poprzednie zaznaczenia
    ListBox1.Selected[ListBox1.Items.Count-1]:= true;
     //ListBox1.Selected[tt-2]:= true; // zaznaczycza takze przedostatni element
     //ListBox1.Selected[0]:= true;  zaznacza pierwszy element
    Result:= Trim(txtAddText);



  end;


end;

function TForm1.ListBoxVerify(txtTextVerify: string): Boolean;
var
TT: Integer;
begin
ListBoxVerify:= False;
for TT := ListBox1.Count-1 downto 0 do
if (AnsiUpperCase(Trim(txtTextVerify)) =
AnsiUpperCase(Trim(ListBox1.Items[TT])))
then
Result:= True;

end;

end.

object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 665
  ClientWidth = 964
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object ScrollBox1: TScrollBox
    Left = 19
    Top = 8
    Width = 382
    Height = 287
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 80
      Top = 16
      Width = 75
      Height = 25
      Caption = 'BitBtn1'
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 80
      Top = 64
      Width = 75
      Height = 25
      Caption = 'BitBtn2'
      TabOrder = 1
    end
    object Memo1: TMemo
      Left = 40
      Top = 104
      Width = 185
      Height = 89
      Lines.Strings = (
        'Memo1')
      TabOrder = 2
    end
    object RadioGroup1: TRadioGroup
      Left = 161
      Top = 218
      Width = 185
      Height = 105
      Caption = 'RadioGroup1'
      TabOrder = 3
    end
    object RadioButton1: TRadioButton
      Left = 40
      Top = 329
      Width = 113
      Height = 17
      Caption = 'RadioButton1'
      TabOrder = 4
    end
    object Memo2: TMemo
      Left = 27
      Top = 414
      Width = 185
      Height = 89
      Lines.Strings = (
        'Memo2')
      TabOrder = 5
    end
    object edt1: TEdit
      Left = 16
      Top = 511
      Width = 121
      Height = 21
      TabOrder = 6
      Text = 'edt1'
    end
  end
  object RadioButton2: TRadioButton
    Left = 415
    Top = 316
    Width = 113
    Height = 17
    Caption = 'RadioButton2'
    TabOrder = 1
  end
  object grp1: TGroupBox
    Left = 415
    Top = 8
    Width = 473
    Height = 241
    Caption = 'grp1'
    TabOrder = 2
    object lbl1: TLabel
      Left = 200
      Top = 96
      Width = 16
      Height = 13
      Caption = 'lbl1'
    end
    object edt2: TEdit
      Left = 40
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'edt2'
    end
    object btn1: TButton
      Left = 72
      Top = 120
      Width = 75
      Height = 25
      Caption = 'btn1'
      TabOrder = 1
    end
    object mmo1: TMemo
      Left = 304
      Top = 48
      Width = 185
      Height = 89
      Lines.Strings = (
        'Memo3')
      TabOrder = 2
    end
  end
end

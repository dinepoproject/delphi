unit baza_query;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Bde.DBTables, Data.DB, Vcl.Grids,
  Vcl.DBGrids;

type
  TForm1 = class(TForm)
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Database1: TDatabase;
    Table1: TTable;
    Session1: TSession;
    Query1: TQuery;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
KATALOG : string;
const nazwatabeli :  string = 'Tabelamoja';
 Nazwaaliasu : string = 'danielbaza';

begin
   KATALOG := ExtractFileDir(Application.ExeName) ;
   if FileExists(KATALOG + '\' + nazwatabeli + '.db') = false then
   begin
     Table1.Active := False;
     session1.AutoSessionName:= true;
     Session1.DeleteAlias(Nazwaaliasu);
     Session1.AddStandardAlias(Nazwaaliasu,KATALOG,'');
     Table1.DatabaseName := Nazwaaliasu;
     Table1.TableType := ttParadox;
     Table1.TableName := nazwatabeli;
        with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'jeden';
         DataType := ftInteger;

       end;
        with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'dwa';
         DataType := ftString;
          Size := 30;
       end;
         with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'trzy';
         DataType := ftString;
          Size := 30;
       end;

        with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'cztery';
         DataType := ftFmtMemo;

       end;
        with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'E-mail';
         DataType := ftString;
          Size := 30;
       end;
         with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'Uwagi';
         DataType := ftFmtMemo;

       end;
          with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'Zdj�cie';
         DataType := ftGraphic;

       end;


       Table1.CreateTable;
        Showmessage('Utworzono tabel� o nazwie: ' + nazwatabeli );
end;
 Table1.TableName :=  Nazwatabeli;
    Table1.Active := True;
end;

end.


{
 -- Library (S�ownie) --
 Copyright(c)by Jan Biernat
}
unit USlownie;

interface

uses
 SysUtils;

function plnSlownie(txtNumber: String) :String;

implementation

{
Funkcja dodaje zero do pojedynczej liczby traktowanej
jako tekst, w innym przypadku zwraca dwie liczby.

Przyk�ad:
  1) 10 - zwr�ci nam liczb� dziesi�� traktowan� jako tekst
  2)  3 - zwr�ci nam liczb� 03 traktowan� jako tekst, przez dodanie zera przed liczb� trzy
}
function FillZero(txtZero: String): String;
begin
  FillZero:= txtZero;
  if (Length(txtZero) =1) then FillZero:= '0'+txtZero;
end;

{
Funkcja wyszukuje w �a�cuch tekstowym przecinaka i zamienia go na kropk�, w
przeciwnym przypadku nie dokonuje zamiany przecinaka na kropk�.

Przyk�ad:
  1234,45 -> 1234.45
}
function CommaToDot(txtText: String): String;
var
  txtRText: String;
    numPos: Shortint;
begin
  CommaToDot:= Trim(txtText);
  if (Trim(txtText)<>'') then
  begin
    numPos:= 0;
    numPos:= Pos(',', Trim(txtText));
    if (numPos<>0) then
    begin
      txtRText:= '';
      txtRText:= Copy(Trim(txtText), 1, numPos-1)+'.'+
                 Copy(Trim(txtText), numPos+1, Length(Trim(txtText)));
      CommaToDot:= Trim(txtRText);
    end;
  end;
end;

// Funkcja konwertuj�ca liczb� na tekst
function mcIntToStr(numNumber: Longint): String;
var
  txtText: String;
begin
  txtText:= '';
  STR(numNumber, txtText); { Converts a numeric value to a string. }
  mcIntToStr:= txtText;
end;

// Funkcja konwertuj�ca liczb� traktowan� jako tekst na liczb�
function mcStrToInt(txtText: String): Longint;
var
  I: Integer;
  A: Longint;
begin
  A:= 0;
  I:= 0;
  VAL(txtText, A, I); { Converts a string value to its numeric representation. }
  mcStrToInt:= A;
end;

{
Funkcje zamieniaj�ce cyfry traktowane jako tekst na s�owa
 np. 12356,26 -> dwana�cie tysi�cy trzysta pi��dziesi�t sze�� z�otych i
                 dwadzie�cia sze�� groszy

}
function txtSlownieGramatyka(txtNumber: String; numPos, okPenny: Shortint) :String;
const
  MM: array[1..9, 1..4] of String[20] =
    (('sto ',         'dziesi�� ',         'jeden ',    'jedena�cie '),
     ('dwie�cie ',    'dwadzie�cia ',      'dwa ',      'dwana�cie '),
     ('trzysta ',     'trzydzie�ci ',      'trzy ',     'trzyna�cie '),
     ('czterysta ',   'czterdzie�ci ',     'cztery ',   'czterna�cie '),
     ('pi��set ',     'pi��dziesi�t ',     'pi�� ',     'pi�tna�cie '),
     ('sze��set ',    'sze��dziesi�t ',    'sze�� ',    'szesna�cie '),
     ('siedemset ',   'siedemdziesi�t ',   'siedem ',   'siedemna�cie '),
     ('osiemset ',    'osiemdziesi�t ',    'osiem ',    'osiemna�cie '),
     ('dziewi��set ', 'dziewi��dziesi�t ', 'dziewi�� ', 'dziewi�tna�cie '));

  NN: array[1..5, 1..3] of String[11] =
     (('z�oty ',   'z�ote ',    'z�otych '),
      ('tysi�c ',  'tysi�ce ',  'tysi�cy '),
      ('milion ',  'miliony ',  'milion�w '),
      ('miliard ', 'miliardy ', 'miliard�w '),
      ('bilion ',  'biliony ',  'bilion�w '));

  PP: array [1..3] of String[7] = ('grosz', 'grosze', 'groszy');
var
  TT, JJ, numTPos: Shortint;
           txtSay: String;
begin
  // txtSlownieGramatyka
  txtSlownieGramatyka:= '';
  if (numPos < 1) then numPos:= 1;

  if (Length(txtNumber) = 1) then txtNumber:= '00'+txtNumber;
  if (Length(txtNumber) = 2) then txtNumber:= '0'+txtNumber;

  // Start
  txtSay:= '';
  if (txtNumber<>'') and (Length(txtNumber) = 3) then
  begin

    if (mcStrToInt(Copy(txtNumber, 2, 2)) in [11..19]) and
       (mcStrToInt(Copy(txtNumber, 1, 1)) = 0) then
    begin
      // 11..19 and = 0
      txtSlownieGramatyka:= MM[mcStrToInt(Copy(txtNumber, 2, 2))-10, 4]+NN[numPos, 3];
      if (okPenny >0) then
        txtSlownieGramatyka:= MM[mcStrToInt(Copy(txtNumber, 2, 2))-10, 4]+PP[3];
    end
    else
    if (mcStrToInt(Copy(txtNumber, 2, 2)) in [11..19]) and
       (mcStrToInt(Copy(txtNumber, 1, 1)) > 0) then
    begin
      // 11..19 and > 0
      txtSay:= '';
      txtSay:= MM[mcStrToInt(Copy(txtNumber, 2, 2))-10, 4]+NN[numPos, 3];
      if (okPenny >0) then
        txtSay:= MM[mcStrToInt(Copy(txtNumber, 2, 2))-10, 4]+PP[3];
      txtSlownieGramatyka:= MM[mcStrToInt(Copy(txtNumber, 1, 1)), 1]+txtSay;
    end
    else
    begin
      txtSay:= '';
      for TT:= 1 to Length(txtNumber) do
        for JJ:= 1 to 9 do
          if (Copy(txtNumber, TT, 1) = mcIntToStr(JJ)) then
            txtSay:= txtSay+MM[JJ, TT];
      numTPos:= 0;
      numTPos:= 1;
      if (mcStrToInt(Copy(txtNumber, 3, 1)) in [2..4]) then numTPos:= 2;
      if (mcStrToInt(Copy(txtNumber, 3, 1)) in [5..9]) or
         (mcStrToInt(Copy(txtNumber, 2, 1)) in [2..9]) and
         (mcStrToInt(Copy(txtNumber, 3, 1)) =1) or
         (mcStrToInt(Copy(txtNumber, 1, 1)) in [1..9]) and
         (mcStrToInt(Copy(txtNumber, 2, 1)) = 0) and
         (mcStrToInt(Copy(txtNumber, 3, 1)) = 1) or
         (mcStrToInt(Copy(txtNumber, 2, 1)) in [1..9]) and
         (mcStrToInt(Copy(txtNumber, 3, 1)) = 0) or
         (mcStrToInt(Copy(txtNumber, 1, 1)) in [1..9]) and
         (Copy(txtNumber, 2, 2) = '00') then
       begin
         numTPos:= 0;
         numTPos:= 3;
       end;
      txtSlownieGramatyka:= txtSay+NN[numPos, numTPos];
      if (okPenny >0) then txtSlownieGramatyka:= txtSay+PP[numTPos];
      if (Copy(txtNumber, 1, 3) = '000') then txtSlownieGramatyka:= '';
    end;
  end;
end;

function txtSlowniePisz(txtNumber, txtPenny: String) :String;
var
  txtSay, txtSPenny: String;
                 TT: Shortint;
begin
  // txtSlowniePisz
  txtSlowniePisz:= '';

  // Penny Position
  txtSPenny:= '';
  txtSPenny:= 'i zero groszy';
  if (mcStrToInt(txtPenny) >0) then
  begin
    txtSPenny:= '';
    txtSPenny:= 'i '+txtSlownieGramatyka(FillZero(txtPenny), 1, 1);
  end;

  // Uzupe�nij zerami
  for TT:= 1 to 15-Length(txtNumber) do
    txtNumber:= '0'+txtNumber;

  txtSay:= '';
  txtSay:= 'zero z�otych'+CHR(32);
  if (txtNumber <>'') and (Length(txtNumber) = 15) and
     (mcStrToInt(txtNumber) >0) then
  begin
    txtSay:= '';
    txtSay:= txtSlownieGramatyka(Copy(txtNumber, 1, 3), 5, 0)+
             txtSlownieGramatyka(Copy(txtNumber, 4, 3), 4, 0)+
             txtSlownieGramatyka(Copy(txtNumber, 7, 3), 3, 0)+
             txtSlownieGramatyka(Copy(txtNumber, 10, 3), 2, 0)+
             txtSlownieGramatyka(Copy(txtNumber, 13, 3), 1, 0);
  end;
  txtSlowniePisz:= txtSay+txtSPenny;
end;

function plnSlownie(txtNumber: String) :String;
var
                       numComma: Shortint;
  txtBeforeComma, txtAfterComma: String;
begin
  // plnSlownie
  numComma:= 0;
  numComma:= Pos('.', CommaToDot(txtNumber));
  if (numComma<>0) then
  begin
    txtAfterComma:= '';
    txtAfterComma:= FillZero(Copy(txtNumber, numComma+1, 2));
    txtBeforeComma:= '';
    txtBeforeComma:= Copy(txtNumber, 1, numComma-1);
    plnSlownie:= txtSlowniePisz(txtBeforeComma, txtAfterComma);
  end
  else
    plnSlownie:= txtSlowniePisz(txtNumber, '-1');
end;

end.


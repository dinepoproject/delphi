unit DELPHI_7_STR_630;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Bde.DBTables,
  Vcl.Grids, Vcl.DBGrids;

type
  TForm1 = class(TForm)
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Table1: TTable;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
TABLE1.Append;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
TABLE1.Post;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
TABLE1.Delete;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
TABLE1.ACTIVE:= TRUE;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
TABLE1.Active:= FALSE;
end;

end.

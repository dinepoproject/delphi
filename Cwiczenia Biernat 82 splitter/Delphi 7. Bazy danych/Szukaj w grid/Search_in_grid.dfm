object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 673
  ClientWidth = 682
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 112
    Top = 88
    Width = 93
    Height = 13
    Caption = 'SZUKAJ NAZWISKA'
  end
  object Edit1: TEdit
    Left = 288
    Top = 85
    Width = 177
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
    OnChange = Edit1Change
    OnKeyDown = Edit1KeyDown
  end
  object StringGrid1: TStringGrid
    Left = 100
    Top = 128
    Width = 526
    Height = 274
    ColCount = 10
    DefaultColWidth = 30
    FixedCols = 2
    RowCount = 10
    FixedRows = 3
    TabOrder = 1
    OnKeyPress = StringGrid1KeyPress
  end
  object ListBox1: TListBox
    Left = 112
    Top = 408
    Width = 481
    Height = 201
    ItemHeight = 13
    TabOrder = 2
  end
  object Button1: TButton
    Left = 136
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 19
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 4
    OnClick = Button2Click
  end
end

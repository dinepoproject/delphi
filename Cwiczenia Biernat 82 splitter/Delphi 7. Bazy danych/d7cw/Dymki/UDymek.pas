{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad wykorzystania dymka.


Wprowadzenie:

Dymki s� bardzo pomocne, poniewa� umo�liwiaj� dodanie kr�tkiej
podpowiedzi do ka�dego komponentu. Podpowiedzi te ujawni� si� w
momencie najechania na komponent np. klawisz muszk�. Szybko��
pojawiania, znikni�cia i kolor mo�emy sami ustawi�.

Gdy informacja jest za d�uga to dobrze jest przenie�� jej
fragment do nast�pnej linii.
Dokonujemy tego przez wstawienie do naszego �a�cucha w
stosownym miejscu znaku z kodem Enter'a, aby nasza podpowied�
wy�wietlona zosta�a w nast�pnym wierszu.
Np.
  Button1.Hint:= 'To jest podpowied� na temat klawisza, '+CHR(13)+
                 'nad kt�rym znajduje si� wska�nik myszy.'+CHR(13)+
                 'Gdy informacja jest za d�uga to dobrze '+CHR(32)+
                 'jest przenie�� jej fragment do nast�pnej linii';
}
unit UDymek;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Ustawienie parametr�w dla dymk�w.
  Application.HintColor:= clLime; // Ustawienie koloru dla dymku.
  Application.HintPause:= 100; // Ustawienie czasu, po kt�rym uka�e nam si� dymek z podpowiedzi�.
  Application.HintHidePause:= 5000; // Ustawienie czasu, okre�laj�cego jak d�ugo dymek b�dzie wy�wietlany.

  // W��czenie podpowiedzi
  Button1.ShowHint:= TRUE; // W��czenie podpowiedzi. T� metod� mo�emy ustawi� we w�asno�ciach komponentu (Object Inspector).

  // Button1.Hint - Wpisanie podpowiedzi po konkretny komponent (np. klawisz)
  Button1.Hint:= 'To jest podpowied� na temat klawisza, nad kt�rym znajduje si� wska�nik myszy.'+CHR(13)+
                 'Gdy informacja jest za d�uga to dobrze jest przenie�� jej fragment do nast�pnej linii.';
end;

end.

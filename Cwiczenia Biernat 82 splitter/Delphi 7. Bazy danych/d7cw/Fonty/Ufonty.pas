{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad wstawienia czcionek do listy typu ComboBox i ListBox.
}
unit Ufonty;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label3: TLabel;
    ListBox1: TListBox;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  //-- ComboBox --
  // Wstawienie czcionek do komponentu ComboBox.
  ComboBox1.Items.Clear; // Wyczyszczenie listy.

  // Wstawienie dost�pnych czcionek do listy.
  ComboBox1.Items.Assign(Screen.Fonts);

  // Wyczyszczenie zawarto�ci edytora.
  ComboBox1.Text:= '';

  //-- ListBox --
  // Wstawienie czcionek do komponentu ListBox.
  ListBox1.Items.Clear; // Wyczyszczenie listy.

  // Wstawienie dost�pnych czcionek do listy.
  ListBox1.Items.Assign(Screen.Fonts);
end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  // Poka� czcionk� po jej wybraniu.
  Label3.Font.Name:= ComboBox1.Text;
end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
  // Poka� czcionk� po jej wybraniu.

  if (ListBox1.ItemIndex >-1) then
  {
   Sprawdzenie czy zosta� zaznaczony
   element, je�li tak to wykonaj warunek.
  }
  begin
    Label3.Font.Name:= ListBox1.Items[ListBox1.ItemIndex];
  end;
end;

end.

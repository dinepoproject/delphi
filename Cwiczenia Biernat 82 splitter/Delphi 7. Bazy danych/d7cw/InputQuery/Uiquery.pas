{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania funkcji InputQuery.


Wprowadzenie:
Polecenie 'InputQuery' s�u�y do wywo�ania
okna edycyjnego, za pomoc� kt�rego mo�emy
wprowadzi� dowolny tekst.
}
unit Uiquery;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
  InputQueryOK :Boolean; // Zadeklarowanie zmiennej logicznej
  NowyTekst :String; // Zadeklarowanie zmiennej tekstowej
begin
  // Wywo�anie okna edycyjnego 'InputQuery'

  NowyTekst:= ''; // Wyczyszczenie zmiennej

  NowyTekst:= 'To jest tekst domy�lny'; // Przypisanie zmiennej tekstu

  {
   Przypisanie warto�ci TRUE(PRAWDA) lub FALSE(FA�SZ) przez
   funkcj� "InputQuery" zmiennej "InputQueryOK".
  }
  InputQueryOK:= InputQuery('Tekst', 'Wpisz jaki� tekst:', NowyTekst);

  {
   "if (InputQueryOK = TRUE) then"
   Je�eli funkcja "InputQuery" zwr�ci warto�� TRUE(PRAWDA), to
   spe�niony b�dzie warunek "InputQueryOK = TRUE", a co za
   tym idzie, nast�pi wykonanie instrukcji po s�owie THEN.

   ===============================================================

   "Button1.Caption:= NowyTekst"
   Przypisanie przyciskowi tekstu umieszczonego w zmiennej NowyTekst
  }
  if (InputQueryOK = TRUE) then Button1.Caption:= NowyTekst;


  {
   InputQuery - Funkcja wprowadza do programu tekst, wprowadzony
                przez u�ytkownika.
                Wprowadzenie tekstu nast�puje, w momencie
                naci�ni�cia klawisza ENTER lub klikni�cia na
                klawisz z napisem OK.
                W innym przypadku, tekst nie b�dzie
                wprowadzony do programu.

   =====================================================================

   InputQuery(Nazwa_Okna,
              Tekst_Opisuj�cy_Edytorek,
              Tekst_Domyslny);

   Nazwa_Okna - W tym parametrze wpisujemy nazw� okna,
                kt�ra zostanie wy�wietlona na pasku
                tytu�owym, np. "Tekst".

   Tekst_Opisuj�cy_Edytorek - W tym parametrze wpisujemy
                              tekst, kt�ry uka�e si� nad
                              polem edycyjnym,
                              np. "Wpisz jaki� tekst:".

   Tekst_Domyslny - W tym parametrze znajduje si� zmienna (np. NowyTekst),
                    do kt�rej przypisany jest tekst domy�lny.
                    Tekst ten jest wy�wietlany w polu edycyjnym,
                    po uruchomieniu funkcji InputQuery. 
  }
end;

end.

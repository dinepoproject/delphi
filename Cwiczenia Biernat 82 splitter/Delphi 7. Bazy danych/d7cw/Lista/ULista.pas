{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad tworzenia w�asnej listy.
}
unit ULista;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    bDodajDoListyElementyZapisujacDoPliku: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure bDodajDoListyElementyZapisujacDoPlikuClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    List: TStringList; // Obiekt reprezentuj�cy nasz� list�.
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  List:= TStringList.Create; // Dynamiczne tworzenie listy
end;

procedure TForm1.bDodajDoListyElementyZapisujacDoPlikuClick(Sender: TObject);
begin
  // Dodaje elementy do listy i zapisuje j� do pliku
  List.Clear; // Czyszczenie listy
  List.Add('Spectrum'); // Dodanie do listy
  List.Add('Amiga');
  List.Add('IBM');
  List.Add('Atari');
  List.Add('�yrafa');
  List.Add('Star Wars');
  List.Add('Cry');
  List.Add('CPU');
  List.Add('Komputer');
  List.Sort; // W��cza sortowanie listy
  Label1.Caption:= Trim(List.Strings[0]); // Odczytanie elementu z listy o podanym numerze
  List.SaveToFile('Lista.txt'); // Zapisanie w�asnej listy do pliku
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // FormClose
  List.Destroy; // Usuni�cie listy z pami�ci
//  DeleteFile('Lista.txt'); // Usuwa plik ze stworzon� list�.
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad tworzenia aplikacji typu MDI.
}
unit Child1; // Okno Dziecko

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, Menus;

type
  TmdiChild1 = class(TForm)
    ListBox1: TListBox;
    MainMenu1: TMainMenu;
    mListBox1: TMenuItem;
    ListBoxNapis: TMenuItem;
    bOtworzPlik: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ListBoxNapisClick(Sender: TObject);
    procedure bOtworzPlikClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses Main;

{$R *.DFM}

procedure TmdiChild1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // FormClose - Zwolnienie zasob�w
  Action := caFree;
end;

procedure TmdiChild1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  numBtn: Integer;
begin
  // FormCloseQuery - Przy wyj�ciu jest zadawane pytanie, czy na pewno chcemy zamkn�� okno.
  numBtn:= Application.MessageBox('Czy jeste� pewien ?', 'Pytanie',
           MB_ICONQUESTION or MB_YESNOCANCEL);
  if (numBtn = IDYES) then
  begin
    // Kod programu
  end;

  if (numBtn = IDNO) then
  begin
    // Kod programu
  end;
  if (numBtn = IDCancel) then CanClose:= FALSE;
end;

procedure TmdiChild1.FormCreate(Sender: TObject);
var
  TT: Integer;
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie uruchomienia programu.
  ListBox1.Items.Clear;
  for TT:= 0 to 3 do
    ListBoxNapisClick(Sender); // Wywo�anie procedury "ListBoxNapisClick"
end;

procedure TmdiChild1.FormActivate(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie uaktywnienia okna.
end;

procedure TmdiChild1.FormDeactivate(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie dezaktywacji okna.
end;

procedure TmdiChild1.FormShow(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie ukazania si� okna.
end;

procedure TmdiChild1.FormResize(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie
  // powi�kszania lub zmniejszania rozmiaru okna.
end;

procedure TmdiChild1.ListBoxNapisClick(Sender: TObject);
begin
  // ListBoxNapisClick
  ListBox1.Items.Add('Napis '+IntToStr(ListBox1.Items.Count+1));
end;

procedure TmdiChild1.bOtworzPlikClick(Sender: TObject);
begin
  // Uruchamia opcj� znajduj�c� si� w oknie rodzica z poziomu okna dziecka.
  MainForm.FileOpenItemClick(Sender);

  {
  MainForm.FileOpenItemClick(Sender); - Uruchomienie funkcji
  MainForm - Nazwa okna rodzica
  FileOpenItemClick(Sender) - Nazwa funkcji znajduj�cej si� w oknie rodzica
  }
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad tworzenia aplikacji typu MDI.
}
unit Child2; // Okno - Dziecko

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, Menus;

type
  TmdiChild2 = class(TForm)
    Memo1: TMemo;
    MainMenu1: TMainMenu;
    Memo2: TMenuItem;
    MemoWyraz: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure MemoWyrazClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TmdiChild2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // FormClose - Zwolnienie zasob�w
  Action := caFree;
end;

procedure TmdiChild2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  numBtn: Integer;
begin
  // FormCloseQuery - Przy wyj�ciu jest zadawane pytanie, czy na pewno chcemy zamkn�� okno.
  numBtn:= Application.MessageBox('Czy jeste� pewien ?', 'Pytanie',
           MB_ICONQUESTION or MB_YESNOCANCEL);
  if (numBtn = IDYES) then
  begin
    // Kod programu
  end;

  if (numBtn = IDNO) then
  begin
    // Kod programu
  end;
  if (numBtn = IDCancel) then CanClose:= FALSE;
end;

procedure TmdiChild2.FormCreate(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie uruchomienia programu.
  MemoWyrazClick(Sender); // Wywo�anie procedury "MemoWyrazClick"
end;

procedure TmdiChild2.FormActivate(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie uaktywnienia okna.
end;

procedure TmdiChild2.FormDeactivate(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie dezaktywacji okna.
end;

procedure TmdiChild2.FormShow(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie ukazania si� okna.
end;

procedure TmdiChild2.FormResize(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie
  // powi�kszania lub zmniejszania rozmiaru okna.
end;

procedure TmdiChild2.MemoWyrazClick(Sender: TObject);
begin
  // MemoWyraz1Click
  Memo1.Lines.Add('Wyraz '+IntToStr(Memo1.Lines.Count+1));
end;

end.

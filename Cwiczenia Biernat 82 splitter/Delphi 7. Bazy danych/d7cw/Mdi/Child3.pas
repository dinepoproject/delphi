{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad tworzenia aplikacji typu MDI.
}
unit Child3; // Okno - Dziecko

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, Menus;

type
  TmdiChild3 = class(TForm)
    Memo1: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TmdiChild3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // FormClose - Zwolnienie zasob�w
  Action := caFree;
end;

procedure TmdiChild3.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  // FormCloseQuery - Przy wyj�ciu jest zadawane pytanie, czy na pewno chcemy zamkn�� okno.
end;

procedure TmdiChild3.FormCreate(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie uruchomienia programu.
end;

procedure TmdiChild3.FormActivate(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie uaktywnienia okna.
end;

procedure TmdiChild3.FormDeactivate(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie dezaktywacji okna.
end;

procedure TmdiChild3.FormShow(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie ukazania si� okna.
end;

procedure TmdiChild3.FormResize(Sender: TObject);
begin
  // Tu b�dzie kod programu, kt�ry b�dzie wykonywany w momencie
  // powi�kszania lub zmniejszania rozmiaru okna.
end;

end.

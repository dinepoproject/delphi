{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad tworzenia aplikacji typu MDI.
}
unit UFormChB; // Okno - Dziecko

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, ExtDlgs,
  StdCtrls, ExtCtrls, ComCtrls, Menus, Dialogs;

const
  msgBrowseFile = 'Brak pliku(�w) do podgl�du !!!'; // Zmienna sta�a, kt�rej zadaniem jest trzymanie komunikatu.
type
  TfrmFormChildPrzegladaj = class(TForm)
    ListBox1: TListBox;
    Splitter1: TSplitter;
    Memo1: TMemo;
    function ListBoxSzukajPlikow(txtPath, txtExt: String): Shortint;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
  private
    { Private declarations }
    txtIGPathDir, txtIGREPath: String;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

function TfrmFormChildPrzegladaj.ListBoxSzukajPlikow(txtPath, txtExt: String): Shortint;
var
  SR :TSearchRec;
begin
  {
    Wczytanie do listy nazwy plik�w.
    Je�eli funkcja nie znajdzie plik�w o odpowiednim
    rozszerzeniu to zwr�ci warto�� -1.
    W przeciwnym przypadku zwr�ci warto�� 1.
  }
  ListBoxSzukajPlikow:= -1;

  ListBox1.Items.Clear; // Wyczyszczenie zawarto�ci listy.

  {
    W��czenie automatycznego dopasowania wysoko�ci.
    Umo�liwia widoczno�� ostatniego elementu listy.
  }
  ListBox1.IntegralHeight:= FALSE;

  ListBox1.Sorted:= TRUE; // W��czenie sortowania.

  {
   Sprawdzenie czy w katalogu istniej� pliki o podanym rozszerzeniu.
   Funkcja wykonuje si� tak d�ugo, a� wszystkie pliki spe�niaj�ce
   warunek zostan� wczytane do ListBox'a.
  }
  if (FindFirst(Trim(txtPath+txtExt), faAnyFile, SR) = 0) then
  begin
    repeat
      if (SR.Attr<>faDirectory) then
      begin
        ListBox1.Items.Add(Trim(SR.Name));
        ListBoxSzukajPlikow:= 1;
      end;
    until(FindNext(SR)<>0);
    FindClose(SR);
  end;
end;

procedure TfrmFormChildPrzegladaj.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // FormClose
  Action := caFree; // Powoduje zamkni�cie okna i usuni�cie obiektu.
end;

procedure TfrmFormChildPrzegladaj.FormCreate(Sender: TObject);
begin
  // FormCreate

  // Przypisanie nazwy katalogu.
  txtIGPathDir:= ''; // Wyzerowanie zmiennej.
  txtIGPathDir:= GetCurrentDir+'\'; // GetCurrentDir - Zwraca nazw� bie��cego katalogu.


//--- Ini ---
  Memo1.WordWrap:= FALSE; // Wy��czenie zwijania wyraz�w.
  Memo1.ScrollBars:= ssBoth; // W��czenie belek s�u��cych do przewijania tekstu.
  Memo1.ReadOnly:= TRUE; // Wy��czenie mo�liwo�ci edycji.
  Memo1.Lines.Clear; // Wyczyszczenie zawarto�ci komponentu Memo.
  Memo1.Lines.Add(msgBrowseFile); // Wprowadzenie komunikatu znajduj�cego si� w sta�ej "msgBrowseFile"

  // W��czenie mo�liwo�ci zaznaczanie wielu pozycji.
  ListBox1.MultiSelect:= TRUE;
  ListBox1.ExtendedSelect:= TRUE;
  ListBox1.Width:= 223; // Ustawienie szeroko�ci komponentu ListBox.

  ListBoxSzukajPlikow(txtIGPathDir, '*.pas'); // Wywo�anie funkcji "ListBoxSzukajPlikow" odczytuj�cej nazwy plik�w.
  {
   ListBoxSzukajPlikow(txtIGPathDir, '*.pas'); - Wywo�anie funkcji "ListBoxSzukajPlikow" odczytuj�cej nazwy plik�w.
  }
end;

procedure TfrmFormChildPrzegladaj.ListBox1Click(Sender: TObject);
begin
  // Przegl�danie plik�w

  {
    Sprawdzenie czy zosta�a zaznaczona jaka� pozycja,
    je�eli tak to poka� zawarto�� zaznaczonego pliku.
  }
  if (ListBox1.ItemIndex >-1) then
  begin

    {
     Przypisanie zmiennej "txtIGREPath" nazwy katalogu wraz z nazw� wybranego pliku.
    }
    txtIGREPath:= '';
    txtIGREPath:= txtIGPathDir+ListBox1.Items[ListBox1.ItemIndex];

    Memo1.Lines.Clear; // Wyczyszczenie zawarto�ci komponentu Memo.

    {
     Sprawdzenie czy istnieje plik.
     Je�eli plik istnieje to wczytaj ten plik, w
     innym przepadku poka� komunikat.
    }
    if (FileExists(txtIGREPath) = TRUE) then
      Memo1.Lines.LoadFromFile(txtIGREPath)
    else
      Memo1.Lines.Add(msgBrowseFile);

  end;
end;

end.

�
 TMAINFORM 0�	  TPF0	TMainFormMainFormLeft� TopnWidth�Height,CaptionMDI ApplicationColorclAppWorkSpaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle	fsMDIFormMenu	MainMenu1Position	poDefaultWindowStatewsMaximized
WindowMenuWindow1OnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TPanel
SpeedPanelLeft Top Width�Height)AlignalTop
BevelOuterbvNoneParentShowHintShowHint	TabOrder  TBevelBevel1Left Top Width�HeightAlignalTopShape	bsTopLine  TSpeedButtonsbOpenLeft
TopWidthHeightHintOtw�rz (Ctrl+O)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333�33;3�3333�;�w{�w{�7����s3�    33wwwwww330����337�333330����337��?�330� 337�sws330����3?����?���� �ww�wssw;������7w��?�ww30�  337�swws330���3337��7�330��3337�sw�330�� ;�337��w7�3�  3�33www3w�;�3;�3;�7s37s37s�33;333;s3373337	NumGlyphsOnClicksbOpenClick  TSpeedButtonsbWypiszHasloLeft2TopWidthYHeightCaptionWypisz has�oFlat	OnClicksbWypiszHasloClick   	TMainMenu	MainMenu1Left&Top�  	TMenuItemFile1Caption&PlikHintFile related commands 	TMenuItemFileNewItemCaption&Nowy (ListBox)HintCreate a new fileOnClickFileNewItemClick  	TMenuItemFileNowyMemoCaptionNow&y (Memo)OnClickFileNowyMemoClick  	TMenuItemFileOpenItemCaption
&Otw�rz...HintOpen an existing fileShortCutO@OnClickFileOpenItemClick  	TMenuItemFilePrzegldajCaptionPrzeg&l�daj...OnClickFilePrzegldajClick  	TMenuItemFileCloseItemCaption&ZamknijHintClose current fileOnClickFileCloseItemClick  	TMenuItemN1Caption-  	TMenuItemFileExitItemCaption&Wyj�cieHintExit the application   	TMenuItemWindow1Caption&Okno
GroupIndexHint0Window related commands such as Tile and Cascade 	TMenuItemWindowCascadeItemCaption&KaskadaHintArrange windows to overlapOnClickWindowCascadeItemClick  	TMenuItemWindowTileItemCaption&PoziomoHintArrange windows without overlapOnClickWindowTileItemClick  	TMenuItemWindowArrangeItemCaption&Uk�adaj ikonyHint-Arrange window icons at bottom of main windowOnClickWindowArrangeItemClick  	TMenuItemWindowMinimizeItemCaption&Minimalizuj wszystkie oknaHintMinimize all windowsOnClickWindowMinimizeItemClick    TOpenDialogOpenDialog1FilterAll files (*.*)|*.*LeftTop�    
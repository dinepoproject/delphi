{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad tworzenia aplikacji typu MDI.

Opis:
Programu typu MDI (ang. Multiple Document Interface) umo�liwiaj�
jednocze�nie otwieranie i przegl�danie jednego lub wi�kszej
ilo�ci plik�w. Ka�dy dokument znajduje si� w osobnym oknie.
Wszystkie okna wy�wietlane s� na wsp�lnym obszarze, na kt�rym
rozmieszcza si� i zarz�dza oknami. W danej chwili jest tylko
jedno okno aktywne. W celu przej�cia do nast�pnego okna u�ywamy
kombinacji Ctrl+F6 lub klikamy na obszar interesuj�cego nas okna.


Tworzenie aplikacji MDI:
1) Wybra� menu "Plik";
2) Wybra� opcj� "New...";
3) Przej�� do zak�adki "Projects";
4) Wybra� z okienka rysunek z napisem "MDI application" i nacisn�� klawisz OK.
}
unit Main; // Okno Rodzica

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, Menus,
  StdCtrls, Dialogs, Buttons, Messages, ExtCtrls, ComCtrls;

type
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    FileNewItem: TMenuItem;
    FileOpenItem: TMenuItem;
    FileCloseItem: TMenuItem;
    Window1: TMenuItem;
    N1: TMenuItem;
    FileExitItem: TMenuItem;
    WindowCascadeItem: TMenuItem;
    WindowTileItem: TMenuItem;
    WindowArrangeItem: TMenuItem;
    OpenDialog1: TOpenDialog;
    WindowMinimizeItem: TMenuItem;
    SpeedPanel: TPanel;
    Bevel1: TBevel;
    sbOpen: TSpeedButton;
    FileNowyMemo: TMenuItem;
    sbWypiszHaslo: TSpeedButton;
    FilePrzegldaj: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FileNewItemClick(Sender: TObject);
    procedure WindowCascadeItemClick(Sender: TObject);
    procedure UpdateMenuItems(Sender: TObject);
    procedure WindowTileItemClick(Sender: TObject);
    procedure WindowArrangeItemClick(Sender: TObject);
    procedure FileCloseItemClick(Sender: TObject);
    procedure WindowMinimizeItemClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FileOpenItemClick(Sender: TObject);
    procedure sbOpenClick(Sender: TObject);
    procedure FileNowyMemoClick(Sender: TObject);
    procedure sbWypiszHasloClick(Sender: TObject);
    procedure FilePrzegldajClick(Sender: TObject);
  private
    { Private declarations }
    procedure CreateMDIChildPrzegladaj(const Name: string); // Przegl�darka plik�w
    procedure CreateMDIChild1(const Name: string); // ListBox
    procedure CreateMDIChild2(const Name: string); // Memo
    procedure CreateMDIChild3(const Name, txtFileName: string); // Uruchomienie podgl�du wczytanego pliku
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.DFM}

uses UFormChB, Child1, Child2, Child3;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  // FormCreate
  Screen.OnActiveFormChange := UpdateMenuItems;
end;

procedure TMainForm.CreateMDIChildPrzegladaj(const Name: string);
// Przegl�darka plik�w
var
  Child: TfrmFormChildPrzegladaj;
begin
  { create a new MDI child window }
  Child := TfrmFormChildPrzegladaj.Create(Application);
  Child.Caption := Name;
end;

procedure TMainForm.CreateMDIChild1(const Name: string);
// ListBox
var
  Child: TmdiChild1;
begin
  { create a new MDI child window }
  Child := TmdiChild1.Create(Application);
  Child.Caption := Name;
end;

procedure TMainForm.CreateMDIChild2(const Name: string);
// Memo
var
  Child: TmdiChild2;
begin
  { create a new MDI child window }
  Child := TmdiChild2.Create(Application);
  Child.Caption := Name;
end;

procedure TMainForm.CreateMDIChild3(const Name, txtFileName: string);
// Uruchomienie podgl�du wczytanego pliku
var
  Child: TmdiChild3;
begin
  { create a new MDI child window }
  Child := TmdiChild3.Create(Application);
  Child.Memo1.Lines.Clear;
  Child.Memo1.Lines.LoadFromFile(txtFileName);
  Child.Caption := Name;
end;

procedure TMainForm.FileNewItemClick(Sender: TObject);
begin
  // FileNewItemClick
  CreateMDIChild1('ListBox' + IntToStr(MDIChildCount + 1));
end;

procedure TMainForm.FileCloseItemClick(Sender: TObject);
begin
  // Zamkni�cie aktywnego okna dziecka
  if ActiveMDIChild <> nil then
    ActiveMDIChild.Close;
end;

procedure TMainForm.WindowCascadeItemClick(Sender: TObject);
begin
  // WindowCascadeItemClick
  Cascade;
end;

procedure TMainForm.WindowTileItemClick(Sender: TObject);
begin
  // WindowTileItemClick
  Tile;
end;

procedure TMainForm.WindowArrangeItemClick(Sender: TObject);
begin
  // WindowArrangeItemClick
  ArrangeIcons;
end;

procedure TMainForm.WindowMinimizeItemClick(Sender: TObject);
var
  I: Integer;
begin
  { Must be done backwards through the MDIChildren array }
  for I := MDIChildCount - 1 downto 0 do
    MDIChildren[I].WindowState := wsMinimized;
end;

procedure TMainForm.UpdateMenuItems(Sender: TObject);
begin
  // UpdateMenuItems
  FileCloseItem.Enabled := MDIChildCount > 0;
  WindowCascadeItem.Enabled := MDIChildCount > 0;
  WindowTileItem.Enabled := MDIChildCount > 0;
  WindowArrangeItem.Enabled := MDIChildCount > 0;
  WindowMinimizeItem.Enabled := MDIChildCount > 0;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  // FormDestroy
  Screen.OnActiveFormChange := nil;
end;

procedure TMainForm.FileOpenItemClick(Sender: TObject);
begin
  // FileOpenItemClick
  OpenDialog1.FileName:= '';
  OpenDialog1.Execute;
  if (Trim(OpenDialog1.FileName)<>'') then
  begin
    CreateMDIChild3(ExtractFileName(Trim(OpenDialog1.FileName)),
                    Trim(OpenDialog1.FileName));
  end;
end;

procedure TMainForm.sbOpenClick(Sender: TObject);
begin
  // sbOpenClick
  FileOpenItemClick(Sender);
end;

procedure TMainForm.FileNowyMemoClick(Sender: TObject);
begin
  // FileNowyMemoClick
  CreateMDIChild2('Memo1' + IntToStr(MDIChildCount + 1));
end;

procedure TMainForm.sbWypiszHasloClick(Sender: TObject);
begin
  // Uruchamia funkcj� z menu, kt�re znajduj� si� w oknach dzieci

  // Uruchomienie Funkcji "ListBoxNapis1Click(Sender);" w Dziecku 1 (Child1)
  if ActiveMDIChild is TmdiChild1 then
    TmdiChild1(ActiveMDIChild).ListBoxNapisClick(Sender);

  // Uruchomienie Funkcji "MemoWyraz1Click(Sender);" w Dziecku 2 (Child2)
  if ActiveMDIChild is TmdiChild2 then
    TmdiChild2(ActiveMDIChild).MemoWyrazClick(Sender);
end;

procedure TMainForm.FilePrzegldajClick(Sender: TObject);
const
  txtPrzeglad ='Przegl�daj...'; // Zadeklarowanie sta�ej okre�laj�cej nazw� okienka s�u��cego do przegl�dania plik�w.
var
             TT: Integer; // Zadeklarowanie zmiennej liczbowej
  okCzyIstnieje: Boolean; // Zadeklarowanie zmiennej logicznej
begin
  // Uruchomienie przegl�dania plik�w.
  okCzyIstnieje:= FALSE; // Przypisanie zmiennej warto�ci FALSE (fa�sz)

  // Sprawdzenie, kt�re okno s�u�y do przegl�dania plik�w.
  for TT:= MDIChildCount - 1 downto 0 do
    if (MDIChildren[TT].Caption = txtPrzeglad) then
    begin
      // Gdy okno zostanie znalezione to uaktywnij go i daj na pierwszy plan.
      MDIChildren[TT].BringToFront; // Przesuwa okna na pierwszy plan
      MDIChildren[TT].WindowState:= wsNormal; // Powoduje ukazanie si� okna
      okCzyIstnieje:= TRUE; // Przypisanie zmiennej 'okCzyIstnieje' warto�ci TRUE je�eli okno do przegl�dania plik�w zosta�o znalezione.
    end;

  // Uruchomienie okna do przegl�dania plik�w, je�eli zmienna 'okCzyIstnieje' r�wna si� FALSE.
  if (okCzyIstnieje = FALSE) then CreateMDIChildPrzegladaj(txtPrzeglad);
end;

end.

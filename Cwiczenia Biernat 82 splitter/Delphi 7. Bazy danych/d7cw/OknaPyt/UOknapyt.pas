{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad zastosowania okien pytaj�cych.
}
unit UOknapyt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  numBtn: Integer;
begin
  // Dwie mo�liwo�ci do wyboru
  numBtn:= 0;
  numBtn:= Application.MessageBox('Kt�r� wybierasz ?', 'Dwie mo�liwo�ci do wyboru',
           MB_ICONQUESTION or MB_YESNO);
  if (numBtn = IDYES) then
  begin
    ShowMessage('To jest PIERWSZA mo�liwo��');
  end;
  if (numBtn = IDNO) then
  begin
    ShowMessage('To jest DRUGA mo�liwo��');
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  numBtn: Integer;
begin
  // Trzy mo�liwo�ci do wyboru
  numBtn:= 0;
  numBtn:= Application.MessageBox('Kt�r� wybierasz ?', 'Trzy mo�liwo�ci do wyboru',
           MB_ICONQUESTION or MB_YESNOCANCEL);
  if (numBtn = IDYES) then
  begin
    ShowMessage('To jest PIERWSZA mo�liwo��');
  end;
  if (numBtn = IDNO) then
  begin
    ShowMessage('To jest DRUGA mo�liwo��');
  end;
  if (numBtn = IDCancel) then
  begin
    ShowMessage('To jest TRZECIA mo�liwo��');
  end;
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Opis podstawowych zdarze� dotycz�cych formatki.


Wprowadzenie:

Formatka jest to okno na kt�rym umieszcza si� komponenty, w
wyniku czego powstaje aplikacja.
}
unit UOpis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    bRun: TButton;
    procedure FormActivate(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bRunClick(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses UOpis2;

{$R *.DFM}

procedure TForm1.FormActivate(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie gdy formatka jest aktywna.
end;

procedure TForm1.FormClick(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie gdy kliknie si� na formatce.
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie zamykania formatki.
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  // Tu wpisujemy instrukcje, kt�re maj� na celu zapytanie u�ytkownika w
  // momencie zamykania formatki np. wywo�anie dialogu z pytaniem.
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane przed utworzeniem okna formatki.
end;

procedure TForm1.FormDblClick(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie
  // dwukrotnego klikni�cia na formatce.
end;

procedure TForm1.FormDeactivate(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie,
  // gdy formatka przestaje by� aktywna.
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie likwidacji formatki.
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie
  // naci�ni�cia klawiszy funkcyjnych np. Enter, F1..F12, PageUp itp.
end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: Char);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie naci�ni�cia
  // dowolnego lub wybranego klawisza alfanumerycznego np. a, b, <, > itp.
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie
  // zwi�kszania lub zmniejszania rozmiar�w formatki.
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie ukazania si� formatki.
end;

procedure TForm1.bRunClick(Sender: TObject);
begin
  // Uruchomienie drugiej formatki
  Form2.ShowModal;
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  // Tu wpisujemy instrukcje, kt�re b�d� wykonywane w momencie poruszania kursorem myszy.
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  {
   Tu wpisujemy instrukcje odpowiedzialne za od�wie�enie zawarto�ci
   formatki w przypadku zas�oni�cia tej formatki przez inn� formatk�.
  }
end;

end.

unit UOpis2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm2 = class(TForm)
    Label1: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.DFM}

procedure TForm2.FormActivate(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie gdy formatka jest aktywna.
  ShowMessage('Formatka jest aktywna.')
end;

procedure TForm2.FormClick(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie gdy kliknie si� na formatce.
  Label1.Caption:= 'Klikn��e� raz na formatce.';
end;

procedure TForm2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie zamykania formatki.
  ShowMessage('Formatka jest zamykana.');
end;

procedure TForm2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  numBtn: Integer;
begin
  // Tu wpisujemy instrukcje, kt�re maj� na celu zapytanie u�ytkownika w
  // momencie zamykania formatki np. wywo�anie dialogu z pytaniem.
  numBtn:= 0;
  numBtn:= Application.MessageBox('Zamkn�� formatk� ?', 'Pytanie',
           MB_ICONQUESTION or MB_YESNO);
  if (numBtn = IDNO) then CanClose:= FALSE;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane przed utworzeniem okna formatki.
  Caption:= 'Zamiast tytu�u, to b�dzie ten tekst w pasku tytu�owym.';
end;

procedure TForm2.FormDblClick(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie
  // dwukrotnego klikni�cia na formatce.
  ShowMessage('Klikn��e� dwa razy na formatce.');
end;

procedure TForm2.FormDeactivate(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie,
  // gdy formatka przestaje by� aktywna.
  ShowMessage('Formatka jest nieaktywna.')
end;

procedure TForm2.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie
  // naci�ni�cia klawiszy funkcyjnych np. Enter, F1..F12, PageUp itp.
  if (Key = VK_RETURN) then Label1.Caption:= 'Nacisn��e� klawisz funkcyjny ENTER';
  if (Key = VK_UP) then Label1.Caption:= 'Nacisn��e� klawisz funkcyjny STRZA�KA w G�R�';
  if (Key = VK_DOWN) then Label1.Caption:= 'Nacisn��e� klawisz funkcyjny STRZA�KA w Dӣ';
  if (Key = VK_LEFT) then Label1.Caption:= 'Nacisn��e� klawisz funkcyjny STRZA�KA w LEWO';
  if (Key = VK_RIGHT) then Label1.Caption:= 'Nacisn��e� klawisz funkcyjny STRZA�KA w PRAWO';
  if (Key = VK_ESCAPE) then Label1.Caption:= 'Nacisn��e� klawisz funkcyjny ESC';
  if (Key = VK_CANCEL) then Label1.Caption:= 'Nacisn��e� klawisz funkcyjny CTRL+BREAK';
  if (Key = VK_INSERT) then Label1.Caption:= 'Nacisn��e� klawisz funkcyjny INSERT';
  if (Key = VK_DELETE) then Label1.Caption:= 'Nacisn��e� klawisz funkcyjny DELETE';
end;

procedure TForm2.FormKeyPress(Sender: TObject; var Key: Char);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie naci�ni�cia
  // dowolnego lub wybranego klawisza alfanumerycznego np. a, b, <, > itp.
  Label1.Caption:= 'Nacisn��e� klawisz funkcyjny: '+Key;
end;

procedure TForm2.FormResize(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie
  // zwi�kszania lub zmniejszania rozmiar�w formatki.
  Height:= 208; // Wysoko�� b�dzie sta�a, natomiast szeroko�� b�dzie zmienna.
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  // Tu wpisujemy instrukcje, kt�re s� wykonywane w momencie ukazania si� formatki.
  ShowMessage('Formatka zaraz si� uka�e.')
end;

end.

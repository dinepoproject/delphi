{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przykład wykorzystania zdarzenia MouseMove.
}
unit Upkursora;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  // Zadeklarowanie obiektu reprezentującego punkty na ekrania.
  Position: TPoint;
begin
  // Podanie pozycji kursora myszy w momencie poruszania nim.
  GetCursorPos(Position); 

  // Wyświetlenie pozycji kursora myszy na ekranie za pomocą komponentu Label.
  Label1.Caption:= 'X:'+CHR(32)+IntToStr(Position.X-Left)+CHR(32)+
                   '/'+CHR(32)+'Y:'+CHR(32)+IntToStr(Position.Y-Top);
end;

end.

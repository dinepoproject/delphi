{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad wykorzystania funkcji 'ParamStr'.

Konstrukcja:
function ParamStr(numIndex: Integer): String;

Opis:
Funkcja "ParamStr" zwraca parametr o numerze 'numIndex',
kt�ry jest podany w parametrze funkcji w momencie
wywo�ania aplikacji (programu).


Przyk�ady:
ParamStr(0) - zwraca �cie�k� dost�pu i nazw� uruchomionej
              aplikacji (programu).
ParamStr(1) - zwraca nazw� 1-go parametru, kt�ry by� podany w momencie
              wywo�ania aplikacji (programu).

Spos�b wywo�ania programu z parametrem, np. view.exe Fale.bmp.
}
unit UForm;

interface

uses
  Windows, SysUtils, Classes, Forms, Dialogs, ExtCtrls, Buttons, StdCtrls,
  Controls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormShow(Sender: TObject);
begin
   // Wczytanie rysunku o nazwie podanej,
   // w trakcie uruchomienia programu.

   {
    Warunek "(ParamStr(1)<>'')" sprawdza, czy zosta�a podana
    nazwa pliku w trakcie uruchamiania programu.

    Je�eli nazwa pliku by�a podana w trakcie wywo�ywania
    programu, to wykonaj instrukcje po s�owie THEN.
    W innym przypadku wykonaj instrukcje po s�owie ELSE.
   }
   if (ParamStr(1)<>'') then
   begin
      // Je�eli warunek zosta� spe�niony, to wykonaj poni�sze instrukcje

      Memo1.Lines.Clear; // Wyczyszczenie komponentu "Memo1"

      // Wczytanie pliku o nazwie podanej w parametrze
      Memo1.Lines.LoadFromFile(ParamStr(1));
   end
   else
   begin
      // Zamknij program, gdy nie podano �adnej
      // nazwy w trakcie wywo�ywania programu
      Application.Terminate;
   end;
end;

end.

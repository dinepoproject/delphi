{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad uruchomienia innego programu z
poziomu tej aplikacji.
}
unit URun;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ShellApi;
// ShellApi - Funkcje i procedury biblioteki API

type
  TForm1 = class(TForm)
    Button2: TButton;
    Bevel1: TBevel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Uruchomienie programu Chemia
  ShellExecute(Handle, 'open', PChar('chemia.exe'), '', '', sw_Normal); // ShellExecute

  // Konwersja do typu "PChar(�ancuch)" zwraca wska�nik do pierwszego znaku �a�cucha.
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  // Uruchomienie programu Kalkulator (calc.exe)
  ShellExecute(Handle, 'open', 'calc.exe', '', '', sw_Normal); // ShellExecute
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

 Przyk�ad pisania cyfr s�ownie
}
unit USlow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Slownie;
{
Slownie - jest to wywo�anie biblioteki, kt�ra musi znajdowa�
          si� w podkatalogu LIB katalogu DELPHI lub w katalogu
          programu aktualnie wykorzystuj�cego t� bibliotek�.

Wywo�anie to wpisujemy w sekcji "USES".
}

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    Label4: TLabel;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  Label1.Caption:= '1234.45 = '+plnSlownie('1234.45');
  Label2.Caption:= '9571234,67 = '+plnSlownie('9571234,67');
  Label3.Caption:= '999999999999999,99 = '+plnSlownie('999999999999999,99');
  Edit1.Text:= '';
  Memo1.lines.Clear;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  // S�ownie
  Memo1.lines.Clear;
  Memo1.lines.Add(plnSlownie(Edit1.Text));
  {
    plnSlownie('Cyfra') - wywo�anie funkcji, kt�ra zamienia cyfry
                          traktowane jako ci�g znak�w na wyrazy.

    Przyk�ad:
    63273,46 - sze��dziesi�t trzy tysi�ce dwie�cie siedemdziesi�t
               trzy z�ote i czterdzie�ci sze�� groszy

    UWAGA:
    Funkcja bierze pod uwag� tylko dwa miejsca po przecinku.
  }
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania bloku obs�ugi wyj�tku.


Wprowadzenie:

Mechanizm obs�ugi wyj�tku jest bardzo wygodnym narz�dziem pozwalaj�cym na
wychwycenie sytuacji wyj�tkowych, np. dzielenie przez zero.
Dzi�ki temu mechanizmowi program jest bardziej stabilny.
Mechanizm wyj�tku dzia�a tylko w przypadkach wyst�pienia b��du.

UWAGA:
Program z mechanizmem obs�ugi b��d�w nale�y
uruchomi� poza �rodowiskiem Delphi.


Konstrukcja:

Try
  ...
  instrukcje mog�ce spowodowa� b��d
  ...
except
  ...
  instrukcje wykonywane po wyst�pieniu b��du
  ...
end;

W celu zadzia�ania bloku obs�ugi wyj�tku, prosz� wyci�gn�� dyskietk� ze stacji.
}
unit UTryEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    bVerify: TButton;
    procedure bVerifyClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.bVerifyClick(Sender: TObject);
const
  txtFileName = 'A:\Test.tmp';
var
  FT: TextFile;
begin
  // Funkcja sprawdza, czy dyskietka jest zabezpieczona przed zapisem.
  try

    // Pr�ba zapisu na dyskietce.
    AssignFile(FT, Trim(txtFileName)); // Skojarzenie zmiennej z nazw� pliku.
    Rewrite(FT); // // Stworzenie i otwarcie nowego pliku do zapisu.
    Writeln(FT, '-- Test.tmp --'); // Zapisanie do pliku '-- Test.tmp -'.
    CloseFile(FT); // Zamkni�cie pliku

  except

    // Informacja o zabezpieczeniu przed zapisem.
    Application.MessageBox('Usu� zabezpieczenie dyskietki !!!',
                   'B��d', MB_ICONERROR or MB_OK);

  end;
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania mechanizmy zwalniania zasob�w.


Wprowadzenie:

Mechanizm zwalniania zasob�w jest bardzo wygodnym narz�dziem
gwarantuj�cym zwolnienie zasob�w (np. plik, pami�� dynamiczna,
zasoby systemowe i obiekty) w przypadku wyst�pienia b��du.
Instrukcje zawarte w bloku zwalniania zasob�w wykonywane s� zawsze.
Mechanizm ten pozwala na stabiln� prac� programu.

Konstrukcja:

Try
  ...
  instrukcje korzystaj�ce z zasob�w, mog�ce spowodowa� b��d
  ...
finally
  ...
  zwalnianie zasob�w (instrukcje tu zawarte wykonywane s� zawsze).
  ...
end;

}
unit UTryfi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    bIkona: TButton;
    procedure bIkonaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.bIkonaClick(Sender: TObject);
var
  Icon: TIcon;
begin
  // Obs�uga bloku zwalniania zasob�w.
  try
    // Instrukcje korzystaj�ce z zasob�w, mog�ce wywo�a� b��d.
    Icon:= TIcon.Create;
    Icon.LoadFromFile('Smurf2.ico');
    Application.Icon:= Icon;
  finally
    // Zwalnianie zasob�w (instrukcje tu zawarte wykonywane s� zawsze).
    Icon.Free;
  end;
end;

end.

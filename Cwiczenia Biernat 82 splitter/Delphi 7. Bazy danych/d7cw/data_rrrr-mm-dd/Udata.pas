{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad daty w formacie RRRR-MM-DD.
}
unit Udata;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function DodajZeroPrzed(txtZero: String): String;
begin
{
Funkcja dodaje zero do pojedynczej liczby traktowanej
jako tekst, w innym przypadku zwraca dwie liczby.

Przyk�ad:
  1) 10 - zwr�ci nam liczb� dziesi�� traktowan� jako tekst
  2)  3 - zwr�ci nam liczb� 03 traktowan� jako tekst, przez
          dodanie zera przed liczb� trzy
}
  DodajZeroPrzed:= txtZero;
  if (Length(txtZero) =1) then DodajZeroPrzed:= '0'+txtZero;
end;

function DATA_DzisiajJest: String;
var
  Rok, Miesiac, Dzien :Word;
begin
  // DATA_DzisiajJest
  DecodeDate(Now, Rok, Miesiac, Dzien);
  {
    DecodeDate() - Procedura ta, odpowiedzialna jest za
                   podzielenie daty podanej w
                   parametrze "Data" na rok, miesi�c i dzie�.

    Funkcja "Date" - zwraca aktualn� dat�.

    IntToStr() - Dokonuje konwersji liczby na liczb� traktowan� jako tekst,
    po rozdzieleniu daty do zmiennych:
                   ROK - przypisywany jest rok bie��cy;
               MIESIAC - przypisywany jest miesi�c bie��cy;
                 DZIEN - przypisywany jest dzie� bie��cy.
   }

  DATA_DzisiajJest:= IntToStr(Rok)+'-'+
                     DodajZeroPrzed(IntToStr(Miesiac))+'-'+
                     DodajZeroPrzed(IntToStr(Dzien));
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Inicjacja programu
  Label1.Caption:= DATA_DzisiajJest;
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu Animate.


Wprowadzenie:
Komponent Animate s�u�y do odtwarzania standardowych
animacji dla Windows, taki jak:
  - Kopiowanie plik�w;
  - Usuwanie plik�w;
  - Wyszukiwanie plik�w;
  - Opr�nianie kosza;
  - itp.

Rodzaj animacji wybieramy za pomoc�
w�a�ciwo�ci CommonAVI komponentu Animate.
}
unit UAnimate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Animate1: TAnimate;
    GroupBox1: TGroupBox;
    Button1: TButton;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    procedure Button1Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure RadioButton7Click(Sender: TObject);
    procedure RadioButton8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  // W��cza lub wy��cza animacj�
  if (Animate1.Active = TRUE) then
    Animate1.Active:= FALSE
  else
    Animate1.Active:= TRUE;
end;

procedure TForm1.RadioButton1Click(Sender: TObject);
begin
  // Brak animacji
  Animate1.CommonAVI:= aviNone;
end;

procedure TForm1.RadioButton2Click(Sender: TObject);
begin
  // Kopiowanie pliku
  Animate1.CommonAVI:= aviCopyFile;
end;

procedure TForm1.RadioButton3Click(Sender: TObject);
begin
  // Usuwanie pliku
  Animate1.CommonAVI:= aviDeleteFile;
end;

procedure TForm1.RadioButton4Click(Sender: TObject);
begin
  // Opr�niane kosza
  Animate1.CommonAVI:= aviEmptyRecycle;
end;

procedure TForm1.RadioButton5Click(Sender: TObject);
begin
  // Szukanie komputera
  Animate1.CommonAVI:= aviFindComputer;
end;

procedure TForm1.RadioButton6Click(Sender: TObject);
begin
  // Szukanie pliku
  Animate1.CommonAVI:= aviFindFile;
end;

procedure TForm1.RadioButton7Click(Sender: TObject);
begin
  // Szukanie katalogu
  Animate1.CommonAVI:= aviFindFolder;
end;

procedure TForm1.RadioButton8Click(Sender: TObject);
begin
  // Usuwanie pliku do kosza
  Animate1.CommonAVI:= aviRecycleFile;
end;

end.

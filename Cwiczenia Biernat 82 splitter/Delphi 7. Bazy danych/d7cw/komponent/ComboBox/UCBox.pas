{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu ComboBox.


Wprowadzenie:

ComboBox jest to lista rozwijana po��czona z edytorem.
Rozwini�cie tej list nast�puje po klikni�ciu na strza�k�
skierowan� w d� (znajduje si� ona z prawej strony edytora)
lub naci�ni�ciu kombinacji klawiszy Alt+Strza�ka w d�.
}
unit UCBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    GroupBox1: TGroupBox;
    ComboBox1: TComboBox;
    sbDodaj: TSpeedButton;
    sbUsun: TSpeedButton;
    Label1: TLabel;
    function ComboBoxVerify: Boolean; // Deklaracja funkcji sprawdzaj�cej
    procedure FormCreate(Sender: TObject);
    procedure sbDodajClick(Sender: TObject);
    procedure ComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbUsunClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function TForm1.ComboBoxVerify: Boolean;
var
  TT :Integer; // Deklaracja zmiennej
begin
  {
   Funkcja sprawdza, czy wpisany napis w edytorze znajduje si� w li�cie.
   Je�eli wpisany napis jest w li�cie to funkcja zwr�ci warto�� TRUE (Prawda),
   w innym przypadku funkcja zwr�ci FALSE (Fa�sz).
  }
  ComboBoxVerify:= FALSE;
  for TT:= 0 to ComboBox1.Items.Count-1 do
    if (AnsiUpperCase(Trim(ComboBox1.Text)) =
        AnsiUpperCase(Trim(ComboBox1.Items[TT]))) then ComboBoxVerify:= TRUE;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

  ComboBox1.Items.Clear; // Czyszczenie zawarto�ci listy.
  ComboBox1.Text:= ''; // Wyczyszczenie zawarto�ci edytora.

  // Wczytanie listy z pliku.
  if (FileExists('Lista.txt') = TRUE) then // Sprawdzenie czy istnieje plik na dysku.
    ComboBox1.Items.LoadFromFile('Lista.txt'); // Odczytanie listy z pliku.

end;

procedure TForm1.sbDodajClick(Sender: TObject);
begin
  // Dodaj do listy
  if (ComboBoxVerify = FALSE) and (Trim(ComboBox1.Text)<>'') then
  begin
    {
     Dodanie do listy nast�pi w momencie, gdy edytor listy nie
     b�dzie pusty i napisany ci�g znak�w nie b�dzie wyst�powa� w li�cie.
     Jest to warunek, kt�ry jest sprawdzany za pomoc� instrukcji If...Then.
    }
    ComboBox1.Items.Add(ComboBox1.Text); // Dodanie nowego ci�gu znak�w do listy.
    ComboBox1.Sorted:= TRUE; // W��czenie sortowania listy.
    ComboBox1.Items.SaveToFile('Lista.txt'); // Zapisanie listy do pliku.
  end;
  ComboBox1.SelectAll; // Zaznaczenie ci�gu znak�w w edytorze.
  ComboBox1.SetFocus; // Ustawienie listy z edytorem jako aktywny.
end;

procedure TForm1.ComboBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // Wprowadzenie nowego ci�gu znak�w do listy przez naci�ni�cie klawisza ENTER.
  if (Key = VK_RETURN) then
  begin
    sbDodajClick(Sender); // Wywo�anie funkcji umo�liwiaj�cej dodanie nowego ci�gu znak�w.
  end;
end;

procedure TForm1.sbUsunClick(Sender: TObject);
var
  numBtn: Integer;
begin
  // Usuni�cie wybranego elementu z listy.

  // Application.MessageBox - wywo�anie okna dialogowego.
  numBtn:= Application.MessageBox('Czy usun�� ten element z listy ?',
              'Pytanie', MB_ICONQUESTION or MB_YESNO);
  if (numBtn = IDYES) then
  begin
    ComboBox1.Items.Delete(ComboBox1.ItemIndex); // Usuni�cie zaznaczonego elementu z listy.
    ComboBox1.Items.SaveToFile('Lista.txt'); // Zapisanie listy do pliku.
    ComboBox1.Text:= ''; // Wyczyszczenie zawarto�ci edytora.
    ComboBox1.SetFocus; // Ustawienie listy z edytorem jako aktywny.
  end;
end;

end.

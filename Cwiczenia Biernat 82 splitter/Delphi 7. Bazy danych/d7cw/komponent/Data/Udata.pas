{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu DateTimePicker.


Wprowadzenie:
Komponent DateTimePicker s�u�y do wybierania daty
za pomoc� rozwijanego kalendarza i czasu.
}
unit Udata;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, Mask;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    Label2: TLabel;
    Label3: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Edit3: TEdit;
    Label6: TLabel;
    MaskEdit1: TMaskEdit;
    Edit4: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function DodajZeroPrzed(txtZero: String): String;
begin
{
Funkcja dodaje zero do pojedynczej liczby traktowanej
jako tekst, w innym przypadku zwraca dwie liczby.

Przyk�ad:
  1) 10 - zwr�ci nam liczb� dziesi�� traktowan� jako tekst
  2)  3 - zwr�ci nam liczb� 03 traktowan� jako tekst, przez
          dodanie zera przed liczb� trzy
}
  DodajZeroPrzed:= txtZero;
  if (Length(txtZero) =1) then DodajZeroPrzed:= '0'+txtZero;
end;

function numIloscDniWMiesiacu(txtDate :String): Shortint;
// Funkcja zwraca liczb� dni w danym miesi�cu
var
  numMonth, numMonthToDays: Shortint;
                   numYear: Integer;
begin
  // numIloscDniWMiesiacu

  // Wyci�cie 4-ro liczbowego roku, np. 2001
  numYear:= 0;
  numYear:= StrToInt(Copy(txtDate, 1, 4));

  // Wyci�cie 2-wu liczbowego miesi�ca, np. 09
  numMonth:= 0;
  numMonth:= StrToInt(Copy(txtDate, 6, 2));

  // Zwraca wiadom� liczb� dni w wybranych miesi�cach
  numMonthToDays:= 0;
  if (numMonth in [1, 3, 5, 7, 8, 10, 12]) then numMonthToDays:= 31;
  if (numMonth in [4, 6, 9, 11]) then numMonthToDays:= 30;
  if (numMonth = 2) then
  begin

    {
      Sprawdzenie czy podany rok jest rokiem
      przest�pnym (dotyczy tylko miesi�ca LUTY)
    }
    numMonthToDays:= 28;
    if ((numYear mod 4 = 0) and (numYear mod 100 <>0)) or
       (numYear mod 400 = 0) then
    begin
      numMonthToDays:= 0;
      numMonthToDays:= 29;
    end;

  end;
  numIloscDniWMiesiacu:= numMonthToDays;
end;

function txtJutroJest(txtDate: String): String;
// Funkcja zwraca dat� jutrzejsz�
var
  numMonth, numDay, numDayInMonth: Shortint;
                          numYear: Integer;
begin
  // txtJutroJest

  // Wyci�cie 4-ro liczbowego roku, np. 2001
  numYear:= 0;
  numYear:= StrToInt(Copy(txtDate, 1, 4));

  // Wyci�cie 2-wu liczbowego miesi�ca, np. 09
  numMonth:= 0;
  numMonth:= StrToInt(Copy(txtDate, 6, 2));

  // Wyci�cie 2-wu liczbowego dnia, np. 15
  numDay:= 0;
  numDay:= StrToInt(Copy(txtDate, 9, 2));

  {
    Przypisanie zmiennej 'numDayInMonth' ilo�ci
    dni w danym miesi�cu
  }
  numDayInMonth:= 0;
  numDayInMonth:= numIloscDniWMiesiacu(txtDate);

  // Jutro jest...
  txtJutroJest:= Copy(txtDate, 1, 5)+DodajZeroPrzed(IntToStr(numMonth))+
             '-'+DodajZeroPrzed(IntToStr(numDay+1));
  if (numDay+1 > numDayInMonth) then
  begin
    {
      Je�eli ilo�� dni jest wi�ksza ni� ilo�� dni w
      danym miesi�cu to spe�niony jest warunek, co
      spowoduje przesuni�cie o jeden miesi�c w
      prz�d i dodanie 1-go dnia nast�pnego miesi�ca.
    }
    txtJutroJest:= Copy(txtDate, 1, 5)+
                 DodajZeroPrzed(IntToStr(numMonth+1))+'-01';

    // Je�eli miesi�c jest 12 to przesu� w prz�d o rok
    if (numMonth = 12) then txtJutroJest:= IntToStr(numYear+1)+'-01-01';
  end;
end;

function txtKiedysBedzie(txtDate: String; numDayPlus: Integer): String;
// Funkcja zwraca dat� dnia za X dni
var
  txtLoopDate: String;
           TT: Integer;
begin
  // txtKiedysBedzie
  for TT:= 0 to numDayPlus-1 do
  begin
    txtLoopDate:= '';
    txtLoopDate:= txtJutroJest(txtDate); // Wywo�anie funkcji txtJutroJest
    txtDate:= '';
    txtDate:= txtLoopDate;
  end;
  txtKiedysBedzie:= txtLoopDate;
end;

function txtWczorajBylo(txtDate: String): String;
// Funkcja zwraca dat� wczorajsz�
var
  numMonth, numDay, numDayInMonth: Shortint;
                          numYear: Integer;
begin
  // txtWczorajBylo

  // Wyci�cie 4-ro liczbowego roku, np. 2001
  numYear:= 0;
  numYear:= StrToInt(Copy(txtDate, 1, 4));

  // Wyci�cie 2-wu liczbowego miesi�ca, np. 09
  numMonth:= 0;
  numMonth:= StrToInt(Copy(txtDate, 6, 2));

  // Wyci�cie 2-wu liczbowego dnia, np. 15
  numDay:= 0;
  numDay:= StrToInt(Copy(txtDate, 9, 2));

  {
    Przypisanie zmiennej 'numDayInMonth' ilo�ci
    dni poprzedniego miesi�ca
  }
  numDayInMonth:= 0;
  numDayInMonth:= numIloscDniWMiesiacu(Copy(txtDate, 1, 5)+
                                 DodajZeroPrzed(IntToStr(numMonth-1))+
                                 Copy(txtDate, 8, 3));

  // Wczoraj by�o...
  txtWczorajBylo:= Copy(txtDate, 1, 5)+DodajZeroPrzed(IntToStr(numMonth))+
             '-'+DodajZeroPrzed(IntToStr(numDay-1));
  if (numDay = 1) then
  begin
    {
      Je�eli jest 1-szy dzie� to spe�niony jest warunek, co
      spowoduje cofni�cie o jeden miesi�c w ty� i dodanie
      ilo�ci dni z poprzedniego miesi�ca.
    }
    txtWczorajBylo:= Copy(txtDate, 1, 5)+
               DodajZeroPrzed(IntToStr(numMonth-1))+'-'+
               DodajZeroPrzed(IntToStr(numDayInMonth));

    // Je�eli miesi�c jest 1 to przesu� w ty� o rok
    if (numMonth = 1) then txtWczorajBylo:= IntToStr(numYear-1)+'-12-31';
  end;
end;

function txtKiedysBylo(txtDate: String; numDayMinus: Integer): String;
// Funkcja zwraca dat� dnia z przed X dni
var
  txtLoopDate: String;
           TT: Integer;
begin
  // txtKiedysBylo
  for TT:= 0 to numDayMinus-1 do
  begin
    txtLoopDate:= '';
    txtLoopDate:= txtWczorajBylo(txtDate); // Wywo�anie funkcji txtWczorajBylo
    txtDate:= '';
    txtDate:= txtLoopDate;
  end;
  txtKiedysBylo:= txtLoopDate;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Inicjacja programu
  MaskEdit1.Text:= '4'; // Wpisanie warto�ci 1 do komponentu MaskEdit

   // Wywo�anie poszczeg�lnych funkcji
  Edit1.Text:= txtJutroJest(DateToStr(DateTimePicker1.Date));
  Edit2.Text:= txtWczorajBylo(DateToStr(DateTimePicker1.Date));
  Edit3.Text:= txtKiedysBedzie(DateToStr(DateTimePicker1.Date), StrToInt(Trim(MaskEdit1.Text)));
  Edit4.Text:= txtKiedysBylo(DateToStr(DateTimePicker1.Date), StrToInt(Trim(MaskEdit1.Text)));
end;

procedure TForm1.DateTimePicker1Change(Sender: TObject);
begin
   // Wywo�anie poszczeg�lnych funkcji w momencie zmiany
  Edit1.Text:= txtJutroJest(DateToStr(DateTimePicker1.Date));
  Edit2.Text:= txtWczorajBylo(DateToStr(DateTimePicker1.Date));
  Edit3.Text:= txtKiedysBedzie(DateToStr(DateTimePicker1.Date), StrToInt(Trim(MaskEdit1.Text)));
  Edit4.Text:= txtKiedysBylo(DateToStr(DateTimePicker1.Date), StrToInt(Trim(MaskEdit1.Text)));
end;

end.

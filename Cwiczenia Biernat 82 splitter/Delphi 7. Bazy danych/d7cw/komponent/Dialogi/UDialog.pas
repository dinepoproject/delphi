{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania okien dialogowych, kt�re s� dost�pne w zak�adce "Dialogs".


Wprowadzenie:

Okna dialogowe to takie okna, kt�re np. umo�liwiaj� nam odczytanie lub 
zapisanie pliku tekstowego, graficznego itp..
Okna dialogowe umo�liwiaj� nam zmian� koloru, ustawie� wydruku itp..
}
unit UDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ExtDlgs;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    bOtworzPlik: TButton;
    OpenDialog1: TOpenDialog;
    Label1: TLabel;
    bZapiszPlik: TButton;
    SaveDialog1: TSaveDialog;
    bUstawieniaWydruku: TButton;
    bZapiszPlikGraficzny: TButton;
    bOtworzPlikGraficzny: TButton;
    bUstawKolorFormatki: TButton;
    OpenPictureDialog1: TOpenPictureDialog;
    SavePictureDialog1: TSavePictureDialog;
    ColorDialog1: TColorDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    procedure bOtworzPlikClick(Sender: TObject);
    procedure bZapiszPlikClick(Sender: TObject);
    procedure bOtworzPlikGraficznyClick(Sender: TObject);
    procedure bZapiszPlikGraficznyClick(Sender: TObject);
    procedure bUstawKolorFormatkiClick(Sender: TObject);
    procedure bUstawieniaWydrukuClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.bOtworzPlikClick(Sender: TObject);
begin
  // Otwiera okno dialogowe, kt�re umo�liwia nam wczytanie pliku
  {
   Nazwa "OpenDialog1" jest nazw� komponentu, kt�r� mo�na
   zmieni� we w�a�ciwo�ciach okna "Object Inspector".
  }
  OpenDialog1.FileName:= '';
  OpenDialog1.Execute;
  if (Trim(OpenDialog1.FileName)<>'') then
  begin
    // TODO (W tym miejscu wpisujemy w�asny kod programu)
    Label1.Caption:= Trim(OpenDialog1.FileName);
    {
     Funkcja Trim(OpenDialog1.FileName) - likwiduje spacje po prawej
                                          jak i po lewej stronie napisu
                                          lub zmiennej
    }
  end;
end;

procedure TForm1.bZapiszPlikClick(Sender: TObject);
begin
  // Zapisuje plik
  SaveDialog1.FileName:= '';
  SaveDialog1.Execute;
  if (Trim(SaveDialog1.FileName)<>'') then
  begin
    // TODO (W tym miejscu wpisujemy w�asny kod programu)
    Label1.Caption:= Trim(SaveDialog1.FileName);
  end;
end;

procedure TForm1.bOtworzPlikGraficznyClick(Sender: TObject);
begin
  // Otwiera okienko do wybrania pliku graficznego
  OpenPictureDialog1.FileName:='';
  OpenPictureDialog1.Execute;
  if (Trim(OpenPictureDialog1.FileName)<>'') then
  begin
    // TODO (W tym miejscu wpisujemy w�asny kod programu)
    Label1.Caption:= Trim(OpenPictureDialog1.FileName);
  end;
end;

procedure TForm1.bZapiszPlikGraficznyClick(Sender: TObject);
begin
  // Zapisz plik graficzny
  SavePictureDialog1.FileName:='';
  SavePictureDialog1.Execute;
  if (Trim(SavePictureDialog1.FileName)<>'') then
  begin
    // TODO (W tym miejscu wpisujemy w�asny kod programu)
    Label1.Caption:= Trim(SavePictureDialog1.FileName);
  end;
end;

procedure TForm1.bUstawKolorFormatkiClick(Sender: TObject);
begin
  // Ustawia kolor formatki
  if ColorDialog1.Execute then
  begin
    Form1.Color:= ColorDialog1.Color; // Ustawia kolor formatki
    Label1.Caption:= IntToStr(ColorDialog1.Color); // Podaje warto�� koloru
  end;
end;

procedure TForm1.bUstawieniaWydrukuClick(Sender: TObject);
begin
  // Otwiera okienko z ustawienia wydruku
  PrinterSetupDialog1.Execute;
end;

end.

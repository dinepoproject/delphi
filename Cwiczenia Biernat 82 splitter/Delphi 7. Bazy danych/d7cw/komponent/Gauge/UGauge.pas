{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu Gauge.


Wprowadzenie:

Komponent Gauge s�u�y do pokazywania post�pu
wykonywanej pracy. Pasek post�pu mo�e by�
przedstawiony w poziomie, pionie
lub w postaci ko�a.
}
unit UGauge;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Gauges, StdCtrls;

type
  TForm1 = class(TForm)
    Gauge1: TGauge;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  {
   Tu s� wpisywane instrukcje, kt�re
   s� wykonane w momencie tworzenia formatki.
  }



  //-- Inicjalizacja komponentu Gauge --

  // Okre�lenie koloru t�a
  Gauge1.BackColor:= clWhite;

  // Okre�lenie koloru paska
  Gauge1.ForeColor:= clNavy;

  // Okre�lenie minimalnej warto�ci
  Gauge1.MinValue:= 0;

  // Okre�lenie maksymalnej warto�ci
  Gauge1.MaxValue:= 100;

  // Okre�lenie ilo�ci wykonanego zadania
  Gauge1.Progress:= 0;

  // Wywo�anie pierwszej opcji
  RadioButton1Click(Sender);
end;

procedure TForm1.RadioButton1Click(Sender: TObject);
begin
  // Ustawienie wykresu w pozycji poziomej
  Gauge1.Kind:= gkHorizontalBar;
  Gauge1.Width:= 144; // Szeroko��
  Gauge1.Height:= 22; // Wysoko��

  // Okre�lenie ilo�ci wykonanego zadania
  Gauge1.Progress:= 0;

  // Uaktywnienie opcji
  RadioButton1.Checked:= TRUE;
end;

procedure TForm1.RadioButton2Click(Sender: TObject);
begin
  // Ustawienie wykresu w pozycji pionowej
  Gauge1.Kind:= gkVerticalBar;
  Gauge1.Width:= 22; // Szeroko��
  Gauge1.Height:= 122; // Wysoko��

  // Okre�lenie ilo�ci wykonanego zadania
  Gauge1.Progress:= 0;

  // Uaktywnienie opcji
  RadioButton2.Checked:= TRUE;
end;

procedure TForm1.RadioButton3Click(Sender: TObject);
begin
  // Ustawienie wykresu w postaci ko�a
  Gauge1.Kind:= gkPie;
  Gauge1.Width:= 122; // Szeroko��
  Gauge1.Height:= 122; // Wysoko��

  // Okre�lenie ilo�ci wykonanego zadania
  Gauge1.Progress:= 0;

  // Uaktywnienie opcji
  RadioButton3.Checked:= TRUE;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  TT: Integer; // Zadeklarowanie zmiennej "TT"
begin
  // Poka� post�p wykonanej procy p�tli FOR
  for TT:= 0 to 99 do
    Gauge1.Progress:= 1+TT;
  // P�tla FOR zostanie wykonana 1000 razy
end;

end.

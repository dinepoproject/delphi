{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad obs�ugi kilku komponent�w.

Wprowadzenie:

Ten spos�b jest przydany, gdy chcemy np. zmieni� kolor czcionek,
tak aby wszystkie czcionki by�y tego samego koloru.
R�wnie� obs�uga kilku przycisk�w za pomoc� jednej funkcji
jest przydatna ze wzgl�du na zmniejszenie wielko�ci kodu programu.
}
unit UKilKom;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    Edit2: TEdit;
    bEdit: TButton;
    Edit4: TEdit;
    Edit3: TEdit;
    GroupBox2: TGroupBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton7: TSpeedButton;
    GroupBox3: TGroupBox;
    Button1: TButton;
    Button2: TButton;
    procedure bEditClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.bEditClick(Sender: TObject);
var
  TT: Byte; // Deklaracja zmiennej
  TC: TEdit; // Utworzenie obiektu TC
begin
  // Wyczy�� wszystkie komponenty Edit i wpisz s�owo "Bond"
  for TT:= 1 to 4 do // Okre�lenie ilo�ci komponent�w na formatce.
  begin
    // Wyszukuje komponenty o podanej nazwie.
    TC:= TEdit(FindComponent('Edit'+IntToStr(TT))); // Pozwala na zmian� w�a�ciwo�ci kilku takim samym komponentom, np. komponent EDIT.
    TC.Font.Color:= clBlue; // Zmiana koloru na niebieski we wszystkich komponentach Edit.
    TC.Text:= 'Bond'; // Wpisanie s�owa "Bond" we wszystkich komponentach Edit.
  end;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
  // Obs�uga kilku przycisk�w za pomoc� jednej funkcji.

  {
   Sender - jest referencj� do obiektu, kt�rego dotyczy dane zdarzenie
           (np. klikni�cie myszk�)
   (Sender as TSpeedButton).Caption - konstrukcja ta umo�liwia
      przekazanie w�asno�ci, kt�re s� wsp�lne dla wszystkich
      komponent�w SpeedButton. W tym przyk�adzie chodzi o
      przekazanie napis�w na przyciskach.
  }
  ShowMessage('Wybra�e�:'+CHR(32)+(Sender as TSpeedButton).Caption);
  if ((Sender as TSpeedButton).Caption = '1') then ShowMessage('Jeden');
  if ((Sender as TSpeedButton).Caption = '2') then ShowMessage('Dwa');
  if ((Sender as TSpeedButton).Caption = '3') then ShowMessage('Trzy');
  if ((Sender as TSpeedButton).Caption = '4') then ShowMessage('Cztery');
  if ((Sender as TSpeedButton).Caption = '5') then ShowMessage('Pi��');
  if ((Sender as TSpeedButton).Caption = '6') then ShowMessage('Sze��');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Obs�uga kilku przycisk�w za pomoc� jednej funkcji.
  if ((Sender as TButton).Tag = 1) then ShowMessage((Sender as TButton).Caption);
  if ((Sender as TButton).Tag = 2) then ShowMessage((Sender as TButton).Caption);
  {
   Tag - jest numer kolejnego komponentu, kt�ry ustawia si� we w�a�ciwo�ciach.
         Za pomoc� tego numeru mo�na �atwo zidentyfikowa� komponent.
  }
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu LABEL do wywo�ania
strony WWW i poczty elektronicznej.
}
unit ULabel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ShellApi;
// ShellApi - Funkcje i procedury biblioteki API

type
  TfrmForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmForm1: TfrmForm1;

implementation

{$R *.DFM}

procedure TfrmForm1.FormCreate(Sender: TObject);
begin
  // Ustawienie w�asno�ci komponentu LABEL1 i  LABEL2

  //-- Strona WWW --
  Label1.Cursor:= crHandPoint; // Zamiana kursora myszki na r�czk� w momencie najechania mysz� na komponent LABEL
  Label1.Font.Color:= clBlue; // Ustawienie koloru czcionek na kolor niebieski.
  Label1.Font.Style:= [fsUnderline]; // Ustawienie podkre�lenia pod napisem.
  Label1.Caption:= 'http://www.pan.pl'; // Wpisanie adresu strony WWW.

  //-- Poczta --
  Label2.Cursor:= crHandPoint; // Zamiana kursora myszki na r�czk� w momencie najechania mysz� na komponent LABEL
  Label2.Font.Color:= clBlue; // Ustawienie koloru czcionek na kolor niebieski.
  Label2.Font.Style:= [fsUnderline]; // Ustawienie podkre�lenia pod napisem.
  Label2.Caption:= 'uzytkownik@serwer.domena'; // Wpisanie adresu poczty.
end;

procedure TfrmForm1.Label1Click(Sender: TObject);
begin
  // Uruchomienie przegl�darki WWW.
  ShellExecute(GetDesktopWindow, 'open', PChar(Label1.Caption),
               nil, nil, SW_SHOWNORMAL);
end;

procedure TfrmForm1.Label2Click(Sender: TObject);
begin
  // Uruchomienie programu do obs�ugi poczty elektronicznej.
  ShellExecute(GetDesktopWindow, 'open', PChar('mailto:'+Label2.Caption),
               nil, nil, SW_SHOWNORMAL);
end;

end.

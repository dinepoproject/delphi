{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad u�ycia komponentu MediaPlayer.

Wprowadzenie:

Komponent ten s�u�y do odtwarzania muzyki i
film�w video (wav, mid, mp3, avi, itd.).
}
unit UMedia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, MPlayer, StdCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    MediaPlayer1: TMediaPlayer;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Otw�rz plik muzyczny
  OpenDialog1.FileName:= '';
  OpenDialog1.Execute;
  if (Trim(OpenDialog1.FileName)<>'') then
  begin
    // Procedura otwierania i odtwarzania muzyki za pomoc� komponentu MediaPlayer
    MediaPlayer1.Close;
    MediaPlayer1.FileName:= Trim(OpenDialog1.FileName);
    MediaPlayer1.Open;
    MediaPlayer1.Play;
  end;
end;

end.

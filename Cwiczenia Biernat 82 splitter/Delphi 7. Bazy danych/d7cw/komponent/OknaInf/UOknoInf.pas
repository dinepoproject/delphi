{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad zastosowania okienek informacyjnych. 
}
unit UOknoInf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    bError: TButton;
    bInformation: TButton;
    bWarning: TButton;
    bInfo: TButton;
    procedure bErrorClick(Sender: TObject);
    procedure bInformationClick(Sender: TObject);
    procedure bWarningClick(Sender: TObject);
    procedure bInfoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.bErrorClick(Sender: TObject);
begin
  // Okienko informuj�ce o b��dzie
  Application.MessageBox('Okienko informuj�ce o b��dzie',
                'B��d', MB_ICONERROR or MB_OK);
  {
  Application.MessageBox(Informacja,
                Tytu� Okna, Rodzaj_Rysunku or Jaki_Klawisz);
  }
end;

procedure TForm1.bInformationClick(Sender: TObject);
begin
  // Okienko informuj�ce o np. sko�czeniu kopiowania
  Application.MessageBox('Okienko informuj�ce np. o sko�czeniu kopiowania',
                 'Informacja', MB_ICONINFORMATION or MB_OK);
end;

procedure TForm1.bWarningClick(Sender: TObject);
begin
  // Okienko ostrzegawcze o np. mo�liwo�ci wyst�pienia jakiego� b��du
  Application.MessageBox('Pokazuje okienko ostrzegawcze',
                 'Ostrze�enie', MB_ICONWARNING or MB_OK);
end;

procedure TForm1.bInfoClick(Sender: TObject);
begin
  // Poka� okienko informacyjne
  ShowMessage('Okienko ostrzegawcze !!');
end;

end.

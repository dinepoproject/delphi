{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu PageControl.


Wprowadzenie:
PageControl jest komponentem umo�liwiaj�cym
tworzenie kart. Na ka�dej karcie mog�
znajdowa� si� zwi�zane tematycznie komponenty.
Przechodzenie mi�dzy zak�adkami jest
mo�liwe za pomoc� kombinacji klawiszy:
 * CTRL+TAB - do przodu;
 * CTRL+SHIFT+TAB do ty�u.
}
unit UPContr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Button1: TButton;
    Memo1: TMemo;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    StringGrid1: TStringGrid;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Poka� stron� pierwsz�
  PageControl1.ActivePage:= TabSheet1;
  {
   TabSheet1 - Nazwa strony pierwszej
  }
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  // Pokazuje/Ukrywa zak�adk� (stron�) nr 2
  if (TabSheet2.TabVisible = TRUE) then
    TabSheet2.TabVisible:= FALSE
  else
    TabSheet2.TabVisible:= TRUE;
  {
   TabSheet2 - Nazwa strony drugiej
  }
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu PaintBox.


Wprowadzenie:
Komponent PaintBox pozwala na rysowanie grafiki
ograniczonej w prostok�tnym obszarze, dzi�ki
temu programista nie musi kontrolowa�
przekroczenia obszaru.
}
unit Upbox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    PaintBox1: TPaintBox;
    procedure PaintBox1Paint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.PaintBox1Paint(Sender: TObject);
begin
  // PaintBox1Paint - Grafika jest od�wie�ana w przypadku zas�oni�cia
  
  {
   Rysuj grafik� na ograniczonym obszarze za
   pomoc� komponentu PainBox
  }

  // Wyczyszczenie obszaru rysowania.
  with PaintBox1.Canvas do
  begin
    // Ustalenie stylu wype�niania
    Brush.Style:= bsSolid;

    // Ustalenie koloru rysowania
    Brush.Color:= clWhite;

    // Wype�nienie aktywnego obszaru
    FillRect(ClipRect);
    {
     ClipRect - Reprezentuje bie��cy prostok�t na
     kt�rym s� dokonywane operacje graficzne.
    }
  end;

  
  with PaintBox1.Canvas do
  begin
    //-- Ko�o --
    // Ustalenie koloru jakim b�dzie narysowana figura.
    Pen.Color:= clBlack;

    // Ustalenie koloru jakim b�dzie wype�niona figura.
    Brush.Color:= clRed;

    // Narysowanie ko�a
    Ellipse(9, 9, 34, 34);


    //-- Kwadrat --
    // Ustalenie koloru jakim b�dzie narysowana figura.
    Pen.Color:= clBlack;

    // Ustalenie stylu pi�ra
    Pen.Style:= psDot;

    // Ustalenie koloru jakim b�dzie wype�niona figura.
    Brush.Color:= clRed;

    // Ustalenie stylu wype�niania
    Brush.Style:= bsDiagCross;

    // Narysowanie kwadratu
    Rectangle(39, 9, 66, 36);


    //-- Linia --
    MoveTo(66, 66); // Okre�lenie pocz�tku linii
    LineTo(366, 66); // Narysowanie linii


    //-- Ko�o pochylone --
    // Ustalenie koloru jakim b�dzie narysowana figura.
    Pen.Color:= clBlack;

    // Ustalenie koloru jakim b�dzie wype�niona figura.
    Brush.Color:= clRed;

    // Ustalenie stylu wype�niania
    Brush.Style:= bsCross;

    // Narysowanie kwadratu
    Chord(199, 9, 77, 46, 8, 8, 8, 8);


    //-- Tekst --
    // Ustalenie czcionki tekstu
    Font.Name:= 'Arial';

    // Ustalenie koloru tekstu
    Font.Color:= clGreen;

    // Ustalenie stylu tekstu
    Font.Style:= [fsBold, fsItalic, fsUnderline];

    // Ustalenie wielko�ci tekstu
    Font.Size:= 33;

    // Wypisanie tekstu na ekranie
    TextOut(23, 88, 'To jest tekst.');
  end;
end;

end.

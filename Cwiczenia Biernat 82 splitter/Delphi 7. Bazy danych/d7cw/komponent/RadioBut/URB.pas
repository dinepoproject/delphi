{
 -- Podstawowy Kurs Delphi --
 Copyright(c) Jan Biernat

Przyk�ad u�ycia komponentu RadioButton i CheckBox.

Wprowadzenie:

RadioButton:
Komponent RadioButton s�u�y do wyboru jednej
funkcji z kilku dost�pnych, kt�re grupujemy.
Do grupowania komponent�w RadioButton s�uzy
komponent GroupBox. Oznacza to, �e zawsze
b�dzie wybrana jedna funkcja.

CheckBox:
Komponent CheckBox s�u�y do ustawiania opcji niezale�nie
od innych ustawie�. Za pomoc� tego komponentu mo�emy
w��czy� lub wy��czy� dzia�anie jakie� funkcji w programie.
}
unit URB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    CheckBox1: TCheckBox;
    Bevel1: TBevel;
    RadioButton3: TRadioButton;
    RadioButton7: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
end;

procedure TForm1.RadioButton1Click(Sender: TObject);
begin
  // Daje kolor czerwony
  Form1.Color:= clRed;
end;

procedure TForm1.RadioButton2Click(Sender: TObject);
begin
  // Daje kolor zielony
  Form1.Color:= clGreen;
end;

procedure TForm1.RadioButton3Click(Sender: TObject);
begin
  // Daje kolor standardowy
  Form1.Color:= clBtnFace;
end;

procedure TForm1.RadioButton4Click(Sender: TObject);
begin
  // Obs�uga RadioButton'�w
  if (RadioButton4.Checked = TRUE) then Caption:= 'Delphi'; // Wy�wietla napis "Delphi"
  if (RadioButton5.Checked = TRUE) then Caption:= 'Atari'; // Wy�wietla napis "Atari"
  if (RadioButton6.Checked = TRUE) then Caption:= 'Komputer'; // Wy�wietla napis "Komputer"
  if (RadioButton7.Checked = TRUE) then Caption:= 'Przyk�ad u�ycia komponentu RadioButton i CheckBox'; // Wy�wietla napis "Przyk�ad u�ycia komponentu RadioButton i CheckBox"
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
  if (CheckBox1.Checked = FALSE) then
  begin
    Form1.WindowState:= wsNormal; // Przywraca poprzedni stan okna.
  end
  else
  begin
    Form1.WindowState:= wsMaximized; // Daje okno na ca�y ekran.
  end;
end;

end.

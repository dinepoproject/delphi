{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk豉d zastosowania komponentu Shape.


Wprowadzenie:
Komponent Shape s逝篡 do rysowania prostych
figur geometrycznych.
}
unit Ushape;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Shape1: TShape;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    RadioButton9: TRadioButton;
    RadioButton10: TRadioButton;
    RadioButton11: TRadioButton;
    RadioButton12: TRadioButton;
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure RadioButton7Click(Sender: TObject);
    procedure RadioButton8Click(Sender: TObject);
    procedure RadioButton9Click(Sender: TObject);
    procedure RadioButton10Click(Sender: TObject);
    procedure RadioButton11Click(Sender: TObject);
    procedure RadioButton12Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.RadioButton1Click(Sender: TObject);
begin
  // Kszta速 - Ko這
  Shape1.Shape:= stCircle;
end;

procedure TForm1.RadioButton2Click(Sender: TObject);
begin
  // Kszta速 - Elipsa
  Shape1.Shape:= stEllipse;
end;

procedure TForm1.RadioButton3Click(Sender: TObject);
begin
  // Kszta速 - Prostok靖
  Shape1.Shape:= stRectangle;
end;

procedure TForm1.RadioButton4Click(Sender: TObject);
begin
  // Kszta速 - Prostok靖 z zaokr鉚lonymi rogami
  Shape1.Shape:= stRoundRect;
end;

procedure TForm1.RadioButton5Click(Sender: TObject);
begin
  // Kszta速 - Kwadrat z zaokr鉚lonymi rogami
  Shape1.Shape:= stRoundSquare;
end;

procedure TForm1.RadioButton6Click(Sender: TObject);
begin
  // Kszta速 - Kwadrat
  Shape1.Shape:= stSquare;
end;

procedure TForm1.RadioButton7Click(Sender: TObject);
begin
  // Kolor konturu - Czarny
  Shape1.Pen.Color:= clBlack;
end;

procedure TForm1.RadioButton8Click(Sender: TObject);
begin
  // Kolor konturu - Zielony
  Shape1.Pen.Color:= clGreen;
end;

procedure TForm1.RadioButton9Click(Sender: TObject);
begin
  // Kolor konturu - Niebieski
  Shape1.Pen.Color:= clBlue;
end;

procedure TForm1.RadioButton10Click(Sender: TObject);
begin
  // Kolor t豉 - Bia造
  Shape1.Brush.Color:= clWhite;
end;

procedure TForm1.RadioButton11Click(Sender: TObject);
begin
  // Kolor t豉 - 草速y
  Shape1.Brush.Color:= clYellow;
end;

procedure TForm1.RadioButton12Click(Sender: TObject);
begin
  // Kolor t豉 - Czerwony
  Shape1.Brush.Color:= clRed;
end;

end.

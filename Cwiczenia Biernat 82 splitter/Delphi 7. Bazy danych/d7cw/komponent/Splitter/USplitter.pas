{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad u�ycia komponentu Splitter.

Wprowadzenie:

Komponent ten umo�liwia zmian� obszaru prostok�tnego
podczas wykonywania programu. Przyk�adem wykorzystania
takiego komponentu jest np. Eksplorator Windows, kt�ry
jest podzielony na dwa panele. Granica tych dw�ch
paneli jest regulowana.
}
unit USplitter;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    Splitter1: TSplitter;
    Memo1: TMemo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

end.

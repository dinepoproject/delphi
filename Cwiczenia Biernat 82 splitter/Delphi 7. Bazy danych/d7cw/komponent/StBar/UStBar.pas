{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad u�ycia komponentu StatusBar i ProgressBar


Wprowadzenie:

Komponent StatusBar s�u�y do wy�wietlania
kr�tkiej informacji w dolnej cz�ci okna,
natomiast komponent ProgressBar s�u�y do
wskazywania post�pu wykonywanego zadania.
}
unit UStBar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    StatusBar1: TStatusBar;
    bPierwszaInformacja: TButton;
    bDrugaInformacja: TButton;
    ProgressBar1: TProgressBar;
    procedure bPierwszaInformacjaClick(Sender: TObject);
    procedure bDrugaInformacjaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.bPierwszaInformacjaClick(Sender: TObject);
begin
  // Pierwsza informacja
  StatusBar1.SimpleText:= 'To jest pierwsza informacja'; // Wy�wietla informacje
end;

procedure TForm1.bDrugaInformacjaClick(Sender: TObject);
var
  TT: Integer;
begin
  // Druga informacja
  StatusBar1.SimpleText:= 'Teraz widzisz komponent "ProgressBar" w dzia�aniu...'; // Wy�wietla informacje

  // ProgressBar
  ProgressBar1.Position:= 0; // Ustawienie pocz�tkowej warto�ci
  ProgressBar1.Max:= 9999; // Okre�lenie maksymalnej dopuszczalnej warto�ci
  for TT:= 0 to 9999 do
    ProgressBar1.Position:= TT; // Wskazanie post�pu pracy p�tli For
  StatusBar1.SimpleText:= 'ProgressBar jest bardzo przydatnym komponentem !!!!'; // Wy�wietla informacje
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad u�ycia komponentu StringGrid.

Wprowadzenie:

Komponent StringGrid to arkusz p�l edycyjnych za pomoc�
kt�rej mo�emy zrobi� ma�y program kalkulacyjny.
}
unit UStrGrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    Bevel1: TBevel;
    Panel1: TPanel;
    Button1: TButton;
    Button4: TButton;
    Button3: TButton;
    Button6: TButton;
    Button2: TButton;
    Button5: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Button7: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  Button7Click(Sender);
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  // Wype�nij ca�� tabel�

  // Tytu�y kolumn
  StringGrid1.Cells[0, 0]:= 'Nazwisko';
  StringGrid1.Cells[1, 0]:= 'Imi�';
  StringGrid1.Cells[2, 0]:= 'Miasto';
  StringGrid1.Cells[3, 0]:= 'Adres';
  StringGrid1.Cells[4, 0]:= 'Telefon';

  // Dane
  StringGrid1.Cells[0, 1]:= 'Biernat';
  StringGrid1.Cells[1, 1]:= 'Jan';
  StringGrid1.Cells[2, 1]:= 'Szczecin';
  StringGrid1.Cells[3, 1]:= 'Pomorska 44';
  StringGrid1.Cells[4, 1]:= '(045)822-54-87';

  StringGrid1.Cells[0, 2]:= 'Biedronka';
  StringGrid1.Cells[1, 2]:= 'Mirek';
  StringGrid1.Cells[2, 2]:= 'Bielsko-Bia�a';
  StringGrid1.Cells[3, 2]:= 'Zachlapana 41';
  StringGrid1.Cells[4, 2]:= 'Brak';

  StringGrid1.Cells[0, 3]:= 'Bodzio';
  StringGrid1.Cells[1, 3]:= 'Irek';
  StringGrid1.Cells[2, 3]:= 'Bielsko-Bia�a';
  StringGrid1.Cells[3, 3]:= 'Zakr�cona 65';
  StringGrid1.Cells[4, 3]:= '0-700-876-876';

  // Wy�wietlenie ilo�ci wierszy i kolumn
  Label1.Caption:= 'Ilo�� wierszy:'+CHR(32)+IntToStr(StringGrid1.RowCount-1);
  Label2.Caption:= 'Ilo�� kolumn:'+CHR(32)+IntToStr(StringGrid1.ColCount);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Dodanie nowego wiersza
  StringGrid1.RowCount:= StringGrid1.RowCount+1;

  // Wy�wietlenie ilo�ci wierszy i kolumn
  Label1.Caption:= 'Ilo�� wierszy:'+CHR(32)+IntToStr(StringGrid1.RowCount-1);
  Label2.Caption:= 'Ilo�� kolumn:'+CHR(32)+IntToStr(StringGrid1.ColCount);
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  AA, BB: Integer;
begin
  // Wstaw nowy wiersz

  // Przesuni�cie (przez przekopiowanie) wierszy o jeden wiersz w d�
  for AA:= StringGrid1.RowCount-1 downto StringGrid1.Row do
    for BB:= 0 to StringGrid1.ColCount-1 do
      StringGrid1.Cells[BB, 1+AA]:= Trim(StringGrid1.Cells[BB, AA]);

  // Wyczyszczenie ca�ego wiersza, przez co powstaje nowy pusty wiersz
  for AA:= 0 to StringGrid1.ColCount-1 do
    StringGrid1.Cells[AA, StringGrid1.Row]:= '';

  // Dodanie nowego wiersza
  StringGrid1.RowCount:= StringGrid1.RowCount+1;

  // Wy�wietlenie ilo�ci wierszy i kolumn
  Label1.Caption:= 'Ilo�� wierszy:'+CHR(32)+IntToStr(StringGrid1.RowCount-1);
  Label2.Caption:= 'Ilo�� kolumn:'+CHR(32)+IntToStr(StringGrid1.ColCount);
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  AA, BB: Integer;
begin
  // Usu� wiersz

  // Czy�ci ca�y wiersz
  for AA:= 0 to StringGrid1.ColCount-1 do
    StringGrid1.Cells[AA, StringGrid1.Row]:= '';

  // Przesuni�cie (przez przekopiowanie) wierszy o jeden wiersz wy�ej
  for AA:= 0 to StringGrid1.RowCount-1 do
    for BB:= 0 to StringGrid1.ColCount-1 do
    begin
      StringGrid1.Cells[BB, StringGrid1.Row+AA]:= Trim(
                  StringGrid1.Cells[BB, StringGrid1.Row+1+AA]);
    end;

  // Usuni�cie wiersza
  StringGrid1.RowCount:= StringGrid1.RowCount-1;

  // Wy�wietlenie ilo�ci wierszy i kolumn
  Label1.Caption:= 'Ilo�� wierszy:'+CHR(32)+IntToStr(StringGrid1.RowCount-1);
  Label2.Caption:= 'Ilo�� kolumn:'+CHR(32)+IntToStr(StringGrid1.ColCount);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  // Dodanie nowej kolumny
  StringGrid1.ColCount:= StringGrid1.ColCount+1;

  // Wy�wietlenie ilo�ci wierszy i kolumn
  Label1.Caption:= 'Ilo�� wierszy:'+CHR(32)+IntToStr(StringGrid1.RowCount-1);
  Label2.Caption:= 'Ilo�� kolumn:'+CHR(32)+IntToStr(StringGrid1.ColCount);
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  AA, BB: Integer;
begin
  // Wstaw now� kolumn�

  // Przesuni�cie (przez przekopiowanie) kolumny o jedn� kolumn� w praw� stron�
  for AA:= StringGrid1.ColCount-1 downto StringGrid1.Col do
    for BB:= 0 to StringGrid1.RowCount-1 do
      StringGrid1.Cells[1+AA, BB]:= Trim(StringGrid1.Cells[AA, BB]);

  // Wyczyszczenie ca�ej kolumny
  for AA:= 0 to StringGrid1.RowCount-1 do
    StringGrid1.Cells[StringGrid1.Col, AA]:= '';

  // Dodanie nowej kolumny
  StringGrid1.ColCount:= StringGrid1.ColCount+1;

  // Wy�wietlenie ilo�ci wierszy i kolumn
  Label1.Caption:= 'Ilo�� wierszy:'+CHR(32)+IntToStr(StringGrid1.RowCount-1);
  Label2.Caption:= 'Ilo�� kolumn:'+CHR(32)+IntToStr(StringGrid1.ColCount);
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  AA, BB: Integer;
begin
  // Usu� kolumn�

  // Wyczyszczenie ca�ej kolumny
  for AA:= 0 to StringGrid1.RowCount-1 do
    StringGrid1.Cells[StringGrid1.Col, AA]:= '';

  // Przesuni�cie (przez przekopiowanie) kolumny o jedn� kolumn� w lew� stron�
  for AA:= 0 to StringGrid1.ColCount-1 do
    for BB:= 0 to StringGrid1.RowCount-1 do
    begin
      StringGrid1.Cells[StringGrid1.Col+AA, BB]:= Trim(
                      StringGrid1.Cells[StringGrid1.Col+1+AA, BB]);
    end;

  // Usuni�cie kolumny
  StringGrid1.ColCount:= StringGrid1.ColCount-1;

  // Wy�wietlenie ilo�ci wierszy i kolumn
  Label1.Caption:= 'Ilo�� wierszy:'+CHR(32)+IntToStr(StringGrid1.RowCount-1);
  Label2.Caption:= 'Ilo�� kolumn:'+CHR(32)+IntToStr(StringGrid1.ColCount);
end;

end.

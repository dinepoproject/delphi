{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad u�ycia komponentu StringGrid.

Wprowadzenie:

Komponent StringGrid to arkusz p�l edycyjnych za pomoc�
kt�rej mo�emy zrobi� ma�y program kalkulacyjny.
}
unit Ustrgrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    ComboBox1: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure StringGrid1DrawCell(Sender: TObject; Col, Row: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure ComboBox1Click(Sender: TObject);
    procedure ComboBox1Exit(Sender: TObject);
    procedure StringGrid1SelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
var
  AA, BB: Integer; // Zadeklarowanie zmiennych
begin
  // FormCreate

  ComboBox1.Visible:= FALSE; // Schowaj Komponent ComboBox
  ComboBox1.Items.Clear; // Wyczyszczenie Listy

  // Stworzenie Listy Element�w Dla Komponentu ComboBox
  ComboBox1.Items.Add('Atari');
  ComboBox1.Items.Add('Spectrum');
  ComboBox1.Items.Add('Amiga');
  ComboBox1.Items.Add('IBM');
  ComboBox1.Items.Add('CRY');
  ComboBox1.Text:= '';

  

  // Wype�nienie Kom�rek Warto�ciami (Nr kolumn i Nr Wierszy)
  for AA:= 0 to StringGrid1.RowCount-1 do
    for BB:= 0 to StringGrid1.ColCount-1 do
      StringGrid1.Cells[BB, AA]:= IntToStr(BB)+'/'+IntToStr(AA);


  // Wype�nienie Wybranych Kom�rek Dowoln� Tre�ci�
  StringGrid1.Cells[4, 4]:= 'a';
  StringGrid1.Cells[1, 1]:= 'a';
  StringGrid1.Cells[3, 3]:= 'Borland Delphi Standard';
  StringGrid1.Cells[3, 4]:= 'Borland Delphi Standard';
  StringGrid1.Cells[3, 0]:= 'Borland Delphi Standard';
  StringGrid1.Cells[1, 3]:= 'Borland Delphi Standard';
  StringGrid1.RowHeights[3]:= 44;
  StringGrid1.Cells[4, 1]:= 'b';
  StringGrid1.Cells[2, 3]:= 'b';
  StringGrid1.Cells[2, 0]:= 'Col';
end;

procedure TForm1.StringGrid1DrawCell(Sender: TObject; Col, Row: Integer;
  Rect: TRect; State: TGridDrawState);
var
  txtStrSG: String; // Zadeklarowanie Zmiennej
begin
  // StringGrid1DrawCell - Kolorowanie StringGrid'a
  if (Row > 0) then
  begin

  
  //--- StringGridColor ---
    // Font Color
    if (Trim(StringGrid1.cells[Col, Row]) ='a') then
    begin
      // Poni�sze Rozkazy(Instrukcje) R�b,
      // Je�eli w Kom�rce Wpisana Jest Litera "a"

      // Wype�nij Kom�rk� Kolorem Szarym
      StringGrid1.Canvas.Brush.Color:= clGray;
      StringGrid1.Canvas.FillRect(Rect);

      // Okre�l Kolor Czcionki na ��ty
      StringGrid1.Canvas.Font.Color:= clYellow;
      StringGrid1.Canvas.TextOut(Rect.Left+2, Rect.Top+2, StringGrid1.cells[Col, Row]);

    end
    else
    if (Trim(StringGrid1.cells[Col, Row]) ='b') then
    begin
      // Poni�sze Rozkazy(Instrukcje) R�b,
      // Je�eli w Kom�rce Wpisana Jest Litera "b"

      // Wype�nij Kom�rk� Kolorem Jasno-Zielonym
      StringGrid1.Canvas.Brush.Color:= clLime;
      StringGrid1.Canvas.FillRect(Rect);

      // Okre�l Kolor Czcionki na Czerwony
      StringGrid1.Canvas.Font.Color:= clRed;
      StringGrid1.Canvas.Textout(Rect.Left+2, Rect.Top+2, StringGrid1.cells[Col, Row]);

    end
    else
    if (Trim(StringGrid1.cells[Col, Row]) ='') then
    begin
      // Poni�sze Rozkazy(Instrukcje) R�b,
      // Je�eli Kom�rka Jest Pusta

      // Wype�nij Kom�rk� Kolorem Bia�ym
      StringGrid1.Canvas.Brush.Color:= clWhite;
      StringGrid1.Canvas.FillRect(Rect);

      // Okre�l Kolor Czcionki na Czarno
      StringGrid1.Canvas.Font.Color:= clBlack;
      StringGrid1.Canvas.Textout(Rect.Left+2, Rect.Top+2, StringGrid1.cells[Col, Row]);

    end;



  //--- StringGridWordWrap ---
    if (Col = 3) and (Row = 3) then
    begin
      // Zwijaj do Lewej Napisy, Kt�re s� w Kom�rce o Nr Kolumny 3 i Nr Wiersza 3
      txtStrSG:= '';
      txtStrSG:= Trim(StringGrid1.Cells[Col, Row]);
      StringGrid1.Canvas.FillRect(Rect);
      DrawText(StringGrid1.Canvas.Handle,
               PChar(txtStrSG), Length(txtStrSG),
               Rect, DT_WORDBREAK or DT_LEFT);
    end;


  //--- StringGridColor ---
    if (Trim(StringGrid1.cells[1, Row]) ='k') then
    begin
      // Poni�sze Rozkazy(Instrukcje) R�b,
      // Je�eli w Kom�rce Wpisana Jest Litera "k"

      // Wype�nij Kom�rk� Kolorem Zielony
      StringGrid1.Canvas.Brush.Color:= clGreen;
      StringGrid1.Canvas.FillRect(Rect);

      // Okre�l Kolor Czcionki na Czarny
      StringGrid1.Canvas.Font.Color:= clBlack;
      StringGrid1.Canvas.TextOut(Rect.Left+2, Rect.Top+2, StringGrid1.cells[Col, Row]);
    end;



  //--- StringGridColor ---
    if (Row = 4) then
    begin
      // Poni�sze Rozkazy(Instrukcje) R�b,
      // Je�eli w Nr Wiersza = 4

      // Wype�nij Kom�rk� Kolorem Czerwony
      StringGrid1.Canvas.Brush.Color:= clRED;
      StringGrid1.Canvas.FillRect(Rect);

      // Okre�l Kolor Czcionki na Czarny
      StringGrid1.Canvas.Font.Color:= clBlack;
      StringGrid1.Canvas.TextOut(Rect.Left+2, Rect.Top+2, StringGrid1.cells[Col, Row]);
    end;



  //--- StringGridAlignment ---
    if (Col = 4) then
    begin
      // Zastosowanie Justowania do Prawej Dla Kolumny Nr 4
      txtStrSG:= '';
      txtStrSG:= Trim(StringGrid1.Cells[Col, Row]);
      StringGrid1.Canvas.FillRect(Rect);
      InflateRect(Rect, -7, 0);
      DrawText(StringGrid1.Canvas.Handle,
               PChar(txtStrSG), Length(txtStrSG),
               Rect, DT_SINGLELINE or DT_RIGHT);
    end;





  //--- Selected ---
    if (gdSelected in State) then
    begin
      // Ustaw Kolor Turkusowy Dla Kursora Wskazuj�cego Aktualn� Kom�rk�
      StringGrid1.Canvas.Brush.Color:= clAqua;
      StringGrid1.Canvas.FillRect(Rect);
      StringGrid1.Canvas.Font.Color:= clBlack;
      StringGrid1.Canvas.Font.Style:= StringGrid1.Canvas.Font.Style+[fsBold];
      StringGrid1.Canvas.TextOut(Rect.Left+2, Rect.Top+2, StringGrid1.Cells[Col, Row]);
    end;


  //END
  end;
end;

procedure TForm1.ComboBox1Click(Sender: TObject);
begin
  // ComboBox1Click

  // Wstaw Wybran� Warto�� z Komponentu ComboBox do Kom�rki w Kt�rej Jest Kursor
  StringGrid1.Cells[StringGrid1.Col, StringGrid1.Row]:= Trim(ComboBox1.Items[ComboBox1.ItemIndex]);

  ComboBox1.Visible:= FALSE; // Schowaj Komponent ComboBox
  StringGrid1.SetFocus; // Uaktywnij Komponent StringGrid
end;

procedure TForm1.ComboBox1Exit(Sender: TObject);
begin
  // ComboBox1Exit
  ComboBox1.Visible:= FALSE; // Schowaj Komponent ComboBox
  StringGrid1.SetFocus; // Uaktywnij Komponent StringGrid
end;

procedure TForm1.StringGrid1SelectCell(Sender: TObject; Col, Row: Integer;
  var CanSelect: Boolean);
var
  R: TRect; // Zadeklarowanie typu TRect
  {
   TRect - s�u�y do zapami�tania czterech punkt�w,
           tj. wsp�rz�dne lewego i g�rnego wierzcho�ka
           oraz wsp�rz�dne prawego i dolnego wierzcho�ka.
  }
begin
  // StringGrid1SelectCell
  if ((Col = 1) and (Row = 4)) or
     ((Col = 2) and (Row = 2)) then
  begin
    // Poni�sze Rozkazy(Instrukcje) R�b,
    // Je�eli w Nr Wiersza = 2 lub 4 i Nr Kolumny = 1 Lub 2


    // Pobranie Parametr�w Kom�rki (Po�o�enie oraz Rozmiar)
    R:= StringGrid1.CellRect(Col, Row);
    R.Left:= R.Left+StringGrid1.Left;
    R.Right:= R.Right+StringGrid1.Left;
    R.Top:= R.Top+StringGrid1.Top;
    R.Bottom:= R.Bottom+StringGrid1.Top;

    // Dopasowanie Rozmiar�w Komponentu ComboBox do Wysoko�ci i Szeroko�ci Kom�rki
    ComboBox1.Left:= R.Left+1;
    ComboBox1.Top:= R.Top+1;
    ComboBox1.Width:= (R.Right+1)-R.Left;
    ComboBox1.Height:= (R.Bottom+1)-R.Top;

    // Poka� Komponent ComboBox
    ComboBox1.Text:= '';
    ComboBox1.Visible:= TRUE;
    ComboBox1.SetFocus;
  end;
  CanSelect:= TRUE;
end;

procedure TForm1.ComboBox1KeyPress(Sender: TObject; var Key: Char);
begin
  // ComboBox1KeyPress
  if (Key = CHR(27)) then
  begin
    ComboBox1Exit(Sender); // Wywo�aj Procedur� Po Naci�ni�ciu Klawisza ESC
  end
  else
  begin
    //TODO: write here
  end;
end;

end.

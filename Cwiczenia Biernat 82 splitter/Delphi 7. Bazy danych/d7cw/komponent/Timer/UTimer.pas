{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad u�ycia komponentu Timer.

Wprowadzenie:

Komponent Timer s�u�y do generowania zdarze� w pewnych
odst�pach czasu, kt�re mo�na sobie ustawi� we
w�a�ciwo�ciach Object Inspectora.

W tym przyk�adzie komponent ten b�dzie
wykorzystany do wy�wietlenia godziny.
}
unit UTimer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    Timer1: TTimer;
    Label1: TLabel;
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  // Wy�wietlenie godziny
  Label1.Caption:= TimeToStr(Time);
  // TimeToStr() - Dokonuje konwersji czasu na tekst
end;

end.

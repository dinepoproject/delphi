{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad u�ycia komponentu TrackBar


Wprowadzenie:

Komponent TrackBar s�u�y do wyboru warto�ci,
np. zakres losowanych liczb.
}
unit UTBar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    TrackBar1: TTrackBar;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  TrackBar1.Max:= 100; // Okre�lenie maksymalnej warto�ci
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  // Wykonuje poni�sze instrukcje w momencie ruszania wska�nikiem
  Label1.Caption:= 'Wy�wietl wybran� warto��: '+IntToStr(TrackBar1.Position); // Wy�wietla wybran� warto��
  {
   Label1.Caption - ten komponent s�u�y do wy�wietla tekstu
   IntToStr('123') - ta funkcja wykonuje konwersj� liczby na tekst
  }
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu TreeView.


Wprowadzenie:
TreeView jest komponentem przedstawiaj�cym list� element�w,
kt�ra jest uporz�dkowana w spos�b hierarchiczny.

Przyk�ad drzewa uporz�dkowanego hierarchicznie:

         Kr�l
            |
            -- Kr�lowa
            |      |
            |      -- S�u�ba
            |
         Rycerz
            |
            -- Giermek
            |
            -- itd.

}
unit Utview;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    TreeView1: TTreeView;
    Label1: TLabel;
    bDodaj: TButton;
    bPopraw: TButton;
    bUsun: TButton;
    bWyczysc: TButton;
    bDodajElementSzczegolowy: TButton;
    bLiczbaElementw: TButton;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure bWyczyscClick(Sender: TObject);
    procedure bDodajClick(Sender: TObject);
    procedure bDodajElementSzczegolowyClick(Sender: TObject);
    procedure bPoprawClick(Sender: TObject);
    procedure bUsunClick(Sender: TObject);
    procedure bLiczbaElementwClick(Sender: TObject);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  bWyczyscClick(Sender); // Wywo�anie procedury odpowiedzialnej za wyczyszczenie listy


//--- Tworzenie Listy ---

  TreeView1.Items.AddFirst(TreeView1.Selected, 'Kr�l');
  // TreeView1.Items.AddFirst - Dodaje do listy jako pierwsz� pozycj�

  TreeView1.Items.AddChildFirst(TreeView1.Items[0], 'Poddani');
  {
   TreeView1.Items.AddChildFirst - dodanie pozycji 'Poddani' do pozycji 'Kr�l'
                                   uprzednio zaznaczaj�c pozycj� 'Kr�l'.

   Przez to tworzy si� drzewo hierarchiczne, np.:
         Kr�l
           |
           -- Poddani
           |     |
           |     -- itd.
           |
           -- itd.
  }
  // TreeView1.Items[0] - Okre�la na kt�rej pozycji w hierarchii listy ma by� dodany tekst

  // Pozycja 'Komputery'
  TreeView1.Items.AddFirst(TreeView1.Selected, 'Komputery');
  TreeView1.Items.AddChildFirst(TreeView1.Items[0], 'IBM');
  TreeView1.Items.AddChildFirst(TreeView1.Items[0], 'Amiga');
  TreeView1.Items.AddChildFirst(TreeView1.Items[0], 'Atari');
  TreeView1.Items.AddChildFirst(TreeView1.Items[1], 'Atari 65XE');
  TreeView1.Items.AddChildFirst(TreeView1.Items[1], 'Atari TT');

  // Pozycja 'Zabawki'
  TreeView1.Items.AddFirst(TreeView1.Selected, 'Zabawki');
  TreeView1.Items.AddChildFirst(TreeView1.Items[0], 'Mi�');
  TreeView1.Items.AddChildFirst(TreeView1.Items[0], 'Lalki');
end;

procedure TForm1.bWyczyscClick(Sender: TObject);
begin
  // Wyczyszczenie zawarto�ci listy
  TreeView1.Items.Clear;
end;

procedure TForm1.bDodajClick(Sender: TObject);
begin
  // Dodanie do listy tekstu
  TreeView1.Items.AddFirst(TreeView1.Selected,
                           InputBox('Dodaj', 'Tekst:', 'Pozycja'));
end;

procedure TForm1.bDodajElementSzczegolowyClick(Sender: TObject);
begin
  // Dodaje element szczeg�owy
  TreeView1.Items.AddChildFirst(TreeView1.Selected,
                           InputBox('Dodaj', 'Tekst:', 'Pozycja'));
  {
   TreeView1.Items.AddChildFirst - dodanie pozycji do listy lub
                                   dodanie 'Elementu2' do 'Elementu1'
                                   uprzednio zaznaczaj�c 'Element1'.
                                   Przez co tworzy si� hierarchia.

   Przez to tworzy si� drzewo hierarchiczne, np.:

         Element 1a
            |
            -- Element 2a
            |      |
            |      -- Element 3a
            |
         Element 1b
            |
            -- Element 2b
            |
            -- itd.
  }
  // InputBox('Dodaj', 'Tekst:', 'Pozycja') - otwiera okno edycyjne
end;

procedure TForm1.bPoprawClick(Sender: TObject);
begin
  // Poprawia zaznaczony tekst
  TreeView1.Items.Insert(TreeView1.Selected,
                         InputBox('Dodaj', 'Tekst:', TreeView1.Selected.Text));
  // TreeView1.Items.Insert - Wstawia nowy tekst na pozycj� elementu zaznaczonego

  TreeView1.Items.Delete(TreeView1.Selected); // Usuwa zaznaczony tekst
end;

procedure TForm1.bUsunClick(Sender: TObject);
begin
  // Usuwa zaznaczony tekst
  TreeView1.Items.Delete(TreeView1.Selected);
end;

procedure TForm1.bLiczbaElementwClick(Sender: TObject);
begin
  // Podaje liczb� element�w
  bLiczbaElementw.Caption:= 'Liczba element�w: '+IntToStr(TreeView1.Items.Count); // Podaje Liczb� element�w
end;

procedure TForm1.TreeView1Change(Sender: TObject; Node: TTreeNode);
begin
  // Wy�wietla zaznaczony tekst

  {
   Zmiana koloru wy�wietlanego napisu w zale�no�ci od
   spe�nionego warunku, w innym przypadku kolor b�dzie czarny.
  }
  if (TreeView1.Selected.Text = 'Atari 65XE') then
    Label2.Font.Color:= clBlue
  else
  if (TreeView1.Selected.Text = 'IBM') then
    Label2.Font.Color:= clGreen
  else
  if (TreeView1.Selected.Text = 'Lalki') then
    Label2.Font.Color:= clRed
  else
    Label2.Font.Color:= clBlack;


  // Wy�wietla zaznaczony tekst
  Label2.Caption:= TreeView1.Selected.Text;
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu UpDown.


Wprowadzenie:

UpDown to komponent, kt�ry umo�liwia zwi�kszanie lub
zmniejszanie liczby.

Komponent ten mo�e by� powi�zany z innym komponentem za
pomoc� w�a�ciwo�ci Associate komponentu UpDown.

Wybrane w�a�ciwo�ci komponentu UpDown:
Increment - w tej w�a�ciwo�ci podajemy warto�� o jak�
            b�dziemy zmniejsza� lub zwi�ksza� liczb�
            (np. 2 to liczba b�dzie zwi�kszana co 2 punkty.
             Domy�lnie wpisana jest warto�� 1).
ArrowKeys - umo�liwia zwi�kszanie i zmniejszanie za  pomoc� klawiatury
            (TRUE - w��czana klawiatura; FALSE - wy��czona klawiatura).
      Max - okre�la maksymaln� warto��.
      Min - okre�la minimaln� warto��.
}
unit Uupdown;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    UpDown1: TUpDown;
    Edit1: TEdit;
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

end.

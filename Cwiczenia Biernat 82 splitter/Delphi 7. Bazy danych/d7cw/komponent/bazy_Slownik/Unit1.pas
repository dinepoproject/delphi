{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

S�owniczek - przyk�adowa baza danych.


Wprowadzenie:
Baza danych jest to tabela lub kilka tabel ze sob� powi�zanych.
Ka�da tabela sk�ada si� z kolumn i wierszy. Ka�dy wiersz to
jeden rekord na kt�ry sk�ada si� jedna kolumna lub kilka kolumn.

Wygl�d przyk�adowej tabeli:

 Nazwisko  | Imi�     | Miasto
-----------|----------|----------
 Frajer    | Tadeusz  | Warszawa
 Wojna     | Marian   | Gda�sk
 Kanapka   | Andrzej  | Wroc�aw
}
unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, DBGrids, Db, DBTables, Mask, DBCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Query1: TQuery;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label4: TLabel;
    Edit1: TEdit;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Unit2;

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  {
   Application.Title - Wy�wietlenie nazwy aplikacji na pasku zada�.
                       W innym przypadku nazwa b�dzie pobierana z
                       nazwy pliku wykonywalnego.
  }
  Application.Title:= Caption;
  Edit1.Text:= ''; // Wyczyszczenie zawarto�ci komponentu EDIT.
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  // Wy�wietlenie zawarto�ci bazy s�ownika

  Query1.Close; // Zamkni�cie bazy danej
  Query1.SQL.Clear; // Czyszczenie zapytania SQL

  // Zapytanie SQL
  Query1.SQL.Add('SELECT * FROM "slownik.db" ORDER BY Wyraz');
  {
   SELECT * FROM "slownik.db" ORDER BY Wyraz
   Wy�wietla zawarto�� tablicy SLOWNIK.DB posortowan�
   alfabetycznie wed�ug kolumny "Wyraz"

   SELECT - U�ywany jest do formu�owania zapyta� do
            bazy danych w celu uzyskania informacji.

   * - Oznacza wy�wietlenie wszystkich kolumn z danej bazy.
       Gdyby by�a napisana nazwa kolumny (np. wyraz) zamiast
       gwiazdki to wy�wietlona zosta�oby kolumna o nazwie "Wyraz".

   FROM - okre�la z jakiej tablicy lub z jakich tablic s� pobierane dane.
          W naszym przypadku jest to tablica o nazwie "slownik.db".

   ORDER BY - klauzula ta s�u�y do sortowania wed�ug wybranej kolumny.
              W naszym przyk�adzie jest to kolumna "Wraz".
  }

  Query1.Open; // Otwarcie bazy danej

  // Wy�wietla ilo�� wierszy (rekord�w) w tabeli.
  Label5.Caption:= 'Ilo�� s��w: '+IntToStr(Query1.RecordCount);


  {Tworzenie bazy robimy przez wstawienie kodu pod jaki� klawisz:
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('CREATE TABLE "slownik.db" ('+
                          'Wyraz CHAR(32), '+
                          'Znaczenie CHAR(255), '+
                          'ID CHAR(20))');
  Query1.ExecSQL;  }

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Dodanie nowego s��wka (w bazie rekordu).

  // Wy�wietlenie nag��wka okna edycyjnego.
  Form2.Caption:= 'Dodaj';

  // Wyczyszczenie zawarto�ci komponentu EDIT w oknie edycyjnym.
  Form2.Edit1.Text:= '';

  // Wyczyszczenie zawarto�ci komponentu EDIT w oknie edycyjnym.
  Form2.Edit2.Text:= '';

  // Wywo�anie nowej formatki - okna edycyjnego do wprowadzenia nowego s��wka.
  Form2.ShowModal;

  {
   Sprawdzenie czy zosta� naci�ni�ty klawisz OK,
   je�eli tak dodaj s��wko do tabeli.
  }
  if (Form2.okForm = TRUE) then
  begin
    // Dodanie nowego s��wka (nowego rekordu).
    Query1.Close;
    Query1.SQL.Clear;

    // INSERT INTO - Wstawia jeden lub wi�cej wierszy do tabeli.

    Query1.SQL.Add('INSERT INTO "slownik.db" ('+
                          'Wyraz, '+
                          'Znaczenie, '+
                          'ID) '+
                   'VALUES (:p0, :p1, :p2)');

    // Pole WYRAZ
    Query1.Params[0].AsString:= Trim(Form2.Edit1.Text);
    {
     W�a�ciwo�� Params jest tablic� numerowan� od zera, kt�r�
     mo�na wykorzysta� do przypisania warto�ci w czasie
     wykonywania programu. Zamiast w�a�ciwo�ci Params mo�na
     zastosowa� w�a�ciwo�� ParamByName(), np.
      Query1.ParamByName('Wyraz').AsString:= 'think';
    }

    // Pole "Znaczenie".
    Query1.Params[1].AsString:= Trim(Form2.Edit2.Text);

    // Pole identyfikacyjne
    Query1.Params[2].AsString:= DateTimeToStr(Now);
    {
     DateTimeToStr(Now) - Zwraca dat� i czas.
     DateTimeToStr() - Dokonuje konwersji daty i czasu na tekst (string).
     Now - Zwraca aktualn� dat� i czas.

     Pole identyfikacyjne, za pomoc� kt�rego b�dzie
     mo�na poprawia� lub usuwa� dane s��wko.
     W tym celu wykorzystana zosta�a data i godzina.
     Parametry te s� r�ne dla ka�dego s��wka.
    }

    Query1.ExecSQL; // Wykonanie zapytania SQL.

    FormShow(Sender);
    {
     FormShow(Sender); - Wywo�anie funkcji odczytuj�ce tabel� w celu
                         od�wie�enia wy�wietlanej informacji.
    }
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  txtID: String; // Zadeklarowanie zmiennej "txtID".
begin
  // Edycja wybranego wiersza (rekordu).

  // Przekazanie warto�ci z pola identyfikacyjnego "ID" do zmiennej "txtID".
  txtID:= '';
  txtID:= Query1.FieldByName('ID').AsString;

  // Wy�wietlenie nag��wka okna edycyjnego.
  Form2.Caption:= 'Edycja';

  // Odczytanie wiersza z komponentu DBGrid (wyraz). Kolumna 0.
  Form2.Edit1.Text:= DBGrid1.Fields[0].AsString;

  // Odczytanie wiersza z komponentu DBGrid (znaczenie). Kolumna 1.
  Form2.Edit2.Text:= DBGrid1.Fields[1].AsString;

  // Wywo�anie nowej formatki - okna edycyjnego do wprowadzenia nowego s��wka.
  Form2.ShowModal;

  {
   Sprawdzenie czy zosta� naci�ni�ty klawisz OK,
   je�eli tak dodaj s��wko do tabeli.
  }
  if (Form2.okForm = TRUE) then
  begin
    Query1.Close;
    Query1.SQL.Clear;
    Query1.SQL.Add('UPDATE "slownik.db" SET Wyraz = :p0, Znaczenie = :p1 '+
                   'WHERE ID = :p2');
    Query1.Params[0].AsString:= Trim(Form2.Edit1.Text); // Wyraz
    Query1.Params[1].AsString:= Trim(Form2.Edit2.Text); // Znaczenie
    Query1.Params[2].AsString:= Trim(txtID); // Pole identyfikacyjne
    Query1.ExecSQL; // Wykonanie zapytania SQL.

    FormShow(Sender);
    {
     FormShow(Sender); - Wywo�anie funkcji odczytuj�ce tabel� w celu
                         od�wie�enia wy�wietlanej informacji.
    }
  end;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  // Wyszukanie wyrazu (s��wka).
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('SELECT * FROM "slownik.db" '+
                 'WHERE UPPER(Wyraz) LIKE :p_Wyraz '+
                 'ORDER BY Wyraz');
  {
   WHERE - Po s�owie WHERE wyst�puje predykat, kt�ry sk�ada
           si� z jednego lub wi�cej wyra�e�.
           W tym przypadku UPPER(Wyraz) LIKE :p_Wyraz.

   UPPER() - Konwertuje ci�g znak�w na ci�g znak�w pisany du�ymi literami.

   LIKE - Wyra�enie to pozwala nam na poszukiwanie okre�lonego ci�gu znak�w.
  }

  {
   Przekazanie ci�gu znak�w jako parametru
   wed�ug kt�rego nast�pi wyszukiwanie (pole Wyraz).
   % - Pozwala na wy�wietlenie wszystkich wyraz�w
       zaczynaj�cych si� od litery np. A, a dzi�ki
       operatorowi "%" dalsze litery nie s� brane pod uwag�.
  }
  Query1.Params[0].AsString:= UpperCase(Trim(Edit1.Text))+'%';
  Query1.Open; // Otwarcie bazy danej do odczytu.
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  txtID: String; // Zadeklarowanie zmiennej "txtID".
begin
  // Usuwa zaznaczony wiersz (rekord).

  // Przekazanie warto�ci z pola identyfikacyjnego "ID" do zmiennej "txtID".
  txtID:= '';
  txtID:= Query1.FieldByName('ID').AsString;

  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('DELETE FROM "slownik.db" WHERE ID = :p0');

  // Wykorzystanie identyfikatora do usuni�cia wybranego rekordu.
  Query1.Params[0].AsString:= txtID;

  Query1.ExecSQL; // Wykonanie zapytania SQL.
  FormShow(Sender);
  {
    FormShow(Sender); - Wywo�anie funkcji odczytuj�ce tabel� w celu
                         od�wie�enia wy�wietlanej informacji.
  }
end;

end.

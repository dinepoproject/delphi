unit Unit2;
// Format za pomoc�, kt�rej mo�na dodawa� dane do bazy

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm2 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    okForm :Boolean; // Zadeklarowanie zmiennej logicznej
  end;

var
  Form2: TForm2;

implementation

{$R *.DFM}

procedure TForm2.FormCreate(Sender: TObject);
begin
   Edit1.Text:= ''; // Wyczyszczenie zawarto�ci komponentu EDIT.
   Edit2.Text:= '';
end;

procedure TForm2.FormShow(Sender: TObject);
begin
   // FormShow
   okForm:= FALSE; // Przypisanie zmiennej "okForm" warto�ci FALSE (Fa�sz)
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
   {
    Przypisanie zmiennej "okForm" warto�ci TRUE (Prawda),
    gdy zostanie naci�ni�ty klawisz OK.
   }
   okForm:= TRUE;
   Close; // Zamkni�cie formatki
end;

end.

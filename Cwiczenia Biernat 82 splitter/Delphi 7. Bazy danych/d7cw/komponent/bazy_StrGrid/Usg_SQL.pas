{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu Query i StringGrid.


Wprowadzenie:
Baza danych jest to tabela lub kilka tabel ze sob� powi�zanych.
Ka�da tabela sk�ada si� z kolumn i wierszy. Ka�dy wiersz to
jeden rekord na kt�ry sk�ada si� jedna kolumna lub kilka kolumn.

Wygl�d przyk�adowej tabeli:

 Nazwisko  | Imi�     | Miasto
-----------|----------|----------
 Frajer    | Tadeusz  | Warszawa
 Wojna     | Marian   | Gda�sk
 Kanapka   | Andrzej  | Wroc�aw
}
unit Usg_SQL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, StdCtrls, Buttons, Grids;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Edit1: TEdit;
    GroupBox2: TGroupBox;
    StringGrid1: TStringGrid;
    Query1: TQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  {
   Tu wpisujemy funkcje, kt�re s� wykonywane w
   momencie tworzenia formatki.
  }
  Edit1.Text:= ''; // Wyczyszczenie komponentu Edit1
end;

procedure TForm1.FormShow(Sender: TObject);
var
  TT: Integer; // Deklaracja zmiennej "TT"
begin
  {
   Tu wpisujemy funkcje, kt�re s� wykonywane w
   momencie otwierania formatki.
  }

  // Ustawienia dla komponentu StringGrid
  StringGrid1.Cells[0, 0]:= 'Pa�stwo'; // Tytu� kolumny
  StringGrid1.ColWidths[0]:= 144; // Szeroko�� kolumny

  StringGrid1.Cells[1, 0]:= 'Stolica'; // Tytu� kolumny
  StringGrid1.ColWidths[1]:= 199; // Szeroko�� kolumny

  StringGrid1.RowCount:= 2; // Ilo�� wierszy 

  // Zapytanie SQL
  Query1.Close; // Zamkni�cie bazy danej
  Query1.SQL.Clear; // Czyszczenie zapytania SQL

  Query1.SQL.Add('SELECT * FROM "Country.db" ORDER BY Name');
  {
   SELECT * FROM "Country.db" ORDER BY Name
   Wy�wietla zawarto�� tablicy COUNTRY.DB posortowan�
   alfabetycznie wed�ug kolumny "Name"

   SELECT - U�ywany jest do formu�owania zapyta� do
            bazy danych w celu uzyskania informacji.

   * - Oznacza wy�wietlenie wszystkich kolumn z danej bazy.
       Gdyby by�a napisana nazwa kolumny (np. name) zamiast
       gwiazdki to wy�wietlona zosta�oby kolumna o nazwie "Name".

   FROM - okre�la z jakiej tablicy lub z jakich tablic s� pobierane dane.
          W naszym przypadku jest to tablica o nazwie "Country.db".

   ORDER BY - klauzula ta s�u�y do sortowania wed�ug wybranej kolumny.
              W naszym przyk�adzie jest to kolumna "Name".
  }
  Query1.Open; // Otwarcie bazy danej

  // Ustawienia na pierwszy rekord w bazie
  Query1.First;

  {
   Ustawia liczb� wierszy w komponencie StringGrid,
   wed�ug ilo�ci rekord�w w bazie
  }
  StringGrid1.RowCount:= 2+Query1.RecordCount;

  TT:= 0; // Przypisanie zmiennej "TT" warto�ci 0

  {
   Odczytywanie rekord�w z bazy.
   Odczyt zostanie zako�czony dopiero po odczytaniu
   ostatniego rekordu w bazie.
  }
  while not(Query1.EOF) do
  begin

    {
     Odczytanie Nazwy z kolumny NAME i przypisanie
     jej do pierwszej kolumny komponentu StringGrid.
    }
    StringGrid1.Cells[0, 1+TT]:= Trim(Query1.FieldByName('Name').AsString);

    {
    Odczytanie Stolicy z kolumny CAPITAL i przypisanie
    jej do drugiej kolumny komponentu StringGrid.
    }
    StringGrid1.Cells[1, 1+TT]:= Trim(Query1.FieldByName('Capital').AsString);

    Inc(TT); // Licznik wierszy

    // Przesuni�cie do nast�pnego rekordu
    Query1.Next;
  end;
end;

procedure TForm1.Edit1Change(Sender: TObject);
var
  TT, numCLS: Integer; // Zadeklarowanie zmiennych
begin
  {
   Tu wpisujemy funkcje, kt�re b�d� wykonywane w
   momencie zmiany zawarto�ci komponentu Edit.
  }

  Query1.Close; // Zamkni�cie bazy danej
  Query1.SQL.Clear; // Czyszczenie zapytania SQL

  Query1.SQL.Add('SELECT * FROM "Country.db" '+
                 'WHERE UPPER(Capital) LIKE :p_Capital '+
                 'ORDER BY Name');
  {
   WHERE - Po s�owie WHERE wyst�puje predykat, kt�ry sk�ada
           si� z jednego lub wi�cej wyra�e�.
           W tym przypadku UPPER(Capital) LIKE :p_Capital.

   UPPER() - Konwertuje ci�g znak�w na ci�g znak�w pisany du�ymi literami.

   LIKE - Wyra�enie to pozwala nam na poszukiwanie okre�lonego ci�gu znak�w.
  }

  {
   Przekazanie ci�gu znak�w jako parametru
   wed�ug kt�rego nast�pi wyszukiwanie (pole Capital).
   % - Pozwala na wy�wietlenie wszystkich wyraz�w
       zaczynaj�cych si� od litery np. A, a dzi�ki
       operatorowi "%" dalsze litery nie s� brane pod uwag�.
  }
  Query1.Params[0].AsString:= UpperCase(Trim(Edit1.Text))+'%';

  Query1.Open; // Otwarcie bazy danej

  // Ustawienia na pierwszy rekord w bazie
  Query1.First;

  {
   Ustawia liczb� wierszy w komponencie StringGrid,
   wed�ug ilo�ci rekord�w w bazie
  }
  StringGrid1.RowCount:= 2+Query1.RecordCount;

  TT:= 0; // Przypisanie zmiennej "TT" warto�ci 0

  {
   Odczytywanie rekord�w z bazy.
   Odczyt zostanie zako�czony dopiero po odczytaniu
   ostatniego rekordu w bazie.
  }
  while not(Query1.EOF) do
  begin

    {
     Odczytanie Nazwy z kolumny NAME i przypisanie
     jej do pierwszej kolumny komponentu StringGrid.
    }
    StringGrid1.Cells[0, 1+TT]:= Trim(Query1.FieldByName('Name').AsString);

    {
    Odczytanie Stolicy z kolumny CAPITAL i przypisanie
    jej do drugiej kolumny komponentu StringGrid.
    }
    StringGrid1.Cells[1, 1+TT]:= Trim(Query1.FieldByName('Capital').AsString);

    Inc(TT); // Licznik wierszy

    // Przesuni�cie do nast�pnego rekordu
    Query1.Next;
  end;

  // Wyczy�� wiersze
  for numCLS:= TT+1 to StringGrid1.RowCount-1 do
  begin

    // Wyczyszczenie wiersza w pierwszej kolumnie
    StringGrid1.Cells[0, numCLS]:= '';

    // Wyczyszczenie wiersza w drugiej kolumnie
    StringGrid1.Cells[1, numCLS]:= '';
  end;
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu Query do wy�wietlenia nazw p�l z bazy.


Wprowadzenie:
Komponent Query s�u�y do reprezentowania
danych, kt�re s� efektem zadanego
pytania SQL w stosunku do jednej lub wi�kszej
ilo�ci tabel.
}
unit Udbfield;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    ListBox1: TListBox;
    Query1: TQuery;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
var
  TT: Integer; // Deklaracja zmiennej TT
begin
  {
   Tu wpisujemy instrukcje, kt�re s�
   wykonywane w momencie tworzenia formatki.
  }


  // Zapytanie SQL
  Query1.Close; // Zamkni�cie bazy danej
  Query1.SQL.Clear; // Czyszczenie zapytania SQL

  Query1.SQL.Add('SELECT * FROM "country.db"');
  {
   SELECT * FROM "Reservat.db"
   Wy�wietla zawarto�� tablicy RESERVAT.DB

   SELECT - U�ywany jest do formu�owania zapyta� do
            bazy danych w celu uzyskania informacji.

   * - Oznacza wy�wietlenie wszystkich kolumn z danej bazy.
       Gdyby by�a napisana nazwa kolumny (np. name) zamiast
       gwiazdki to wy�wietlona zosta�oby kolumna o nazwie "Name".

   FROM - okre�la z jakiej tablicy lub z jakich tablic s� pobierane dane.
          W naszym przypadku jest to tablica o nazwie "Country.db".
  }

  Query1.Open; // Otwarcie bazy danej


  //-- Odczytanie nazw p�l z bazy danej --
  ListBox1.Items.Clear;
  for TT:= 0 to Query1.FieldCount-1 do
    ListBox1.Items.Add(Trim(Query1.Fields[TT].DisplayName));
  {
    ListBox1.Items.Clear;
    Wyczyszczenie zawarto�ci komponentu ListBox

    for TT:= 0 to Query1.FieldCount-1 do
    Wykonanie p�tli tyle razy ile jest p�l w bazie danych

    Query1.FieldCount - Zwraca liczb� p�l w bazie

    ListBox1.Items.Add() - Dodaje element do listy

    Trim()
    Likwiduje puste znaki (Spacje) po obu stronach tekstu (ci�gu znak�w)

    Query1.Fields[Index].DisplayName
    Wy�wietla nazw� pola wyst�puj�cego w bazie o zadanym indeksie
  }
end;

end.

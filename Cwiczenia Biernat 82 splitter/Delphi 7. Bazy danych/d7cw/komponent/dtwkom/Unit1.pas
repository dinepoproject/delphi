{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad dynamicznego tworzenia komponent�w.

Wprowadzenie:
Dynamiczne tworzenie komponent�w jest
wygodnym rozwi�zaniem, kiedy chcemy na formatce
umie�ci� r�wnocze�nie np. 30 klawiszy.
}
unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Menus, Gauges, ExtCtrls;

const
   {
    Zadeklarowanie Sta�ej Przechowuj�cej Szeroko�� i Wysoko�ci
    Klawisza do Generowania Planszy
   }
   numGV_RozmiarKlawisza = 32;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

    // Zadeklarowanie Zmiennych, Kt�re Przechowuj� Po�o�enie Klawisza
    numGV_PozX, numGV_PozY :Shortint;

    // Zadeklarowanie Obiektu Typu "TSpeedButton"
    SB_Klawisz :array[1..10, 1..10] of TSpeedButton;

    procedure Klik(Sender: TObject);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Klik(Sender: TObject);
begin
   // Wy�wietl nazw� naci�ni�tego klawisza
   ShowMessage((Sender as TSpeedButton).Name);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
   AA, BB: Shortint; // Zadeklarowanie Zmiennych
begin
   // FormCreate - Tworzenie klawiszy
   for BB:= 0 to 9 do
   begin
      for AA:= 0 to 9 do
      begin
         {
          Dynamiczne Tworzenie Klawiszy o Nazwie "SB_Klawisz",
          Kt�rych W�a�cicielem Jest Komponent GroupBox1
          (na kt�rym to klawisze s� tworzone)
         }
         SB_Klawisz[AA+1, BB+1]:= TSpeedButton.Create(GroupBox1);


         // Ustalenie Dodatkowych Parametr�w Tworzonych Dynamicznie Klawiszy
         SB_Klawisz[AA+1, BB+1].Top:= 15+(BB*numGV_RozmiarKlawisza); // Pozycja Pierwszego Klawisza

         // Pozycja Pierwszego Klawisza
         SB_Klawisz[AA+1, BB+1].Left:= 9+(AA*numGV_RozmiarKlawisza);

         SB_Klawisz[AA+1, BB+1].Width:= numGV_RozmiarKlawisza; // Szeroko��
         SB_Klawisz[AA+1, BB+1].Height:= numGV_RozmiarKlawisza; // Wysoko��

         // Nadaj Nazw� Klawiszowi
         SB_Klawisz[AA+1, BB+1].Name:= 'k'+IntToStr(AA+1)+'_'+IntToStr(BB+1);

         // Opisz klawisz
         SB_Klawisz[AA+1, BB+1].Caption:= IntToStr(AA+1)+', '+IntToStr(BB+1);

         // Nadanie Wygl�du Wypuk�ego Klawiszom
         SB_Klawisz[AA+1, BB+1].Flat:= FALSE;

         // Przypisanie Do Klawisza Procedury "Klik"
         SB_Klawisz[AA+1, BB+1].OnClick:= Klik;


         // Umieszczenie Komponent�w "SB_Klawisz" Tworzonych
         // Dynamicznie na Komponencie GroupBox1
         GroupBox1.InsertControl(SB_Klawisz[AA+1, BB+1]);
      end;
   end;
end;

end.

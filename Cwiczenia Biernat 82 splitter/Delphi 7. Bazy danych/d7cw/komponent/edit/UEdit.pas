{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad zastosowania komponentu Edit i BitBtn.


Wprowadzenie:

Spos�b u�ywania komponentu BitBtn jest taki
sam jak komponentu Button z t� r�nic�, �e
komponent BitBtn ma mo�liwo�� wczytania rysunku.
}
unit UEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    Edit1: TEdit;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  Edit1.Text:= ''; // Wyczyszczenie zawarto�ci komponentu Edit
  Edit1Change(Sender); // Ponowne wykorzystanie zdarzenia Edit1Change
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  // Aktywacja i dezaktywacja klawisza
  BitBtn1.Enabled:= FALSE; // Wy��czenie mo�liwo�� naci�ni�cia klawisza.
  if (Length(Trim(Edit1.Text)) > 0) then
  begin
    {
     W��czenie mo�liwo�ci naci�ni�cia
     klawisza, je�eli jest wpisany
     przynajmniej jeden znak.
    }
    BitBtn1.Enabled:= TRUE;
  end;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  // OK
  Caption:= Edit1.Text; // Wy�wietlenie w pasku tytu�owym zawarto�ci komponentu Edit.
end;

end.

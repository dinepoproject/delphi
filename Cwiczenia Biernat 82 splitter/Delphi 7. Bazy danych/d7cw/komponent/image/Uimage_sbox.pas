{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad zastosowania komponentu ScrollBar i Image.


Wprowadzenie:

ScrollBar:
Komponent SCROLLBAR s�u�y do grupowania wi�kszej ilo�ci innych
komponent�w, np. komponent�w ComboBox - w celu ich p�niejszego
przewijania, gdy nie mieszcz� si� na formatce lub do
przewijania obrazu, kt�ry jest wi�kszy ni� formatka.

Image:
Komponent IMAGE s�u�y do wy�wietlania obrazk�w bitmapowych.
}
unit Uimage_sbox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ExtDlgs;

type
  TForm1 = class(TForm)
    ScrollBox1: TScrollBox;
    Image1: TImage;
    Button1: TButton;
    OpenPictureDialog1: TOpenPictureDialog;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Odczytanie pliku graficznego


  {
   W��czenie dopasowania si� komponentu IMAGE1 do wielko�ci wczytanego
   rysunku (tzn. szeroko�� i wysoko�� komponentu IMAGE1 b�dzie taka
   sama jak wczytanego rysunku)
  }
  Image1.AutoSize:= TRUE;

  // Uruchomienie Okna Dialogowego, Kt�ry Umo�liwia Wyb�r Pliku Graficznego
  OpenPictureDialog1.Execute;
  if (Trim(OpenPictureDialog1.FileName)<>'') then
  begin
    // Je�eli Plik Zostanie Wybrany, To Wykonaj Poni�sze Instrukcje

    // Wczytanie pliku graficznego
    Image1.Picture.LoadFromFile(Trim(OpenPictureDialog1.FileName));
  end;
end;

end.

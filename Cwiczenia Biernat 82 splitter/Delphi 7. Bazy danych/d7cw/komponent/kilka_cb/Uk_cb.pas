{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad obs�ugi kilku komponent�w ComboBox jedn� funkcj�.


Wprowadzenie:

ComboBox jest to lista rozwijana po��czona z edytorem.
Rozwini�cie tej list nast�puje po klikni�ciu na strza�k�
skierowan� w d� (znajduje si� ona z prawej strony edytora)
lub naci�ni�ciu kombinacji klawiszy Alt+Strza�ka w d�.
}
unit Uk_cb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox; // Deklaracja funkcji sprawdzaj�cej
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CB_WczytajListe(txtNazwaPliku :String; NazwaKomponentu :TComboBox);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}


procedure TForm1.CB_WczytajListe(txtNazwaPliku :String; NazwaKomponentu :TComboBox);
begin
   {
    Parametry procedury "CB_WczytajListe":
    txtNazwaPliku - Zmienna ta przechowuje podan� nazw� pliku
    NazwaKomponentu - Zmienna ta przechowuje podan� nazw� komponentu
   }

   
   // Wyczyszczenie komponentu o podanej nazwie w
   // parametrze funkcji "NazwaKomponentu"
   NazwaKomponentu.Items.Clear;


   
   // Wyczy�� zawarto�� pola edycyjnego
   NazwaKomponentu.Text:= '';


   // Sprawdzenie czy plik o podanej nazwie istnieje
   if (FileExists(txtNazwaPliku) = TRUE) then
   begin
      // Je�eli plik istnieje, to wykonaj instrukcje po s�owie THEN.


      // Wczytaj zawarto�ci pliku do listy
      NazwaKomponentu.Items.LoadFromFile(txtNazwaPliku);

      // Wstaw w polu edycyjnym pierwszy element listy
      NazwaKomponentu.Text:= NazwaKomponentu.Items[0];
   end;
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
   // Wywo�anie procedury "CB_WczytajListe" z parametrami
   CB_WczytajListe('nazw_kom.txt', ComboBox1);
   CB_WczytajListe('imiona.txt', ComboBox2);
   CB_WczytajListe('miasta.txt', ComboBox3);
end;

end.

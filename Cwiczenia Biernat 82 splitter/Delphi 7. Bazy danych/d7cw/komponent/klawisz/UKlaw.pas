{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad obs�ugi klawiatury.
}
unit Uklaw;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Menus;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
   // FormCreate
end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: Char);
begin
   // Przyk�ad obs�ugi klawisza ESC
   if (Key = CHR(27)) then ShowMessage('Zosta� naci�ni�ty klawisz ESC.'); 

   // Przyk�ad obs�ugi klawisza ENTER
   if (Key = CHR(13)) then ShowMessage('Zosta� naci�ni�ty klawisz ENTER.');
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   // Przyk�ad obs�ugi kombinacji klawiszy CTRL+ALT+SHIFT+Q
   if (Shift = [ssCtrl, ssAlt, ssShift]) then
   begin
      if (CHR(Key) in ['Q', 'q']) then
      begin
         ShowMessage('Nacisn��e� kombinacj� klawiszy CTRL+ALT+SHIFT+Q.');
      end;
   end
   else
   if ((ssCtrl in Shift) and (CHR(Key) in ['Q', 'q'])) then
   begin
      // Przyk�ad obs�ugi kombinacji CTRL+Q
      ShowMessage('To jest kombinacja CTRL+Q');
   end;
end;

end.

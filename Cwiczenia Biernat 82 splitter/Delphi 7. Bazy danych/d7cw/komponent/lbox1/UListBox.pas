{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

 Przyk�ad wykorzystania komponentu ListBox


Wprowadzenie:

ListBox jest komponentem s�u��cym do wy�wietlania listy �a�cuch�w.

}
unit UListBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TfrmMain = class(TForm)
    ListBox1: TListBox;
    Bevel1: TBevel;
    bDodaj: TButton;
    bWczytajPlik: TButton;
    bZaznaczWszystko: TButton;
    bOdznaczWszystko: TButton;
    bZaznaczOdznaczWszystko: TButton;
    bZapiszDoPliku: TButton;
    Edit1: TEdit;
    bDodajZawartoscEdita: TButton;
    procedure FormCreate(Sender: TObject);
    procedure bDodajClick(Sender: TObject);
    procedure bWczytajPlikClick(Sender: TObject);
    procedure bZaznaczWszystkoClick(Sender: TObject);
    procedure bOdznaczWszystkoClick(Sender: TObject);
    procedure bZaznaczOdznaczWszystkoClick(Sender: TObject);
    procedure bZapiszDoPlikuClick(Sender: TObject);
    procedure bDodajZawartoscEditaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.DFM}

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  ListBox1.Items.Clear; // Wyczyszczenie zawarto�� komponentu ListBox
  Edit1.Text:= ''; // Czy�ci zawarto�� komponentu Edit.
end;

procedure TfrmMain.bDodajClick(Sender: TObject);
begin
  // Dodaj
  ListBox1.Items.Clear; // Czy�ci zawarto�� komponentu ListBox
  ListBox1.Items.Add('Spectrum'); // Dodaje pozycj� do komponentu ListBox
  ListBox1.Items.Add('Amiga');
  ListBox1.Items.Add('IBM');
  ListBox1.Items.Add('Atari');
  ListBox1.Items.Add('�yrafa');
  ListBox1.Items.Add('Cry');
  ListBox1.Items.Add('CPU');
  ListBox1.Items.Add('Komputer');
  ListBox1.Sorted:= TRUE; // Sortuje pozycje w komponencie ListBox
end;

procedure TfrmMain.bWczytajPlikClick(Sender: TObject);
begin
  // Wczytaj z pliku
  ListBox1.Items.Clear; // Czy�ci zawarto�� komponentu ListBox
  if (FileExists(Trim('Lista.txt'))=TRUE) then
  begin
    ListBox1.Items.LoadFromFile(Trim('Lista.txt')); // Wczytuje list� do komponentu ListBox
    ListBox1.Sorted:= TRUE; // Sortuje pozycje w komponencie ListBox
  end;
  {
    if (FileExists(Trim('Lista.txt'))=TRUE) then
    begin
    end;

   Funkcja ta sprawdza, czy na dysku istnieje plik o
   nazwie "Lista.txt", je�eli istnieje to go
   wczytuje, w przeciwnym przypadku nie wykonuje nic.
  }
end;

procedure TfrmMain.bZaznaczWszystkoClick(Sender: TObject);
var
  TT: Integer;
begin
  // Zaznacza wszystko
  ListBox1.MultiSelect:= TRUE; // W��cza mo�liwo�� zaznaczania wi�cej ni� jednej pozycji w komponencie ListBox 
  for TT:= 0 to ListBox1.Items.Count-1 do
    ListBox1.Selected[TT]:= TRUE;
  {
   ListBox1.Selected[TT]:= TRUE;
   Zaznacza pozycj� o podanym numerze, kt�ry znajduje si� pod zmienn� "TT".
  }
end;

procedure TfrmMain.bOdznaczWszystkoClick(Sender: TObject);
var
  TT: Integer;
begin
  // Odznacza wszystko
  ListBox1.MultiSelect:= TRUE; // W��cza mo�liwo�� zaznaczania wi�cej ni� jednej pozycji w komponencie ListBox
  for TT:= 0 to ListBox1.Items.Count-1 do
    ListBox1.Selected[TT]:= FALSE;
  {
   ListBox1.Selected[TT]:= FALSE;
   Odznacza pozycj� o podanym numerze, kt�ry znajduje si� pod zmienn� "TT".
  }
end;

procedure TfrmMain.bZaznaczOdznaczWszystkoClick(Sender: TObject);
var
  TT: Integer;
begin
  // Zaznacza/Odznacza wszystko
  ListBox1.MultiSelect:= TRUE; // W��cza mo�liwo�� zaznaczania wi�cej ni� jednej pozycji w komponencie ListBox
  for TT:= 0 to ListBox1.Items.Count-1 do
    if (ListBox1.Selected[TT] = TRUE) then
    begin
      ListBox1.Selected[TT]:= FALSE;
      {
       ListBox1.Selected[TT]:= FALSE;
       Odznacza pozycj� o podanym numerze, kt�ry znajduje si� pod zmienn� "TT",
       je�eli by� zaznaczony.
      }
    end
    else
    begin
      ListBox1.Selected[TT]:= TRUE;
      {
       ListBox1.Selected[TT]:= FALSE;
       Zaznacza pozycj� o podanym numerze, kt�ry znajduje si� pod zmienn� "TT",
       je�eli by� odznaczony.
      }
    end;
end;

procedure TfrmMain.bZapiszDoPlikuClick(Sender: TObject);
begin
  // Zapisz do pliku
  ListBox1.Items.SaveToFile('Lista2.txt');
end;

procedure TfrmMain.bDodajZawartoscEditaClick(Sender: TObject);
begin
  // Dodaj zawarto�� Edit'a do ListBox'u

  {
   W momencie spe�nienia warunku, tzn. gdy komponent Edit b�dzie
   zawiera� chocia� jeden znak to nast�pi dodanie tego znaku do ListBox'u.
  }
  if (Trim(Edit1.Text)<>'') then
  begin
    ListBox1.Items.Add(Trim(Edit1.Text)); // Dodaje pozycj� do komponentu ListBox
    ListBox1.Sorted:= TRUE; // Sortuje pozycje w komponencie ListBox
    Edit1.Text:= ''; // Czy�ci zawarto�� komponentu Edit
  end;
end;

end.

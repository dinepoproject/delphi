{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad wykorzystania komponentu ListBox


Wprowadzenie:

ListBox jest komponentem s�u��cym do wy�wietlania listy �a�cuch�w.

}
unit ULBox3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TfrmMain = class(TForm)
    Bevel1: TBevel;
    Label1: TLabel;
    ListBox1: TListBox;
    Label2: TLabel;
    ListBox2: TListBox;
    bDoListy2: TButton;
    bDoListy1: TButton;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure bDoListy2Click(Sender: TObject);
    procedure bDoListy1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.DFM}

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  // FormCreate

  // Lista 1
  ListBox1.Items.Clear; // Czy�ci zawarto�� komponentu ListBox
  ListBox1.Items.Add('Spectrum'); // Dodaje pozycj� do komponentu ListBox
  ListBox1.Items.Add('Amiga');
  ListBox1.Items.Add('IBM');
  ListBox1.Items.Add('Atari');
  ListBox1.Items.Add('�yrafa');
  ListBox1.Items.Add('Star Wars');
  ListBox1.Items.Add('Cry');
  ListBox1.Items.Add('CPU');
  ListBox1.Items.Add('Komputer');

  // Lista 2
  ListBox2.Items.Clear; // Czy�ci zawarto�� komponentu ListBox
  ListBox2.Items.Add('Mama'); // Dodaje pozycj� do komponentu ListBox
  ListBox2.Items.Add('Tata');
  ListBox2.Items.Add('Brat');
  ListBox2.Items.Add('C�rka');
  ListBox2.Items.Add('Dziewczyna');
  ListBox2.Items.Add('Ch�opak');

  {
   W��cza mo�liwo�� zaznaczania wi�cej
   ni� jednej pozycji w komponencie ListBox
  }
  ListBox1.MultiSelect:= TRUE;
  ListBox2.MultiSelect:= TRUE;
end;

procedure TfrmMain.bDoListy2Click(Sender: TObject);
var
  TT: Integer;
begin
  // Przenosi elementy z 1 listy do 2 listy
  for TT:= ListBox1.Items.Count-1 downto 0 do
    if (ListBox1.Selected[TT] =TRUE) then
    begin
      ListBox2.Items.Add(Trim(ListBox1.Items[TT]));
      ListBox1.Items.Delete(TT);
    end;
end;

procedure TfrmMain.bDoListy1Click(Sender: TObject);
var
  TT: Integer;
begin
  // Przenosi elementy z 2 listy do 1 listy
  for TT:= ListBox2.Items.Count-1 downto 0 do
    if (ListBox2.Selected[TT] =TRUE) then
    begin
      ListBox1.Items.Add(Trim(ListBox2.Items[TT]));
      ListBox2.Items.Delete(TT);
    end;
end;

end.

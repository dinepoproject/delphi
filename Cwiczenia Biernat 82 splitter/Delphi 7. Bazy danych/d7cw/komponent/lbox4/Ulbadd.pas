{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad wykorzystania komponentu ListBox


Wprowadzenie:

ListBox jest komponentem s�u��cym do wy�wietlania listy �a�cuch�w.

}
unit Ulbadd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    Label1: TLabel;
    Edit1: TEdit;
    bAdd: TButton;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure bAddClick(Sender: TObject);
  private
    { Private declarations }
    function ListBoxVerify(txtTextVerify: String): Boolean;
    function ListBoxSelectAdd(txtAddText: String): String;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function TForm1.ListBoxVerify(txtTextVerify: String): Boolean;
var
  TT :Integer;
begin
  {
   Sprawdza, czy w li�cie istnieje ci�g znak�w wpisanych w komponencie Edit.
   Je�eli wpisany ci�g znak�w istnie to funkcja zwr�ci
   warto�� TRUE (Prawda), w innym przypadku zwr�ci FALSE.

   AnsiUpperCase - zmienia ca�y ci�g znak�w na du�e litery.
   Trim - likwiduje spacje po obu stronach ci�gu znak�w.
  }
  ListBoxVerify:= FALSE;
  for TT:= ListBox1.Items.Count-1 downto 0 do
    if (AnsiUpperCase(Trim(txtTextVerify)) =
        AnsiUpperCase(Trim(ListBox1.Items[TT]))) then ListBoxVerify:= TRUE;
end;

function TForm1.ListBoxSelectAdd(txtAddText: String): String;
var
  TT: Integer;
begin
  {
   Dodanie nowego ci�gu znak�w z zaznaczeniem dodawanego ci�gu znak�w.
   Je�eli nast�pi�o dodanie ci�gu znak�w to funkcja zwr�ci dodany ci�g
   znak�w, w przeciwnym przypadku nast�pi zwr�cenie znaku pustego.
  }
  ListBoxSelectAdd:= '';
  ListBox1.MultiSelect:= TRUE; // Ustawienie mo�liwo�ci zaznaczanie liku pozycji w li�cie.
  ListBox1.IntegralHeight:= TRUE; // Ustawienie wysoko�ci, kt�ra umo�liwia widoczno�� ostatniego elementu listy. Zmiana wysoko�ci nast�puje skokowo.
  ListBox1.Sorted:= FALSE; // Wy��czenie sortowania.

  if (Trim(txtAddText)<>'') then // Dodanie ci�gu znak�w, je�eli zmienna "txtAddText" nie jest pusta.
  begin
    ListBox1.Items.Add(Trim(txtAddText)); // Dodanie ci�gu znak�w.


    for TT:= 0 to ListBox1.Items.Count-1 do
      ListBox1.Selected[TT]:= FALSE; // Odznaczenie wszystkich pozycji w li�cie.

    ListBox1.Selected[ListBox1.Items.Count-1]:= TRUE; // Zaznaczenie dodanej pozycji (ci�gu znak�w).
    ListBoxSelectAdd:= Trim(txtAddText); // Zwr�cenie dodanego ci�gu znak�w.

  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

  Edit1.Text:= ''; // Wyczyszczenie komponentu Edit.
  ListBox1.Items.Clear; // Wyczyszczenie listy.
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  {
   Edit1Change - w tej procedurze wpisujemy rozkazy, kt�re
   b�d� wykonywane w momencie zmiany zawarto�ci komponentu Edit.
  }

  bAdd.Enabled:= FALSE; // Wy��czenie przycisku DODAJ.

  {
    if...then
    Sprawdza, czy jest wpisany chocia� jeden znak.
    Je�eli tak to zostanie uaktywniony przycisk DODAJ.
  }
  if (Length(Trim(Edit1.Text)) > 0) then
    bAdd.Enabled:= TRUE; // W��czenie przycisku DODAJ.
end;

procedure TForm1.bAddClick(Sender: TObject);
begin
  // Dodanie ci�gu znak�w do listy.

  if (ListBoxVerify(Edit1.Text) = FALSE) and
     (Trim(Edit1.Text)<>'') then
  begin
    {
     Dodanie nowego ci�gu znak�w nast�pi w momencie, gdy
     tego znaku nie ma w li�cie i zawarto�� komponentu Edit
     nie jest pusta. W przeciwnym przypadku nie nast�pi
     dodanie do listy ci�gu znak�w.
    }
    ListBoxSelectAdd(Edit1.Text);
    Edit1.Text:= ''; // Wyczyszczenie komponentu Edit.
  end;
end;

end.

{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad odtwarzania utworu w k�ko za pomoc� komponentu MediaPlayer.

Wprowadzenie:

Komponent ten s�u�y do odtwarzania muzyki i
film�w video (wav, mid, mp3, avi, itd.).
}
unit Uml;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, MPlayer, StdCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    MediaPlayer1: TMediaPlayer;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    procedure Button1Click(Sender: TObject);
    procedure MediaPlayer1Notify(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Otw�rz plik muzyczny
  OpenDialog1.FileName:= '';
  OpenDialog1.Execute;
  if (OpenDialog1.FileName<>'') then
  begin
      {
       Je�eli Plik D�wi�kowy Jest Na Dysku, To
       Wykonaj Instrukcje Po S�owie THEN.
      }
      MediaPlayer1.Close; // Zamkni�cie Urz�dzenia Odtwarzaj�cego D�wi�k

      // Pobiera Nazw� Pliku, Kt�ry Zostanie Otwarty, a P�niej Odtworzony
      MediaPlayer1.FileName:= OpenDialog1.FileName;

      MediaPlayer1.Open; // Otwiera Urz�dzenie Odtwarzaj�ce

      {
       Ustawienie Tej Metody "MediaPlayer1.Notify" na TRUE Spowoduje
       Wywo�anie Zdarzenia "OnNotify", Kt�re Umo�liwi Wychwycenie
       Momentu Zako�czenia Utworu.
      }
      MediaPlayer1.Notify:= TRUE;

      MediaPlayer1.Play; // Odtwarza Wczytany Plik Muzyczny
  end;
end;

procedure TForm1.MediaPlayer1Notify(Sender: TObject);
begin
  // MediaPlayer1Notify = OnNotify
  {
   Je�eli W�a�ciwo�� "NotifyValue" Jest R�wna
   Warto�ci "nvSuccessful", To Odtw�rz Utw�r Od Pocz�tku
  }
  if (MediaPlayer1.NotifyValue = nvSuccessful) then
     MediaPlayer1.Play
  else
     MediaPlayer1.Rewind;
end;

end.

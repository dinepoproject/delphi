{
 -- Podstawowy Kurs Delphi --
 Copyright(C)by Jan Biernat

Przyk�ad u�ycia komponentu Memo


Wprowadzenie:

Komponent Memo jest prostym edytorem tekstu,
kt�ry umo�liwia nam zapisanie i odczytanie
pliku tekstowego. Mo�na go r�wnie� u�ywa� do
wy�wietlania r�nych informacji.
}
unit UMemo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    Memo1: TMemo;
    bCzysc: TButton;
    bWczytajPlik: TButton;
    bZapiszPlik: TButton;
    bWlaczWylaczEdycje: TButton;
    procedure FormCreate(Sender: TObject);
    procedure bCzyscClick(Sender: TObject);
    procedure bWczytajPlikClick(Sender: TObject);
    procedure bZapiszPlikClick(Sender: TObject);
    procedure bWlaczWylaczEdycjeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  Memo1.Lines.Clear; // Wyczyszczenie zawarto�ci komponentu Memo
end;

procedure TForm1.bCzyscClick(Sender: TObject);
begin
  Memo1.Lines.Clear; // Wyczyszczenie zawarto�ci komponentu Memo
end;

procedure TForm1.bWczytajPlikClick(Sender: TObject);
begin
  Memo1.Lines.Clear; // Wyczyszczenie zawarto�ci komponentu Memo
  if (FileExists('Memo.txt') = TRUE) then // Je�eli plik istnie to nast�pi odczytanie tego pliku
  begin
    Memo1.Lines.LoadFromFile('Memo.txt'); // Odczytanie pliku tekstowego "Memo.txt"
  end;
end;

procedure TForm1.bZapiszPlikClick(Sender: TObject);
begin
  Memo1.Lines.SaveToFile('Memo.txt'); // Zapisanie do pliku tekstowego
end;

procedure TForm1.bWlaczWylaczEdycjeClick(Sender: TObject);
begin
  // W��cz/Wy��cz edycj�
  if (Memo1.ReadOnly = FALSE) then
  begin
    Memo1.ReadOnly:= TRUE; // Wy��cza mo�liwo�� edytowania
    Memo1.TabStop:= FALSE; // Wy��cza mo�liwo�� uaktywnienia za pomoc� klawisza TAB
    Memo1.Color:= clBtnFace; // Ustawia na kolor szary
  end
  else
  begin
    Memo1.ReadOnly:= FALSE; // W��cza mo�liwo�� edytowania
    Memo1.TabStop:= TRUE; // W��cza mo�liwo�� uaktywnienia za pomoc� klawisza TAB
    Memo1.Color:= clWindow; // Ustawia na kolor bia�y
  end;
end;

end.

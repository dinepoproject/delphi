{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad zastosowania komponentu MainMenu.

W celu dodania pozycji do menu wystarczy dwa
razy szybko klikn�� na komponent MainMenu lub
we w�a�ciwo�ciach okienka Object Inspector'a
wybra� w�asno�� Items.
}
unit UMenu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ComCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    Plik1: TMenuItem;
    PlikElementpierwszy: TMenuItem;
    PlikElementdrugi: TMenuItem;
    N1: TMenuItem;
    PlikWyjcie: TMenuItem;
    PlikRozwiniciemenu: TMenuItem;
    PlikRozwMenuElement1: TMenuItem;
    PlikRozwMenuElement2: TMenuItem;
    MainMenu2: TMainMenu;
    Menu2: TMenuItem;
    PlikDrugiemenu: TMenuItem;
    Ala1: TMenuItem;
    Atari1: TMenuItem;
    Menupierwsze1: TMenuItem;
    procedure PlikElementpierwszyClick(Sender: TObject);
    procedure PlikElementdrugiClick(Sender: TObject);
    procedure PlikRozwMenuElement1Click(Sender: TObject);
    procedure PlikRozwMenuElement2Click(Sender: TObject);
    procedure PlikWyjcieClick(Sender: TObject);
    procedure PlikDrugiemenuClick(Sender: TObject);
    procedure Menupierwsze1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.PlikElementpierwszyClick(Sender: TObject);
begin
  // Wywo�anie Elementu pierwszego z menu g��wnego
  ShowMessage(PlikElementpierwszy.Caption);
end;

procedure TForm1.PlikElementdrugiClick(Sender: TObject);
begin
  // Wywo�anie Elementu drugiego z menu g��wnego
  ShowMessage(PlikElementdrugi.Caption);
end;

procedure TForm1.PlikRozwMenuElement1Click(Sender: TObject);
begin
  // Wywo�anie Elementu pierwszego z podmenu
  ShowMessage(PlikRozwMenuElement1.Caption);
end;

procedure TForm1.PlikRozwMenuElement2Click(Sender: TObject);
begin
  // Wywo�anie Elementu drugiego z podmenu
  ShowMessage(PlikRozwMenuElement2.Caption);
end;

procedure TForm1.PlikWyjcieClick(Sender: TObject);
begin
  // Wyj�cie z programu
  Close;
end;

procedure TForm1.PlikDrugiemenuClick(Sender: TObject);
begin
  // Prze��cza si� na drugie menu
  Menu:= MainMenu2;
end;

procedure TForm1.Menupierwsze1Click(Sender: TObject);
begin
  // Prze��cza si� na pierwsze menu
  Menu:= MainMenu1;
end;

end.

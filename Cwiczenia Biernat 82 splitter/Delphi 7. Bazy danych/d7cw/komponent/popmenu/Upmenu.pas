{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad wykorzystania komponentu PopupMenu.

W celu dodania pozycji do menu wystarczy dwa
razy szybko klikn�� na komponent PopupMenu lub
we w�a�ciwo�ciach okienka Object Inspector'a
wybra� w�asno�� Items.

Pod��czenie PopupMenu:
  - znajd� we w�a�ciwo�ciach (okienko Object Inspector)
    w�asno�� PopupMenu;
  - rozwijaj�c strza�k� skierowan� w d� wybierz
    menu podr�czne, kt�re chcesz pod��czy�.
}
unit Upmenu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, StdCtrls;

type
  TForm1 = class(TForm)
    PopupMenu1: TPopupMenu;
    PopupMenu2: TPopupMenu;
    Button1: TButton;
    Element11: TMenuItem;
    Element21: TMenuItem;
    Element31: TMenuItem;
    pm2Element1: TMenuItem;
    pm2Element2: TMenuItem;
    pm2Elementrozwijajcy: TMenuItem;
    pm2reElement1: TMenuItem;
    pm2reElement2: TMenuItem;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure pm2reElement1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Programowe pod��czenie menu podr�cznego do formatki.
  PopupMenu:= PopupMenu1; 
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Uruchom PopupMenu
  PopupMenu2.Popup(Left+Button1.Left+2, Top+Button1.Top-35); // Wywo�anie menu podr�cznego
end;

procedure TForm1.pm2reElement1Click(Sender: TObject);
begin
  // Poka� okienko informacyjne.
  ShowMessage(pm2reElement1.Caption);
end;

end.

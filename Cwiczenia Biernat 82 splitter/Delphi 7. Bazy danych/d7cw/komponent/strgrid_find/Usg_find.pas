{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad szukania danych w komponencie StringGrid.
}
unit Usg_find;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    StringGrid1: TStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure StringGrid1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    function StringGridSearch(numCol: Integer; txtFind: String): Integer;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function TForm1.StringGridSearch(numCol: Integer; txtFind: String): Integer;
// Funkcja wskazuje znaleziony element
var
  TT, numLen: Integer;
     txtText: String;
begin
  // StringGridSearch
  StringGridSearch:= -1;

  {
   txtFind:= AnsiLowerCase(Trim(txtFind));
   Przypisanie warto�ci ze zmiennej 'txtFind' do tej samej
   zmiennej, po zlikwidowaniu zbytecznych spacji i zamianie
   wszystkich liter na ma�e
   ---------------------------------------------------
   AnsiLowerCase() - Zmniejszenie wszystkich liter
   Trim() - wyczyszczenie spacji po obu stronach
            wprowadzonego ci�gu znak�w
  }
  txtFind:= AnsiLowerCase(Trim(txtFind));

  
  // Obliczenie ilo�ci znak�w podanego ci�gu
  numLen:= 0;
  numLen:= Length(txtFind);

  {
   Przypisanie numeru kolumny, kt�ry
   podany jest w parametrze funkcji
  }
  StringGrid1.Col:= numCol;

  // Przypisanie nr 1 pierwszemu wierszowi
  StringGrid1.Row:= 1;

  // Wskazanie pierwszego elementu
  StringGrid1.CellRect(StringGrid1.Col, StringGrid1.Row);


  // Przeszukanie wszystkich element�w
  for TT:= 0 to StringGrid1.RowCount-1 do
  begin

    txtText:= ''; // Wyczyszczenie zmiennej

    txtText:= AnsiLowerCase(Trim(StringGrid1.Cells[numCol, 1+TT]));
    {
    txtText:= AnsiLowerCase(Trim(StringGrid1.Cells[numCol, 1+TT]));
     Przypisanie warto�ci ze zmiennej 'txtFind' do tej samej
     zmiennej, po zlikwidowaniu zbytecznych spacji i zamianie
     wszystkich liter na ma�e
     ---------------------------------------------------
     AnsiLowerCase() - Zmniejszenie wszystkich liter
     Trim() - wyczyszczenie spacji po obu stronach
              wprowadzonego ci�gu znak�w
    }

    if (Copy(txtText, 1, numLen) = txtFind) then
    begin
      {
       Je�eli element zostanie znaleziony, to zostan�
       wykonane instrukcje po konstrukcji if...then
      }

      {
       Przypisanie numeru wierszowi, w kt�rym
       jest znaleziony element
      }
      StringGrid1.Row:= 1+TT;

      // Wskazanie znalezionego elementu
      StringGrid1.CellRect(StringGrid1.Col, StringGrid1.Row);

      // Funkcja zwraca numer wiersza, w kt�ry zosta� wskazany
      StringGridSearch:= StringGrid1.Row;

      Break; // Przerwanie i opuszczenie p�tli
    end;

  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Wyczyszczenie zawarto�ci komponentu Edit
  Edit1.Text:= '';

  // Wprowadzenie danych do komponentu StringGrid

  // Nag��wki
  StringGrid1.Cells[0, 0]:= 'Imi�';
  StringGrid1.Cells[1, 0]:= 'Nazwisko';

  // Dane
  StringGrid1.Cells[0, 1]:= 'Jan';
  StringGrid1.Cells[1, 1]:= 'Biernat';

  StringGrid1.Cells[0, 2]:= 'Jan';
  StringGrid1.Cells[1, 2]:= 'Kowalski';

  StringGrid1.Cells[0, 3]:= 'Tadek';
  StringGrid1.Cells[1, 3]:= 'Kowalski';

  StringGrid1.Cells[0, 4]:= 'Mirek';
  StringGrid1.Cells[1, 4]:= 'Dalton';

  StringGrid1.Cells[0, 5]:= 'Ja�';
  StringGrid1.Cells[1, 5]:= 'Filutek';

  StringGrid1.Cells[0, 6]:= 'Ma�gosia';
  StringGrid1.Cells[1, 6]:= 'Piernik';

  StringGrid1.Cells[0, 7]:= 'Sta�';
  StringGrid1.Cells[1, 7]:= 'Wiatrak';

  StringGrid1.Cells[0, 8]:= 'Nel';
  StringGrid1.Cells[1, 8]:= 'Wojna';

  StringGrid1.Cells[0, 9]:= 'Maja';
  StringGrid1.Cells[1, 9]:= 'Listek';

  StringGrid1.Cells[0, 9]:= 'Damian';
  StringGrid1.Cells[1, 9]:= 'Listek';

  StringGrid1.Cells[0, 10]:= 'Darek';
  StringGrid1.Cells[1, 10]:= 'Listek';

  StringGrid1.Cells[0, 11]:= 'Mirek';
  StringGrid1.Cells[1, 11]:= 'Listek';

  StringGrid1.Cells[0, 12]:= 'Joasia';
  StringGrid1.Cells[1, 12]:= 'Pawlik';
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  // Wywo�anie funkcji wyszukuj�cej dane
  StringGridSearch(1, Edit1.Text);
end;

procedure TForm1.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // Przej�cie do komponentu StringGrid
  if (Key = VK_DOWN) or (Key = VK_UP) then StringGrid1.SetFocus;
end;

procedure TForm1.StringGrid1KeyPress(Sender: TObject; var Key: Char);
begin
  // Przej�cie do komponentu Edit
  Edit1.SetFocus;
end;

end.

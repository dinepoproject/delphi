{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad wyselekcjonowania liczb z wprowadzonego tekstu.
}
unit Uliczby;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    bOK: TButton;
    procedure FormCreate(Sender: TObject);
    procedure bOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function jbTylkoLiczby(txtString: String; chrComma: Char): String;
{
 Funkcja z wprowadzonego tekstu
 wybiera liczby i zwraca je jako tekst.
 Je�eli nie podamy �adnego tekstu, to
 funkcja zwr�ci liczb� 0 traktowan� jako tekst.
 W przypadku, gdy tekst nie zawiera liczb to
 funkcja nie zwr�ci �adnej liczby
 traktowanej jako tekst.

 Przyk�ad:
  1) wprowadzony tekst "-Przy123k�a-d4owy te5k6,st 7.8"
  2) Wyselekcjonowany tekst "-123456,78"
}
var
  txtText: String; // Zadeklarowanie zmiennej tekstowej
       AA: Integer; // Zadeklarowanie zmiennej liczbowej
begin
  // jbTylkoLiczby
  jbTylkoLiczby:= '0';
  txtString:= Trim(txtString);
  {
   Trim() - Likwiduje spacje (znaku puste) po
            obu stronach wprowadzonego tekstu
  }

  {
   Je�eli zmienna "txtString" zawiera ci�g znak�w
   to spe�niony jest warunek i zostan� wykonane
   instrukcje po s�owie IF...THEN
  }
  if (txtString<>'') then
  begin
    txtText:= ''; // Wyczyszczenie zmiennej tekstowej

    for AA:= 0 to Length(txtString) do
      if (txtString[AA] in ['0'..'9', chrComma]) then
        txtText:= txtText+txtString[AA];
    {
     FOR AA:= 0 TO Length(txtString) DO
     P�tla FOR...TO...DO jest wykonywane tyle
     razy ile jest znak�w w wprowadzonym tek�cie


     Length() - Oblicza z ilu znak�w
                sk�ada si� wprowadzony tekst

     
     IF (txtString[AA] in ['0'..'9', chrComma]) THEN
     Sprawdza czy znak jest zgodny zez znakami
     od 0 do 9 oraz znakiem reprezentuj�cym przecinek.


     txtText:= txtText+txtString[AA];
     Dodanie do zmiennej "txtText" znaku, w
     przypadku spe�nienia warunku
    }

    // Zwraca wyselekcjonowane liczby traktowane jako tekst
    jbTylkoLiczby:= Trim(txtText);

    {
     Sprawdza, czy znak minus by� na pocz�tku
     wprowadzonego tekstu. Je�eli tak to warunek
     jest spe�niony i nast�pi dodanie znaku
     minus na pocz�tku tekstu. Przyk�ad: -ab34cd -> -34
    }
    if (txtString[1] ='-') then jbTylkoLiczby:= '-'+Trim(txtText);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  {
   Wprowadzenie przyk�adowego tekstu
   zawieraj�cego r�wnie� liczby
  }
  Edit1.Text:= '-Przy123k�a-d4owy te5k6,st 7u8';
end;

procedure TForm1.bOKClick(Sender: TObject);
begin
  {
   Wywo�uje funkcj� do wyselekcjonowania
   liczb z wprowadzonego tekstu

   jbTylkoLiczby(Wprowad�_Tekst,
                 Znak_reprezentuj�cy_przecinek);
  }
  Edit1.Text:= jbTylkoLiczby(Edit1.Text, ',');
end;

end.

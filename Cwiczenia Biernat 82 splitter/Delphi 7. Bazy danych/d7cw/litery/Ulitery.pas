{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad wykorzystania zamiany liter na ma�e i du�e.
}
unit Ulitery;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function ZmianaZnakow(txtString: String; chrFind: Char;
      okSwitch: Shortint): String;
{
Funkcja zmienia tekst na tekst pisany literami
ma�ymi lub du�ymi albo ka�dy wyraz rozpoczyna
si� z wielkiej litery.
}

var // Inicjalizacja zmiennych
       TT: Integer;
  txtTemp: String;
begin
  // ZmianaZnakow
  txtString:= Trim(txtString);
  ZmianaZnakow:= txtString;
  if (txtString<>'') then
  begin
    {
     Funkcja b�dzie wykonana, je�eli zmienna
     'txtString' b�dzie zawiera�a jakie� dane.
     W przeciwnym przypadku funkcja nie b�dzie wykonana.
    }

    {
     Zamiana tekstu na ma�e litery.
     Zamiana ta dokonywana jest
     przez funkcj� 'AnsiLowerCase()'
    }
    if (okSwitch <= 1) then
      ZmianaZnakow:= AnsiLowerCase(txtString);

    {
     Zamiana tekstu na du�e litery.
     Zamiana ta dokonywana jest
     przez funkcj� 'AnsiUpperCase()'
    }
    if (okSwitch = 2) then
      ZmianaZnakow:= AnsiUpperCase(txtString);

    {
     Zamiana tekstu tak, aby wszystkie
     wyrazy rozpoczyna�y si� du�� liter�.
    }
    if (okSwitch = 3) then
    begin

      {
       Zamiana pierwszej litery w tek�cie na du��,
       reszta znak�w w tek�cie zostaje zamieniona na ma�e litery.
      }
      txtString:= AnsiUpperCase(Copy(txtString, 1, 1))+
                  AnsiLowerCase(Copy(txtString, 2, Length(txtString)-1));


      txtTemp:= ''; // Wyczyszczenie zmiennej 'txtTemp'

      {
       P�tla wykonywana jest tyle razy ile jest
       znak�w w tek�cie. Ilo�� znak�w w teksie
       obliczona jest za pomoc� funkcji 'Length()'
      }
      for TT:= 1 to Length(txtString) do
        if (txtString[TT-1] = CHR(32)) or
           (txtString[TT-1] = chrFind) then
          {
           Sprawdzenie czy znak na pozycji TT zmniejszonej
           o jeden jest r�wny znakowi spacji lub znakowi,
           kt�ry jest wprowadzony do zmiennej 'chrFind' w
           wywo�aniu funkcji. Je�eli warunek jest spe�niony
           tzn. znak le��cy na pozycji TT zmniejszonej o
           jeden b�dzie r�wny znakowi pustemu lub znakowi
           znajduj�cemu si� w zmiennej 'chrFind' to powi�ksz
           znak, kt�ry le�y na pozycji TT. Zmienna TT zawiera
           pozycj� kolejnego znaku w tek�cie.


           txtTemp:= txtTemp+AnsiUpperCase(txtString[TT])
           Dodanie do zmiennej 'txtTemp' kolejnego powi�kszonego znaku
          }
          txtTemp:= txtTemp+AnsiUpperCase(txtString[TT])
        else
          txtTemp:= txtTemp+txtString[TT];
          {
           txtTemp:= txtTemp+ txtString[TT]
           Dodanie do zmiennej 'txtTemp' bez zmian
           kolejnego znaku.
          }
      ZmianaZnakow:= txtTemp;

    end;

  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  {
   Wprowadzenie domy�lnego tekstu w trakcie
   tworzenia formatki do komponentu Edit1.
  }
  Edit1.Text:= 'Wprowad� jaki� tekst';
end;

procedure TForm1.RadioButton1Click(Sender: TObject);
begin
  // Zmienia tekst na ma�e litery.
  Edit1.Text:= ZmianaZnakow(Edit1.Text, '-', 1);
end;

procedure TForm1.RadioButton2Click(Sender: TObject);
begin
  // Zmienia tekst na du�e litery.
  Edit1.Text:= ZmianaZnakow(Edit1.Text, '-', 2);
end;

procedure TForm1.RadioButton3Click(Sender: TObject);
begin
  {
   Zmienia tekst na tekst, w kt�rym wszystkie
   wyrazy rozpoczynaj� si� z du�ej litery.
  }
  Edit1.Text:= ZmianaZnakow(Edit1.Text, '-', 3);
end;

end.

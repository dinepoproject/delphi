////////////////////////////////////////////////////////////////////////
//
// TAlignEdit is an improved TEdit component with alignment (that handles
// with left, center or right text justification), ColorOnFocus,
// ColorOnNoFocus, TextOnFocus, TextOnNoFocus, ExitOnReturn and Multiline
// properties
//
// (c) Jos� Mar�a Ferri
// Version 2.2, August 1998
//
// This component is freeware
// Any change to this component must be notified to the author:
//
//  jmferri@hotmail.com
//   or
//  etimoe@lobocom.es
//
//
// TAlignEdit es un componente TEdit mejorado, con las propiedades de
// alineaci�n (que soporta texto justificado a la izquierda, derecha o centro),
// ColorOnFocus, ColorOnNoFocus, TextOnFocus, TextOnNoFocus, ExitOnReturn y
// Multiline
//
// (c) Jos� Mar�a Ferri
// Versi�n 2.2, Agosto 1998
//
// Este componente es freeware
// Cualquier cambio a este componente debe ser notificado al autor:
//
//  jmferri@hotmail.com
//   o
//  etimoe@lobocom.es
//
////////////////////////////////////////////////////////////////////////
//
//  Ver. 2.1: Bug fixed:
//   The component was created in a multiline state but Multiline property was 'false'. This
//  bug didn't let the form to process 'key pressed' events, such as ESC key.
//  Thanks to
//  W. Chanon.
//
//  Ver. 2.2: Bug fixed:
//
//   Alignment only works if the control is a multiline control, so if multiline property is false,
//  alignment is always left :-(
//  Now, if multiline is false, in fact, it's true but it is simulated to be false :-)
//
//  Ver. 2.1: Errores solucionados:
//   El componente era creado en estado multilinea pero la propidad Multiline era 'false'. Este
//  error no dejaba al formulario procesar los eventos de 'teclas pulsadas', como la tecla ESC.
//  Gracias a
//  W. Chanon.
//
//  Ver. 2.2: Errores solucionados:
//
//   Alignment solo funciona si el control es multilinea, as� pues, si la propiedad multilinea es
//  falsa, la alineaci�n es siempre izquierda :-(
//  Ahora, si multilinea es falso, en realidad, es verdadera, pero se simula que es falsa :-)
//
////////////////////////////////////////////////////////////////////////
unit AlignEdit;

{$R-}

interface

uses Messages, Windows, SysUtils, Classes, StdCtrls, Controls, Graphics, Forms;

type

TAlignEdit = class (TCustomEdit)

private
    FAlignment: TAlignment;
    FColorOnFocus: TColor;
    FColorOnNoFocus: TColor;
    FFontOnFocus: TFont;
    FFontOnNoFocus: TFont;
    FExitOnReturn: Boolean;
    FFocused: Boolean;
    FMultiline: Boolean;
    FWordWrap:Boolean;
protected
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure CreateParams(var Params:TCreateParams); override;
    procedure DoEnter;override;
    procedure DoExit;override;
    procedure FontChanged(Sender: TObject);
    procedure KeyDown(var Key: Word; Shift: TShiftState);override; // ExitOnReturn
    procedure KeyPress(var Key: Char);override;                    // ExitOnReturn
    procedure SetAlignment(Value: TAlignment);
    procedure SetColorOnFocus(Value: TColor);
    procedure SetColorOnNoFocus(Value: TColor);
    procedure SetExitOnReturn(Value: Boolean);
    procedure SetFontOnFocus(Value: TFont);
    procedure SetFontOnNoFocus(Value: TFont);
    procedure SetFocused(Value: Boolean);
    procedure SetMultiline(Value: Boolean);
    procedure SetWordWrap(Value: Boolean);
published
// TEdit original

    property AutoSelect;
    property AutoSize;
    property BorderStyle;
    property CharCase;
//    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
//    property Font;
    property HideSelection;
    property MaxLength;
    property OEMConvert;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PasswordChar;

    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Text;
    property Visible;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
// Fin TEdit

    property Alignment: TAlignment read FAlignment write SetAlignment default taLeftJustify;
    property ColorOnFocus: TColor read FColorOnFocus write SetColorOnFocus;
    property ColorOnNoFocus: TColor read FColorOnNoFocus write SetColorOnNoFocus;
    property ExitOnReturn: boolean read FExitOnReturn write SetExitOnReturn default false;
    property FontOnFocus: TFont read FFontOnFocus write SetFontOnFocus;
    property FontOnNoFocus: TFont read FFontOnNoFocus write SetFontOnNoFocus;
    property MultiLine: Boolean read FMultiline write SetMultiline default false;
    property WordWrap: Boolean read FWordWrap write SetWordWrap default False;
public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
end;

procedure Register;

implementation

constructor TAlignEdit.Create(AOwner: TComponent);
begin
     inherited Create(AOwner);
     FColorOnFocus:=clWindow;
     FColorOnNoFocus:=clWindow;
     FFontOnFocus:=TFont.Create;
     FFontOnNoFocus:=TFont.Create;
     FFontOnNoFocus.OnChange:=FontChanged;
     FAlignment:=taLeftJustify;
     FMultiline:=false;
     FWordWrap:=false;
end;

procedure TAlignEdit.CreateParams(var Params: TCreateParams);
const Alignments: array[TAlignment] of Longint=(ES_LEFT,ES_RIGHT,ES_CENTER);
      WordWraps: array[Boolean] of LongInt = (0, ES_AUTOHSCROLL);
//      Multilines: array[Boolean] of LongInt = (0, ES_MULTILINE);
begin
     inherited CreateParams(Params);
//     if Multiline then
        Params.Style:=Params.Style and (not WordWraps[FWordWrap])
//                      or Multilines[Multiline]
                      or ES_MULTILINE
                      or Alignments[FAlignment];
end;

destructor TAlignEdit.Destroy;
begin
     FFontOnFocus.free;
     FFontOnNoFocus.free;
     inherited Destroy;
end;

procedure TAlignEdit.SetAlignment(Value: TAlignment);
begin
     if FAlignment <> Value then
     begin
          FAlignment := Value;
          RecreateWnd;
     end;
end;

procedure TAlignEdit.SetColorOnFocus(Value: TColor);
begin
     if not (csDesigning in ComponentState) and FFocused then
        Color:=value;
     FColorOnFocus:=value;
end;

procedure TAlignEdit.SetColorOnNoFocus(Value: TColor);
begin
     Color:=value;
     FColorOnNoFocus:=value;
end;

procedure TAlignEdit.SetFontOnFocus(Value: TFont);
begin
     if not (csDesigning in ComponentState) and FFocused then
        Font.Assign(value);
     FFontOnFocus.Assign(Value);
end;

procedure TAlignEdit.SetFontOnNoFocus(Value: TFont);
begin
     Font.Assign(Value);
     FFontOnNoFocus.Assign(Value);
end;

procedure TAlignEdit.DoEnter;
begin
     SetColorOnFocus(FColorOnFocus);
     SetFontOnFocus(FFontOnFocus);
     inherited;
end;

procedure TAlignEdit.DoExit;
begin
     SetColorOnNoFocus(FColorOnNoFocus);
     SetFontOnNoFocus(FFontOnNoFocus);
     inherited;
end;

procedure TAlignEdit.FontChanged(Sender: TObject);
begin
     Font.Assign(FFontOnNoFocus);
end;

procedure TAlignEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if (FExitOnReturn) then
     begin
          if (Key = VK_RETURN) then
             PostMessage(Handle, WM_KEYDOWN, VK_TAB, 0);
     end
     else
         if (key=VK_RETURN) and (not FMultiLine) then
         begin
              key:=0;
              beep;
         end
         else if (not FMultiline) and (key=VK_UP) then
              key:=VK_LEFT
         else if (not FMultiline) and (key=VK_DOWN) then
              key:=VK_RIGHT;

     inherited KeyDown(key,Shift);
end;

procedure TAlignEdit.KeyPress(var Key: Char);
begin
     if (FExitOnReturn) and (ord(Key) = VK_RETURN) then
           Key := char(VK_TAB)
     else if (not FMultiLine) and (ord(Key) = VK_RETURN) then
          key:=chr(0);
     inherited KeyPress(key);
end;

procedure TAlignEdit.SetFocused(Value: Boolean);
begin
     if FFocused <> Value then
     begin
          FFocused := Value;
          if (FAlignment <> taLeftJustify) then
             Invalidate;
   end;
end;

procedure TAlignEdit.CMEnter(var Message: TCMEnter);
begin
     SetFocused(True);
     if AutoSelect and not (csLButtonDown in ControlState) then
        SelectAll;
     inherited;
end;

procedure TAlignEdit.CMExit(var Message: TCMExit);
begin
     SetFocused(false);
     DoExit;
end;

procedure TAlignEdit.SetMultiline(Value: Boolean);
var
   i:integer;
   aux:string;
begin
     if Value <> FMultiline then
     begin
          if Value then
             SetExitOnReturn(false)
          else
          begin // eliminamos los INTRO
               aux:=text;
               i:=pos(chr(13),aux);
               while(i<>0) do
               begin
                    Delete(aux,i,2);
                    i:=pos(chr(13),aux);
               end;
               text:=aux;
               FWordWrap:=false;
          end;
          FMultiline:=Value;
          RecreateWnd;
     end;
end;

procedure TAlignEdit.SetExitOnReturn(Value: Boolean);
begin
     if FExitOnReturn <> Value then
     begin
          if FMultiline <> not Value then
             RecreateWnd;
          if Value then
               FMultiline:=false;
          FExitOnReturn := Value;
     end;
end;

procedure TAlignEdit.SetWordWrap(Value: Boolean);
begin
     if Value <> FWordWrap then
     begin
          FWordWrap := Value;
          SetMultiLine(true);
          RecreateWnd;
     end;
end;


procedure register;
begin
     RegisterComponents('Additional', [TAlignEdit]);
end;

end.

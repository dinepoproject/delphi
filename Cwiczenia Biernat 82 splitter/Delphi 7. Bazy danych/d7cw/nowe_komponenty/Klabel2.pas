{***********************************************************************
*  TKabel version 2.0                                                  *
*                                                                      *
*    Copyright � 1997, 1998 - All Rights Reserved.                     *
*    Developed By: Karlos Jorge Pinto                                  *
*    E-MAIL: Karlospinto@hotmail.com                                   *
*    Visit: o---> Delphi@pt <---o in:                                  *
*    http://www.terravista.pt/bilene/1412 ---> In English/Portuguese   *
*                                                                      *
************************************************************************
*  IRC: #portugal - Undernet. Nick: kArLoS.                            *
*  This is component is FREEWARE. *But* is Unregistered. If you like   *
*  To have a registered copy of this component please read the         *
*  Register.wri, for more details.                    Build n�:   2/98 *
***********************************************************************}
{
This is a 3D text component whit full scr.
Whit this component you may put in your aplications
a new style of write. Whit Recessed, Raised and Shadowed options.
Put a new look in your software.
}

unit Klabel2;

interface

uses
  SysUtils, windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Menus, DsgnIntf;

type
  T3DEffect = (Normal, Raised, Recessed, Shadowed);

  TKLabel2 = class(TLabel)
  private
    { Private declarations }
    FAbout: string;
    procedure setStyleEffect(Value : T3DEffect);
    procedure SetShadowColor(Value:TColor);
    procedure SetWhiteColor(Value:TColor);
    procedure DoDrawText(var Rect: TRect; Flags: Word);
    procedure SetFhOffSet(value: integer);
    procedure SetFvOffSet(value: integer);
    procedure SetShadeLT(value: boolean);
    procedure WMMouseMove(var msg: TWMMouseMove); message WM_MOUSEMOVE;
    procedure DoMouseOut(msg:TWMMouseMove);
  protected
    { Protected declarations }
    MouseOut:TNotifyEvent;
    F3DEffect : T3DEffect;
    FShadowColor : TColor;
    FWhiteColor : TColor;
    FLast : TColor;
    FhOffSet,FvOffSet : integer;
    FShadeLTSet : boolean;
    procedure Paint; override;
  public
    { Public declarations }
    procedure ShowAbout;Virtual;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property About: string read FAbout write FAbout stored False;
    property Align;
    property Caption;
    property CaptionStyle: T3DEffect read F3DEffect write setStyleEffect default Raised;
    property StyleColor: TColor read FShadowColor write SetShadowColor default clGray;
    property StyleColor2: TColor read FWhiteColor write SetWhiteColor default clWhite;
    property StyleShadeH: integer read FhOffSet write SetFhOffSet default 4;
    property StyleShadeV: integer read FvOffSet write SetFvOffSet default -4;
    property AShadeLTSet: boolean read FShadeLTSet write setShadeLT default true; // TIRAR DAQUI ESTA CENA
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Visible;
    property Transparent;
    property Width;
    property Top;
    property Left;
    property Height;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp; 
    property OnMouseOut:TNotifyEvent read MouseOut write MouseOut;
  end;
procedure Register;

implementation



type
  TAboutProperty = class(TPropertyEditor)
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
    function GetValue:string; override;
  end;

{/////////////////////////////////////////////////////////////////////////////}
{/////////////////////////////////////////////////////////////////////////////}


procedure TAboutProperty.Edit;
{call the 'About' dialog window when clicking on in the Object Inspector}
begin
  TKlabel2(GetComponent(0)).ShowAbout;
end;
function TAboutProperty.GetAttributes: TPropertyAttributes;
{set up to display a string in the Object Inspector}
begin
  GetAttributes := [paDialog, paReadOnly];
end;
function TAboutProperty.GetValue: String;
{set string to appear in the Object Inspector}
begin
  GetValue := '(About Klabel v.2.0)';
end;
procedure TKlabel2.ShowAbout;
var
	msg: string;
const
  cr = chr(13);
begin
  msg := '--==[ The KLabel a 3D Label. Version: 2.0 ]==--' + cr;
  msg := msg + 'TKlabel:' + ' a 16/32-Bit Component for Delphi' + cr;
  {$IFDEF WIN32}
  msg := msg + '(This is the 32 bit version)' + cr + cr;
  {$ELSE}
  msg := msg + '(This is the 16 bit version)' + cr + cr;
  {$ENDIF}
  msg := msg + 'Copyright � 1997/98 - All Rights Reserved' + cr;
  msg := msg + 'Made By: Karlos Pinto' + cr;
  msg := msg + '------------------------------------------------------------------------------------------------' + cr;
  msg := msg + '   Delphi@pt site: http://www.terravista.pt/bilene/1412' + cr;
  msg := msg + '             E-Mail: Karlospinto@hotmail.com' + cr;
  msg := msg + '------------------------------------------------------------------------------------------------' + cr;
  msg := msg + '                This Component is FREEWARE' + cr;
  msg := msg + 'But is Unregistered, for more details read the Register.wri file.' + cr;
  msg := msg + '     IRC: #portugal - Undernet, Nick: kArLoS :)' + cr;


    MessageDlg(msg,mtInformation,[MBOK],0);
end;

{/////////////////////////////////////////////////////////////////////////////}
{/////////////////////////////////////////////////////////////////////////////}

  procedure Register;
  begin
    RegisterComponents('Win32', [TKLabel2]);
    {register the 'About' property editor}
     RegisterPropertyEditor(TypeInfo(String), TKlabel2, 'About',
     TAboutProperty);

  end;
{########################################################################}
{########################################################################}



constructor TKlabel2.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Transparent := True;
  ParentColor := False;
  FShadowColor := clGray;
  FWhiteColor := clWhite;
  FhOffSet := 4;
  FvOffSet := -4;
  FLast := clWhite;
end;

destructor TKlabel2.Destroy;
{---------------------------------------}
begin
  inherited Destroy;
  
end;
 procedure TKlabel2.DoDrawText( var Rect : TRect; Flags : Word );
  var
    {Text       : array[ 0..255 ] of Char; }
    Text : PChar;
    Size: Byte;
    TmpRect    : TRect;
    UpperColor : TColor;
    LowerColor : TColor;
  begin
  Size := GetTextLen;
  Inc(Size);         
  GetMem(Text, Size);
  GetTextBuf(Text, Size);    
  {GetTextBuf(Text, SizeOf(Text)); }
    if ( Flags and DT_CALCRECT <> 0) and
       ( ( Text[0] = #0 ) or ShowAccelChar and
         ( Text[0] = '&' ) and
         ( Text[1] = #0 ) ) then
      StrCopy(Text, ' ');

    if not ShowAccelChar then
      Flags := Flags or DT_NOPREFIX;
    Canvas.Font := Font;

    if F3DEffect = Recessed then
    begin
      UpperColor := FShadowColor;
      LowerColor := FWhiteColor;
    end
    else
    begin
      UpperColor := FWhiteColor;
      LowerColor := FShadowColor;
    end;

    if F3DEffect in [ Recessed, Raised ] then
    begin
      TmpRect := Rect;
      OffsetRect( TmpRect, 1, 1 );
      Canvas.Font.Color := LowerColor;
      DrawText(Canvas.Handle, Text, StrLen(Text), TmpRect, Flags);

      TmpRect := Rect;
      OffsetRect( TmpRect, -1, -1 );
      Canvas.Font.Color := UpperColor;
      DrawText(Canvas.Handle, Text, StrLen(Text), TmpRect, Flags);
    end
    else if F3DEffect = Shadowed then
    begin
      TmpRect := Rect;
      OffsetRect( TmpRect, FhOffSet, FvOffSet );
      Canvas.Font.Color := LowerColor;
      DrawText(Canvas.Handle, Text, StrLen(Text), TmpRect, Flags);
    end;

    Canvas.Font.Color := Font.Color;
    if not Enabled then
      Canvas.Font.Color := clGrayText;
    DrawText(Canvas.Handle, Text, StrLen(Text), Rect, Flags);
    FreeMem(Text, Size); 
  end;


  procedure TKlabel2.Paint;
  const
    Alignments: array[TAlignment] of Word = (DT_LEFT, DT_RIGHT, DT_CENTER);
  var
    Rect: TRect;
  begin
    with Canvas do
    begin
      if not Transparent then
      begin
        Brush.Color := Self.Color;
        Brush.Style := bsSolid;
        FillRect(ClientRect);
      end;
      Brush.Style := bsClear;
      Rect := ClientRect;
      DoDrawText( Rect, ( DT_EXPANDTABS or DT_WORDBREAK ) or
                  Alignments[ Alignment ] );
    end;
  end;

procedure TKlabel2.SetShadowColor(value: TColor);
begin
  if not (FShadowColor = value) then
  begin
     FShadowColor := value;
     invalidate;
   end;
 end;

procedure TKlabel2.SetFhOffSet(value: integer);
begin
  if value<>FhOffSet then
  begin
    FhOffSet := value;
    invalidate;
  end;
end;

procedure TKlabel2.SetFvOffSet(value: integer);
begin
  if value<>FvOffSet then
  begin
    FvOffSet := value;
    invalidate;
  end;
end;

procedure TKlabel2.SetWhiteColor(value: TColor);
begin
  if not (FWhiteColor = value) and (FShadeLTSet=false) then
  begin
     FWhiteColor := value;
     invalidate;
   end;
 end;

 procedure TKlabel2.setStyleEffect(value: T3DEffect);
 begin
   if F3DEffect <> value then
   begin
     F3DEffect := value;
     invalidate;
   end;
 end;

procedure TKlabel2.SetShadeLT(value: boolean);
begin
  if  FShadeLTSet <> value then
  begin
    FShadeLTSet := value;
    if FShadeLTSet = true then
    begin
      FLast := FWhiteColor;
      FWhiteColor := clWhite;
    end
    else
      FWhiteColor := Flast;
   end;
  invalidate;
end;
procedure TKlabel2.WMMouseMove(var msg: TWMMouseMove);
begin
  if not MouseCapture then
    SetCaptureControl(Self);
  if MouseCapture and ((msg.XPos < 0) or (msg.YPos < 0) or (msg.XPos > Width) or
     (msg.YPos > Height)) then {MouseOut}
  begin
    SetCaptureControl(nil);
    DoMouseOut(msg);
  end;
end;

procedure TKlabel2.DoMouseOut(msg:TWMMouseMove);
begin
  if assigned(MouseOut) then MouseOut(self);
end;


end.

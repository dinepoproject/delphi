{*********************************************************************************
*       Copyright (C) 1997-1998 Fe Software & Development, All Rights Reserved.  *
*       Fe Software & Development is in the business of developing Delphi        *
*       business applications, and development tools.                            *
*       Fe Software & Development can be reached at (818) 838-1932,              *
*       via fax at (818) 838-1952, or via e-mail at info@fesoft.com              *
*                                                                                *
*       Visit our web site at http://www.fesoft.com                              *
*                                                                                *
*********************************************************************************}


{*********************************************************************************
*      This unit contains four components that are being made                    *
*      available free of charge with the source code.  If you use the            *
*      components contained in this unit to develop an application we            *
*      require that you mention our company name ( Fe Software & Development )   *
*      in your about box.                                                        *
*                                                                                *
*      LIMITATION OF WARRANTIES AND LIABILITY: This Unit is provided on an       *
*      "as is" basis, without warranties, or conditions, expressed or implied,   *
*      including but not limited to warranties of merchantable quality,          *
*      merchantability or fitness for a particular purpose, or those arising     *
*      by law, statute, usage of trade or course of dealing.  You assume the     *
*      entire risk as to the result and performance of this Unit.  Neither       *
*      we nor our dealers or suppliers shall have any liability to you or any    *
*      other person or entity for any indirect, incidental, special or           *
*      consequential damages whatsoever, including but not limited to loss of    *
*      revenue or profit, lost or damaged data or other commercial or economic   *
*      loss, even if we have been advised of the possibility to you, and that    *
*      of our dealers and suppliers shall not exceed the amount paid by you for  *
*      this Unit.  The limitations in this section shall apply whether or not    *
*      the alleged breach or default is a breach of a fundamental condition or   *
*      term, or a fundamental breach.                                            *
*                                                                                *
*                                                                                *
*                        **********  COMPONENTS *********                        *
*      TFEJpeg - This control is a decendent of TImage and only surfaces the     *
*      jpeg functionality if a TImage when combined with the jpeg unit           *
*      that comes with Delphi 3                                                  *
*                                                                                *
*********************************************************************************}


unit TJpgImg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, jpeg;

type

  TFEJpeg = class;
  TJPEGChangeEvent= procedure (Val : Byte) of object;

  TJPEGProperties = Class(TPersistent)
  private
    FGrayScale : Boolean;
    FPixelFormat : TJPEGPixelFormat;
    FProgressiveDisplay : Boolean;
    FPerformance  : TJPEGPerformance;
    FScale : TJPEGScale;
    FSmoothing : Boolean;
    FOnChange: TJPEGChangeEvent;
  protected
    procedure SetGrayScale(Value : Boolean);
    procedure SetPixelFormat(Value : TJPEGPixelFormat);
    procedure SetProgressiveDisplay(Value : Boolean);
    procedure SetPerformance(Value : TJPEGPerformance );
    procedure SetScale(Value : TJPEGScale);
    procedure SetSmoothing(Value : Boolean);
  public
    constructor Create;
    procedure Change(Val : Byte);
  published
    property GrayScale : Boolean REad FGrayScale write SetGrayScale;
    property PixelFormat : TJPEGPixelFormat read FPixelFormat write SetPixelFormat;
    property ProgressiveDisplay : Boolean read FProgressiveDisplay write SetProgressiveDisplay;
    property Performance : TJPEGPerformance read FPerformance write SetPerformance;
    property Scale : TJPEGScale read FScale write SetScale;
    property Smoothing : Boolean Read FSmoothing write SetSmoothing;
    property OnChange : TJPEGChangeEvent read FOnChange write FOnChange;
  end;

  TFEJpeg = class(TImage)
  private
    FExit : Boolean;
    FDrawing : boolean;
    FJPEGProperties : TJpegProperties;
    FOldChanged : TNotifyEvent;
    procedure PictureChanged(Sender : TObject);
    procedure UpdateJPEG(Flag : Byte);
  public
    procedure Paint;override;
    constructor create(AOwner : TComponent);override;
    destructor Destroy;override;
    procedure UpdateImage;
  published
    property JPEGProperties : TJPEGProperties read FJPEGProperties write FJPEGProperties;
  end;

procedure Register;

implementation

constructor TJPEGProperties.Create;
begin
  inherited Create;
  FGrayScale := false;
  FPixelFormat := jf24Bit;
  FProgressiveDisplay := false;
  FPerformance := jpBestQuality;
  FScale := jsFullSize;
  FSmoothing := true;
end;

procedure TJPEGProperties.Change(Val : Byte);
begin
  if Assigned(OnChange) then OnChange(val); 
end;

procedure TJPEGProperties.SetGrayScale(Value : Boolean);
begin
  if Value <> FGrayScale then
  begin
    FGrayScale := Value;
    Change(1);
  end;
end;
procedure TJPEGProperties.SetPixelFormat(Value : TJPEGPixelFormat);
begin
  if Value <> FPixelFormat then
  begin
    FPixelFormat := Value;
    Change(2);
  end;
end;
procedure TJPEGProperties.SetProgressiveDisplay(Value : Boolean);
begin
  if Value <> FProgressiveDisplay then
  begin
    FProgressiveDisplay := Value;
    Change(3);
  end;
end;
procedure TJPEGProperties.SetPerformance(Value : TJPEGPerformance );
begin
  if Value <> FPerformance then
  begin
    FPerformance := Value;
    Change(4);
  end;
end;
procedure TJPEGProperties.SetScale(Value : TJPEGScale);
begin
  if Value <> FScale then
  begin
    FScale := Value;
    Change(5);
  end;
end;
procedure TJPEGProperties.SetSmoothing(Value : Boolean);
begin
  if Value <> FSmoothing then
  begin
    FSmoothing := Value;
    Change(6);
  end;
end;


//  TJpeg

procedure TFEJpeg.Paint;
begin
  FDrawing := true;
  inherited Paint;
  FDrawing := false;
end;
constructor TFEJpeg.create(AOwner : TComponent);
begin
  inherited create(AOwner);
  FJPEGProperties := TJPEGProperties.Create;
  FJPEGProperties.OnChange := UpdateJPEG;
  FExit := false;
  FDrawing := false;
  FOldChanged := Picture.OnChange;
  Picture.OnChange := PictureChanged;
end;
destructor TFEJpeg.Destroy;
begin
  Picture.OnChange := nil;
  FOldChanged := nil;
  FJPEGProperties.Free;
  inherited Destroy;
end;
procedure TFEJpeg.PictureChanged(Sender : TObject);
begin
  if FExit then EXIT;
  if Picture.Graphic is TJPEGImage then with TJPEGImage(Picture.Graphic) do
  begin
    FEXit := true;
    PixelFormat := FJPEGProperties.PixelFormat;
    Scale := FJPEGProperties.Scale;
    Grayscale := FJPEGProperties.GrayScale;
    Performance := FJPEGProperties.Performance;
    ProgressiveDisplay := FJPEGProperties.ProgressiveDisplay;
    Smoothing := FJPEGProperties.Smoothing;
    FEXit := false;
  end;
  if Assigned(FOldChanged) then FOldChanged(Sender);
end;

procedure TFEJpeg.UpdateJPEG(Flag : Byte);
begin
  if Picture.Graphic is TJPEGImage then with TJPEGImage(Picture.Graphic) do
  begin
    case Flag of
      1  : Grayscale := FJPEGProperties.GrayScale;
      2  : PixelFormat := FJPEGProperties.PixelFormat;
      3  : ProgressiveDisplay := FJPEGProperties.ProgressiveDisplay;
      4  : Performance := FJPEGProperties.Performance;
      5  : Scale := FJPEGProperties.Scale;
      6  : Smoothing := FJPEGProperties.Smoothing;
      end;
    if Flag <> 3 then if Assigned(FOldChanged) then FOldChanged(Picture);
  end;
end;

procedure TFEJpeg.UpdateImage;
begin
  if Picture.Graphic is TJPEGImage then with TJPEGImage(Picture.Graphic) do
  begin
    Grayscale := FJPEGProperties.GrayScale;
    PixelFormat := FJPEGProperties.PixelFormat;
    ProgressiveDisplay := FJPEGProperties.ProgressiveDisplay;
    Performance := FJPEGProperties.Performance;
    Scale := FJPEGProperties.Scale;
    Smoothing := FJPEGProperties.Smoothing;
  end;
end;

procedure Register;
begin
  RegisterComponents('Win32', [TFEJpeg]);
end;

end.

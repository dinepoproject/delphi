{$D-}
unit FBrowse;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ShlObj;

type

  TFolderBrowse = class(TComponent)
  private
    FDirectory  : string;
    FTitle      : string;
    FBrowseInfo : TBrowseInfo;
  public
    function Execute( Form : TForm ) : Boolean;
  published
    property Directory : string read FDirectory write FDirectory;
    property Title : string read FTitle write FTitle;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Working', [TFolderBrowse]);
end;

function BrowseCallback(Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer stdcall;
begin
  Result := 0;
  if uMsg = BFFM_Initialized then
    with TFolderBrowse( lpData ) do
      if Length( Directory ) > 0 then
        SendMessage( Wnd , BFFM_SetSelection , 1 , Longint( PChar( Directory ) ) );
end;

function TFolderBrowse.Execute( Form : TForm ) : Boolean;
var
  Buffer : array [ 0..MAX_PATH ] of char;
  ItemIdList : PItemIDList;
begin
  Result := False;

  with FBrowseInfo do
    begin
      hwndOwner := Form.Handle;
      pidlRoot  := nil;
      pszDisplayName := Buffer;
      lpszTitle := PChar( FTitle );
      ulFlags := BIF_RETURNONLYFSDIRS;
      lpfn := BrowseCallback;
      lParam := Longint( Self );
    end;

  ItemIdList := ShBrowseForFolder( FBrowseInfo );

  if ItemIDList = nil then
    Exit;

  Result := SHGetPathFromIDList( ItemIDList , Buffer );
  FDirectory := Buffer;
end;

end.

{$A+} { Aligned record fields }
{$O+} { Code Optimization }
{*****************************************************************************
*
*LZCompress file compression component.
*----------------------------------
*
*Compresses a file with :
*------------------------
*
*    either the LZRW1/KH or LZH compression algorithm,
*           with code posted by Kurt Haenen on the SWAG (lzrw1).
*    or the Japanese LZH compression algorithm
*           ( LZSS coded by Haruhiko OKUMURA
*             Adaptive Huffman Coding coded by Haruyasu YOSHIZAKI
*             Edited and translated to English by Kenji RIKITAKE
*             Translated from C to Turbo Pascal by Douglas Webb   2/18/91
*             posted by Doug Webb on the SWAG (preskit2\lzh).)
*
*
**The Getblock/PutBlock procedures are based on the code in
*lzhtest.pas by Douglas Webb.
*
*The files lzh.pas and lzrw1kh.pas are essentially untouched
*(only some cosmetic changes, also added exceptions)
*
*--------------------------------------------------------------------
* V2.00.00 :
*
* Code for using a Stream  instead of a File added by Stefan Westner
*                          25 May 1997 (stefan.westner@stud.uni-bamberg.de)
* D.H. removed the seeks to the beginning of the stream (except for the auto guess)
* and the Steeam.Clear call, so that you have more freedom using TFileStream.
*                          30 May 1997 (Danny.Heijl@cevi.be)
* Danny Heijl added a "Threshold" property that dictates the behaviour of Advise.
*---------------------------------------------------------------------
* V2.1.0
* +=====================================================================================+
* | The original name of component I used as model and improve it (???) calles TLZRW132 |
* +=====================================================================================+
*
* I add OnProgress event in order to customize the events during reading/writing or
* compressing/decompressing;
* I added a new feature: ARCHIVE CABinet which allow you to store more archived file
* into a TTable(Paradox driver) as WinZip is doing. This table has following structure:
  +------------------------------------------------------------------+
  | NAME  | DATE  | TIME  | SIZE  | RATIO  | PACKED  | PATH  | BLOB  |
  +------------------------------------------------------------------+
  | String| TDate | TTime |Longint|  Byte  | Longint | String| TBlob |
  +------------------------------------------------------------------+
* Using this feature you can add/extract/delete/rename/view any file to/from archive cabinet
* Also, this implementation have the encription capability using Paradox encription style.
* Also I tried to improve little bit the interface for properties at design-time by
* adding ProperyEditors for selections' FilesInput/ArchiveOutput files.
*                          September 05, 1998 (Eugen Mihailescu)
*----------------------------------------------------------------------
*
* Feel free to use or give away this software as you see fit.
* Just leave the credits in place if you alter the source.
*
* This software is delivered to you "as is",
* no guarantees, it may blow up or trigger World War Three
* for all I know.
*
* If you find any bugs and let me know, I will try to fix them.
*
* I believe in programmers around the world helping each other
* out by distributing free source code.
*
* Eugen Mihailescu, September 05, 1998
* Bucharest, Romania
* eugen.mihailescu@scala.ro
*
*----------------------------------------------------------------
*****************************************************************}
unit LZCompress;

interface

uses
  WinTypes, WinProcs, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Lzrw1kh, Lzh, LZProp, DB, DBTables;
{$IFDEF WIN32}
type
  Int16 = SmallInt;
  SString = ShortString;
{$ELSE}
type
  Int16 = Integer;
{$ENDIF}

type
  ELzrw1Exception = class(Exception);
  TCompressMode = (Good, Fast, Auto);
  TProgressType = (ptNothing, ptReading, ptWriting, ptCompressing,
    ptUncompressing);
  TProgressStatus = procedure(Sender: TObject; DoingWhat: TProgressType;
    BytesIn, BytesOut: Longint; PercComplete: integer) of object;
  TLZArcOptions = (aoAllowOverride, aoAskIfOverride, aoPromptOnExtract,
    aoPromptOnDelete);

  TLZCompress = class(TComponent)
  private
    { Private declarations }
    OriginalSize: integer;
    CompressedSize: integer;
    FIn: TLZFilesList;
    FOut: TLZFileStr;
    fArchiveName: TLZFileStr;
    FCompressMode: TCompressMode;
    fFileSize: Longint;
    FUseStream: Boolean;
    FInputStream: TStream;
    FOutputStream: TStream;

    FThreshold: Integer; { autoguess "fast" threshold }
    fProgressStatus: TProgressStatus;
    //    F : File;
    fAbout: TLZAbout;

    procedure CheckWrite(Actual, Desired: Longint);
    procedure CheckRead(Actual, Desired: Longint);

    procedure GetBlock(var Target; NoBytes: Word; var Actual_Bytes: Word);
    procedure PutBlock(var Source; NoBytes: Word; var Actual_Bytes: Word);

    procedure LZrw1Compress;
    procedure LZrw1Decompress;
    procedure LZHCompress;
    procedure LZHDecompress;
  protected
    { Protected declarations }
    function CompressFile: Longint;
    function DeCompressFile: Longint;
    function GetBestMode: TcompressMode;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property InputStream: TStream read FInputStream write FInputStream;
    property OutputStream: TStream read FOutputStream write FOutputStream;
    function AddFiles(ArcFile: string; FilesList: TStrings; Options:
      TLZArcOptions): boolean;
    function ExtractFiles(ArcFile: string; FilesList: TStrings; Options:
      TLZArcOptions; ToPath: string): boolean;
    function DeleteFiles(ArcFile: string; FilesList: TStrings; Options:
      TLZArcOptions): boolean;
  published
    property OnProgress: TProgressStatus read fProgressStatus write
      fProgressStatus;
    function Compress: LongInt;
    function Decompress: Longint;
    function Advise: TcompressMode;
    property UseStream: Boolean read FUseStream write FUseStream
      default False;
    property InputFile: TLZFilesList read FIn write FIn;
    //    Property TempFile : TLZFileStr Read FOut Write FOut;
    property ArchiveName: TLZFileStr read fArchiveName write fArchiveName;
    property CompressMode: TCompressMode read FCompressMode
      write FcompressMode default Good;
    property Threshold: Integer read FThreshold
      write Fthreshold default 40;
    property About: TLZAbout read fAbout;
    { Published declarations }
  end;

procedure Register;

implementation

const
  LZRWIdentifier: LONGINT =
    ((((((ORD('L') shl 8) + ORD('Z')) shl 8) + ORD('R')) shl 8
    ) + ORD('W'));

  LZHIdentifier: LONGINT =
    ((((((ORD('L') shl 8) + ORD('Z')) shl 8) + ORD('H')) shl 8
    ) + ORD('!'));

  ChunkSize = 32768;
  IOBufSize = (ChunkSize + 16);

type

  LZHBuf = array[1..ChunkSize] of BYTE;
  PLZHBuf = ^LZHBuf;

var
  LZHInBuf, LZHOutBuf: PLZHBuf;
  SRCBuf, DSTBuf: BufferPtr; { defined in LZRW1KH }

  SrcFh, DstFh: Integer;
  SRCSize, DSTSize: LongInt;

  Tmp: Longint;
  Identifier: LONGINT;
  CompIdentifier: LONGINT;
  InSize, OutSize: LONGINT;

  Size: Longint;

  Buf: Longint; { getblock }
  PosnR: Word; { getblock }
  PosnW: Word; { putblock }

  ReadProc: TreadProc; { must be passed to LZHPACK/UNPACK }
  WriteProc: TWriteProc; { must be passed to LZHPACK/UNPACK }

constructor TLZCompress.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  { initialize defaults }
  FcompressMode := Good;
  FUseStream := False;
  FThreshold := 40; { 40 % autoguess default threshold }
end;

procedure TLZCompress.Loaded;
begin
  inherited Loaded;
  if (CsDesigning in ComponentState) then
  begin
    if assigned(fProgressStatus) then
      fProgressStatus(self, ptNothing, 0, 0, 0);
  end;
end;

{ the 2 execute methods : compress and decompress }
{-------------------------------------------------}

function TLZCompress.Compress: Longint;
begin
  Result := CompressFile; { compress stream/file }
end;


function TLZCompress.DeCompress: Longint;
begin
  Result := DeCompressFile; { decompress stream/file }
end;

{ the 3d execute method : advise compression method }
{---------------------------------------------------}

function TLZCompress.Advise: TcompressMode;
begin
  Result := GetBestMode; { get a guess on file/stream }
end;

{ some common subroutine functions }
{----------------------------------}
const
  SLZRW1FILERIO = 'Lzrw1 : Error reading from input file : ';
  SLZRW1STREAMRIO = 'Lzrw1 : Error reading from input stream !';
  SLZRW1FILEWIO = 'Lzrw1 : Error writing to output file : ';
  SLZRW1STREAMWIO = 'Lzrw1 : Error writing to output stream !';

  { Check if Write was successfull, raise an exception if not }

procedure TLZCompress.CheckWrite(Actual, Desired: Longint);
begin
  if (Actual <> Desired) then
  begin
    if FUseStream then
      raise ELzrw1Exception.Create(SLZRW1STREAMWIO)
    else
      raise ELzrw1Exception.Create(SLZRW1FILEWIO + string(FOut))
  end;
  Application.ProcessMessages;
end;

{ check if Read was successfull, raise an exception if not }

procedure TLZCompress.CheckRead(Actual, Desired: Longint);
begin
  if (Actual <> Desired) then
  begin
    if FUseStream then
      raise ELzrw1Exception.Create(SLZRW1STREAMRIO)
    else
      raise ELzrw1Exception.Create(SLZRW1FILERIO + string(FIn));
  end;
  Application.ProcessMessages;
end;

{ the LZH reader and writer procedures }
{--------------------------------------}

const
  SLZHIN = '(LZH) Bytes in : ';
  SLZHOUT = ' Bytes out : ';

  { the reader : GetBlock }

procedure TLZCompress.GetBlock(var Target; NoBytes: Word; var Actual_Bytes:
  Word
  );
begin
  if (PosnR > Buf) or (PosnR + NoBytes > SUCC(Buf)) then
  begin
    if PosnR > Buf then
    begin
      if not FUseStream then
        Buf := FileRead(SrcFh, LZHInBuf^, ChunkSize)
      else
        Buf := FInputStream.Read(LZHInBuf^, ChunkSize);
      if (Buf < 0) then
      begin
        if FUseStream then
          raise ELzrw1Exception.Create(SLZRW1STREAMRIO + ' (LZH)')
        else
          raise ELzrw1Exception.Create(SLZRW1FILERIO + string(FIn) +
            ' (LZH)');
      end;
      INC(InSize, Buf);
      Application.ProcessMessages;
      if assigned(fProgressStatus) then
        if fFileSize > 0 then
          fProgressStatus(self, ptReading, InSize, OutSize, Trunc(100 *
            InSize / fFileSize));
    end
    else
    begin
      Move(LZHInBuf^[PosnR], LZHInBuf^[1], Buf - PosnR);
      if not FUseStream then
        Tmp := FileRead(SrcFh, LZHInBuf^[Buf - PosnR], ChunkSize - (Buf
          - PosnR))
      else
        Tmp := FInputStream.Read(LZHInBuf^[Buf - PosnR], ChunkSize - (
          Buf - PosnR));
      if (Tmp < 0) then
      begin
        if FuseStream then
          raise ELzrw1Exception.Create(SLZRW1STREAMRIO + ' (LZH)')
        else
          raise ELzrw1Exception.Create(SLZRW1FILERIO + string(Fin) +
            ' (LZH)');
      end;
      Application.ProcessMessages;
      if assigned(fProgressStatus) then
        if fFileSize > 0 then
          fProgressStatus(self, ptReading, InSize, OutSize, Trunc(100 *
            InSize / fFileSize));
      INC(InSize, Tmp);
      Buf := Buf - PosnR + Tmp;
    end;

    if Buf = 0 then
    begin
      Actual_Bytes := 0;
      Exit;
    end;

    PosnR := 1;
  end;

  Move(LZHInBuf^[PosnR], Target, NoBytes);
  INC(PosnR, NoBytes);

  if PosnR > SUCC(Buf) then
    Actual_Bytes := NoBytes - (PosnR - SUCC(Buf))
  else
    Actual_Bytes := NoBytes;

end;

{ and the writer : PutBlock }

procedure TLZCompress.PutBlock(var Source; NoBytes: Word; var Actual_Bytes:
  Word
  );

begin
  if NoBytes = 0 then
  begin { Flush condition }
    if not FUseStream then
      Tmp := FileWrite(DstFh, LZHOutBuf^, PRED(PosnW))
    else
      Tmp := FOutputStream.Write(LZHOutBuf^, PRED(PosnW));
    CheckWrite(Tmp, PRED(PosnW));
    Inc(OutSize, Tmp);
    if assigned(fProgressStatus) then
      if fFileSize > 0 then
        fProgressStatus(self, ptWriting, InSize, OutSize, Trunc(100 *
          InSize / fFileSize));
    EXIT;
  end;
  if (PosnW > ChunkSize) or (PosnW + NoBytes > SUCC(ChunkSize)) then
  begin
    if not FUseStream then
      Tmp := FileWrite(DstFh, LZHOutBuf^, PRED(PosnW))
    else
      Tmp := FOutputStream.Write(LZHOutBuf^, PRED(PosnW));
    CheckWrite(Tmp, PRED(PosnW));
    Inc(OutSize, Tmp);
    PosnW := 1;
    if assigned(fProgressStatus) then
      if fFileSize > 0 then
        fProgressStatus(self, ptWriting, InSize, OutSize, Trunc(100 *
          InSize / fFileSize));
  end;
  Move(Source, LZHOUTBuf^[PosnW], NoBytes);
  INC(PosnW, NoBytes);
  Actual_Bytes := NoBytes;
end;

{ compress a file with LZRW1/KH (FAST) }
{--------------------------------------}

const
  SLZRW1KHIN = '(LZRW1/KH) Bytes in : ';
  SLZRW1KHOUT = ' Bytes out : ';
  SLZRW1KHSIZEEXC =
    'Lzrw1 (decompression LZRW1/KH) : Original and compressed sizes do not match !';

procedure TLZCompress.LZRW1Compress;
begin { start compressing }
  SRCSize := ChunkSize;
  InSize := 0;
  while (SRCSize = ChunkSize) do
  begin
      { read a block af data }
    if FUseStream then
      SrcSize := FInputStream.Read(SrcBuf^, ChunkSize)
    else
      SrcSize := FileRead(SrcFh, SrcBuf^, ChunkSize);

      { this fix is BAD, and not needed because    }
      { LZRW1KH.PAS handles the problem now        }
{    if (SrcSize = 0) then exit;}{      fix of bug in decompression discovered}
{                               }{      by <Domus@compuserve.com> }

    if (SrcSize < 0) then
    begin
      if FUseStream then
        raise ELzrw1Exception.Create(SLZRW1STREAMRIO + ' (LZRW1/KH)')
      else
        raise ELzrw1Exception.Create(SLZRW1FILERIO + string(Fin) +
          ' (LZRW1/KH)');
    end;
    Application.ProcessMessages;
    INC(InSize, SRCSize);
      { compress it }
    DSTSize := Compression(SRCBuf, DSTBuf, SRCSize);
      { write out compressed size }
    if not FUseStream then
      Tmp := FileWrite(DstFh, DstSize, SizeOf(Word))
    else
      Tmp := FOutputStream.Write(DstSize, SizeOf(Word));
    CheckWrite(Tmp, Sizeof(Word));
    INC(OutSize, Tmp);
      { write out compressed data }
    if not FUseStream then
      Tmp := FileWrite(DstFh, DstBuf^, DstSize)
    else
      Tmp := FOutputStream.Write(DstBuf^, DstSize);
    CheckWrite(Tmp, DstSize);
    INC(OutSize, Tmp);
    if assigned(fProgressStatus) and (fFileSize > 0) then
      fProgressStatus(self, ptCompressing, InSize, OutSize, Trunc(100 *
        InSize / fFileSize));

  end; { endwhile SRCSize = ChunkSize }
end;

{ compress a file with LZH (GOOD) }
{---------------------------------}

procedure TLZCompress.LZHCompress;
var
  Bytes_Written: Longint;
  Temp: Word;

begin
  ReadProc := GetBlock;
  WriteProc := PutBlock;

  { initialize put/getblock variables }
  Buf := 0;
  PosnR := 1;
  PosnW := 1;
  { pack the file with LZH }
  LZHPack(Bytes_written, ReadProc, WriteProc);

  { flush last buffer }
  PutBlock(Size, 0, Temp);

end;

{ decompress a file with LZRW1 (FAST) }
{-------------------------------------}

procedure TLZCompress.LZRW1Decompress;
var
  OrigSize: Longint;

begin
  { read in uncompressed filesize }
  if not FUseStream then
    Tmp := FileRead(SrcFh, OrigSize, sizeof(Longint))
  else
    Tmp := FInputStream.Read(OrigSize, sizeof(Longint));
  CheckRead(Tmp, Sizeof(Longint));
  Inc(InSize, SIZEOF(LONGINT));
  { start decompression }
  while (DSTSize = ChunkSize) do
  begin
      { read size of compressed block }
    if not FUseStream then
      Tmp := FileRead(SrcFh, SrcSize, SizeOf(Word))
    else
      Tmp := FInputStream.Read(SrcSize, SizeOf(Word));
    CheckRead(Tmp, Sizeof(Word));
      { read compressed block }
    if not FUseStream then
      Tmp := FileRead(SrcFh, SrcBuf^, SrcSize)
    else
      Tmp := FInputStream.Read(SrcBuf^, SrcSize);
    Checkread(Tmp, SrcSize);
    INC(InSize, Tmp + SIZEOF(WORD));
      { decompress block }
    DSTSize := Decompression(SRCBuf, DstBuf, SRCSize);
      { write it out }
    if FUseStream then
      Tmp := FOutputStream.Write(DstBuf^, DstSize)
    else
      Tmp := FileWrite(DstFh, DstBuf^, DstSize);
    CheckWrite(Tmp, DstSize);
    INC(OutSize, Tmp);
    if assigned(fProgressStatus) then
      if CompressedSize > 0 then
        fProgressStatus(self, ptUncompressing, InSize, OutSize, Trunc(100 *
          InSize / CompressedSize));
  end; { endwhile data to read }

  if (OutSize <> OrigSize) then
    raise ELzrw1Exception.Create(SLZRW1KHSIZEEXC);

end;

{ decompress a file with LZH (GOOD) }
{-----------------------------------}

const
  SLZHSIZEEXC =
    'Lzrw1 (LZH decompression) : Original and compressed sizes do not match !';

procedure TLZCompress.LZHDecompress;
var
  OrigSize: Longint;
  Temp: Word;

begin
  ReadProc := GetBlock;
  WriteProc := PutBlock;

  { read in uncompressed filesize }
  if FUseStream then
    Tmp := FInputStream.Read(OrigSize, sizeof(Longint))
  else
    Tmp := FileRead(SrcFh, OrigSize, sizeof(Longint));

  CheckRead(Tmp, Sizeof(Longint));
  Inc(InSize, SIZEOF(LONGINT));

  { initialize put/getblock variables }
  PosnR := 1;
  Buf := 0;
  PosnW := 1;
  { unpack the file with LZH }
  LZHUnPack(OrigSize, ReadProc, WriteProc);

  { flush last buffer }
  PutBlock(Size, 0, Temp);

  if (OutSize <> OrigSize) then
  begin
    raise ELzrw1Exception.Create(SLZHSIZEEXC);
  end;

end;

{ the main code common to both (de)compression methods  }
{-------------------------------------------------------}

const
  SNOMEMORY =
    'Lzrw1 (de)compression : Failed to get memory for I/O buffers !';
  SNOSIZESTREAM =
    'Lzrw1 compression : Failed to obtain the size of the input stream !';
  SNOSIZEFILE = 'Lzrw1 compression : Failed to obtain the size of : ';
  SINSTREAMERR =
    'Lzrw1 (de)compression : Failed to initialize input stream !';
  SOUTSTREAMERR =
    'Lzrw1 (de)compression : Failed to initialize output stream !';
  SINFILEERR = 'Lzrw1 (de)compression : Failed to open input file : ';
  SOUTFILEERR = 'Lzrw1 (de)compression : Failed to open output file : ';
  SINSTREAMFMT =
    'Lzrw1 decompression : input stream contains no valid header !';
  SINFILEFMT =
    ' : Lzrw1 decompression : this file has no valid header !';
  SINSTREAMSTATC = 'Lzrw1 : inputstream successfully compressed at ';
  SINFILESTATC = ' : Lzrw1 : successfully compressed at ';
  SINSTREAMSTATD = 'LZrw1 : inputstream successfully decompressed at ';
  SINFILESTATD = ' : Lzrw1 : successfully decompressed at ';

  { compress a file }
  {-----------------}

function TLZCompress.CompressFile: Longint;
var
  Infile: file;
  Mode: TcompressMode;

begin

  if (FcompressMode = Auto) then
    Mode := GetBestMode
  else
    Mode := FcompressMode;

  try
    Getmem(SRCBuf, IOBufSize);
    Getmem(DSTBuf, IOBufSize);
    LZHInBuf := PLZHBuf(SRCBuf);
    LZHOutBuf := PLZHBuf(DSTBuf);
  except
    raise ELzrw1Exception.Create(SNOMEMORY);
  end;


  if (Mode = Fast) then
    CompIdentifier := LZRWIdentifier
  else
    CompIdentifier := LZHIdentifier;

  try
    SrcFh := 0;
    DstFh := 0;
    try
      if FUseStream then
        Size := FInputStream.Size
      else
      begin
        AssignFile(Infile, string(Fin));
        Reset(Infile, 1);
        try
          Size := FileSize(Infile);
        finally;
          CloseFile(Infile);
        end;
      end;
      fFileSize := Size;
    except
      if FuseStream then
        raise ELzrw1Exception.Create(SNOSIZESTREAM)
      else
        raise ELzrw1Exception.Create(SNOSIZEFILE + string(Fin));
    end;

    try { try to open the streams/files }
      if FUseStream then
      begin
        if FInputStream.Seek(0, soFromCurrent) < 0 then
          raise ELzrw1Exception.Create(SINSTREAMERR);
        if FOutputStream.Seek(0, soFromCurrent) < 0 then
          raise ELzrw1Exception.Create(SOUTSTREAMERR);
      end
      else
      begin
        SrcFh := FileOpen(string(Fin), fmOpenRead);
        if (SrcFh < 0) then
          raise ELzrw1Exception.Create(SINFILEERR + string(Fin));
        DstFh := FileCreate(string(Fout));
        if (DstFh < 0) then
          raise ELzrw1Exception.Create(SOUTFILEERR + string(Fout));
      end;

      try { try to compress the file }
        { write out compression ID }
        if FUseStream then
          Tmp := FOutputStream.Write(CompIdentifier, sizeof(Longint))
        else
          Tmp := FileWrite(DstFh, CompIdentifier, sizeof(Longint));
        CheckWrite(Tmp, Sizeof(Longint));
        OutSize := SIZEOF(LONGINT);
        InSize := 0;
        { write out uncompressed filesize }
        if FUseStream then
          Tmp := FOutputStream.Write(Size, sizeof(Longint))
        else
          Tmp := FileWrite(DstFh, Size, sizeof(Longint));
        CheckWrite(Tmp, Sizeof(Longint));
        Inc(OutSize, SIZEOF(LONGINT));

        if (Mode = Fast) then
          LZRW1Compress
        else
          LZHCompress;

      except { error while compressing }
        { leave streams alone , delete outputfile }
        on Exception do
        begin
          if not FUseStream then
          begin
            FileClose(DstFH);
            DstFH := 0;
                { get rid of output file }
            SysUtils.DeleteFile(string(Fout));
            raise; { and reraise to inform user }
          end;
        end;
      end;

    finally
      if not FUseStream then
      begin
        if (SrcFh > 0) then FileClose(Srcfh);
        if (DstFh > 0) then FileClose(DstFh);
      end;
    end;

    {    If Visible Then
          Begin
            If FUseStream Then
              Caption := SINSTREAMSTATC + IntToStr ( ( Outsize * 100 ) Div Insize ) +
                '%'
            Else
              Caption := Fin + SINFILESTATC + IntToStr ( ( Outsize * 100 ) Div Insize
                 ) + '%';
            Update;
          End;}

  finally
    Freemem(SRCBuf, IOBufSize);
    Freemem(DSTBuf, IOBufSize);
  end;

  Result := OutSize;

end;


{ decompress a file }
{-------------------}

function TLZCompress.DeCompressFile: Longint;

begin

  try
    Getmem(SRCBuf, IOBufSize);
    Getmem(DSTBuf, IOBufSize);
    LZHInBuf := PLZHBuf(SRCBuf);
    LZHOutBuf := PLZHBuf(DSTBuf);
  except
    raise ELzrw1Exception.Create(SNOMEMORY);
    exit;
  end;

  try
    SrcFh := 0;
    DstFh := 0;

    try
      if FUseStream then
      begin
        if FInputStream.Seek(0, soFromCurrent) < 0 then
          raise ELzrw1Exception.Create(SINSTREAMERR);
        if FOutputStream.Seek(0, soFromCurrent) < 0 then
          raise ELzrw1Exception.Create(SOUTSTREAMERR);
      end
      else
      begin
        SrcFh := FileOpen(string(Fin), fmOpenRead);
        if (SrcFh < 0) then
          raise ELzrw1Exception.Create(SINFILEERR + string(Fin));
        DstFh := FileCreate(string(Fout));
        if (DstFh < 0) then
          raise ELzrw1Exception.Create(SOUTFILEERR + string(Fout));
      end;

      try
        { read compression ID }
        if FUseStream then
          Tmp := FInputStream.Read(Identifier, Sizeof(Longint))
        else
          Tmp := FileRead(SrcFh, Identifier, Sizeof(Longint));
        CheckRead(Tmp, Sizeof(Longint));
        if (Identifier <> LZRWIdentifier)
          and (Identifier <> LZHIdentifier) then
        begin
          if FUseStream then
            raise ELzrw1Exception.Create(SINSTREAMFMT)
          else
            raise ELzrw1Exception.Create(string(Fin) + SINFILEFMT);
        end;
        DSTSize := ChunkSize;
        InSize := SIZEOF(LONGINT);
        OutSize := 0;

        if (Identifier = LZRWIdentifier) then
          LZRW1Decompress
        else
          LZHDecompress;

        {        If Visible Then
                  Begin
                    If FUseStream Then
                      Caption := SINSTREAMSTATD + IntToStr ( ( Outsize * 100 ) Div Insize
                         ) + '%'
                    Else
                      Caption := Fin + SINFILESTATD + IntToStr ( ( Outsize * 100 ) Div
                        Insize ) + '%';
                    Update;
                  End;}

      except
        on Exception do
        begin
            { leave streams alone }
          if FuseStream then
          begin
            FileClose(DstFH);
            DstFH := 0;
                { get rid of output file }
            SysUtils.DeleteFile(string(Fout));
            raise;
          end;
        end;
      end;

    finally
      if not FUseStream then
      begin
        if (SrcFh > 0) then FileClose(Srcfh);
        if (DstFh > 0) then FileClose(DstFh);
      end;
    end;

  finally
    Freemem(SRCBuf, IOBufSize);
    Freemem(DSTBuf, IOBufSize);
  end;

  Result := OutSize;

end;

{ Guess the best compression mode }
{ returns Good or Fast }
const
  SGUESSSTRM = 'Guessing the best compression mode for the inputstream';
  SGUESSFILE = 'Guessing the best compression mode for : ';
  SGUESSMEMERR = 'Lzrw1 advise : Error getting I/O buffers';
  SGUESSSTRMSIZEERR = 'Lzrw1 advise : can not get size of inputstream !';
  SGUESSFILESIZEERR = 'Lzrw1 advise : can not get the size of : ';
  SGUESSSTRMOPENERR = 'Lzrw1 edvise : can not initialize the input stream!';
  SGUESSFILEOPENERR = 'Lzrw1 advise : can open input file : ';
  SGUESSSTRMRDERR = 'Lzrw1 advise : input stream read error !';
  SGUESSFILERDERR = 'Lzrw1 advise : read error on input file : ';
  SGUESSFAST = 'Lzrw1 recommends FAST compression : ';
  SGUESSGOOD = 'Lzrw1 recommends GOOD compression : ';

function TLZCompress.GetBestMode: TCompressMode;
var
  Infile: file;
  CompressedSize: Longint;
  UncompressedSize: Longint;
  SavedStreamPos: Longint;

begin

  Result := Good;

  if FUseStream then
  begin
      //      Caption := SGUESSSTRM;
    SavedStreamPos := FInputStream.Position;
  end
  else
    //    Caption := SGUESSFILE + Fin;

  try
    Getmem(SRCBuf, IOBufSize);
    Getmem(DSTBuf, IOBufSize);
  except
    raise ELzrw1Exception.Create(SGUESSMEMERR);
  end;

  try { use I/O buffers, finally release }
    SrcFh := 0;
    if FUseStream then
    begin
      try
        Size := FInputStream.Size;
      except
        raise ELzrw1Exception.Create(SGUESSSTRMSIZEERR);
      end;
    end
    else
    begin
      try { need the filesize to start, get it }
        AssignFile(Infile, string(Fin));
        Reset(Infile, 1);
        try
          Size := FileSize(Infile);
        finally;
          CloseFile(Infile);
        end;
        fFileSize := Size;
      except
        raise ELzrw1Exception.Create(SGUESSFILESIZEERR + string(Fin));
      end;
    end;

    if FUseStream then
    begin
      if FInputStream.Seek(0, soFromBeginning) < 0 then
        raise ELzrw1Exception.Create(SGUESSSTRMOPENERR);
    end
    else
    begin
      SrcFh := FileOpen(string(Fin), fmOpenRead);
      if (SrcFh < 0) then
      begin
        raise ELzrw1Exception.Create(SGUESSFILEOPENERR + string(Fin));
      end;
    end;

    try { try to use the inputfile, finally close }

      { small files can afford LZH }
      if (Size < (3 * ChunkSize)) then
      begin
        FCompressMode := Good;
        Result := Good;
          //          Caption := SGUESSGOOD + ' small file !!';
        exit;
      end;
      { try 2 blocks with fast at 1/3 and 2/3 of file }
      if FUseStream then
        FInputStream.Seek((Size div 3) and $7FFF8000, soFromBeginning)
      else
        FileSeek(Srcfh, (Size div 3) and $7FFF8000, soFromBeginning);

      if FUseStream then
      begin
        SrcSize := FInputStream.Read(SrcBuf^, ChunkSize);
        if (SrcSize < 0) then
        begin
          raise ELzrw1Exception.Create(SGUESSSTRMRDERR);
        end;
      end
      else
      begin
        SrcSize := FileRead(SrcFh, SrcBuf^, ChunkSize);
        if (SrcSize < 0) then
        begin
          raise ELzrw1Exception.Create(SGUESSFILERDERR + string(Fin));
        end;
      end;
      UncompressedSize := SrcSize;
      Application.ProcessMessages;
      CompressedSize := Compression(SRCBuf, DSTBuf, SrcSize);
      if FUseStream then
      begin
        FInputStream.Seek(((Size * 2) div 3) and $7FFF8000,
          soFromBeginning);
        SrcSize := FInputStream.Read(SrcBuf^, ChunkSize);
        if (SrcSize < 0) then
        begin
          raise ELzrw1Exception.Create(SGUESSSTRMRDERR);
        end;
      end
      else
      begin
        FileSeek(Srcfh, ((Size * 2) div 3) and $7FFF8000, soFromBeginning
          );
        SrcSize := FileRead(SrcFh, SrcBuf^, ChunkSize);
        if (SrcSize < 0) then
        begin
          raise ELzrw1Exception.Create(SGUESSFILERDERR + string(Fin));
        end;
      end;
      Inc(UncompressedSize, SrcSize);
      Application.ProcessMessages;
      Inc(CompressedSize, Compression(SRCBuf, DSTBuf, SRCSize));

      if (((UnCompressedSize * FThreshold) div 100) > CompressedSize) then
      begin
        Result := Fast;
          //          Caption := SGUESSFAST + IntToStr ( ( CompressedSize * 100 )
          //            Div UncompressedSize ) + '%.';
      end
      else
      begin
        Result := Good;
          //          Caption := SGUESSGOOD + IntToStr ( ( CompressedSize * 100 )
          //            Div UncompressedSize ) + '%.';
      end;

    finally
      if FUseStream then
        InputStream.Seek(SavedStreamPos, soFromBeginning)
      else
      begin
        if (SrcFh > 0) then FileClose(Srcfh);
      end;
    end;

  finally
    Freemem(SRCBuf, IOBufSize);
    Freemem(DSTBuf, IOBufSize);
  end;

end;

function TLZCompress.AddFiles(ArcFile: string; FilesList: TStrings; Options
  : TLZArcOptions): boolean;
var
  T: TTable;
  S: TStream;
  M: TMemoryStream;
  I: TSearchRec;
  k: integer;
  sPacked: longint;
  Size: longint;
begin
  //  result := false;
  T := TTable.Create(Application);
  try
    T.TableName := ArcFile;
    if not fileexists(ArcFile) then
    begin
      T.FieldDefs.Clear;
      T.FieldDefs.Add('Name', ftString, 254, false);
      T.FieldDefs.Add('DateTime', ftDateTime, 0, false);
      T.FieldDefs.Add('Size', ftInteger, 0, false);
      T.FieldDefs.Add('Ratio', ftInteger, 0, false);
      T.FieldDefs.Add('Packed', ftInteger, 0, false);
      T.FieldDefs.Add('File', ftParadoxOle, 0, false);
      T.TableType := ttParadox;
      T.CreateTable;
      if T.IndexDefs.IndexOf('Name') < 0 then
        T.AddIndex('Name', 'Name', [ixPrimary, ixUnique]);
    end;
    T.Active := true;
    for k := 0 to FilesList.Count - 1 do
    begin
      if FindFirst(FilesList.Strings[k], faAnyFile, I) = 0 then
      begin
        FIn := FilesList.Strings[k];
        FOut := extractfilepath(application.exename) + '\_temp.$$$';
        sPacked := Compress;
        M := TMemoryStream.Create;
        try
          M.LoadFromFile(FOut);
          if I.Size <> 0 then
            Size := Trunc(100 * sPacked / I.Size)
          else
            Size := 0;
          if T.FindKey([FilesList.Strings[k]]) then T.delete;
          T.AppendRecord([FilesList.Strings[k], FileDateToDateTime(
              FileAge(FilesList.Strings[k])), I.Size, Size, sPacked]);
          T.Edit;
          S := T.CreateBlobStream(T.Fields[5], bmWrite);
          S.CopyFrom(M, sPacked);
        finally
          M.Free;
          S.Free;
        end;
        T.Post;
      end;
      FindClose(I);
    end;
    T.Active := False;
    DeleteFile(extractfilepath(application.exename) + '\_temp.$$$');
  finally
    T.Free;
  end;
  result := true;
end;

function TLZCompress.ExtractFiles(ArcFile: string; FilesList: TStrings;
  Options: TLZArcOptions; ToPath: string): boolean;
var
  T: TTable;
  I: integer;
  S: TStream;
  M: TMemoryStream;
begin
  T := TTable.Create(Self);
  try
    T.TableName := ArcFile;
    T.IndexFieldNames := 'Name';
    T.Active := True;
    for i := 0 to FilesList.Count - 1 do
    begin
      if T.FindKey([FilesList.Strings[i]]) then
      begin
        OriginalSize := T.Fields[2].AsInteger;
        CompressedSize := T.Fields[4].AsInteger;

        if assigned(fProgressStatus) then
          if CompressedSize > 0 then
            fProgressStatus(self, ptReading, 0, 0, 0);

        S := T.CreateBlobStream(T.Fields[5], bmRead);
        M := TMemoryStream.Create;
        try
          M.CopyFrom(S, S.Size);
          M.SaveToFile(extractfilepath(application.exename) + '\_temp.$$$');
        finally
          M.Free;
          S.Free;
        end;
        FIn := extractfilepath(application.exename) + '\_temp.$$$';
        if ToPath[length(ToPath)] <> '\' then ToPath := ToPath + '\';
        FOut := ToPath + extractfilename(FilesList.Strings[i]);
        Decompress;
      end;
    end;
    T.Active := False;
  finally
    T.Free;
  end;
  DeleteFile(extractfilepath(application.exename) + '\_temp.$$$');
  result := true;
end;

function TLZCompress.DeleteFiles(ArcFile: string; FilesList: TStrings;
  Options: TLZArcOptions): boolean;
var
  i: integer;
  T: TTable;
begin
  T := TTable.Create(Self);
  try
    T.TableName := ArcFile;
    T.IndexFieldNames := 'Name';
    T.Active := true;
    for i := 0 to FilesList.Count - 1 do
    begin
      if T.FindKey([FilesList.Strings[i]]) then
        T.Delete;
    end;
    T.Active := false;
  finally
    T.Free;
  end;
end;

procedure Register;
begin
  RegisterComponents('Eugen''s Components', [TLZCompress]);
end;
end.



Unit LZProp;

Interface

Uses Windows, Forms, dsgnIntf, Dialogs;

Type
  TStr254 = String [ 254 ];
  TLZFileStr = Type TStr254;
  TLZFilesList = Type String;
  TLZAbout = Class ( TPropertyEditor )
  Public
    Procedure Edit;
      Override;
    Function GetAttributes : TPropertyAttributes;
      Override;
    Function GetValue : String;
      Override;
  End;

  TLZFileName = Class ( TStringProperty )
  Public
    Procedure Edit; Override;
    Function GetAttributes : TPropertyAttributes; Override;
  End;

  TLZFilesNames = Class ( TStringProperty )
  Public
    Procedure Edit; Override;
    Function GetAttributes : TPropertyAttributes; Override;
  End;

Procedure Register;

Implementation

Const
  CRLF              = #13#10;

Procedure TLZAbout.Edit;
Begin
  Application.MessageBox ( 'LZCompress for Borland Delphi 3.0' + CRLF +
    'Freeware Component and Source Code' + CRLF +
    'You have rights to use it or distribute it as freeware' + CRLF + CRLF +
    'For updates and technical support, ' + CRLF +
    'write to eugen.mihailescu@scala.ro' + CRLF + CRLF +
    'Source code in this package may be distributed ' + CRLF +
    'or modified without written consent.',
    'About LZCompress...', MB_OK + MB_ICONINFORMATION );
End;

Function TLZAbout.GetAttributes : TPropertyAttributes;
Begin
  Result := [ paMultiSelect, paDialog, paReadOnly ];
End;

Function TLZAbout.GetValue : String;
Begin
  Result := '[ About... ]';
End;

Function TLZFileName.GetAttributes : TPropertyAttributes;
Begin
  Result := [ paDialog, paMultiSelect, paAutoUpdate ];
End;

Procedure TLZFileName.Edit;
Var
  Editor            : TOpenDialog;

Begin
  Editor := TOpenDialog.Create ( Nil );
  Try
    editor.filename := GetStrValue;
    If editor.filename = '' Then
      editor.filename := '*.lzh';
    Editor.Filter :=
      'LZH Files|*.lzh|Data Files|*.Dat;*.Dta|All Files|*.*';
    Editor.FilterIndex := 0;
    If editor.execute Then
      SetStrValue ( editor.filename );
  Finally
    Editor.Free;
  End;
End;

Function TLZFilesNames.GetAttributes : TPropertyAttributes;
Begin
  Result := [ paDialog, paMultiSelect, paAutoUpdate ];
End;

Procedure TLZFilesNames.Edit;
Var
  Editor            : TOpenDialog;

Begin
  Editor := TOpenDialog.Create ( Nil );
  Try
    editor.Options := [ ofAllowMultiSelect, ofPathMustExist, ofFileMustExist ];
    editor.Files.CommaText := GetStrValue;
    If editor.filename = '' Then
      editor.filename := '*.*';
    Editor.Filter :=
      'All Files|*.*|Data Files|*.Dat;*.Dta';
    Editor.FilterIndex := 0;
    If editor.execute Then
      SetStrValue ( editor.Files.CommaText );
  Finally
    Editor.Free;
  End;
End;

Procedure Register;
Begin
  RegisterPropertyEditor ( TypeInfo ( TLZAbout ), Nil, '', TLZAbout );
  RegisterPropertyEditor ( TypeInfo ( TLZFileStr ), Nil, '', TLZFileName );
  RegisterPropertyEditor ( TypeInfo ( TLZFilesList ), Nil, '', TLZFilesNames );
End;

End.


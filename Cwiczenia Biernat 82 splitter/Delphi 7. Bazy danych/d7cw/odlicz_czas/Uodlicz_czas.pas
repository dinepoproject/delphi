{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad pokazuj�cy odliczanie czasu do ty�u.


Wprowadzenie:
Algorytm odliczaj�cy czas do ty�u jest przydatny, gdy
piszemy program do testowania i jest konieczne
wprowadzenie ograniczenia czasowego.
}
unit Uodlicz_czas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

{
Funkcja dodaje zero do pojedynczej liczby traktowanej
jako tekst, w innym przypadku zwraca dwie liczby.

Przyk�ad:
  1) 10 - zwr�ci nam liczb� dziesi�� traktowan� jako tekst
  2)  3 - zwr�ci nam liczb� 03 traktowan� jako tekst, przez dodanie zera przed liczb� trzy
}
function FillZero(txtZero: String): String;
begin
  FillZero:= txtZero;
  if (Length(txtZero) =1) then FillZero:= '0'+txtZero;
end;

function IleCzasuPozostalo(txtCzas: String): String;
{
 Funkcja zwraca czas jaki zosta�
 do zako�czenia odliczenia.
}
var
 // Deklaracja zmiennych
  numGodz, numMinut, numSekund: Shortint;
begin
  // IleCzasuPozostalo


  numGodz:= 0; // Wyzerowanie zmiennej

  // Przypisanie zmiennej GODZIN
  numGodz:= StrToInt(Copy(txtCzas, 1, 2));


  numMinut:= 0; // Wyzerowanie zmiennej

  // Przypisanie zmiennej MINUT
  numMinut:= StrToInt(Copy(txtCzas, 4, 2));


  numSekund:= 0; // Wyzerowanie zmiennej

  // Przypisanie zmiennej SEKUND
  numSekund:= StrToInt(Copy(txtCzas, 7, 2));

  // Liczenie...
  if (numSekund = 0) then
  begin
    {
     Je�eli zmienna 'numSekund' jest r�wna zeru
     to przypisz jej warto�� 59, w przeciwnym
     przypadku zmniejsz warto�� zmiennej
     'numSekund' o jeden.

     UWAGA:
     Tak samo jest w przypadku
     zmiennej 'numMinut' i 'numGodz'.
    }
    numSekund:= 59; // Przypisanie zmiennej warto�� 59

    if (numMinut = 0) then
    begin
      numMinut:= 59; // Przypisanie zmiennej warto�� 59

      if (numGodz = 0) then
        numGodz:= 23 // Przypisanie zmiennej warto�� 23
      else
        numGodz:= numGodz-1; // Zmniejszanie liczby GODZ. o warto�� 1

    end
    else
      numMinut:= numMinut-1; // Zmniejszanie liczby MINUT o warto�� 1

  end
  else
    numSekund:= numSekund-1; // Zmniejszanie liczby SEKUND o warto�� 1

  // Odliczanie...
  IleCzasuPozostalo:= FillZero(IntToStr(numGodz))+':'+
                      FillZero(IntToStr(numMinut))+':'+
                      FillZero(IntToStr(numSekund));
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Edit1.Text:= '00:02:00'; // Wprowadzenie czasu

  // Wy��czenie komponentu TIMER
  Timer1.Enabled:= FALSE;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Uruchomienie odliczania w ty�,
  // przez w��czenie komponentu TIMER
  Timer1.Enabled:= TRUE;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  // Wywo�anie funkcji odliczaj�cej czas do ty�u
  Edit1.Text:= IleCzasuPozostalo(Edit1.Text);
end;

end.

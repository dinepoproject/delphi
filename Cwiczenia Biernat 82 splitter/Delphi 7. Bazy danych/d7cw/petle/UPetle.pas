{
 -- Podstawowy Kurs Delphi --
 Copyright(c) by Jan Biernat

Przyk�ad u�ycia p�tli FOR, WHILE..DO, REPEAT...UNTIL


Wprowadzenie:

P�tle s�u�� do konstruowania blok�w instrukcji,
kt�re maj� si� powtarza� w spos�b kontrolowany z
g�ry zamierzon� ilo�� razy.
}
unit UPetle;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  TT, numSum: Integer;
begin
  // P�tla FOR
  {
   P�tla FOR wykonuje blok instrukcji z g�ry okre�lon� ilo�� razy.

   Przyk�ad z u�yciem p�tli FOR b�dzie przedstawia� obliczenie sumy.
   Wynik operacji p�dzie wy�wietlany na ekranie za pomoc� komponentu LABEL1.
  }

  numSum:= 0;
  for TT:= 0 to 9 do
  begin
    numSum:= numSum+1; // Liczy sum�
  end;
  Label1.Caption:= IntToStr(numSum); // Wy�wietl wynik operacji

  {
   IntToStr(TT) - zamienia zmienn� traktowan� jako liczb� na znak, kt�ry mo�na wy�wietli�.
  }
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  numSum: Integer;
begin
  // P�tla WHILE...DO
  {
   P�tla WHILE...DO powoduje wykonanie bloku instrukcji tak d�ugo,
   jak d�ugo spe�niony jest warunek. Warunek ten jest
   postawiony przed wykonaniem bloku instrukcji, tak wi�c
   mo�liwe jest, �e blok instrukcji uwarunkowanych
   warunkiem na pocz�tku mo�e nie by� wykonany ani razu.
   W przyk�adzie tym jest sumowana liczba.

   Gdy warunek jest spe�niony, tzn. zmienna "numSum" jest
   mniejsza ni� 10 to blok instrukcji jest wykonywany.
   Gdyby�my warunek zmienili z warunku "numSum < 10" na
   "numSum > 10" to p�tla nie zosta�yby wykonana, poniewa�
   warunek nie by�by spe�niony.
  }
  numSum:= 0;
  while (numSum < 10) do
  begin
    numSum:= numSum+1; // Liczy sum�
  end;
  Label1.Caption:= IntToStr(numSum); // Wy�wietl wynik operacji
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  numSum: Integer;
begin
  // P�tla REPEAT...UNTIL

  {
   W p�tli REPEAT...UNTIL warunek jest sprawdzany po
   wykonaniu bloku instrukcji, tak wi�c blok instrukcji
   jest wykonany przynajmniej jeden raz.

   Je�eli warunek jest spe�niony nast�puje przerwanie
   wykonywania p�tli, w przeciwnym przypadku blok
   instrukcji jest wykonywany a� do spe�nienia warunku.
  }
  numSum:= 0;
  repeat
    numSum:= numSum+1; // Liczy sum�
  until (numSum > 10);
  Label1.Caption:= IntToStr(numSum); // Wy�wietl wynik operacji
end;

end.

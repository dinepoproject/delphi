unit Upierwszy;
//M�j pierwszy program

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Label1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  Application.Title:= 'M�j pierwszy program';
  {
   Wpisanie nazwy programu, kt�ra b�dzie wy�witlana na
   pasku zada� zamiast nazwy domyslnej pobieranej z nazwy pliku.
  }
end;

procedure TForm1.Label1Click(Sender: TObject);
begin
  // Wypisanie s�owa "Do widzenie" po klikni�ciu na komponent LABEL.
  Label1.Caption:= 'Do widzenia';
end;

end.

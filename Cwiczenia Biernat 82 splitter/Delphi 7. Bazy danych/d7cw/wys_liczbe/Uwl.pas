{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad wy�wietlania liczby w formacie 1 234 567.
}
unit Uwl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    Edit1: TEdit;
    Label2: TLabel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  Edit1.Text:= ''; // Wyczyszczenie zawarto�ci komponentu Edit
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  // Wy�wietlenie w pasku tytu�owym zawarto�ci komponentu Edit.
  Caption:= FormatFloat('#,', StrToFloat(Edit1.Text));
  {
   FormatFloat('#,', Liczba)
   Funkcja s�u�y do formatowania wprowadzonej liczby jako parametr "Liczba".
   Dzi�ki tej funkcji liczba jest wy�wietlana w bardzo czytelny
   spos�b, tj. 1 234 567.
  }
end;

end.

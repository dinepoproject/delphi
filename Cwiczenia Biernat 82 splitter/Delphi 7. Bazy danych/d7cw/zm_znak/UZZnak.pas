{
 -- Podstawowy Kurs Delphi --
 Copyright(c)by Jan Biernat

Przyk�ad zamiany tekstu (ci�gu znak�w) na inny tekst.
}
unit Uzznak;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    Bevel1: TBevel;
    Label1: TLabel;
    Edit2: TEdit;
    Label2: TLabel;
    Edit3: TEdit;
    BitBtn1: TBitBtn;
    Label3: TLabel;
    Memo1: TMemo;
    Label4: TLabel;
    Edit1: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function jbZnajdzZamien(txtText, txtFind, txtReplace: String) :String;
var
  numPos, numLen: Integer; // Deklaracja zmiennych
begin
  // Funkcja zamienia ci�g znak�w na inny ci�g znak�w

  numLen:= 0;
  numLen:= Length(txtFind); // Obliczenie d�ugo�ci ci�gu znak�w

  {
   AnsiUpperCase()
   Zamiana liter (ci�gu znak�w) na du�e litery (ci�gu znak�w)
   -----------------------------------------------------------
   Pos(Szukany_Tekst, Tekst_w_Kt�rym_si�_Szuka)
   Zwraca pozycj� znalezionego ci�gu znak�w
  }
  while (Pos(AnsiUpperCase(txtFind), AnsiUpperCase(txtText)) > 0) do
  begin
    {
     P�tla jest wykonywana tak d�ugo, jak d�ugo b�dzie wyst�powa�
     wyszukiwany ci�g znak�w. W przypadku nie znalezienia szukanego
     ci�gu znak�w p�tla nie wykona si� ani razu.
    }

    // Podawanie pozycji znalezionego tekstu (ci�gu znak�w)
    numPos:= 0;
    numPos:= Pos(AnsiUpperCase(txtFind), AnsiUpperCase(txtText));

    // Usuni�cie znalezionego tekstu (ci�gu znak�w)
    Delete(txtText, numPos, numLen);

    // Wstawienie nowego tekstu (ci�gu znak�w) w miejsce starego
    Insert(txtReplace, txtText, numPos);
  end;
  jbZnajdzZamien:= txtText;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // FormCreate
  Edit1.Text:= 'To jest przyk�adowy tekst demonstruj�cy zamian� ci�gu ZNAK�W na inny ci�g ZNAK�W.';
  Edit2.Text:= 'Znak�w'; // Ci�g szukany
  Edit3.Text:= 'DELPHI'; // Ci�g nowy, kt�ry b�dzie wstawiony w miejsce starego ci�gu
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  // Wywo�anie funkcji zamieniaj�cej ci�gi znak�w.
  Memo1.Lines.Clear; // Wyczyszczenie komponentu Memo1

  // Dodanie efektu zamiany tekstu (ci�gu znak�w) do Memo1
  Memo1.Lines.Add(jbZnajdzZamien(
                      Edit1.Text,
                      Edit2.Text,
                      Edit3.Text
                      ));
end;

end.

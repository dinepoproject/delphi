object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 445
  ClientWidth = 766
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 8
    Top = 8
    Width = 621
    Height = 212
    TabOrder = 0
    OnDrawCell = StringGrid1DrawCell
    OnSelectCell = StringGrid1SelectCell
  end
  object ComboBox1: TComboBox
    Left = 29
    Top = 243
    Width = 145
    Height = 21
    TabOrder = 1
    Text = 'ComboBox1'
    OnClick = ComboBox1Click
    OnExit = ComboBox1Exit
    OnKeyPress = ComboBox1KeyPress
  end
end

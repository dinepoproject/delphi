object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Stw'#243'rz List'#281' Podr'#243#380'n'#261' '
  ClientHeight = 444
  ClientWidth = 1050
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clOlive
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 14
  object TLabel
    Left = 72
    Top = 400
    Width = 4
    Height = 14
  end
  object Splitter1: TSplitter
    AlignWithMargins = True
    Left = 516
    Top = 3
    Width = 6
    Height = 438
    Color = clHighlight
    ParentColor = False
    ExplicitLeft = 552
    ExplicitTop = 0
    ExplicitHeight = 444
  end
  object GroupBox2: TPanel
    AlignWithMargins = True
    Left = 528
    Top = 3
    Width = 519
    Height = 438
    Align = alClient
    Color = clHighlight
    ParentBackground = False
    TabOrder = 0
    OnResize = GroupBox2Resize
    DesignSize = (
      519
      438)
    object Label3: TLabel
      Left = 36
      Top = 65
      Width = 109
      Height = 23
      Caption = 'MOJA LISTA:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 44
      Top = 9
      Width = 70
      Height = 14
      Caption = 'WPROWAD'#377
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Edit3: TEdit
      Left = 17
      Top = 26
      Width = 257
      Height = 22
      TabOrder = 0
      OnChange = Edit3Change
      OnKeyUp = Edit3KeyUp
    end
    object Button10: TButton
      Left = 307
      Top = 6
      Width = 192
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'WPROWAD'#377' DO MOJEJ LISTY'
      TabOrder = 1
      OnClick = Button10Click
    end
    object Button3: TButton
      Left = 309
      Top = 37
      Width = 191
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'USU'#323' Z MOJEJ LISTY'
      TabOrder = 2
      OnClick = Button3Click
    end
    object ListBox2: TListBox
      Left = 5
      Top = 96
      Width = 508
      Height = 280
      Anchors = [akLeft, akTop, akRight, akBottom]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      MultiSelect = True
      ParentFont = False
      TabOrder = 3
    end
    object Button9: TButton
      Left = 183
      Top = 382
      Width = 156
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'ZAZNACZ WSZYSTKO'
      TabOrder = 4
      OnClick = Button9Click
    end
    object Button1: TButton
      Left = 182
      Top = 408
      Width = 156
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'DRUKUJ MOJ'#260' LIST'#280
      TabOrder = 5
      OnClick = Button1Click
    end
  end
  object GroupBox1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 507
    Height = 438
    Align = alLeft
    Color = clHighlight
    ParentBackground = False
    TabOrder = 1
    OnResize = GroupBox1Resize
    DesignSize = (
      507
      438)
    object Label2: TLabel
      Left = 43
      Top = 67
      Width = 174
      Height = 23
      Caption = 'LISTA PROPOZYCJI :'
      Color = clBackground
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label4: TLabel
      Left = 32
      Top = 8
      Width = 70
      Height = 14
      Caption = 'WPROWAD'#377
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object ListBox1: TListBox
      Left = 4
      Top = 96
      Width = 499
      Height = 283
      Anchors = [akLeft, akTop, akRight, akBottom]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      MultiSelect = True
      ParentFont = False
      TabOrder = 0
    end
    object Button4: TButton
      Left = 271
      Top = 9
      Width = 218
      Height = 25
      Caption = 'WPROWAD'#377' DO LISTY PROPOZYCJI'
      TabOrder = 1
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 271
      Top = 40
      Width = 218
      Height = 25
      Caption = 'USU'#323' Z LISTY PROPOZYCJI'
      TabOrder = 2
      OnClick = Button5Click
    end
    object Edit2: TEdit
      Left = 8
      Top = 28
      Width = 257
      Height = 22
      TabOrder = 3
      OnChange = Edit2Change
      OnKeyUp = Edit2KeyUp
    end
    object Button8: TButton
      Left = 151
      Top = 385
      Width = 146
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'ZAZNACZ WSZYSTKO'
      TabOrder = 4
      OnClick = Button8Click
    end
    object Button7: TButton
      Left = 72
      Top = 411
      Width = 327
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'PRZYWR'#211#262' DOMY'#346'LN'#260' LISTE PROPOZYCJI PROGRAMU'
      TabOrder = 5
      OnClick = Button7Click
    end
    object Button2: TButton
      Left = 271
      Top = 71
      Width = 218
      Height = 25
      Caption = '>>>DODAJ DO MOJEJ LISTY>>>'
      TabOrder = 6
      OnClick = Button2Click
    end
  end
end

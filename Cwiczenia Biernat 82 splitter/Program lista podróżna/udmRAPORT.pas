unit udmRAPORT;

interface

uses
  System.SysUtils, System.Classes, frxClass;

type
  Tdmraport = class(TDataModule)
    frxRAPORT1: TfrxReport;
    procedure frxRAPORT1NewGetValue(Sender: TObject; const VarName: string;
      var Value: Variant);
  private
  fMOJA_LISTA: string;
    { Private declarations }
  public
    { Public declarations }
    procedure PODGLAD;
    property moja_lista:STRING read Fmoja_lista write Fmoja_lista;
  end;

var
  dmraport: Tdmraport;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure Tdmraport.frxRAPORT1NewGetValue(Sender: TObject;
  const VarName: string; var Value: Variant);
begin
if SameText(VarName, 'moja_lista') then
Value := moja_lista;
end;

procedure Tdmraport.PODGLAD;
begin
frxRAPORT1.ShowReport;
end;

end.

unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Winapi.ShellAPI, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
 Label1.Cursor:= crHandPoint;
 Label1.Font.Color:= clBlue;
 Label1.Font.Style:= [fsUnderline];
 Label1.Caption:= 'http://www.onet.pl';
  Label2.Cursor:= crHandPoint;
 Label2.Font.Color:= clBlue;
 Label2.Font.Style:= [fsUnderline];
 Label2.Caption:= 'danhiel@op.pl';

end;

procedure TForm1.Label1Click(Sender: TObject);
begin
ShellExecute(GetDesktopWindow,'open',PChar(Label1.Caption),nil,nil,SW_SHOWNORMAL);

end;

procedure TForm1.Label2Click(Sender: TObject);
begin
 ShellExecute(GetDesktopWindow,'open',PChar('mail to:' + Label1.Caption),nil,nil,SW_SHOWNORMAL);
end;

end.

unit UMigajacyPrzycisk;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Button1: TButton;
    Button2: TButton;
    Timer2: TTimer;
    Button3: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button2Click(Sender: TObject);
begin
Timer1.Enabled:= True;
Timer2.Enabled:= True;
 Button1.Caption:= 'Zacznij od poczatku';
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
 Timer1.Enabled:= false;
Timer2.Enabled:= false;
Button1.Caption:= 'OK!'
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
Button1.Enabled:= true;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin
if Button1.Enabled = True then   Button1.Enabled:= False;

end;

end.

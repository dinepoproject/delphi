unit U_TVsamaclas;

interface

type
  T_sama_class = class(TObject)
  private
    rozmiar: Integer;
    nazwa: string;

    wlacz: Boolean;
    function odczytrozmiar: integer;
    function odczytnazwa: string;
    procedure ustawTV(value: Boolean);
  public
    property odczyt_rozmiar: integer read odczytrozmiar;
    property odczyt_nazwa: string read odczytnazwa;
    property setonoff: Boolean write ustawTV;
    constructor Create(arozmiar: Integer; anazwa: string);
    destructor Destroy; override;

    // w poniedzia�ek obejrzec film o klasach javy i przerobic
    // wlasciwosci poniewaz tak jak w javie nie dzia�a.
  end;

implementation

{ T_sama_class }

constructor T_sama_class.Create(arozmiar: Integer; anazwa: string);
begin
  arozmiar := rozmiar;    // trzeba to zmienic tutaj przypisac wlasciwosciom
  anazwa := nazwa;
end;

destructor T_sama_class.Destroy;
begin

  inherited;
end;

function T_sama_class.odczytnazwa: string;
begin
  Result := nazwa;
end;

function T_sama_class.odczytrozmiar: integer;
begin
  Result := rozmiar;
end;

procedure T_sama_class.ustawTV(value: Boolean);
begin
  value := wlacz;
end;

end.


object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'SPRAWD'#377' '
  ClientHeight = 423
  ClientWidth = 782
  Color = clActiveCaption
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 782
    Height = 97
    Align = alTop
    Color = clRed
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 778
    object Label1: TLabel
      Left = 32
      Top = 16
      Width = 226
      Height = 25
      Caption = ' TONA'#379' OG'#211#321'EM Z SAP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 40
      Top = 56
      Width = 145
      Height = 25
      Caption = 'DOBA OG'#211#321'EM'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 504
      Top = 56
      Width = 50
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 97
    Width = 782
    Height = 168
    Align = alTop
    Color = clRed
    ParentBackground = False
    TabOrder = 1
    ExplicitWidth = 778
    object Label4: TLabel
      Left = 32
      Top = 24
      Width = 152
      Height = 25
      Caption = 'TONA'#379' Z GIDEX'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 32
      Top = 72
      Width = 218
      Height = 25
      Caption = 'DOBA BEZ ODBIOR'#211'W'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 504
      Top = 128
      Width = 50
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 32
      Top = 121
      Width = 776
      Height = 25
      Caption = 'RAZEM EXPORT I AMP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 265
    Width = 782
    Height = 160
    Align = alTop
    Color = clRed
    ParentBackground = False
    TabOrder = 2
    ExplicitWidth = 778
    object Label8: TLabel
      Left = 32
      Top = 16
      Width = 173
      Height = 25
      Caption = 'WAGONY Z GIDEX'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 32
      Top = 60
      Width = 351
      Height = 25
      Caption = 'WAGONY Z RAPORTU EXPORT I AMP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 32
      Top = 104
      Width = 235
      Height = 25
      Caption = 'WAGONY Z RAPORTU P4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 504
      Top = 120
      Width = 41
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
end

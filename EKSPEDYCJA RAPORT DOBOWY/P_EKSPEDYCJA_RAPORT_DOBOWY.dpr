program P_EKSPEDYCJA_RAPORT_DOBOWY;

uses
  Vcl.Forms,
  U_EKSPEDYCJA_RAPORT_DOBOWY in 'U_EKSPEDYCJA_RAPORT_DOBOWY.pas' {Form2},
  OKIENKO_SPRAWDZ in 'OKIENKO_SPRAWDZ.pas' {Form1},
  OKIENKO_LOGOWANIA in 'OKIENKO_LOGOWANIA.pas' {Form3},
  U_Data_Module in 'U_Data_Module.pas' {DataModule1: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.Run;
end.

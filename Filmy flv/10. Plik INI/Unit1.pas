unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  INIFiles;
{type
 Typdane = record

 Imie : string;
 Nazwisko : string;
 Wiek : Integer;
 Email : string;
 end; }

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    TrackBar1: TTrackBar;
    Button1: TButton;
    Label4: TLabel;
    Edit3: TEdit;
    Label5: TLabel;
    Edit4: TEdit;
    Edit2: TEdit;
    Label6: TLabel;
    Button2: TButton;
    Button3: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Dane: array of record
    Imie: string;
    Nazwisko: string;
    Wiek: Integer;
    Email: string;
  end;
  cyfra_startu_tablicy: Integer;
  cyfra_konca_tablicy: Integer;

implementation

{$R *.dfm}
procedure Odswierz();
var
  D: Integer;
begin
  D := StrToIntDef(Form1.edit4.Text, 0);
  //Form1.edit4.Text:=  IntToStr(Dane [cyfra_startu_tablicy].Wiek);
  Form1.edit1.Text := Dane[cyfra_startu_tablicy].Imie;
  Form1.edit2.Text := Dane[cyfra_startu_tablicy].Nazwisko;
  Form1.edit3.Text := Dane[cyfra_startu_tablicy].Email;
  Form1.edit4.Text := IntToStr(Dane[cyfra_startu_tablicy].Wiek);
  Form1.Label6.Caption := IntToStr(cyfra_startu_tablicy + 1);

end;

procedure Odswiez;
begin
  if (cyfra_startu_tablicy > (cyfra_konca_tablicy - 1)) then
    cyfra_startu_tablicy := (cyfra_konca_tablicy - 1);
  if cyfra_startu_tablicy < 0 then
    cyfra_startu_tablicy := 0;
  if cyfra_konca_tablicy > 0 then
  begin
    Form1.TrackBar1.Max := cyfra_konca_tablicy - 1;
    Form1.TrackBar1.Position := cyfra_startu_tablicy;
    Odswierz;
  end;

  Form1.Refresh;

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  cyfra_konca_tablicy := cyfra_konca_tablicy + 1; //1
  setlength(Dane, cyfra_konca_tablicy);
  cyfra_startu_tablicy := cyfra_konca_tablicy - 1; //0
  TrackBar1.Max := cyfra_konca_tablicy - 1;  //0
  Odswierz;

end;

procedure TForm1.Button2Click(Sender: TObject);
var
  PlikINI: TIniFile;
  cyfra_petli: Integer;
begin
  PlikINI := TIniFile.Create('C:\Test.ini');
  try
    for cyfra_petli := 0 to cyfra_konca_tablicy - 1 do
    begin
      PlikINI.WriteString('Record' + IntToStr(cyfra_petli + 1), 'Imie', Dane[cyfra_petli].Imie);
      PlikINI.WriteString('Record' + IntToStr(cyfra_petli + 1), 'Nazwisko', Dane[cyfra_petli].Nazwisko);
      PlikINI.WriteInteger('Record' + IntToStr(cyfra_petli + 1), 'Wiek', Dane[cyfra_petli].Wiek);
      PlikINI.WriteString('Record' + IntToStr(cyfra_petli + 1), 'Email', Dane[cyfra_petli].Email);

    end;
  finally
    PlikINI.Free;
    Odswiez;
    ShowMessage('Zapisano rekordy');
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  PlikINI: TIniFile;
begin
  cyfra_konca_tablicy := 1;
  cyfra_startu_tablicy := 0;
  PlikINI := TIniFile.Create('C:\Test.ini');

  try
    repeat
      SetLength(Dane, cyfra_konca_tablicy);
      Dane[cyfra_startu_tablicy].imie := PlikINI.ReadString('Record' + IntToStr(cyfra_startu_tablicy + 1), 'Imie', '*END*');
      Dane[cyfra_startu_tablicy].Nazwisko := PlikINI.ReadString('Record' + IntToStr(cyfra_startu_tablicy + 1), 'Nazwisko', '*END*');
      Dane[cyfra_startu_tablicy].Wiek := PlikINI.ReadInteger('Record' + IntToStr(cyfra_startu_tablicy + 1), 'Wiek', -1);
      Dane[cyfra_startu_tablicy].Email := PlikINI.ReadString('Record' + IntToStr(cyfra_startu_tablicy + 1), 'Email', '*END*');
      cyfra_konca_tablicy := cyfra_konca_tablicy + 1;
      cyfra_startu_tablicy := cyfra_startu_tablicy + 1;
    until Dane[cyfra_startu_tablicy - 1].Imie = '*END*';
    //end to wartosc domyslna ktora wskakuje kiedy nie ma nic w kluczu
    // metody odczytujabe pozwalaja wpisanie wartosci domyslnej w przypadku braku wartosci
    // przypisuje wiec end i konczy petle.
  finally
    cyfra_konca_tablicy := cyfra_konca_tablicy - 2;
    cyfra_startu_tablicy := cyfra_startu_tablicy - 1;
    SetLength(Dane, cyfra_konca_tablicy);
    TrackBar1.Max := cyfra_konca_tablicy - 1;
    TrackBar1.Position := cyfra_startu_tablicy;
    PlikINI.Free;
    ShowMessage('Wczytano rekordy' + IntToStr(cyfra_konca_tablicy));
  end;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  Dane[cyfra_startu_tablicy].Imie := Edit1.Text;
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
  Dane[cyfra_startu_tablicy].Nazwisko := Edit2.Text;

end;

procedure TForm1.Edit3Change(Sender: TObject);
begin
  Dane[cyfra_startu_tablicy].Email := Edit3.Text;
end;

procedure TForm1.Edit4Change(Sender: TObject);
begin
  Dane[cyfra_startu_tablicy].Wiek := StrToInt(Edit4.Text);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  setlength(Dane, 1);
  cyfra_konca_tablicy := 1;
  cyfra_startu_tablicy := 0;
  odswierz;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  cyfra_startu_tablicy := TrackBar1.Position;
  odswierz;
end;

end.


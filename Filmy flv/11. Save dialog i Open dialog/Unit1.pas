unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, INIFiles;
{type
 Typdane = record

 Imie : string;
 Nazwisko : string;
 Wiek : Integer;
 Email : string;
 end; }

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    TrackBar1: TTrackBar;
    Button1: TButton;
    Label4: TLabel;
    Edit3: TEdit;
    Label5: TLabel;
    Edit4: TEdit;
    Edit2: TEdit;
    Label6: TLabel;
    Button2: TButton;
    Button3: TButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Dane : array of Record
                   Imie : string;
                   Nazwisko : string;
                   Wiek : Integer;
                   Email : string;
                    end;
  Numer : Integer;
  Ile : Integer;

implementation

{$R *.dfm}
 procedure Odswierz();
 var
    D: Integer;
 begin
 D:= StrToIntDef(Form1.edit4.Text,0);
  Form1.edit4.Text:=  IntToStr(Dane [Numer].Wiek);
  Form1.edit1.Text:=  Dane [Numer].Imie;
  Form1.edit2.Text:=  Dane [Numer].Nazwisko;
  Form1.edit3.Text:=  Dane [Numer].Email;
  Form1.edit4.Text:=  IntToStr(Dane [Numer].Wiek);
  Form1.Label6.Caption := IntToStr(Numer + 1);


 end;

 procedure Odswiez;
  begin
  if  (Numer > (ile-1)) then Numer := (ile-1);
  if  Numer < 0 then Numer := 0;
  if  Ile > 0 then
  begin
    Form1.TrackBar1.Max := Ile-1;
    Form1.TrackBar1.Position := Numer;
    Odswierz;
  end;

  Form1.Refresh;

  end;




procedure TForm1.Button1Click(Sender: TObject);
begin
 Ile := Ile + 1;
 setlength (Dane,Ile);
 Numer := Ile - 1;
 TrackBar1.Max := ile-1;
 Odswierz;

end;

procedure TForm1.Button2Click(Sender: TObject);
var
PlikINI : TIniFile;
  I: Integer;
begin
  if SaveDialog1.Execute = True then
     begin
     PlikINI := TIniFile.Create(SaveDialog1.FileName);
     try
       for I := 0 to ile-1 do
     begin
       PlikINI.WriteString('Record' + IntToStr(I+1),'Imie',Dane [I].Imie);
       PlikINI.WriteString('Record' + IntToStr(I+1),'Nazwisko',Dane [I].Nazwisko);
       PlikINI.WriteInteger('Record' + IntToStr(I+1),'Wiek',Dane [I].Wiek);
       PlikINI.WriteString('Record' + IntToStr(I+1),'Email',Dane [I].Email);

     end;
     finally
      PlikINI.Free;
      Odswiez;
       ShowMessage('Zapisano rekordy');
     end;
     end;
end;

procedure TForm1.Button3Click(Sender: TObject);
 var
PlikINI : TIniFile;


begin
   if OpenDialog1.Execute = True then
   begin
      Ile := 1 ;
      Numer := 0;
     PlikINI := TIniFile.Create(OpenDialog1.FileName);

     try
      repeat
        SetLength(Dane,Ile);
        Dane [Numer].imie:= PlikINI.ReadString('Record' + IntToStr(Numer + 1),'Imie', '*END*') ;
        Dane [Numer].Nazwisko:= PlikINI.ReadString('Record' + IntToStr(Numer + 1),'Nazwisko', '*END*') ;
        Dane [Numer].Wiek:= PlikINI.ReadInteger('Record' + IntToStr(Numer + 1),'Wiek', -1) ;
        Dane [Numer].Email:= PlikINI.ReadString('Record' + IntToStr(Numer + 1),'Email', '*END*') ;
         Ile := Ile+1 ;
         Numer := Numer + 1;
      until
        Dane[Numer-1].Imie = '*END*';
        finally
        Ile := Ile -2;
        Numer := Numer -1;
        SetLength(Dane,Ile);

      PlikINI.Free;
       ShowMessage('Wczytano rekordy' + IntToStr(ILE));
     end;
  end;
  end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
 Dane [Numer].Imie := Edit1.Text;
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
  Dane [Numer].Nazwisko := Edit2.Text;

end;

procedure TForm1.Edit3Change(Sender: TObject);
begin
 Dane [Numer].Email := Edit3.Text;
end;

procedure TForm1.Edit4Change(Sender: TObject);
begin
 Dane [Numer].Wiek := StrToInt(Edit4.Text);
end;

procedure TForm1.FormCreate(Sender: TObject);

begin
  setlength (Dane,1);
  Ile := 1;
  Numer := 0;
  odswierz;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
Numer := TrackBar1.Position;
odswierz;
end;

end.

object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 372
  ClientWidth = 703
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 80
    Top = 43
    Width = 37
    Height = 23
    Caption = 'Imi'#281
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 80
    Top = 88
    Width = 76
    Height = 23
    Caption = 'Nazwisko'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 56
    Top = 216
    Width = 135
    Height = 23
    Caption = 'Numer rekordu:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 392
    Top = 132
    Width = 40
    Height = 23
    Caption = 'Wiek'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 80
    Top = 135
    Width = 39
    Height = 19
    Caption = 'Email'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 200
    Top = 216
    Width = 6
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 200
    Top = 43
    Width = 217
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = Edit1Change
  end
  object TrackBar1: TTrackBar
    Left = 32
    Top = 264
    Width = 627
    Height = 45
    TabOrder = 1
    OnChange = TrackBar1Change
  end
  object Button1: TButton
    Left = 480
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Nowy'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = Button1Click
  end
  object Edit3: TEdit
    Left = 200
    Top = 132
    Width = 121
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnChange = Edit3Change
  end
  object Edit4: TEdit
    Left = 480
    Top = 132
    Width = 215
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnChange = Edit4Change
  end
  object Edit2: TEdit
    Left = 200
    Top = 88
    Width = 217
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnChange = Edit2Change
  end
  object Button2: TButton
    Left = 528
    Top = 49
    Width = 131
    Height = 25
    Caption = 'ZAPISZ'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 528
    Top = 80
    Width = 131
    Height = 25
    Caption = 'WCZYTAJ'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 512
    Top = 176
    Width = 147
    Height = 25
    Caption = 'Zapami'#281'taj'
    TabOrder = 8
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 512
    Top = 207
    Width = 147
    Height = 25
    Caption = 'Przywr'#243#263
    TabOrder = 9
    OnClick = Button5Click
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.ini'
    Filter = 'Plik INI|*.ini|Plik TXT|*.txt|Wszystkie|*.*'
    Options = [ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 328
    Top = 200
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.ini'
    Filter = 'Plik INI|*.ini|Plik TXT|*.txt|Wszystkie pliki|*.*'
    Options = [ofOverwritePrompt, ofEnableSizing]
    Left = 408
    Top = 200
  end
end

object Form2: TForm2
  Left = 0
  Top = 0
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Form2'
  ClientHeight = 441
  ClientWidth = 813
  Color = clBackground
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 776
    Top = 8
    Width = 17
    Height = 25
    Caption = 'X'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 0
    Top = 8
    Width = 75
    Height = 25
    Caption = '!'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 96
    Top = 264
    Width = 625
    Height = 25
    Caption = 'Button3'
    TabOrder = 2
  end
  object Edit1: TEdit
    Left = 96
    Top = 96
    Width = 625
    Height = 21
    TabOrder = 3
    Text = 'Edit1'
  end
  object RadioGroup1: TRadioGroup
    Left = 112
    Top = 144
    Width = 185
    Height = 105
    Caption = 'RadioGroup1'
    TabOrder = 4
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 5
    OnTimer = Timer1Timer
    Left = 664
    Top = 24
  end
end

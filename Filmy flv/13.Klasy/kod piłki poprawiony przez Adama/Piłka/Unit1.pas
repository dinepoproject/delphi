unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Timer1: TTimer;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  //const
  //Max = 5;

var
  Form1: TForm1;

  type
  ttest = class
  function nazwa:string;
  end;
  Tklasa = class (TTest)
    private
    obiekt : TRadioButton;
    KX,KY : Integer;
    szerokosc, wysokosc: Integer;
    wynik: Boolean;
    //cosmo: Boolean;
    // nie mozna przypisywac wartosci do p�l klasy w czasie jej tworzenia,
    //mozemy przypisac wartosci wewnatrz konstruktora.
    //Warto�ci te pobrane bed� poprzez argumenty konstruktora
    public

    function Ruch : Boolean;
    constructor Create(szer,wys: Integer; okno:Tform);
   destructor Destroy; override; //dlatego, ze jest metoda wirtualna, nie posiada kodu
    function nazwa:string;
  end;
  var
  lista : array of Tklasa;// PONIEWA� NA PODSTAWIE KLASY TWORZYMY KILKA OBIEKT�W
   Rozmiar: Integer;

implementation

{$R *.dfm}

{ Tklasa }
  const
  Max = 5;

  //cosmo: Boolean;
  //wynik: Boolean;
constructor Tklasa.Create(szer, wys: Integer; okno: Tform);
begin
 szerokosc:= szer;
 wysokosc:= wys;
 KX := 1;
 KY := 1;
 obiekt:= Tradiobutton.Create(okno);  //PODAJEMY UCHWYT DO OBIEKTY WE WNETRZU KTOREGO UMIESCIMY TRADIOBUTTON
 obiekt.Parent := Okno; //OKRESLAMY  OBIEKT NADRZEDNY
 obiekt.Caption:= '';
 obiekt.Height := 17;
 obiekt.Width:= 17;
 obiekt.Left:= 0;
 obiekt.top:= 0;
 obiekt.Visible:= True;

end;
 destructor Tklasa.destroy;  //PONIEWAZ destruktor bez tego ZWALNIA UCHWYT do OBIEKTU A NIE SAM OBIEKT
  begin //destruktor w naszej klasie przeslania destruktor z klasy z kt�rej dziedziczymy
   // skutkuje to pominieciem kodu destruktura z klasy Tobject mozemy pos�uzyc sie s�owem Inheerited
   //aby wywo�ac przes�aniana metode, wywy�ana zostanie metoda destroy z klasy tobject
  inherited;
     // nastepnie zwolnione zostana zasoby wskazywane przez pole obiekt:
    obiekt.free; //USUWAMY OBIEKT UTWORZONY DYNAMICZNIE


  end;
 function Tklasa.nazwa: string;
 var
 Tekst: string;
begin
Tekst:= inherited  nazwa;
Tekst:= Tekst + 'zmodyfikowano ja wewnatrz klasy tklasa';

Result := tekst;
end;

function Tklasa.Ruch : Boolean; //jezeli nastapi zderzenie pilki z krawedzia metoda w wyniku zwroci wartosc true
  var
  wynik: Boolean;
  begin
   wynik := False;
   obiekt.Left:= obiekt.left + KX;
   obiekt.top:= obiekt.top + KY;
   if obiekt.Left < 0 then
    //if obiekt.Left < szerokosc then  // powstana dwie pilki jedna poza arkuszem
   begin
    obiekt.Left:= 0;
     KX := -KX;
     wynik:= True;
   end;

   //if (obiekt.Left+obiekt.Width) > szerokosc then
    if (obiekt.Left) > szerokosc - obiekt.Width then     // A co jesli nie dodamy szerokosci (nic to nie zmieni�o)
   begin
     obiekt.Left:= szerokosc - obiekt.Width ;   //Zostawiam sama szerokosc, zobaczymy jak zadzia�a
//      obiekt.Left:= szerokosc;     // zadnych zmian nie ma
//      obiekt.Left:= szerokosc-150; //znaczne przesunienie
     KX := -KX;
     wynik:= True;
   end;
    if obiekt.top < 0 then
   begin

    obiekt.top:= 0;
     KY := -KY;
     wynik:= True;
   end;

     //if (obiekt.top +obiekt.Height) > Wysokosc then
      if (obiekt.top) > Wysokosc - obiekt.Height then
   begin
    //obiekt.top:= wysokosc - obiekt.Width ;
     //obiekt.top:= wysokosc + 100 ;  // pi�ka poza rejonem
      obiekt.top:= wysokosc - obiekt.Height;
     KY := -KY;
     wynik:= True;
   end;
   result := wynik;  // MAMY GOTOWA KLASE, CZYLE PRZEPIS NA TWORZENIE OBIEKT�W TERAZ TWORZYMY OBIEKTY KLASY
  end;

procedure TForm1.Button1Click(Sender: TObject);
begin
Close;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
Timer1.Enabled:= not Timer1.Enabled;
//ShowMessage(lista[0].nazwa);
 // ShowMessage(nazwa);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
Rozmiar:= 1;
SetLength(lista,Rozmiar);
lista[Rozmiar - 1]:= Tklasa.Create(Self.ClientWidth, Self.ClientHeight, Self);  // OBIEKT TWORZYMY WYWOLUJAC METODE CREATE DANEJ KLASY

end;

procedure TForm1.Timer1Timer(Sender: TObject); //pozostaje nam utowrzyc odpowiednia ilosc obiektow
 var
 i: Integer;
 Trafiony: Boolean;

begin
  for i := 0 to Rozmiar -1 do

  begin
   Trafiony := Lista[i].Ruch;
   if trafiony then
    if Rozmiar<= max then
     begin
      SetLength(lista,Rozmiar+1);// zwiekszamy rozmiar tablicy dynamicznej;
//        ShowMessage(lista[0].nazwa);
      lista[Rozmiar]:= Tklasa.Create(ClientWidth, ClientHeight,self);  // jako numer elementu tablicy podajemy wartosc r�wn� zmiennej rozmiar
      //gdy� zmienna ta nie zosta�a jeszcze zwiekszona po dodaniu elemntu do tablicy.
      // chyba tworzymy nowy konstruktor
      Rozmiar := Rozmiar +1; // now� PILKE dodajemy na koncu tablicy, wiec kod aktualnej petli wykona sie rowniez dla nowego obiektu.
      //Trafiony := False;    // nie dzia�a
     end;


  end;
end;

{ ttest }

function ttest.nazwa: string;
begin
Result := 'Klasa bed�ca w�ascicielem metody Nazwa :TTEST';
end;

end.
  //jestes na 8.3o
  //jestes na 15.20 - tworzymy rozmiar formulata poprzez oncreate itd.
  //jestes na 18.4o
  //jestes na 23.4o  nie tworzy nowych pi�ek, obejrzec od poczatku i sprawdzic gdzie jest blad

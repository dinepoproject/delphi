object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 337
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBEdit1: TDBEdit
    Left = 48
    Top = 48
    Width = 121
    Height = 21
    DataField = 'Imi'#281
    DataSource = DataSource1
    TabOrder = 0
  end
  object DBEdit2: TDBEdit
    Left = 48
    Top = 88
    Width = 121
    Height = 21
    DataField = 'Nazwisko'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DBEdit3: TDBEdit
    Left = 48
    Top = 136
    Width = 121
    Height = 21
    DataField = 'E-mail'
    DataSource = DataSource1
    TabOrder = 2
  end
  object DBImage1: TDBImage
    Left = 296
    Top = 48
    Width = 297
    Height = 257
    AutoDisplay = False
    DataField = 'Zdj'#281'cie'
    DataSource = DataSource1
    Proportional = True
    QuickDraw = False
    Stretch = True
    TabOrder = 3
    OnClick = DBImage1Click
  end
  object DBNavigator1: TDBNavigator
    Left = 80
    Top = 264
    Width = 240
    Height = 25
    DataSource = DataSource1
    TabOrder = 4
  end
  object Table1: TTable
    DatabaseName = 'prostabaza'
    SessionName = 'Session1_1'
    TableName = 'Tabela.DB'
    Left = 544
    Top = 272
  end
  object DataSource1: TDataSource
    DataSet = Table1
    Left = 432
    Top = 272
  end
  object Database1: TDatabase
    SessionName = 'Session1_1'
    Left = 312
    Top = 272
  end
  object Session1: TSession
    Active = True
    AutoSessionName = True
    Left = 312
    Top = 176
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.bmp'
    Filter = 'Bitmapa|*.bmp'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 472
    Top = 200
  end
end

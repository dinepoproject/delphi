unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Data.DB, Bde.DBTables, Vcl.ExtCtrls, Vcl.DBCtrls,
  Vcl.StdCtrls, Vcl.Mask;

type
  TForm1 = class(TForm)
    Table1: TTable;
    DataSource1: TDataSource;
    Database1: TDatabase;
    Session1: TSession;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBImage1: TDBImage;
    DBNavigator1: TDBNavigator;
    OpenDialog1: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure DBImage1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.DBImage1Click(Sender: TObject);
begin

if OpenDialog1.execute then
begin
Table1.Edit;
  DBImage1.Picture.loadfromfile(OpenDialog1.filename);
 Table1.Post;
  end;
  // komentarz na potrzeby source tree

end;

procedure TForm1.FormCreate(Sender: TObject);
var
KATALOG : string;
const nazwa :  string = 'Tabela';
 Nazwaaliasu : string = 'prostabaza';

begin
   KATALOG := ExtractFileDir(Application.ExeName) ;
   if FileExists(KATALOG + '\' + nazwa + '.db') = false then
   begin
     Table1.Active := False;
     Session1.DeleteAlias(Nazwaaliasu);
     Session1.AddStandardAlias(Nazwaaliasu,KATALOG,'');
     Table1.DatabaseName := Nazwaaliasu;
     Table1.TableType := ttParadox;
     Table1.TableName := nazwa;
        with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'Wiek';
         DataType := ftInteger;

       end;
        with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'Imi�';
         DataType := ftString;
          Size := 30;
       end;
         with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'Nazwisko';
         DataType := ftString;
          Size := 30;
       end;

        with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'Adres';
         DataType := ftFmtMemo;

       end;
        with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'E-mail';
         DataType := ftString;
          Size := 30;
       end;
         with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'Uwagi';
         DataType := ftFmtMemo;

       end;
          with Table1.FieldDefs.AddFieldDef do
       begin
         Name := 'Zdj�cie';
         DataType := ftGraphic;

       end;


       Table1.CreateTable;
        Showmessage('Utworzono tabel� o nazwie: ' + nazwa );

   end;
   Table1.TableName :=  Nazwa;
    Table1.Active := True;
end;

end.

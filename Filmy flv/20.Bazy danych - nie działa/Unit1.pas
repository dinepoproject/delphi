unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Data.DB, Bde.DBTables;

type
  TForm1 = class(TForm)
    Table1: TTable;
    DataSource1: TDataSource;
    Database1: TDatabase;
    Session1: TSession;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
 var
Katalog :   string;
const  Nazwa : string = 'Tabela' ;
NazwaAliasu : string = 'ProstaBaza';
 begin
  Katalog := ExtractFileDir(Application.ExeName);
  if FileExists(Katalog + '\' + Nazwa +'.DB') = false then
  begin
    Table1.Active := FALSE;
    Session1.AutoSessionName := True;
    Session1.DeleteAlias(NazwaAliasu);
    Session1.AddStandardAlias(NazwaAliasu,Katalog,'');
    Table1.DatabaseName := NazwaAliasu;
    Table1.TableType := ttParadox;
    Table1.TableName := Nazwa;

     with Table1.FieldDefs.AddFieldDef do
     begin
      Name := 'Wiek';
      DataType := ftInteger;
     end;
      with Table1.FieldDefs.AddFieldDef do
     begin
      Name := 'Imi�';
      DataType := ftString;
      Size := 30;


     end;
       with Table1.FieldDefs.AddFieldDef do
     begin
      Name := 'Nazwisko';
      DataType := ftString;
      Size := 30;
  end;
           with Table1.FieldDefs.AddFieldDef do
     begin
      Name := 'Nazwisko2';
      DataType := ftFmtMemo;

  end;
      with Table1.FieldDefs.AddFieldDef do
     begin
      Name := 'E-mail';
      DataType := ftString;
      Size := 30;
  end;
        with Table1.FieldDefs.AddFieldDef do
     begin
      Name := 'Uwagi';
      DataType := ftFmtMemo;


  end;
        with Table1.FieldDefs.AddFieldDef do
     begin
      Name := 'Zdj�cie';
      DataType := ftGraphic;


  end;
  Table1.CreateTable;
  ShowMessage('Utworzono now� tabel� o nazwie: ' + Nazwa);

  end;
  Table1.TableName := Nazwa;
  Table1.Active := True;
 end;
end.

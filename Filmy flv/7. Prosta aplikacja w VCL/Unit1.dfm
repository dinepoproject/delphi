object Form1: TForm1
  Left = 0
  Top = 0
  Width = 727
  Height = 411
  AutoScroll = True
  Caption = 'Okno g'#322#243'wne'
  Color = clInactiveBorder
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clLime
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsItalic]
  GlassFrame.Enabled = True
  OldCreateOrder = False
  DesignSize = (
    711
    373)
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 40
    Top = 56
    Width = 143
    Height = 23
    Caption = 'Etykieta pierwsza'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clTeal
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 528
    Top = 56
    Width = 121
    Height = 23
    Anchors = [akRight]
    Caption = 'Etykieta druga'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clOlive
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic, fsStrikeOut]
    ParentFont = False
  end
  object Button1: TButton
    Left = 24
    Top = 320
    Width = 273
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Przycisk pierwszy'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'Tarzan'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 0
    OnClick = Button1Click
    OnMouseEnter = Button1MouseEnter
    OnMouseLeave = Button1MouseLeave
  end
  object Button2: TButton
    Left = 424
    Top = 320
    Width = 257
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Przycisk drugi'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -19
    Font.Name = 'Tarzan'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 1
    OnClick = Button2Click
  end
end

object Form1: TForm1
  Left = 0
  Top = 0
  Width = 727
  Height = 411
  AutoScroll = True
  Caption = 'Okno g'#322#243'wne'
  Color = clInactiveBorder
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clLime
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsItalic]
  GlassFrame.Enabled = True
  OldCreateOrder = False
  DesignSize = (
    711
    373)
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 24
    Top = 56
    Width = 146
    Height = 22
    Caption = 'Etykieta pierwsza'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clTeal
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 95
    Width = 122
    Height = 22
    Anchors = [akRight]
    Caption = 'Etykieta druga'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clTeal
    Font.Height = -19
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
    ExplicitLeft = 40
    ExplicitTop = 106
  end
  object Button1: TButton
    Left = 8
    Top = 180
    Width = 273
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Przycisk pierwszy'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'Tarzan'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 0
    OnClick = Button1Click
    OnMouseEnter = Button1MouseEnter
    OnMouseLeave = Button1MouseLeave
  end
  object Button2: TButton
    Left = -8
    Top = 228
    Width = 257
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Przycisk drugi'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -19
    Font.Name = 'Tarzan'
    Font.Style = [fsItalic]
    ParentFont = False
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 448
    Top = 136
    Width = 113
    Height = 25
    Caption = 'INFORMACJE'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 456
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Zwyk'#322'e'
    TabOrder = 3
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 376
    Top = 288
    Width = 171
    Height = 25
    Caption = 'Zamknij okno zwyk'#322'e'
    TabOrder = 4
    OnClick = Button5Click
  end
end

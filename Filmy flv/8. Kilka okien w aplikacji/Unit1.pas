unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button1MouseEnter(Sender: TObject);
    procedure Button1MouseLeave(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses Unit2, Unit3;

procedure TForm1.Button1Click(Sender: TObject);
begin
Label1.Caption:= 'kliknieto przycisk pierwszy';
end;

procedure TForm1.Button1MouseEnter(Sender: TObject);
begin
 Label1.Caption:= 'kursor wszed� w obszar przycisku';
end;

procedure TForm1.Button1MouseLeave(Sender: TObject);
begin
Label1.Caption:= 'kursor opu�ci� obszar przycisku';
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Button1.Enabled:= not  Button1.Enabled ;

  if Button1.Enabled  then
  begin
  Label2.Caption := ' Przycisk pierwszy aktywny';
  end
  else
  begin
  Label2.Caption := ' Przycisk pierwszy nieaktywny'
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
Form2.ShowModal;
 Label2.Caption:= 'Wywo�ano okno modalne'
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
Form3.Show;
Button5.Enabled := True;
Label2.Caption:= 'Wywo�ano okno zwyk�e'
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
 Form3.Hide;
 Button5.Enabled := False;
end;

end.

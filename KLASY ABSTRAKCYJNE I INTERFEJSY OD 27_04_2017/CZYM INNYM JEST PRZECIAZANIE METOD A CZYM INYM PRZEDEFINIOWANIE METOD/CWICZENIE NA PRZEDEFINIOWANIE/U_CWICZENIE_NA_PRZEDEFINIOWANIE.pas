unit U_CWICZENIE_NA_PRZEDEFINIOWANIE;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFiat = class
    function Jedz: string; virtual; //VIRTUAL !!!
  end;

  TMaluch = class(TFiat)
    function Jedz: string; override; // OVERRIDE !!!
  end;

  TFiat125 = class(TFiat)
    function Jedz: string; override;
  end;

type
  TForm1 = class(TForm)
    Button1: TButton;
    btn1_klasa_tfiat: TButton;
    btn1_FIAT_PLUS_FIAT125: TButton;
    procedure btn1_FIAT_PLUS_FIAT125Click(Sender: TObject);
    procedure btn1_klasa_tfiatClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
  { TFiat125 }

function TFiat125.Jedz;
var
  MOJE: string;
begin
  MOJE     := inherited JEDZ;  // NA PODSTAWIE FILMU O KLASACH ZMODYFIKOWANO JEDZ
 // PRZENIESIONO SHOWMESSAGE DO  tn1_FIAT_PLUS_FIAT125Click

// TYLKO CZEMU W FILMIE O KLASACH NIE NA VIRTUAL I OVERRIDE??
  MOJE     := MOJE + '    ' + 'MOJ DOPISEK';
  RESULT   := MOJE;

end;

{ TFiat }

function TFiat.Jedz;
begin
    Result := 'Metoda Jedz z klasy TFiat';
  end;

{ TMaluch }

function TMaluch.Jedz;
begin
  Result   := 'Metoda Jedz z klasy TMaluch';

end;

procedure TForm1.btn1_FIAT_PLUS_FIAT125Click(Sender: TObject);
var
  Klasa: Tfiat;
begin
  Klasa    := TFiat125.Create;
  ShowMessage(Klasa.Jedz);

  Klasa.Free;

end;

procedure TForm1.btn1_klasa_tfiatClick(Sender: TObject);
var
  Klasa: Tfiat;
begin
  Klasa    := TFiat.Create;
  ShowMessage(Klasa.Jedz);
  Klasa.Free;

end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Klasa: Tmaluch;  // TO SAM DODALEM, TUTAJ DEKLARUJEMY KLASE TMALUCH
                    // TYM SAMYM JAK ROZUMIEM NIC NIE DZIEDZICZYMY PO PROSTU
begin
  Klasa    := TMaluch.Create;
  ShowMessage(Klasa.Jedz);
  Klasa.Free;

end;

end.


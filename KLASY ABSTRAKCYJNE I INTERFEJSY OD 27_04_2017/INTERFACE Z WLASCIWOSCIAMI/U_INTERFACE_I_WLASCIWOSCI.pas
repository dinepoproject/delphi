unit U_INTERFACE_I_WLASCIWOSCI;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

  type
  Iwiek = interface
    ['{F8644D9A-75D0-4165-9AEF-3E429FEC3517}']
    function odczytaj_wiek: Integer;
    procedure zapisz_wiek(const wartosc:Integer);
    property wiek: Integer read odczytaj_wiek write zapisz_wiek;
    end;
  type
  Tklasa_wieku = class (TInterfacedObject, Iwiek)
  private
  Fwiek:Integer;
  function odczytaj_wiek: Integer;
  procedure zapisz_wiek(const wartosc:Integer);
  public
  property wiek: Integer read odczytaj_wiek write zapisz_wiek;

  end;



type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    btn1_odczytaj_wartosc_wlasciwosci: TButton;
    btn2_zapisz_wartosc_wlasciwosci: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1_odczytaj_wartosc_wlasciwosciClick(Sender: TObject);
    procedure btn2_zapisz_wartosc_wlasciwosciClick(Sender: TObject);
  private
    { Private declarations }
   // klasawew: Tklasa_wieku;
    WiekIntf: Iwiek;
  public
    { Public declarations }


  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

{ Tklasa_wieku }

function Tklasa_wieku.odczytaj_wiek: Integer;
begin
  result:= Fwiek ;
end;

procedure Tklasa_wieku.zapisz_wiek(const wartosc: Integer);
begin
 Fwiek := WARTOSC;
end;

procedure TForm1.FormCreate(Sender: TObject);


begin

  WiekIntf:= tklasa_wieku.Create;
end;

procedure TForm1.btn1_odczytaj_wartosc_wlasciwosciClick(Sender: TObject);

 begin
 Edit2.Text:= Wiekintf.wiek.ToString;



 end;

procedure TForm1.btn2_zapisz_wartosc_wlasciwosciClick(Sender: TObject);

begin
 wiekintf.wiek := StrToInt(Edit1.Text);
  // alt shitf r  - odwraca   wiekintf.wiek:= StrToInt(Edit1.Text) na
                              //  StrToInt(Edit1.Text) := wiekintf.wiek
end;

end.

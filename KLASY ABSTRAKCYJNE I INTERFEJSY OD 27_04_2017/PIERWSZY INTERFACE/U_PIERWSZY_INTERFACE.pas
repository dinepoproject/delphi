unit U_PIERWSZY_INTERFACE;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;
  type
  IDANEOSOBOWE = interface
     ['{607522FE-40BE-465B-BAF8-FA591CF20772}']
     function IMIE (WPRO: string) : string;
     function NAZWISKO:string;
     end;
  type
  TOSOBA = class (TInterfacedObject, IDANEOSOBOWE)
    public
    function IMIE (WPRO: string ) : string;
    function NAZWISKO : string;


  end;
type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

{ TOSOBA }

function TOSOBA.IMIE (WPRO: string ) : string;
begin
Result:=  WPRO;

end;

function TOSOBA.NAZWISKO: string;
begin
Result:= 'PALUBSKI';
end;

procedure TForm1.Button1Click(Sender: TObject);
var
ZMIENNA_INTERFACE: IDANEOSOBOWE;
begin
 ZMIENNA_INTERFACE:= TOSOBA.Create;
Button1.Caption:= ZMIENNA_INTERFACE.IMIE('DAN') + '  ' +ZMIENNA_INTERFACE.NAZWISKO;
end;

end.

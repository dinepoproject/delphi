object Form8: TForm8
  Left = 0
  Top = 0
  Caption = 'Form8'
  ClientHeight = 533
  ClientWidth = 924
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 520
    Top = 80
    Width = 329
    Height = 338
    Lines.Strings = (
      'Memo1')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    OnKeyDown = FormKeyDown
  end
  object Edit1: TEdit
    Left = 104
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
    OnKeyDown = FormKeyDown
  end
  object CheckBox1: TCheckBox
    Left = 104
    Top = 136
    Width = 97
    Height = 17
    Caption = 'CheckBox1'
    TabOrder = 2
    OnKeyDown = FormKeyDown
  end
  object Button1: TButton
    Left = 136
    Top = 320
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
    OnClick = Button1Click
  end
end

program KODZIDELKO;

uses
  Vcl.Forms,
  OKNO_GLOWNE in 'OKNO_GLOWNE.pas' {Form1},
  RECORDY_W_fUNKCJI in 'RECORDY_W_fUNKCJI.pas' {Form2},
  TABLICE in 'TABLICE.pas' {Form3},
  Proced_parametry in 'Proced_parametry.pas' {Form4},
  Tablice_dynamiczne in 'Tablice_dynamiczne.pas' {Form5},
  Tablice_Rekordy in 'Tablice_Rekordy.pas' {Form6},
  Wyszukaj_email in 'Wyszukaj_email.pas' {Form7},
  Info_nacisn.klawisz in 'Info_nacisn.klawisz.pas' {Form8},
  FUNKCJE_Z_PARAMETRAMI in 'FUNKCJE_Z_PARAMETRAMI.pas' {Form9},
  wlasciwosci in 'wlasciwosci.pas' {Form10},
  Tab_dynam_w_petli in 'Tab_dynam_w_petli.pas' {Form11},
  Tablice_scratch_losuj in 'Tablice_scratch_losuj.pas' {Form12},
  klasa_losuj in 'klasa_losuj.pas',
  Tabl_zwrac_przez_funkcje in 'Tabl_zwrac_przez_funkcje.pas' {Form13},
  wskazniki in 'wskazniki.pas' {Form14};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TForm8, Form8);
  Application.CreateForm(TForm9, Form9);
  Application.CreateForm(TForm10, Form10);
  Application.CreateForm(TForm11, Form11);
  Application.CreateForm(TForm12, Form12);
  Application.CreateForm(TForm13, Form13);
  Application.CreateForm(TForm14, Form14);
  Application.Run;
end.

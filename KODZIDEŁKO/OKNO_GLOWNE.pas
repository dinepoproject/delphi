unit OKNO_GLOWNE;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.ImageList, Vcl.ImgList,
  Vcl.ComCtrls, Vcl.ToolWin, Vcl.Menus;

type
  TForm1 = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    btn1: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ImageList1: TImageList;
    MainMenu1: TMainMenu;
    RECORDY1: TMenuItem;
    WSKAZNIKI1: TMenuItem;
    TABLICE1: TMenuItem;
    RECORDYWFUNKCJI1: TMenuItem;
    Proceduryzparametrem1: TMenuItem;
    procedura5parametrw1: TMenuItem;
    tA1: TMenuItem;
    TABLICEDYNAMICZNE: TMenuItem;
    TABLICEZREKORDAMI1: TMenuItem;
    PTLE1: TMenuItem;
    Cwiczenia1: TMenuItem;
    Wyszukajadresu1: TMenuItem;
    infoonacisnklawiszu1: TMenuItem;
    ablicedynamicznewpetli11: TMenuItem;
    ablice1: TMenuItem;
    FUNKCJEZPARAMETRAMI2: TMenuItem;
    wlasc1: TMenuItem;
    tablicelosowaniescratch1: TMenuItem;
    Atablicezwracaneprzezfunkcj1: TMenuItem;
    tubedawskazniki1: TMenuItem;
    procedure RECORDYWFUNKCJI1Click(Sender: TObject);
    procedure TABLICE1Click(Sender: TObject);
    procedure procedura5parametrw1Click(Sender: TObject);
    procedure TABLICEDYNAMICZNE1Click(Sender: TObject);
    procedure TABLICEDYNAMICZNEClick(Sender: TObject);
    procedure TABLICEZREKORDAMI1Click(Sender: TObject);
    procedure Wyszukajadresu1Click(Sender: TObject);
    procedure infoonacisnklawiszu1Click(Sender: TObject);
    procedure ablice1Click(Sender: TObject);
    procedure FUNKCJEZPARAMETRAMI2Click(Sender: TObject);

    procedure wlasc1Click(Sender: TObject);
    procedure ablicedynamicznewpetli11Click(Sender: TObject);
    procedure tablicelosowaniescratch1Click(Sender: TObject);
    procedure Atablicezwracaneprzezfunkcj1Click(Sender: TObject);
    procedure tubedawskazniki1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses RECORDY_W_fUNKCJI, TABLICE, Proced_parametry, Tablice_dynamiczne,
  Tablice_Rekordy, Wyszukaj_email, Info_nacisn.klawisz, FUNKCJE_Z_PARAMETRAMI,
  wlasciwosci, Tab_dynam_w_petli, Tablice_scratch_losuj,
  Tabl_zwrac_przez_funkcje,wskazniki;

procedure TForm1.TABLICE1Click(Sender: TObject);
begin
 Form3.show;
end;

procedure TForm1.TABLICEDYNAMICZNE1Click(Sender: TObject);
begin
 Form5.Show;
end;

procedure TForm1.TABLICEDYNAMICZNEClick(Sender: TObject);
begin
  Form5.Show;
end;

procedure TForm1.tablicelosowaniescratch1Click(Sender: TObject);
begin
  Form12.Show;
end;

procedure TForm1.TABLICEZREKORDAMI1Click(Sender: TObject);
begin
form6.Show;
end;

procedure TForm1.tubedawskazniki1Click(Sender: TObject);
begin
  Form14.Show;
end;

procedure TForm1.wlasc1Click(Sender: TObject);
begin
Form10.Show;
end;



procedure TForm1.Wyszukajadresu1Click(Sender: TObject);
begin
Form7.show;

end;

procedure TForm1.ablice1Click(Sender: TObject);
begin
form3.Show;
end;

procedure TForm1.ablicedynamicznewpetli11Click(Sender: TObject);
begin
form11.Show;
end;

procedure TForm1.Atablicezwracaneprzezfunkcj1Click(Sender: TObject);
begin
 Form13.Show;
end;

procedure TForm1.FUNKCJEZPARAMETRAMI2Click(Sender: TObject);
begin
form9.Show;
end;

procedure TForm1.infoonacisnklawiszu1Click(Sender: TObject);
begin
form8.show;
end;

procedure TForm1.procedura5parametrw1Click(Sender: TObject);
begin
Form4.show;

end;

procedure TForm1.RECORDYWFUNKCJI1Click(Sender: TObject);
begin
Form2.Show;
end;

end.
// dodano dla git

object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 337
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 48
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 129
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
  end
  object Button2: TButton
    Left = 48
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Edit2: TEdit
    Left = 129
    Top = 41
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'Edit2'
  end
  object Button3: TButton
    Left = 48
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Button3'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Edit3: TEdit
    Left = 136
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'Edit3'
  end
  object Button4: TButton
    Left = 48
    Top = 101
    Width = 75
    Height = 25
    Caption = 'Button4'
    TabOrder = 6
    OnClick = Button4Click
  end
  object Edit4: TEdit
    Left = 129
    Top = 103
    Width = 185
    Height = 21
    TabOrder = 7
    Text = 'Edit4'
  end
  object Button5: TButton
    Left = 48
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Nocna furia'
    TabOrder = 8
    OnClick = Button5Click
  end
  object Edit5: TEdit
    Left = 129
    Top = 146
    Width = 392
    Height = 21
    TabOrder = 9
    Text = 'Edit5'
  end
end

unit Tabl_zwrac_przez_funkcje;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
   TForm13 = class(TForm)
   Edit1: TEdit;
    Button1: TButton;
    Edit2: TEdit;
    procedure Button1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  Form13: TForm13;

implementation

{$R *.dfm}
 // http://4programmers.net/Delphi/Procedury_i_funkcje#id-Procedury
 // patrz tablice zwracane przez funkcje


procedure TForm13.Button1Click(Sender: TObject);

 type
  TBar = array of String;

  function Foo : Tbar;
  Var
    Tablica2: Tbar;
    rozmiar : Integer;
  begin
  rozmiar := 1;
 SetLength(tablica2,4);
  tablica2[rozmiar-1]:= 'oko';
 tablica2[rozmiar]:= 'nos';
 tablica2[rozmiar+1]:= 'ucho';
 Tablica2[rozmiar+2]:= (tablica2[rozmiar-1] + '   ' + tablica2[rozmiar+1]);
 result:= tablica2;
end;

var
  tablica6: TBar;
 begin
  tablica6 := foo();
 Edit1.Text:= Tablica6[+3];
 Edit2.Text:= 'z klasy:'  + Sender.ClassName; // cwiczono sender: http://4programmers.net/Delphi/Kompendium/Rozdzia%C5%82_3#id-Dziedziczenie
 end;
 //  Dla procedur lokalnych cia�o piszesz od razu - nie ma oddzielnej
 //deklaracji i cia�a po deklaracji od razu wpisujesz begin end;
 // i umieszczasz w nich cia�o funkcji
end.

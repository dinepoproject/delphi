unit Engine;

interface
  uses
  Winapi.Windows, system.Classes, system.SysUtils;
   type
  TTemplate = array of String;

  TEngine = class
  private
    FFileName : String;
    FFileLines : TStringList;
  protected
    procedure Execute(Path : String); virtual;
  public
    Pattern : TTemplate;
    Replace : TTemplate;
    procedure Parse;
    constructor Create(FileName : String);
    destructor Destroy; override;
  end;
implementation
 { TEngine }

uses ShellAPI; // włączenie modułu ShellAPI

constructor TEngine.Create(FileName : String);
begin
  FFileName := FileName;  // przypisanie wartości parametru do zmiennej w sekcji private
  FFileLines := TStringList.Create; // utworzenie typu TStringList
  FFileLines.LoadFromFile(FileName); // załadowanie zawartości zmiennej z pliku
end;

destructor TEngine.Destroy;
begin
  FFileLines.Free; // zwolnienie typu
  { zwolnienie tablic }
  Pattern := nil;
  Replace := nil;
  DeleteFile('temporary.html'); // wykasowanie pliku tymczasowego
  inherited; // wywołanie destruktora klasy bazowej
end;

procedure TEngine.Execute(Path: String);
begin
// otwarcie pliku w przeglądarce Internetowej
  ShellExecute(0, 'open', PChar(Path), nil, nil, SW_SHOW);
end;

procedure TEngine.Parse;
var
  i : Integer;
begin
  for I := Low(Pattern) to High(Pattern) do
  { zastąpienie określonych wartości w FFileLines }
    FFileLines.Text := StringReplace(FFileLines.Text, Pattern[i], Replace[i], [rfReplaceAll]);

  FFileLines.SaveToFile('temporary.html');
  Execute('temporary.html');

end;


end.

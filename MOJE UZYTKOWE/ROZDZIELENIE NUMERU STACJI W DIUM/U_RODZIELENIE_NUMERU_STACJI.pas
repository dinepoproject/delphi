unit U_RODZIELENIE_NUMERU_STACJI;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, Vcl.Buttons;


type
  TForm2 = class(TForm)
    mm1: TMainMenu;
    OPERACJA1: TMenuItem;
    ROZDZIELSPACJE1: TMenuItem;
    ROZDZIELMYSLNIK1: TMenuItem;
    Memo1: TMemo;
    Memo2: TMemo;
    BitBtn1: TBitBtn;
    WKLEJ_D0_MEMO: TButton;
    KOPIUJ_Z_MEMO: TButton;
    Holandliczbalitera1: TMenuItem;
    procedure BitBtn1Click(Sender: TObject);
    procedure Holandliczbalitera1Click(Sender: TObject);
    procedure KOPIUJ_Z_MEMOClick(Sender: TObject);
    procedure ROZDZIELMYSLNIK1Click(Sender: TObject);
    procedure ROZDZIELSPACJE1Click(Sender: TObject);
    procedure WKLEJ_D0_MEMOClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.BitBtn1Click(Sender: TObject);
begin
Memo1.Text:= '';
Memo2.Text:= '';
end;

procedure TForm2.Holandliczbalitera1Click(Sender: TObject);
{var
  //http://pascal.kurs-programowania.pl/turbo_pascal,operacje_na_tekstach.html

TEKST_PRZED_ZMIANA:string;
STRING_PO_ZMIANIE: string;
NewStr:   Char;
OldStr : set of Char;
// STRING REPLACE : https://4programmers.net/Delphi/StringReplace
// JAK ZNALEZC ZNAK TABULATORA: http://www.delphibasics.co.uk/RTL.asp?Name=chr
// FUNKCJE DLA LANCUCHOW:https://4programmers.net/Delphi/Artyku%C5%82y/%C5%81a%C5%84cuchy#trim }
begin  {
OldStr:= ['a'];

 TEKST_PRZED_ZMIANA:= Trim(Memo1.Text);
 NewStr:= CHR(9);

 STRING_PO_ZMIANIE:= StringReplace(TEKST_PRZED_ZMIANA,OldStr,NewStr,[rfReplaceAll]);
 Memo2.Lines.Text:=STRING_PO_ZMIANIE;
 memo2.SelectAll;
 Memo2.SetFocus; }

end;

procedure TForm2.KOPIUJ_Z_MEMOClick(Sender: TObject);
begin
Memo2.CopyToClipboard;
end;

procedure TForm2.ROZDZIELMYSLNIK1Click(Sender: TObject);
var
TEKST_PRZED_ZMIANA:string;
STRING_PO_ZMIANIE: string;
NewStr, OldStr : CHAR;
// STRING REPLACE : https://4programmers.net/Delphi/StringReplace
// JAK ZNALEZC ZNAK TABULATORA: http://www.delphibasics.co.uk/RTL.asp?Name=chr
// FUNKCJE DLA LANCUCHOW:https://4programmers.net/Delphi/Artyku%C5%82y/%C5%81a%C5%84cuchy#trim
begin
 TEKST_PRZED_ZMIANA:= Trim(Memo1.Text);
 NewStr:= CHR(9);
 OldStr:= '-';
 STRING_PO_ZMIANIE:= StringReplace(TEKST_PRZED_ZMIANA,OldStr,NewStr,[rfReplaceAll]);
 Memo2.Lines.Text:=STRING_PO_ZMIANIE;
 memo2.SelectAll;
 Memo2.SetFocus;

end;


procedure TForm2.ROZDZIELSPACJE1Click(Sender: TObject);
var
TEKST_PRZED_ZMIANA:string;
STRING_PO_ZMIANIE: string;
NewStr, OldStr : CHAR;
// STRING REPLACE : https://4programmers.net/Delphi/StringReplace
// JAK ZNALEZC ZNAK TABULATORA: http://www.delphibasics.co.uk/RTL.asp?Name=chr
// FUNKCJE DLA LANCUCHOW:https://4programmers.net/Delphi/Artyku%C5%82y/%C5%81a%C5%84cuchy#trim
//  https://4programmers.net/Delphi/Modu%C5%82y/StrUtils
begin
 TEKST_PRZED_ZMIANA:= Trim(Memo1.Text);
 NewStr:= CHR(9);
 OldStr:= ' ';
 STRING_PO_ZMIANIE:= StringReplace(TEKST_PRZED_ZMIANA,OldStr,NewStr,[rfReplaceAll]);
 Memo2.Lines.Text:=STRING_PO_ZMIANIE;
 memo2.SelectAll;
 Memo2.SetFocus;

end;

procedure TForm2.WKLEJ_D0_MEMOClick(Sender: TObject);
begin
Memo1.PasteFromClipboard;
end;

end.

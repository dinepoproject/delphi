unit U_TABLICZKA_MNO�ENIA_FUNKCJA.TEST;

interface

uses
  TestFramework;
 type

 Ttest_oblicz = class(TTestCase)
 procedure Test_Mnozenie_Razy_Jeden;
 procedure Test_Mnozenie_Razy_Dwa;
 procedure Test_Mnozenie_Razy_Zero;
 end;
implementation

uses
  U_tabliczka_mno�enia_funkcja;

{ Ttest_oblicz }

procedure Ttest_oblicz.Test_Mnozenie_Razy_Dwa;
begin
     CheckEquals(8,oblicz(4,2));
end;


procedure Ttest_oblicz.Test_Mnozenie_Razy_Jeden;
begin
CheckEquals(8,oblicz(8,1));
end;
 procedure Ttest_oblicz.Test_Mnozenie_Razy_Zero;
begin
CheckEquals(0,oblicz(8,0));
end;

initialization
 RegisterTest(Ttest_oblicz.Suite);
end.

﻿object Form1: TForm1
  Left = 373
  Top = 243
  Caption = 'Nauka tabliczki mno'#380'enia'
  ClientHeight = 340
  ClientWidth = 573
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 184
    Top = 154
    Width = 207
    Height = 19
    Caption = 'Wpisz sw'#243'j wynik do okienka'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 208
    Top = 24
    Width = 123
    Height = 33
    Caption = 'Ile to jest:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 176
    Top = 109
    Width = 16
    Height = 35
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 280
    Top = 104
    Width = 11
    Height = 35
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 392
    Top = 109
    Width = 16
    Height = 35
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Sprawdź: TButton
    Left = 224
    Top = 232
    Width = 121
    Height = 41
    Caption = 'Sprawd'#378
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = SprawdźClick
  end
  object Edit1: TEdit
    Left = 224
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 1
    OnChange = Edit1Change
  end
  object btnLOSUJ: TButton
    Left = 48
    Top = 104
    Width = 75
    Height = 25
    Caption = 'LOSOWANIE'
    TabOrder = 2
    OnClick = btnLOSUJClick
  end
  object Button1: TButton
    Left = 96
    Top = 288
    Width = 393
    Height = 25
    Caption = 'CZY'#346#262
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 424
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Drukuj'
    TabOrder = 4
    OnClick = Button2Click
  end
end

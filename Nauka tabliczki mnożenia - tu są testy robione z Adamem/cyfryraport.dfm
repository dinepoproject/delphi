object liczbyraport: Tliczbyraport
  OldCreateOrder = False
  Height = 333
  Width = 439
  object frxReport1: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42199.605855914300000000
    ReportOptions.LastChange = 42199.605855914300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnNewGetValue = frxReport1NewGetValue
    Left = 216
    Top = 168
    Datasets = <>
    Variables = <
      item
        Name = ' wartosci'
        Value = Null
      end
      item
        Name = 'Wpisanywynik'
        Value = '11'
      end
      item
        Name = 'prawid'#322'owywynik'
        Value = '10'
      end
      item
        Name = 'wylosowanaliczba1'
        Value = '10'
      end
      item
        Name = 'wylosowanaliczba2'
        Value = '5'
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 245.669450000000000000
        Top = 64.252010000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 11.338590000000000000
          Width = 147.401670000000000000
          Height = 30.236240000000000000
          Memo.UTF8W = (
            'wylosowana liczba1: [wylosowanaliczba1]')
        end
        object Memo2: TfrxMemoView
          Left = 56.692950000000000000
          Top = 139.842610000000000000
          Width = 139.842610000000000000
          Height = 30.236240000000000000
          Memo.UTF8W = (
            'Wpisany wynik: [Wpisanywynik]')
        end
        object Memo3: TfrxMemoView
          Left = 294.803340000000000000
          Top = 143.622140000000000000
          Width = 136.063080000000000000
          Height = 22.677180000000000000
          Memo.UTF8W = (
            'prawid'#322'owy wynik:[prawid'#322'owywynik]')
        end
        object Memo4: TfrxMemoView
          Left = 283.464750000000000000
          Top = 15.118120000000000000
          Width = 200.315090000000000000
          Height = 37.795300000000000000
          Memo.UTF8W = (
            'wylosowanaliczba2: [wylosowanaliczba2]')
        end
      end
    end
  end
end

unit cyfryraport;

interface

uses
  System.SysUtils, System.Classes, frxClass;

type
  Tliczbyraport = class(TDataModule)
    frxReport1: TfrxReport;

    procedure frxReport1NewGetValue(Sender: TObject; const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    Fwylosowanaliczba1 : Integer;
    Fwylosowanaliczba2 : Integer;
    Fwpisanywynik : Integer;
    Fprawid這wywynik : Integer;


  public
    { Public declarations }
    procedure patrz;
   property  wylosowanaliczba1 : Integer read Fwylosowanaliczba1 write Fwylosowanaliczba1;
   property  wylosowanaliczba2 : Integer read Fwylosowanaliczba2 write Fwylosowanaliczba2;
   property  wpisanywynik :      Integer read Fwpisanywynik      write Fwpisanywynik;
   property  prawid這wywynik :   Integer read Fprawid這wywynik   write Fprawid這wywynik;

  end;

var
  liczbyraport: Tliczbyraport;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


procedure Tliczbyraport.frxReport1NewGetValue(Sender: TObject;
  const VarName: string; var Value: Variant);
begin
 if VarName = 'wylosowanaliczba1' then
 Value := wylosowanaliczba1;
   if VarName = 'wylosowanaliczba2' then
 Value := wylosowanaliczba2;
   if VarName = 'wpisanywynik' then
 Value := wpisanywynik;
    if VarName = 'prawid這wywynik' then
 Value := prawid這wywynik;

end;

procedure Tliczbyraport.patrz;
begin
frxReport1.ShowReport;
end;

end.

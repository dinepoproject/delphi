unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    function CyfraOblicz(const ALancuch: String): Integer;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  wynik:Integer;
implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);


begin

wynik := CyfraOblicz(Edit1.text);
Label1.Caption:= IntToStr(wynik) ;
end;

function TForm1.CyfraOblicz(const ALancuch: String): Integer;
var
  i, j: Integer;
begin
  Result := 0;
  for i := 1 to Length(ALancuch) do
  begin
    j := (Byte(ALancuch[i]) - 48) * (2 - (i + 1) mod 2);
    Inc(Result, (j div 10) + (j mod 10));
  end;
  Result := 10 - Result mod 10;
  if Result = 10 then Result := 0;
end;
end.

object Form1: TForm1
  Left = 0
  Top = 0
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'M'#261#380' '#380'artowni'#347
  ClientHeight = 743
  ClientWidth = 927
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  DesignSize = (
    927
    743)
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 280
    Top = 270
    Width = 409
    Height = 25
    Caption = 'TU KLIKNIJ '
    TabOrder = 0
    OnClick = Button1Click
    OnMouseEnter = Button1MouseEnter
    OnMouseLeave = Button1MouseLeave
  end
  object Memo2: TMemo
    Left = 248
    Top = 24
    Width = 569
    Height = 193
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Lines.Strings = (
      'Pewnego razu '#380'ona zwr'#243'cila sie do m'#281#380'a z problemem: '
      '"Mam w folderze 245 zdj'#281#263' chcia'#322'abym je ponumerowa'#263' '
      '- Pomo'#380'esz?"'
      '- zapyta'#322'a'
      ''
      'Oczywi'#347'cie m'#261#380' informatyk do tego '#380'artowni'#347' znalaz'#322' '
      'spos'#243'b, aby sie dowiedzie'#263' kliknij na przycisk.')
    ParentFont = False
    TabOrder = 1
  end
end

unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, uZwrotka;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo2: TMemo;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button1MouseEnter(Sender: TObject);
    procedure Button1MouseLeave(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    procedure PokazOkno(const AZwrotka: TZwrotka);
  public
    procedure koniec_zdania;
  end;

var
  Form1: TForm1;
  Dane: Record
    jeden: string;
    dwa: string;
    trzy: Integer;
  end;
  tekst: string;

implementation

{$R *.dfm}

uses
  Unit2, UZwrotka_tablica, UZwrotka_lancuch;

const
  max = 260;

procedure TForm1.Button1Click(Sender: TObject);
begin
  PokazOkno(Tzwrotka_tablica.Create);
end;

procedure TForm1.Button1MouseEnter(Sender: TObject);
begin
  Button1.Caption := 'DOWIEDZ SI� JAK PROBLEM ROZWI�ZA� M��';
end;

procedure TForm1.Button1MouseLeave(Sender: TObject);
begin
  Button1.Caption := 'TU KLIKNIJ';
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  PokazOkno(Tzwrotka_lancuch.Create);
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
  ///Form2.memo1.Text := '';
end;

procedure TForm1.koniec_zdania;
begin
  Form2.memo1.Text := 'I to ca�a filozofia';
end;

procedure TForm1.PokazOkno(const AZwrotka: TZwrotka);
begin
  try
    Form2.Show;
    Form2.memo1.Text := AZwrotka.ZWROTKA;
  finally
    AZwrotka.Free;
  end;
end;

Initialization

finalization


end.


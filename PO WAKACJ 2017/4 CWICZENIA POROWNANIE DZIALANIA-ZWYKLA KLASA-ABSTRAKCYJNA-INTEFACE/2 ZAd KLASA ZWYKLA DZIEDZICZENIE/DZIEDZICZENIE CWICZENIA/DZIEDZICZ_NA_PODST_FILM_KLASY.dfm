object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 201
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnZ_KLASY_PRZESLANIANEJ: TButton
    Left = 24
    Top = 72
    Width = 211
    Height = 25
    Caption = 'Z KLASY PRZESLANIANEJ'
    TabOrder = 0
    OnClick = btnZ_KLASY_PRZESLANIANEJClick
  end
  object btnZ_KLASY_MATKI: TButton
    Left = 8
    Top = 8
    Width = 297
    Height = 25
    Caption = 'btnZ_KLASY_MATKI'
    TabOrder = 1
    OnClick = btnZ_KLASY_MATKIClick
  end
  object WLASCIWOSC_AUTOMAT: TButton
    Left = 40
    Top = 112
    Width = 329
    Height = 25
    Caption = 'WLASCIWOSC_AUTOMAT_WPROWADZ_DO_PPO_POLA'
    TabOrder = 2
    OnClick = WLASCIWOSC_AUTOMATClick
  end
  object WLASCIWOSC_AUTOMAT_ODCZYT_Z_POLA_WLASC: TButton
    Left = 40
    Top = 143
    Width = 329
    Height = 25
    Caption = 'WLASCIWOSC_AUTOMAT_ODCZYT_Z_POLA_WLASC'
    TabOrder = 3
    OnClick = WLASCIWOSC_AUTOMAT_ODCZYT_Z_POLA_WLASCClick
  end
end

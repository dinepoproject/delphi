unit INTERFACE_MNOZENIE;

interface
TYPE
I_MNOZENIE = interface
['{D2D48885-0B8D-48B6-803C-21C53FBDA47B}']

    function FUNKCJA_INTEFRACE_MNOZENIE(X,Y:Integer):Integer;
    end;

  TYPE
  T_KLASA_DO_INTER_I_MNOZENIE = class (TInterfacedObject, I_MNOZENIE)
    function FUNKCJA_INTEFRACE_MNOZENIE(X,Y:Integer):Integer;
  end;

  implementation

 { T_KLASA_DO_INTER_I_MNOZENIE }

function T_KLASA_DO_INTER_I_MNOZENIE.FUNKCJA_INTEFRACE_MNOZENIE(X,
  Y: Integer): Integer;
begin
Result := X*Y;
end;

end.


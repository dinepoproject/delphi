unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    btn1_ZADANIE_1: TButton;
    btn2_ZADANIE_2: TButton;
    procedure btn1_ZADANIE_1Click(Sender: TObject);
    procedure btn2_ZADANIE_2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
  USES
  U_ZADANIE_1,U_ZADANIE_2;
{$R *.dfm}

procedure TForm1.btn1_ZADANIE_1Click(Sender: TObject);
var
UCHWYT_DO_T_ZADANIE1:T_ZADANIE1;
begin
 try  // CTRL+ALR+J ZEBY WYWOWAC TRY,FINALLY,END
 BEGIN
  UCHWYT_DO_T_ZADANIE1:=T_ZADANIE1.Create;
   btn1_ZADANIE_1.Caption:=IntToStr(UCHWYT_DO_T_ZADANIE1.F_ZADANIE1);
 END;
 finally
  UCHWYT_DO_T_ZADANIE1.Free;
 end;
end;

procedure TForm1.btn2_ZADANIE_2Click(Sender: TObject);
var
UCHWYT_DO_ZADANIE_2:T_ZADANIE2;
begin
 try  // CTRL+ALR+J ZEBY WYWOWAC TRY,FINALLY,END
 BEGIN
  UCHWYT_DO_ZADANIE_2:=T_ZADANIE2.Create;
   btn2_ZADANIE_2.Caption:=IntToStr(UCHWYT_DO_ZADANIE_2.F_ZADANIE2);
 END;
 finally
  UCHWYT_DO_ZADANIE_2.Free;
 end;
end;

end.

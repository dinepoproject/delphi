unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, KLASA_ABSTRAKCYJNA, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    btn1_ZADANIE_1: TButton;
    ZADANIE_2: TButton;
    procedure btn1_ZADANIE_1Click(Sender: TObject);
    procedure ZADANIE_2Click(Sender: TObject);
  private
    { Private declarations }
    procedure PokazOkno(const AZwrotka: MNOZENIE_ABSTR);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
uses ZADANIE_1,ZADANIE_2;

procedure TForm1.btn1_ZADANIE_1Click(Sender: TObject);
begin
PokazOkno(TZADANIE_1.CREATE);
end;

{ TForm1 }

procedure TForm1.PokazOkno(const AZwrotka: MNOZENIE_ABSTR);
BEGIN
try

    EDIT1.Text := IntToStr(AZwrotka.MNOZENIE_ABSTRAK);
  finally
    AZwrotka.Free;
  end;
END;

procedure TForm1.ZADANIE_2Click(Sender: TObject);
begin
  PokazOkno(TZADANIE_2.CREATE);
end;

end.

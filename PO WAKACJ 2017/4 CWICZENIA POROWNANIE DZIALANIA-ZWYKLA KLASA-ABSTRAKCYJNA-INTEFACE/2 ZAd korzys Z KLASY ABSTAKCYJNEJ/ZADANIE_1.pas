unit ZADANIE_1;

interface

uses
  KLASA_ABSTRAKCYJNA;

type
  TZADANIE_1 = class(MNOZENIE_ABSTR)
  public
    function MNOZENIE_ABSTRAK: INTEGER; override;
  end;

implementation

{ TZADANIE_1 }

function TZADANIE_1.MNOZENIE_ABSTRAK: INTEGER;
begin
RESULT:= 4*5;
end;

end.

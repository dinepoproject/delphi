unit KLASA_ZWYKLA;

interface
TYPE
  TMNOZENIE = class
  function POMNOZ (LICZBA1:Integer;LICZBA2:Integer): Integer;
  end;
implementation

{ MNOZENIE }

function TMNOZENIE.POMNOZ(LICZBA1, LICZBA2: Integer): Integer;
begin
Result:= LICZBA1 * LICZBA2;
end;

end.

object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 424
  ClientWidth = 619
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 128
    Width = 625
    Height = 17
    Align = alNone
    Color = clHighlight
    ParentColor = False
  end
  object Label1: TLabel
    Left = 424
    Top = 48
    Width = 82
    Height = 13
    Caption = 'string list i listbox'
  end
  object Label2: TLabel
    Left = 528
    Top = 151
    Width = 129
    Height = 13
    Caption = 'OPEN DIALOG'
  end
  object Splitter2: TSplitter
    Left = 0
    Top = 303
    Width = 619
    Height = 8
    Align = alNone
    Color = clBlue
    ParentColor = False
  end
  object Edit1: TEdit
    Left = 8
    Top = 8
    Width = 177
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object WCZYTAJ_DO_LISTY: TButton
    Left = 8
    Top = 35
    Width = 177
    Height = 25
    Caption = 'WCZYTAJ_DO_LISTY'
    TabOrder = 1
    OnClick = WCZYTAJ_DO_LISTYClick
  end
  object ODCZYTAJ_Z_LISTY: TButton
    Left = 399
    Top = 8
    Width = 169
    Height = 25
    Caption = 'ODCZYTAJ_Z_LISTY'
    TabOrder = 2
    OnClick = ODCZYTAJ_Z_LISTYClick
  end
  object Memo1: TMemo
    Left = 208
    Top = 8
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object ListBox1: TListBox
    Left = 8
    Top = 97
    Width = 145
    Height = 25
    ItemHeight = 13
    TabOrder = 4
    OnClick = ListBox1Click
  end
  object Wczytaj_z_edit_do_listbox: TButton
    Left = 7
    Top = 66
    Width = 195
    Height = 25
    Caption = 'Wczytaj_z_edit_do_listbox'
    TabOrder = 5
    OnClick = Wczytaj_z_edit_do_listboxClick
  end
  object btn1_ZAPISZ_EDITA_DO_SAVE_DIALOG: TButton
    Left = 248
    Top = 168
    Width = 241
    Height = 25
    Caption = 'btn1_ZAPISZ_EDITA_DO_SAVE_DIALOG'
    TabOrder = 6
    OnClick = btn1_ZAPISZ_EDITA_DO_SAVE_DIALOGClick
  end
  object Memo2: TMemo
    Left = 17
    Top = 208
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo2')
    TabOrder = 7
  end
  object btn1_WCZYTAJ_Z_OPEN_DIALOG: TButton
    Left = 248
    Top = 208
    Width = 241
    Height = 25
    Caption = 'btn1_WCZYTAJ_Z_OPEN_DIALOG'
    TabOrder = 8
    OnClick = btn1_WCZYTAJ_Z_OPEN_DIALOGClick
  end
  object Edit2: TEdit
    Left = 24
    Top = 160
    Width = 161
    Height = 21
    TabOrder = 9
    Text = 'Edit2'
  end
  object btn1_WCZYTAJ_LANCUCH_POTEM_MEMO: TButton
    Left = 24
    Top = 184
    Width = 218
    Height = 25
    Caption = 'btn1_WCZYTAJ_LANCUCH_POTEM_MEMO'
    TabOrder = 10
    OnClick = btn1_WCZYTAJ_LANCUCH_POTEM_MEMOClick
  end
  object OpenDialog1: TOpenDialog
    Left = 520
    Top = 176
  end
  object SaveDialog1: TSaveDialog
    Left = 576
    Top = 176
  end
end

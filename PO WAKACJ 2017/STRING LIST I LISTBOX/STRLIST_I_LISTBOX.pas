unit STRLIST_I_LISTBOX;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    WCZYTAJ_DO_LISTY: TButton;
    ODCZYTAJ_Z_LISTY: TButton;
    Memo1: TMemo;
    ListBox1: TListBox;
    Wczytaj_z_edit_do_listbox: TButton;
    Splitter1: TSplitter;
    Label1: TLabel;
    btn1_ZAPISZ_EDITA_DO_SAVE_DIALOG: TButton;
    Memo2: TMemo;
    Label2: TLabel;
    btn1_WCZYTAJ_Z_OPEN_DIALOG: TButton;
    Edit2: TEdit;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    btn1_WCZYTAJ_LANCUCH_POTEM_MEMO: TButton;
    Splitter2: TSplitter;
    procedure btn1_WCZYTAJ_LANCUCH_POTEM_MEMOClick(Sender: TObject);
    procedure btn1_WCZYTAJ_Z_OPEN_DIALOGClick(Sender: TObject);
    procedure btn1_ZAPISZ_EDITA_DO_SAVE_DIALOGClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ODCZYTAJ_Z_LISTYClick(Sender: TObject);
    procedure WCZYTAJ_DO_LISTYClick(Sender: TObject);
    procedure Wczytaj_z_edit_do_listboxClick(Sender: TObject);
  private
    { Private declarations }
    Lancuch: TStringList;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1_WCZYTAJ_LANCUCH_POTEM_MEMOClick(Sender: TObject);

    var
  I: Integer;
begin


begin
   Lancuch.Add(Edit2.Text);
   Memo2.LINES.Assign(Lancuch);

  END;
end;

procedure TForm1.btn1_WCZYTAJ_Z_OPEN_DIALOGClick(Sender: TObject);
begin
OpenDialog1.FileName:='';
OpenDialog1.Execute;
Memo2.Lines.LoadFromFile(OpenDialog1.FileName)
end;

procedure TForm1.btn1_ZAPISZ_EDITA_DO_SAVE_DIALOGClick(Sender: TObject);
  BEGIN
   SaveDialog1.FileName:= '';
SaveDialog1.Execute;
   begin
   if SaveDialog1.FileName <> '' then

   Memo2.LINES.SaveToFile(SaveDialog1.FileName);
   Memo2.Text:= '';
end;
  END;

procedure TForm1.FormCreate(Sender: TObject);
begin
 lancuch:= TStringList.Create;
 Edit1.Text:='';
 Memo1.Text:='';
end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
//ListBox1.Items.Add(Edit1.Text)
end;

procedure TForm1.ODCZYTAJ_Z_LISTYClick(Sender: TObject);
var
I:Integer;
begin
 for I := 0 to Lancuch.Count-1 do
 BEGIN

   Memo1.Text:= Memo1.Text + Lancuch[I];

 END;
  Memo1.Lines.Add('');

end;

procedure TForm1.WCZYTAJ_DO_LISTYClick(Sender: TObject);
begin
Memo1.Text:='';
Lancuch.Add(Edit1.Text);
Memo1.Lines.Assign(lancuch);
Edit1.Text:='';
Edit1.SetFocus;
end;

procedure TForm1.Wczytaj_z_edit_do_listboxClick(Sender: TObject);
begin
  ListBox1.Items.Add(Edit1.Text)
end;

end.

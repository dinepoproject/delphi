program Ksiega_przyslowia_losowanie;

uses
  Vcl.Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Ulosowanie in 'Ulosowanie.pas',
  U_losuj in 'U_losuj.pas',
  Unit2 in 'Unit2.pas' {Form2},
  Unit3 in 'Unit3.pas' {Form3};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.

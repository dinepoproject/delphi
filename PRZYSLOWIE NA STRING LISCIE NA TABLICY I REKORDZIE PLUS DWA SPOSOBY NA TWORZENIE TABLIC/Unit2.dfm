object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 463
  ClientWidth = 695
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 472
    Top = 80
    Width = 195
    Height = 13
    Caption = 'WCZYTAJ DO MEMO PIERWSZY SPOSOB'
    OnClick = Label1Click
  end
  object Button1: TButton
    Left = 376
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 80
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
    OnChange = Edit1Change
  end
  object Memo1: TMemo
    Left = 80
    Top = 72
    Width = 371
    Height = 353
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
  end
  object Button2: TButton
    Left = 512
    Top = 152
    Width = 129
    Height = 25
    Caption = 'ZMIEN 2 ELEMENT'
    TabOrder = 3
    OnClick = Button2Click
  end
  object LabeledEdit1: TLabeledEdit
    Left = 520
    Top = 120
    Width = 121
    Height = 24
    EditLabel.Width = 125
    EditLabel.Height = 13
    EditLabel.Caption = 'KTORY ELEMENT ZMIENIC'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
end

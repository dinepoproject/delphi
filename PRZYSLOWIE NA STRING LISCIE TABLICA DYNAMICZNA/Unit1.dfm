object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 436
  ClientWidth = 788
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 32
    Top = 59
    Width = 217
    Height = 25
    Caption = 'Zapisz przys'#322'owie'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 32
    Top = 32
    Width = 580
    Height = 21
    TabOrder = 1
    OnChange = Edit1Change
  end
  object Button2: TButton
    Left = 32
    Top = 207
    Width = 249
    Height = 25
    Caption = 'Poka'#380' przyslowia'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 32
    Top = 238
    Width = 249
    Height = 25
    Caption = 'Losuj przys'#322'owie na dzisiaj'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Memo1: TMemo
    Left = 32
    Top = 88
    Width = 580
    Height = 113
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object Memo2: TMemo
    Left = 24
    Top = 280
    Width = 697
    Height = 89
    Lines.Strings = (
      'Memo2')
    TabOrder = 5
  end
  object Button4: TButton
    Left = 328
    Top = 238
    Width = 273
    Height = 25
    Caption = 'Czy'#347#263
    TabOrder = 6
    OnClick = Button4Click
  end
  object btn5_wlacz_podlad: TButton
    Left = 268
    Top = 59
    Width = 139
    Height = 25
    Caption = 'W'#321#260'CZ PODGL'#260'D'
    TabOrder = 7
    OnClick = btn5_wlacz_podladClick
  end
  object btn5_wylacz_podglad: TButton
    Left = 427
    Top = 59
    Width = 185
    Height = 25
    Caption = 'WY'#321#260'CZ PODGL'#260'D'
    TabOrder = 8
    OnClick = btn5_wylacz_podgladClick
  end
end

object Form5: TForm5
  Left = 0
  Top = 0
  Caption = 'AMP DGT'
  ClientHeight = 334
  ClientWidth = 748
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 288
    Top = 288
    Width = 241
    Height = 25
    Caption = 'Dodaj do obserwowanych'
    TabOrder = 0
    OnClick = Button1Click
  end
  object CheckBox1: TCheckBox
    Left = 160
    Top = 40
    Width = 97
    Height = 17
    Caption = 'E-mail'
    TabOrder = 1
  end
  object CheckBox2: TCheckBox
    Left = 160
    Top = 63
    Width = 97
    Height = 17
    Caption = 'Dyspozytor'
    TabOrder = 2
  end
  object CheckBox3: TCheckBox
    Left = 160
    Top = 96
    Width = 97
    Height = 17
    Caption = 'Godzina do SAP'
    TabOrder = 3
  end
  object CheckBox4: TCheckBox
    Left = 160
    Top = 119
    Width = 153
    Height = 17
    Caption = 'Potwierdzenie w GIDEX'
    TabOrder = 4
  end
  object CheckBox5: TCheckBox
    Left = 160
    Top = 142
    Width = 177
    Height = 17
    Caption = 'Wykre'#347'lone z listy tor'#243'w'
    TabOrder = 5
  end
  object CheckBox6: TCheckBox
    Left = 160
    Top = 165
    Width = 193
    Height = 17
    Caption = 'Wpisane do zeszytu - przewo'#378'nik'
    TabOrder = 6
  end
  object CheckBox7: TCheckBox
    Left = 160
    Top = 188
    Width = 185
    Height = 17
    Caption = 'Wpisane do zeszytu wysy'#322'ek'
    TabOrder = 7
  end
  object Button2: TButton
    Left = 592
    Top = 24
    Width = 129
    Height = 25
    Caption = 'Czy'#347#263' formularz'
    TabOrder = 8
    OnClick = Button2Click
  end
end

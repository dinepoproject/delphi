object Form6: TForm6
  Left = 0
  Top = 0
  Caption = 'EKO'
  ClientHeight = 314
  ClientWidth = 732
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object CheckBox4: TCheckBox
    Left = 104
    Top = 97
    Width = 97
    Height = 17
    Caption = 'Dyspozytor'
    TabOrder = 0
  end
  object CheckBox9: TCheckBox
    Left = 104
    Top = 184
    Width = 169
    Height = 17
    Caption = 'Wpisane do zeszytu wysy'#322'ek'
    TabOrder = 1
  end
  object CheckBox7: TCheckBox
    Left = 104
    Top = 166
    Width = 177
    Height = 17
    Caption = 'Wykre'#347'lone z listy tor'#243'w'
    TabOrder = 2
  end
  object CheckBox6: TCheckBox
    Left = 104
    Top = 143
    Width = 145
    Height = 17
    Caption = 'Potwierdzenie w Gidex'
    TabOrder = 3
  end
  object CheckBox5: TCheckBox
    Left = 104
    Top = 120
    Width = 97
    Height = 17
    Caption = 'Godzina do Sap'
    TabOrder = 4
  end
  object CheckBox3: TCheckBox
    Left = 104
    Top = 74
    Width = 97
    Height = 17
    Caption = 'E-maile'
    TabOrder = 5
  end
  object CheckBox2: TCheckBox
    Left = 104
    Top = 51
    Width = 97
    Height = 17
    Caption = 'Waren'
    TabOrder = 6
  end
  object CheckBox1: TCheckBox
    Left = 104
    Top = 28
    Width = 97
    Height = 17
    Caption = 'Fax'
    TabOrder = 7
  end
  object CheckBox10: TCheckBox
    Left = 104
    Top = 230
    Width = 97
    Height = 17
    Caption = 'Kontrola tona'#380'u'
    TabOrder = 8
  end
  object Button1: TButton
    Left = 584
    Top = 24
    Width = 121
    Height = 25
    Caption = 'Czy'#347#263' formularz'
    TabOrder = 9
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 408
    Top = 281
    Width = 217
    Height = 25
    Caption = 'Dodaj do obserwowanych'
    TabOrder = 10
    OnClick = Button2Click
  end
  object CheckBox8: TCheckBox
    Left = 104
    Top = 208
    Width = 201
    Height = 17
    Caption = 'Wpisane do zeszytu przewo'#378'nika'
    TabOrder = 11
  end
end

object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Ustawienia czasu powiadomie'#324
  ClientHeight = 201
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 312
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Wykonaj'
    TabOrder = 0
    OnClick = Button1Click
  end
  object RadioGroup1: TRadioGroup
    Left = 48
    Top = 8
    Width = 185
    Height = 169
    Caption = 'Wybierz czas powiadomiena'
    TabOrder = 1
  end
  object RadioButton1: TRadioButton
    Left = 48
    Top = 32
    Width = 113
    Height = 17
    Caption = 'Co 20 minut'
    TabOrder = 2
  end
  object RadioButton2: TRadioButton
    Left = 48
    Top = 55
    Width = 113
    Height = 17
    Caption = 'co 40 minut'
    TabOrder = 3
  end
  object RadioButton3: TRadioButton
    Left = 48
    Top = 78
    Width = 113
    Height = 17
    Caption = 'Co jedn'#261' godzin'#281
    TabOrder = 4
  end
  object RadioButton4: TRadioButton
    Left = 48
    Top = 101
    Width = 113
    Height = 17
    Caption = 'Co dwie godziny'
    TabOrder = 5
  end
  object RadioButton5: TRadioButton
    Left = 48
    Top = 124
    Width = 113
    Height = 17
    Caption = 'Co trzy godziny'
    TabOrder = 6
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 336
    Top = 56
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 20000
    OnTimer = Timer2Timer
    Left = 288
    Top = 16
  end
end

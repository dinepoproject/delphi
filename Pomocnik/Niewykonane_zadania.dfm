object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Zadania nie wykonane'
  ClientHeight = 730
  ClientWidth = 1109
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 368
    Top = 688
    Width = 353
    Height = 16
    Caption = 'Okienko ukrywa sie automatycznie po dw'#243'ch minutach'
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 1073
    Height = 643
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Lines.Strings = (
      '')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Button1: TButton
    Left = 424
    Top = 657
    Width = 209
    Height = 25
    Caption = 'Zatrzymaj powiadomienia'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 72
    Top = 685
    Width = 75
    Height = 25
    Caption = 'Ukryj'
    TabOrder = 2
    OnClick = Button2Click
  end
end

program Pomocnik;

uses
  Vcl.Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Rumunia in 'Rumunia.pas' {Form2},
  Formatka_czas in 'Formatka_czas.pas' {Form3},
  Niewykonane_zadania in 'Niewykonane_zadania.pas' {Form4},
  AMP_DGT in 'AMP_DGT.pas' {Form5},
  EKO in 'EKO.pas' {Form6};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm6, Form6);
  Application.Run;
end.

object Form2: TForm2
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = 'Rumunia'
  ClientHeight = 369
  ClientWidth = 708
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object CheckBox1: TCheckBox
    Left = 104
    Top = 28
    Width = 97
    Height = 17
    Caption = 'Fax'
    TabOrder = 0
  end
  object CheckBox2: TCheckBox
    Left = 104
    Top = 51
    Width = 97
    Height = 17
    Caption = 'Waren'
    TabOrder = 1
  end
  object CheckBox3: TCheckBox
    Left = 104
    Top = 74
    Width = 97
    Height = 17
    Caption = 'E-maile'
    TabOrder = 2
  end
  object CheckBox4: TCheckBox
    Left = 104
    Top = 97
    Width = 97
    Height = 17
    Caption = 'Dyspozytor'
    TabOrder = 3
  end
  object CheckBox5: TCheckBox
    Left = 104
    Top = 120
    Width = 97
    Height = 17
    Caption = 'Godzina do Sap'
    TabOrder = 4
  end
  object CheckBox6: TCheckBox
    Left = 104
    Top = 143
    Width = 145
    Height = 17
    Caption = 'Potwierdzenie w Gidex'
    TabOrder = 5
  end
  object Button1: TButton
    Left = 416
    Top = 304
    Width = 203
    Height = 25
    Caption = 'Dodaj do obserwowanych'
    TabOrder = 6
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 520
    Top = 24
    Width = 145
    Height = 25
    Caption = 'Czy'#347#263' formularz'
    TabOrder = 7
    OnClick = Button2Click
  end
  object CheckBox7: TCheckBox
    Left = 104
    Top = 166
    Width = 177
    Height = 17
    Caption = 'Wykre'#347'lone z listy tor'#243'w'
    TabOrder = 8
  end
  object CheckBox8: TCheckBox
    Left = 104
    Top = 189
    Width = 193
    Height = 17
    Caption = 'Wpisane do zeszytu - przewo'#378'nik'
    TabOrder = 9
  end
  object CheckBox9: TCheckBox
    Left = 104
    Top = 212
    Width = 169
    Height = 17
    Caption = 'Wpisane do zeszytu wysy'#322'ek'
    TabOrder = 10
  end
end

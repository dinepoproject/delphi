object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 446
  ClientWidth = 706
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object btn1Rekord1: TButton
    Left = 32
    Top = 19
    Width = 75
    Height = 25
    Caption = 'btn1Rekord1'
    TabOrder = 0
    OnClick = btn1Rekord1Click
  end
  object btn1_rek_jako_zmienna: TButton
    Left = 129
    Top = 19
    Width = 174
    Height = 25
    Caption = 'btn1_rek_jako_zmienna'
    TabOrder = 1
    OnClick = btn1_rek_jako_zmiennaClick
  end
  object btn1Rekord2: TButton
    Left = 21
    Top = 50
    Width = 75
    Height = 25
    Caption = 'btn1Rekord2'
    TabOrder = 2
    OnClick = btn1Rekord2Click
  end
  object btn1tab_rec: TButton
    Left = 129
    Top = 50
    Width = 75
    Height = 25
    Caption = 'btn1tab_rec'
    TabOrder = 3
    OnClick = btn1tab_recClick
  end
  object btn1tab_rec_licz: TButton
    Left = 259
    Top = 59
    Width = 75
    Height = 25
    Caption = 'btn1tab_rec_licz'
    TabOrder = 4
    OnClick = btn1tab_rec_liczClick
  end
  object grp1: TGroupBox
    Left = 50
    Top = 145
    Width = 478
    Height = 224
    Caption = 'grp1'
    TabOrder = 5
    object edt1: TEdit
      Left = 3
      Top = 19
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'edt1'
      OnChange = edt1Change
    end
    object Button1: TButton
      Left = 150
      Top = 15
      Width = 75
      Height = 25
      Caption = 'dodaj'
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 268
      Top = 15
      Width = 75
      Height = 25
      Caption = 'wyswietl'
      TabOrder = 2
      OnClick = Button2Click
    end
    object Memo1: TMemo
      Left = 0
      Top = 83
      Width = 216
      Height = 138
      Lines.Strings = (
        'Memo1')
      TabOrder = 3
    end
    object btn1usunlinie: TButton
      Left = 389
      Top = 18
      Width = 75
      Height = 25
      Caption = 'wyczysc ekranik'
      TabOrder = 4
      OnClick = btn1usunlinieClick
    end
    object Edit1: TEdit
      Left = 3
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 5
      Text = 'Edit1'
      OnChange = Edit1Change
    end
    object btn1_usun_pamieci: TButton
      Left = 359
      Top = 71
      Width = 116
      Height = 25
      Caption = 'btn1_usun_pamieci'
      TabOrder = 6
      OnClick = btn1_usun_pamieciClick
    end
  end
end

object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Forma_glowna'
  ClientHeight = 501
  ClientWidth = 847
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 425
    Top = 8
    Width = 13
    Height = 485
    Align = alCustom
  end
  object grp1: TGroupBox
    Left = -10
    Top = -2
    Width = 418
    Height = 438
    Caption = 'grp1'
    TabOrder = 0
    object Button1: TButton
      Left = 12
      Top = 13
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Edit1: TEdit
      Left = 110
      Top = 15
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'Edit1'
    end
  end
  object GroupBox1: TGroupBox
    Left = 444
    Top = 8
    Width = 395
    Height = 438
    Caption = 'GroupBox1'
    TabOrder = 1
    object btn1funkcjamnozenie: TButton
      Left = 3
      Top = 13
      Width = 96
      Height = 25
      Caption = 'btn1funkcjamnozenie'
      TabOrder = 0
      OnClick = btn1funkcjamnozenieClick
    end
    object Edit2: TEdit
      Left = 105
      Top = 15
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'Edit2'
    end
    object Edit3: TEdit
      Left = 247
      Top = 15
      Width = 121
      Height = 21
      TabOrder = 2
      Text = 'Edit3'
    end
    object Memo1: TMemo
      Left = 3
      Top = 44
      Width = 185
      Height = 52
      Lines.Strings = (
        'Memo1')
      TabOrder = 3
    end
  end
end

unit Klasy_forma;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

  type
  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    grp1: TGroupBox;
    GroupBox1: TGroupBox;
    btn1funkcjamnozenie: TButton;
    Edit2: TEdit;
    Edit3: TEdit;
    Memo1: TMemo;
    Splitter1: TSplitter;
    procedure Button1Click(Sender: TObject);
    procedure btn1funkcjamnozenieClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  // Klasy mo�na zadeklarowa�
  //albo w sekcji Interface modu�u, albo w sekcji Implementation(nie polecam).
  //
var
  Form1: TForm1;

implementation

{$R *.dfm}
 uses
 funkcje,Procedury;

  procedure TForm1.btn1funkcjamnozenieClick(Sender: TObject);
  var
  funkci:Tfunkcje;
begin
try
   funkci:= tfunkcje.Create;

   // w filmie 'przyslowie Adam robi zmiany w klasie, wywolujemy konstruktor klasy w form create
   // a free w destruktorze

  Memo1.Text:= IntToStr((funkci.mnozenie( StrToInt(Edit2.text),StrToInt(Edit3.text))));
finally
   funkci.Free;
end;

end;

procedure TForm1.Button1Click(Sender: TObject);
 var
proci: tprocedury;
begin
try
 proci:=tprocedury.create;
 proci.iloraz(StrTofloat(Edit1.Text));
finally
 proci.Free
end;

end;

end.

unit Powt_cz_2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;
type
Tmojreko=record
  mojx:Integer;
  mojy:Integer;
  end;
type
Tdru_rec=record
  dru_x:Integer;
  dru_y:Integer;
end;

  rekord_mmx = record
  end;
  // rekord probny dokonany przez mmx
type
Trec_tab= array [1..2] of record
imie: string;
nazwisko: string;
end;
type
Trec_tab_liczby= array [1..2] of record
jed:Integer;
dwa:Integer;
end;
type
Tdodajlinie = array of record
pierwsza_linia:string;
druga_linia:string;
//druga_linia:string;
end;

type
  TForm1 = class(TForm)
    btn1Rekord1: TButton;
    btn1_rek_jako_zmienna: TButton;
    btn1Rekord2: TButton;
    btn1tab_rec: TButton;
    btn1tab_rec_licz: TButton;
    grp1: TGroupBox;
    edt1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    btn1usunlinie: TButton;
    Edit1: TEdit;
    btn1_usun_pamieci: TButton;
    procedure btn1Rekord1Click(Sender: TObject);
    procedure btn1_rek_jako_zmiennaClick(Sender: TObject);
    procedure btn1Rekord2Click(Sender: TObject);
    procedure btn1tab_recClick(Sender: TObject);
    procedure btn1tab_rec_liczClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edt1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btn1usunlinieClick(Sender: TObject);
    procedure btn1_usun_pamieciClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
    uchwyt_do_reckordu_Tdodaj_linie: Tdodajlinie;
    ile: Integer;
    numer: Integer;
    lancuch: TStringList;

  public
    { Public declarations }
    function recordo (mojreko:Tmojreko):integer;
    function rek_zmienna:Integer;
    function drugi_rec (sec_rec:Tdru_rec):Integer;
    function trzeci_rec (dane:Trec_tab):string;
    function czwart_rec:Integer;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
     procedure odswiez;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

{ TForm1 }

 procedure TForm1.ODSWIEZ;
 BEGIN
 uchwyt_do_reckordu_Tdodaj_linie[numer].pierwsza_linia := edt1.Text;
  uchwyt_do_reckordu_Tdodaj_linie[numer].druga_linia:=Edit1.Text;
 END;

procedure TForm1.btn1Rekord1Click(Sender: TObject);
var
yreco:Tmojreko;  // zamienilem tu myreco(patrz w parametrze funkcji recordo)
// na yreco i i tak dziala
begin
 yreco.mojx:= 5;
 yreco.mojy:= 5;
ShowMessage (inttostr(recordo(yreco))); // podajemy tylko nazwe uchwytu (yreco)
// w recordo jest jak liczyc, przypisujemy do Tmojreko tutaj uchwyt yreco
// a w funkcji uchwyt ma nazwe w parametrze mojreco, ale to nie ma znaczenia
//pdf delphi 7 str. 61
end;

procedure TForm1.btn1Rekord2Click(Sender: TObject);
var
ec_rec:Tdru_rec;

begin
ec_rec.dru_x:=60;
ec_rec.dru_y:=80;
ShowMessage (inttostr(drugi_rec(ec_rec)));  // tu tez zmieniam nazwe uchwytu na taki
// jak jest w parametrze funkcji i dzia�a
end;

procedure TForm1.btn1tab_recClick(Sender: TObject);
var
dane:Trec_tab;
begin
 dane[1].imie:= 'Jola';
 dane[2].imie:= 'Julia';
 dane[1].nazwisko:= 'Robak';
 dane[2].nazwisko:='kotek';
  ShowMessage(trzeci_rec(dane));

  //najpierw dla dane przypisujesz tablice rekord�w
  //potem dane[1].imie itd.
end;

procedure TForm1.btn1tab_rec_liczClick(Sender: TObject);
begin
ShowMessage (IntToStr(czwart_rec));
end;

procedure TForm1.btn1usunlinieClick(Sender: TObject);
begin
Memo1.Lines.Clear;

end;

procedure TForm1.btn1_rek_jako_zmiennaClick(Sender: TObject);
begin
 ShowMessage (IntToStr(rek_zmienna));
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
ile:=ile+1;
SetLength(uchwyt_do_reckordu_Tdodaj_linie,ile);// TAK JAKBY DO UCHWYTU REKORDOW
// NADAJEMY NUMERY RECORD�W
numer:=ILE-1;
 // UWAGA UWAGA UWAGA
 // SKOPIOWALEM TO Z FORM.CREATE A NA GORZE DODALEM ILE:=ILE +1
 // ZAMIENILEM SetLength(uchwyt_do_reckordu_Tdodaj_linie,1);  NA SetLength(uchwyt_do_reckordu_Tdodaj_linie,ile);
 // numer:=0; ZAMIENILEM NA NUMER := ILE-1;
 ODSWIEZ;
 // w odswiez jest: uchwyt_do_reckordu_Tdodaj_linie[numer].pierwsza_linia := edt1.Text;
 lancuch.Add(uchwyt_do_reckordu_Tdodaj_linie[numer].pierwsza_linia);
  lancuch.Add(uchwyt_do_reckordu_Tdodaj_linie[numer].druga_linia);
 //  uchwyt_do_reckordu_Tdodaj_linie[numer] to uchwyt do rekordu
 // do kt�rego tworzymy tablice,  .pierwsza_linia to element recordu
 lancuch.SaveToFile('cwiczenie_recordy');
 edt1.Text:='';
 Edit1.Text:='';
  Memo1.Lines.Assign(lancuch);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Memo1.Lines.Clear;
lancuch.LoadFromFile('cwiczenie_recordy');
//Memo1.Lines.AddStrings(lancuch); :) dziala sam to znalazlem
//Memo1.Lines.add(lancuch[0]); dodaje tylko jedna linie

Memo1.Lines.Assign(lancuch);
end;

constructor TForm1.Create(AOwner: TComponent);
begin
  inherited;
lancuch:= TStringList.Create;
 Memo1.Text:= '';

end;

function TForm1.czwart_rec: Integer;
var
wyni: Trec_tab_liczby;
begin
wyni[1].jed:= 1;
wyni[1].dwa :=4;
wyni[2].jed:= 6;
wyni[2].dwa:= 9;
result:= wyni[1].jed * wyni[2].dwa;
end;

destructor TForm1.Destroy;
begin
  lancuch.Free;
  inherited;
end;

procedure TForm1.btn1_usun_pamieciClick(Sender: TObject);
begin
 lancuch.Clear;
lancuch.SaveToFile('cwiczenie_recordy');
Memo1.Lines.Clear;
end;

function TForm1.drugi_rec(sec_rec: Tdru_rec): Integer;

begin
 Result:= sec_rec.dru_x * sec_rec.dru_y;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
uchwyt_do_reckordu_Tdodaj_linie[numer].druga_linia:=Edit1.Text;
end;

procedure TForm1.edt1Change(Sender: TObject);
begin
uchwyt_do_reckordu_Tdodaj_linie[numer].pierwsza_linia := edt1.Text;
// kiedy edt1.text by�o po lewej stronie nie dalo sie pisac w edicie ??
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
 SetLength(uchwyt_do_reckordu_Tdodaj_linie,1);
 ile:=1;
 numer:=0;  // TRZEBA ZADEKLAROWAC TABLICE PODCZAS URUCHAMIANIA
 edt1.Text:='';
 Edit1.Text:='';
end;

function TForm1.recordo(mojreko: Tmojreko): integer;
begin
Result:= mojreko.mojx * mojreko.mojy;
end;

function TForm1.rek_zmienna: Integer;
var
abrec:record
a,b:Integer;
end;
begin
abrec.a:=1;
abrec.b:=2;
Result:=abrec.a * abrec.b;
end;

function TForm1.trzeci_rec(dane: Trec_tab): string;

begin
 result:= dane[1].imie + ' ' + dane[2].nazwisko;  // tu jest tablica rekordow
end;

end.

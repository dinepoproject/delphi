object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 448
  ClientWidth = 670
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btn1_petla_for: TButton
    Left = 234
    Top = 28
    Width = 126
    Height = 25
    Caption = 'btn1_petla_for'
    TabOrder = 0
    OnClick = btn1_petla_forClick
  end
  object Memo1: TMemo
    Left = 25
    Top = 8
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object btn1Petla_while: TButton
    Left = 234
    Top = 59
    Width = 117
    Height = 25
    Caption = 'btn1Petla_while'
    TabOrder = 2
    OnClick = btn1Petla_whileClick
  end
  object btn1_petla_repeat: TButton
    Left = 234
    Top = 90
    Width = 126
    Height = 25
    Caption = 'btn1_petla_repeat'
    TabOrder = 3
    OnClick = btn1_petla_repeatClick
  end
  object btn1CONTINUE: TButton
    Left = 249
    Top = 136
    Width = 75
    Height = 25
    Caption = 'btn1CONTINUE'
    TabOrder = 4
    OnClick = btn1CONTINUEClick
  end
  object btn1BREAK: TButton
    Left = 440
    Top = 34
    Width = 75
    Height = 25
    Caption = 'btn1BREAK'
    TabOrder = 5
    OnClick = btn1BREAKClick
  end
end

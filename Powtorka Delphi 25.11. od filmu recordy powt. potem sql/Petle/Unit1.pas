unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    btn1_petla_for: TButton;
    Memo1: TMemo;
    btn1Petla_while: TButton;
    btn1_petla_repeat: TButton;
    btn1CONTINUE: TButton;
    btn1BREAK: TButton;
    procedure btn1_petla_forClick(Sender: TObject);
    procedure btn1Petla_whileClick(Sender: TObject);
    procedure btn1_petla_repeatClick(Sender: TObject);
    procedure btn1CONTINUEClick(Sender: TObject);
    procedure btn1BREAKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
  uses
  Unit2,Unit3,unit4,Unit5,Unit6;
{$R *.dfm}

procedure TForm1.btn1BREAKClick(Sender: TObject);
VAR
UCHWYT_BREAK: Tbreakklasa;
begin
UCHWYT_BREAK:= TBREAKklasa.Create;

 Memo1.Text:= UCHWYT_BREAK.PetWithBreak;  // w ramach cwiczenia pola w klasie
end;

procedure TForm1.btn1CONTINUEClick(Sender: TObject);
VAR
UCHWYT_CONTINUE: Tcontinueklasa;
begin
UCHWYT_CONTINUE:= Tcontinueklasa.Create;
 Memo1.Text:= Memo1.Text + UCHWYT_CONTINUE.PetWithContinue;
end;

procedure TForm1.btn1Petla_whileClick(Sender: TObject);
var
uchwyt_while: Twhileklasa;
begin
try
 Memo1.Text:= '';
 uchwyt_while:= Twhileklasa.Create;
 uchwyt_while.petwhile(5);
finally
 uchwyt_while.Free;
end;

end;

procedure TForm1.btn1_petla_forClick(Sender: TObject);
var
uchwyt_for:Tforkklasa;
begin
 try
 Memo1.Text:= '';
  uchwyt_for:= Tforkklasa.Create;
  uchwyt_for.petfor(5);
 finally
   uchwyt_for.Free
 end;


 end;


procedure TForm1.btn1_petla_repeatClick(Sender: TObject);
var
uchwyt_repeat:Trepeatklasa;
begin
 try
 Memo1.Text:= '';
  uchwyt_repeat:= Trepeatklasa.Create;
  uchwyt_repeat.petrepeat(5);
 finally
   uchwyt_repeat.Free;
 end;
end;

end.

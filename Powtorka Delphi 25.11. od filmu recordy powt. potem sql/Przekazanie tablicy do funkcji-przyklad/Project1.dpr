program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

{Mo�liwe jest przekazywanie ca�ych tablic jako
 parametr�w do funkcji lub procedury.
 Jednak w przypadku funkcji nie da si� tego uczyni�
 bezpo�rednio ? nale�y w tym celu utworzy� nowy typ danych, np. taki:
  }
type
  TMyType = array of String;
  {Dopiero teraz mo�na przekaza� go jako parametr,
  tak jak przedstawiono to w poni�szym przyk�adzie:
}
function ArrayFunc : TMyType;
begin
  SetLength(Result, 2);
  Result[0] := 'Delphi';
  Result[1] := '2005';
end;

var
  A : TMyType;

begin
  SetLength(A, 2);
  A := ArrayFunc;
  Writeln(A[0]);
  Writeln(A[1]);
  Readln;

end.

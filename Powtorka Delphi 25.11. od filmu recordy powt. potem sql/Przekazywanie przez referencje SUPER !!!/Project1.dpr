program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;
  //http://4programmers.net/Delphi/Wska�niki
procedure Foo(  var Value : String);  // wycinaj i wklejaj var, bedzie wiadomo jak dzia�a
begin                               // wpisuj tez OUT  :)
  Value := Value + ' jest fajne';
end;  // sprobuj wpisywac tez  Value := ' jest fajne';

var
  S : String;        // var - Delphi jest fajne// przetwarza zmienna w procedurze?
                     // out - jest fajne
                     // brak - Delphi // czyli cialo procedury sie nie wykonuje?
begin
  S := 'Delphi';
  Foo(S);

  Writeln(S);
  Readln;
end.
// tu jest to inaczaej: http://kursprogramowania.edu.pl/pascal/lekcja-14/

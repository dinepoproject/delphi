program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  Vcl.Dialogs;

type
  TInfoRec = packed record
    FName : String[30];
    SName : String[30];
    Age : Byte;
    Pesel : Int64;
    Nip : String[60]
  end;

  PInfoRec = ^TInfoRec; // utworzenie wska�nika

  procedure SomeProc(MMInfoRec : PInfoRec);
  begin
    ShowMessage('Dotychczasowa warto�� InfoRec.FName to ' + MMInfoRec.FName + '. Zmieniam na Adam');
    MMInfoRec.FName:= 'ADAM'; // zmiana danych
  end;

var
  InfoRec: TInfoRec;

begin
  InfoRec.FName := 'Jan';
  InfoRec.SName := 'Kowalski';
  InfoRec.Age := 41;
  InfoRec.Pesel := 55012010013;
  InfoRec.Nip := '34234?23432?23423';
     // TUTAJ NA DOLE ZAKOMENTOWUJ I NIE, WTEDY WIDAC JAK DZIA�A
    SomeProc(@InfoRec);

  ShowMessage(InfoRec.FName); // wy�wietlenie zmienionej warto�ci

end.

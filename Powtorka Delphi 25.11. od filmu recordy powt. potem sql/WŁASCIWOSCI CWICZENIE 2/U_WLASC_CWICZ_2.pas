unit U_WLASC_CWICZ_2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;
 type
 Tklasa2 = class
   private
    { Private declarations }
       zmienna_pole: string;
    function odczyt: string;
    procedure zapis (const arg:string); // wed�ug filmu o klasach orgument musi byc
    // poprzedzony const
    public
    { Public declarations }

    property liczba: STRING read odczyt write zapis;
 end;
type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    private
     uchwyt_do_klasy2: Tklasa2;  // zapamaietac ta konstrukcje szczeg�lne, �e uchwyt
     // jest w private form1.
     constructor Create(AOwner: TComponent); override;
     destructor Destroy; override;

  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);

 begin
  uchwyt_do_klasy2.liczba:= Edit2.Text;

 end;



procedure TForm1.Button2Click(Sender: TObject);
  begin
  Edit1.Text:= uchwyt_do_klasy2.liczba;

 end;

constructor TForm1.Create(AOwner: TComponent);
begin
  inherited;
uchwyt_do_klasy2:= Tklasa2.Create;
end;

destructor TForm1.Destroy;
begin
 uchwyt_do_klasy2.Free;
  inherited;
end;

{ TForm1 }

function Tklasa2.odczyt: string;
begin
 result:= zmienna_pole;
end;

procedure Tklasa2.zapis(const arg: string);
begin
 zmienna_pole:= arg;
end;

end.

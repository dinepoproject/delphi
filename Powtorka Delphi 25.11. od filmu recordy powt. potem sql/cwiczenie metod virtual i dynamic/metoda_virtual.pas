unit metoda_virtual;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    btn1_toyota: TButton;
    procedure btn1_toyotaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  T_samochod = class(TObject)
   procedure Jedz;  virtual;
  end;

  T_toYota = class(T_samochod)
   procedure Jedz;override;
  end;

  T_oPel = class(T_samochod)
   procedure Jedz;
  end;

//   https://4programmers.net/Delphi/Przedefiniowanie_metod
var
  Form1: TForm1;

implementation

{$R *.dfm}

{ T_samochod }

procedure T_samochod.Jedz;
begin
ShowMessage('Metoda Jedz z klasy T_samochod');
end;

{ T_toYota }

procedure T_toYota.Jedz;
begin
 ShowMessage('Metoda Jedz z klasy T_toYota');
end;

{ T_opel}

procedure T_oPel.Jedz;
begin
 ShowMessage('Metoda Jedz z klasy T_Opel');
end;

procedure TForm1.btn1_toyotaClick(Sender: TObject);
var
klasa_glowna: T_samochod;
begin
  klasa_glowna:= T_toYota.Create;
  // zwroc uwage, ze jest wywolany konstruktor z klasy toyota
  // jest to mozliwe w przypdadku dziedziczenie bo klasy
  // sa powiazane
  // wpisanie T_samoch�d.create wywoluje .jedz z t_samochod
  // wpisanie T_opel.create wywoluje .jedz z t_samochod  bo
  // nie ma on wpisane override i nie przykrywa ; - )

  klasa_glowna.Jedz;
  klasa_glowna.Free;

//pierwsze cwiczenie bez oznaczenia T_samochod |(g�owna klasa)jako virtual
// pokazuje  'Metoda Jedz z klasy T_samochod'

// dodajemy do procedury jedz w T_samochod virtual
           //do procedury jedz w T_toYota override

           // pokazuje  'Metoda Jedz z klasy T_toYota'
end;
//obejrzyj klasy (pi�ka) tak jest inaczej dziedziczenie
// tu i tam jest przyk�ad kiedy sa metody o tej samej nazwie
// ale tam uzywa sie inherited i wynik przypisuje do zmiennej
end.

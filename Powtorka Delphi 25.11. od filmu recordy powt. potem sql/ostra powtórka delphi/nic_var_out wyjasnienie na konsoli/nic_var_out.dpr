program nic_var_out;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

// zeby zapamietac wpisuj , nic, var, out


{$APPTYPE CONSOLE}
procedure SetValue( var Message : String);
begin
{ Message nie zawiera warto�ci! }
Message:=  Message +'Hello there!';
 // zeby sobie przypomniec  to najpierw
// przy probach zrob cos takiego, zakomentarzuj:

   // Message:= Message + 'Hello there!';

   //  i wtedy wpisuj nic, var, out


   // wytnij tez drugiego message i zrob var  tak:
   // Message:= 'Hello there!';

end;
var
S : String;
begin
S := 'Hello World';
SetValue(S); // przekazanie zmiennej
Writeln(S); // odczyt warto�ci zmiennej
Readln;
end.
// nic - pobiera tylko zmienna zewnetrzna 'hello world'

// var - pobiera zmienna zewnetrzna i wykonuje na niej operacje
// wewnatrz procedury zwraca Hello World Hello there czyli
// var jest po to by pobrac zmienna zewnetrzna
//i przerobic ja w ciele procedury.

// out- zwraca samo hellothere

object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 745
  ClientWidth = 1366
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 100
    Top = 8
    Width = 75
    Height = 25
    Caption = 'undo'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 166
    Top = 39
    Width = 75
    Height = 25
    Caption = 'stala'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'referencja'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 8
    Top = 39
    Width = 129
    Height = 25
    Caption = 'niereferencja'
    TabOrder = 3
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 194
    Top = 8
    Width = 75
    Height = 25
    Caption = 'PI'
    TabOrder = 4
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 290
    Top = 8
    Width = 84
    Height = 25
    Caption = 'tablica zwyk'#322'a'
    TabOrder = 5
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 394
    Top = 8
    Width = 139
    Height = 25
    Caption = 'tablica zwykla z petla'
    TabOrder = 6
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 247
    Top = 39
    Width = 118
    Height = 25
    Caption = 'Tablice jako sta'#322'e'
    TabOrder = 7
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 380
    Top = 39
    Width = 92
    Height = 25
    Caption = 'Tablica wielowy'
    TabOrder = 8
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 8
    Top = 70
    Width = 116
    Height = 25
    Caption = 'tablica dynamiczna'
    TabOrder = 9
    OnClick = Button10Click
  end
  object btnFormatowanie: TButton
    Left = 155
    Top = 70
    Width = 114
    Height = 25
    Caption = 'btnFormatowanie'
    TabOrder = 10
    OnClick = btnFormatowanieClick
  end
  object formatowaniefunkcja: TButton
    Left = 275
    Top = 70
    Width = 148
    Height = 25
    Caption = 'formatowaniefunkcja'
    TabOrder = 11
    OnClick = formatowaniefunkcjaClick
  end
  object formatfunparam: TButton
    Left = 429
    Top = 70
    Width = 75
    Height = 25
    Caption = 'formatfunparam'
    TabOrder = 12
    OnClick = formatfunparamClick
  end
  object btn1lowihightablica: TButton
    Left = 8
    Top = 101
    Width = 117
    Height = 25
    Caption = 'btn1lowihightablica'
    TabOrder = 13
    OnClick = btn1lowihightablicaClick
  end
  object btn1INC: TButton
    Left = 131
    Top = 101
    Width = 75
    Height = 25
    Caption = 'btn1INC'
    TabOrder = 14
    OnClick = btn1INCClick
  end
  object btn1case_of: TButton
    Left = 223
    Top = 101
    Width = 75
    Height = 25
    Caption = 'btn1case_of'
    TabOrder = 15
    OnClick = btn1case_ofClick
  end
  object btn1else_if: TButton
    Left = 319
    Top = 101
    Width = 75
    Height = 25
    Caption = 'btn1else_if'
    TabOrder = 16
    OnClick = btn1else_ifClick
  end
  object btn1funkcjaprzeciazona: TButton
    Left = 400
    Top = 101
    Width = 119
    Height = 25
    Caption = 'btn1funkcjaprzeciazona'
    TabOrder = 17
    OnClick = btn1funkcjaprzeciazonaClick
  end
  object btn1REFERENCJA: TButton
    Left = 8
    Top = 132
    Width = 117
    Height = 25
    Caption = 'btn1REFERENCJA'
    TabOrder = 18
  end
  object btn1DYREKTYWA_OUT: TButton
    Left = 142
    Top = 132
    Width = 127
    Height = 25
    Caption = 'btn1DYREKTYWA_OUT'
    TabOrder = 19
    OnClick = btn1DYREKTYWA_OUTClick
  end
  object btn1PROOBAVAR: TButton
    Left = 279
    Top = 132
    Width = 115
    Height = 25
    Caption = 'btn1PROOBAVAR'
    TabOrder = 20
  end
  object btn1swojtypdanych: TButton
    Left = 415
    Top = 132
    Width = 118
    Height = 25
    Caption = 'btn1swojtypdanych'
    TabOrder = 21
    OnClick = btn1swojtypdanychClick
  end
  object btn1tablicajakoargument: TButton
    Left = 8
    Top = 163
    Width = 167
    Height = 25
    Caption = 'btn1tablicajakoargument'
    TabOrder = 22
    OnClick = btn1tablicajakoargumentClick
  end
end

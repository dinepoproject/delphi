unit powrot;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,System.DateUtils;
 type
Targu = array of string;
type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    btnFormatowanie: TButton;
    formatowaniefunkcja: TButton;
    formatfunparam: TButton;
    btn1lowihightablica: TButton;
    btn1INC: TButton;
    btn1case_of: TButton;
    btn1else_if: TButton;
    btn1funkcjaprzeciazona: TButton;
    btn1REFERENCJA: TButton;
    btn1DYREKTYWA_OUT: TButton;
    btn1PROOBAVAR: TButton;
    btn1swojtypdanych: TButton;
    btn1tablicajakoargument: TButton;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure btnFormatowanieClick(Sender: TObject);
    procedure formatowaniefunkcjaClick(Sender: TObject);
    procedure formatfunparamClick(Sender: TObject);
    procedure btn1lowihightablicaClick(Sender: TObject);
    procedure btn1INCClick(Sender: TObject);
    procedure btn1case_ofClick(Sender: TObject);
    procedure btn1else_ifClick(Sender: TObject);
    procedure btn1funkcjaprzeciazonaClick(Sender: TObject);
    procedure btn1DYREKTYWA_OUTClick(Sender: TObject);
    procedure btn1swojtypdanychClick(Sender: TObject);
    procedure btn1tablicajakoargumentClick(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
    function undo ( a:integer;b:integer):Integer;
    procedure stala(const s:string);
    procedure referencja(  var liczna:Integer);
    procedure niereferencja(liczna:Integer);
    function Pi (licznikpi: extended):Extended;
    procedure tablicazwykla;
    procedure tablicazwyklazpetlo;
    procedure tablicajakostale;
     procedure tablicawielowymiarowa;
    procedure tablicadynamiczna;
    function formatx:string;
    procedure formatprobny;
    function formfuncparam(a1,a2:string):string;
    function lowhightablica:string;
    function increm:Integer;
    procedure cafe_of(typ:integer);
    procedure if_else(typ:integer);
    function przeciaz(x,y:Integer):integer; overload;
    function przeciaz(x,y:Currency):Currency;overload;
    procedure PROCERFERENCJA ( VAR LICZBA:Integer);
    //procedure SetValue(Mess : String);
    //procedure btn1REFERENCJAClick(Sender: TObject);
    //procedure btn1DYREKTYWA_OUTClick(Sender: TObject);
    //procedure btn1PROOBAVARClick(Sender: TObject);
    procedure out_zamiast_var( out liczna: Integer);
    procedure argument_tablica(argu:integer);
  end;
 // Istnieje pewna zasada, kt�r� nale�y zapami�ta� i kt�ra b�dzie wykorzystywana w dalszej cz�ci tego
//rozdzia�u. Ot� wiele instrukcji po s�owie then musi by� umieszczone dodatkowo w bloku tekstu
//mi�dzy begin a end

//konieczne jest umieszczenie wszystkich
// warunk�w w nawiasie: if (Imie = 'Adam') and (Random(10) = 5) then

//Random(100) � spowoduje wylosowanie liczby z zakresu od 0 do 99. Tak!
//Nie pomyli�em si�! Mo�liwe jest, �e wyniku losowania zwr�cona zostanie warto�� 0.

var
  Form1: TForm1;

implementation

{$R *.dfm}

{ TForm1 }
 {procedure SetValue(Mess : String);
begin
{ pr�ba nadania nowej warto�ci dla parametru
Mess := 'Hello there!';
end; }
uses
od_klas;


procedure TForm1.Button10Click(Sender: TObject);
begin
 tablicadynamiczna;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
c:Integer;
d:Integer;

begin
c:=8;
d:=9;
;
ShowMessage(IntToStr(undo(c,d)));

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
stala('zmienna 2');
end;

procedure TForm1.Button3Click(Sender: TObject);
var
x:Integer;
begin
 x:= 4;
 referencja(x);
 //ShowMessage(IntToStr((x))); //kiedy nie ma var, pokazuje 4
 //zamianats 16
 ShowMessage(IntToStr(x));
 end;

procedure TForm1.Button4Click(Sender: TObject);
var
x:Integer;
begin
niereferencja(9);

end;

procedure TForm1.Button5Click(Sender: TObject);
begin
ShowMessage (FloatToStr(pi(50.43)));

end;

procedure TForm1.Button6Click(Sender: TObject);
begin
 tablicazwykla;
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
tablicazwyklazpetlo;
end;

procedure TForm1.Button8Click(Sender: TObject);
begin
 tablicajakostale;
end;

procedure TForm1.Button9Click(Sender: TObject);
begin
 tablicawielowymiarowa;

end;

procedure TForm1.cafe_of(typ:integer);


begin


 case typ of
 0..10:  ShowMessage('zero');
 20..40: ShowMessage('dwa');
 else
 begin
 ShowMessage('pamietaj ze jak jest wiecej lini to w begin..end');
 ShowMessage('pudlo');
  end;
 end;


end;

procedure TForm1.argument_tablica( argu: integer);
 var
 argum:Targu;
 i: integer;
begin
SetLength(argum, 2);
argum[0]:= 'test1';
argum[1]:= 'test2';
argum[2]:= 'test3';
 //for i := 0 to 2 do


 ShowMessage(argum [argu]);  // w tym przypadku argu wskazuje na numer tablicy
end;

procedure TForm1.btn1case_ofClick(Sender: TObject);
begin
cafe_of(8);
end;

{procedure TForm1.btn1DYREKTYWA_OUTClick(Sender: TObject);
begin
//STRONA 57 Z ZIELONEJ KSIAZKI
end;}

procedure TForm1.btn1DYREKTYWA_OUTClick( Sender: TObject);
var
Y:Integer;
begin
Y:= 4;
out_zamiast_var(y);
 ShowMessage(IntToStr(y));

 // Jak jest var lub out przy  , to 4-ka jest brana do procedury
 // out_zamiast_var, jak nie to showmessage bierze zmienn� 4-ke i nie liczy procedury
end;

procedure TForm1.btn1else_ifClick(Sender: TObject);

 begin
  if_else(2);


end;


procedure TForm1.btn1funkcjaprzeciazonaClick(Sender: TObject);
begin
ShowMessage(IntToStr(przeciaz(30, 25)));
ShowMessage(FloatToStr(przeciaz(30.25, 22.48)));
end;

procedure TForm1.btn1INCClick(Sender: TObject);
begin
ShowMessage (inttostr(increm));
end;

procedure TForm1.btn1lowihightablicaClick(Sender: TObject);
begin
 ShowMessage(lowhightablica);
end;

procedure TForm1.btn1swojtypdanychClick(Sender: TObject);
begin
ShowMessage (IntToStr( form2.wlasny_typ));
end;

procedure TForm1.btn1tablicajakoargumentClick(Sender: TObject);

begin
 argument_tablica(2);
end;     //Projekt orginalny tablica jako argument nieskonczony, brak opisu pdf delphi 7 strona 59

{procedure TForm1.btn1PROOBAVARClick(Sender: TObject);
VAR
A:string;
begin
A:= SetValue('GGGG');
ShowMessage (A);
end; }

{procedure TForm1.btn1REFERENCJAClick(Sender: TObject);
var
  X : Integer;
begin
  X := 20;
  PROCERFERENCJA(X);
  //KIEDY NIE MA VARA SHOMESSAGE WYSWIETLA 20
  // KIEDY JEST SHOWMMEGGASE ZACIAGA ZMIENNA Z PROCEDURYBUTTON CLICK.
  // CHODZI O TO �E PROCEDURA WYWOLUJACA MOZE WPLYWAC NA WYWOLYWANA.
  ShowMessage ( IntToStr(X));	{X r�wne jest teraz 400
end; }




procedure TForm1.btnFormatowanieClick(Sender: TObject);
begin
 formatprobny;
end;

procedure TForm1.formatfunparamClick(Sender: TObject);
begin

 ShowMessage(formfuncparam('dzieci','bajce'));
end;

procedure TForm1.formatowaniefunkcjaClick(Sender: TObject);
begin
  ShowMessage(formatx);
end;

procedure TForm1.formatprobny;

 var
  S : string;
  X : Integer;
begin
  X := 10 * 20;
  S:= Format('Wynik: %d', [X]);

  ShowMessage(s) ;
end;


function TForm1.formatx: string;
var
x:Integer;
y:string;
begin
x:= 5;
y:= Format('Wynik: %d', [X]);
result:=y;
end;

function TForm1.formfuncparam(a1, a2: string): string;
begin
result:= Format('Witajcie %s w naszej %s', [a1,a2] )
end;

procedure TForm1.if_else(typ: integer);
begin
  if typ =0 then ShowMessage('zero')
 else if Typ=2 then ShowMessage('dwa')
  else if Typ=3 then ShowMessage('trzy')

 else
 begin
 ShowMessage('pamietaj ze jak jest wiecej lini to w begin..end');
 ShowMessage('pudlo');
  end;
 end;


function TForm1.increm: Integer;
var
Y:Integer;
begin
Y:=5;
inc (Y,6);  // tak dzia�a, ale INC nie moze byc wyliczany w resulcie

Result := Y;
end;

function TForm1.lowhightablica: string;
var
tablica:array[0..3] of string;
begin
tablica [0] := 'jeden';
tablica [1] := 'dwa';
tablica [2] := 'trzy';
tablica [3] := 'cztery';

Result:=IntToStr(low(tablica));//zwraca nie wartosc np "jeden" tylko 0
// Wywo�anie polecenia Low(Tablica) spowoduje, �e funkcja zwr�ci warto�� 10. Natomiast funkcja
//High zwr�ci warto�� 100.
end;

procedure TForm1.niereferencja(liczna: Integer);
begin
liczna:= liczna * liczna;
ShowMessage(IntToStr(liczna));
end;

procedure TForm1.out_zamiast_var( out liczna: Integer);
begin
  liczna:= liczna * liczna;
  // Jak jest var lub out, to liczna jest brana z
end;

function TForm1.Pi(licznikpi: extended): Extended;
const
Pi= 3.14;
begin
 result:= Pi * licznikpi;
end;

procedure TForm1.PROCERFERENCJA( VAR LICZBA: Integer);


begin

begin
  Liczba := Liczba * Liczba;
end;



end;

function TForm1.przeciaz(x, y: Integer): integer;
begin
  Result:= (x * y);
end;

function TForm1.przeciaz(x, y: Currency): Currency;
begin
Result:= (x * y);
end;

procedure TForm1.referencja( var liczna: Integer);
begin
liczna:= liczna * liczna;
end;



procedure TForm1. stala(const s: string);
begin

//delphi 4 dla kazdego strona: strona 12/  Parametry przekazywane przez sta��
ShowMessage(s);
end;

procedure TForm1.tablicadynamiczna;
var
tablica: array of Integer;
i:Integer;
begin
  SetLength(tablica,3);
  tablica [0]:= 1;
  tablica [1] := 2;
  tablica [2] := 3;
  for i := 0 to 2 do
    ShowMessage (inttostr(tablica [i]));

end;

procedure TForm1.tablicajakostale;
const
tablica:array [0..1] of string=(('pierwszyelement'),('drugielement'));
begin
 ShowMessage(tablica[1]);
end;

procedure TForm1.tablicawielowymiarowa;
var

tablica:array[0..1,0..2] of string;
begin
tablica[0][0] := 'tata';
tablica [0][1] := 'ool';
tablica [0][2]:= 'kom';
tablica [1][0] := 'mama';
tablica [1][1]:= 'oll';
tablica [1][2] := 'bluz';
ShowMessage(tablica [1][1]);
end;

procedure TForm1.tablicazwykla;
var
tablica:array [0..1] of string;
begin
tablica[0] := 'zero';
tablica[1] := 'jeden';
ShowMessage(tablica[1]);
end;

procedure TForm1.tablicazwyklazpetlo;
var
tablica:array [0..1] of string;
i:integer;
begin
 tablica[0] := 'zero';
tablica[1] := 'jeden';
for I := 0 to 1 do
 ShowMessage(tablica [i]);
end;

function TForm1.undo( a:integer;b:integer):Integer;


begin
 a:= a*a;
 b:=b*b;
 result:=a*b;

end;

end.

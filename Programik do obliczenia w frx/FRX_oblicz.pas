unit FRX_oblicz;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    btn1Policz: TButton;
    edt1Wprowadz_liczbe: TEdit;
    lbl1wynik: TLabel;
    procedure btn1PoliczClick(Sender: TObject);
    procedure edt1Wprowadz_liczbeKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    function licznik (wprowadz:Currency):Currency;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1PoliczClick(Sender: TObject);
var
i: Currency;
begin
i:=  (licznik (StrToCurr(edt1Wprowadz_liczbe.Text)));
lbl1wynik.Caption:= (FormatCurr('0.00',i));

end;

procedure TForm1.edt1Wprowadz_liczbeKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    btn1Policz.Click;
end;

function TForm1.licznik(wprowadz: Currency): Currency;
begin
result:= wprowadz * 0.8118;
end;

end.


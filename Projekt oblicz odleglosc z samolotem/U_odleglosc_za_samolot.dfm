object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 460
  ClientWidth = 719
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 23
  object Label1: TLabel
    Left = 8
    Top = 312
    Width = 269
    Height = 23
    Alignment = taCenter
    Caption = 'KLIKNIJ MNIE TO OBLICZ'#280' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsItalic, fsUnderline]
    ParentFont = False
    OnClick = Label1Click
  end
  object LabeledEdit1: TLabeledEdit
    Left = 8
    Top = 40
    Width = 689
    Height = 31
    EditLabel.Width = 216
    EditLabel.Height = 23
    EditLabel.Caption = 'PODAJ WARTO'#346#262' CZASU:'
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 77
    Width = 689
    Height = 65
    TabOrder = 1
    object RadioButton1: TRadioButton
      Left = 16
      Top = 24
      Width = 113
      Height = 17
      Caption = 'SEKUND'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object RadioButton2: TRadioButton
      Left = 272
      Top = 24
      Width = 113
      Height = 17
      Caption = 'MINUT'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object RadioButton3: TRadioButton
      Left = 536
      Top = 24
      Width = 113
      Height = 17
      Caption = 'GODZIN'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object LabeledEdit2: TLabeledEdit
    Left = 8
    Top = 168
    Width = 689
    Height = 31
    EditLabel.Width = 235
    EditLabel.Height = 23
    EditLabel.Caption = 'PODAJ PR'#280'DKO'#346#262' W KM/H:'
    TabOrder = 2
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 216
    Width = 689
    Height = 65
    Caption = 'WYNIK W:'
    TabOrder = 3
    object RadioButton4: TRadioButton
      Left = 152
      Top = 32
      Width = 113
      Height = 17
      Caption = 'METRY'
      TabOrder = 0
      OnClick = RadioButton4Click
    end
    object RadioButton5: TRadioButton
      Left = 424
      Top = 32
      Width = 121
      Height = 17
      Caption = 'KILOMETRY'
      TabOrder = 1
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 1
    OnTimer = Timer1Timer
    Left = 528
    Top = 320
  end
end

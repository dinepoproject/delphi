unit U_Przyciski_wysokosc_szerokosc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    btn1_szerokosc_ekranu: TButton;
    btn1_wysokosc_ekranu: TButton;
    procedure btn1_szerokosc_ekranuClick(Sender: TObject);
    procedure btn1_wysokosc_ekranuClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1_szerokosc_ekranuClick(Sender: TObject);
begin
btn1_szerokosc_ekranu.Caption:= inttostr (Screen.Width);
end;

procedure TForm1.btn1_wysokosc_ekranuClick(Sender: TObject);
begin
btn1_wysokosc_ekranu.Caption:= inttostr (Screen.Height);
end;

end.

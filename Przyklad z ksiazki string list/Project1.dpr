program Foo;

{$APPTYPE CONSOLE}
uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls;

var
  Spis : TStringList;               // Deklarujemy zmienn� typu TStringList.
  I     : Integer;

begin
  Spis := TStringList.Create;   // Przed u�yciem tworzymy TStringList kt�ry zadeklarowali�my.

  Spis.Add('Tu jest tekst.');
  Spis.Add('A tu kolejny.');      // Dodanie linijek o podanej zawarto�ci.
  Spis[1] := 'Zmiana tekstu';   // Zmiana zawarto�ci linijki o podanym indeksie. Pami�tamy, by liczy� od zera.

  For I := 0 to Spis.Count-1 do
  begin
    ShowMessage(Spis[i]);      // Wy�wietlanie komunikat�w z zawarto�ci� poszczeg�lnych linii po kolei.
  end;

  Spis.Free;                         // Gdy sko�czymy korzysta� z klasy, czy�cimy zarezerwowan� pami��.
end.

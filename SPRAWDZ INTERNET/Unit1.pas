unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,WinInet,WinSock;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);

  private
    { Private declarations }
  IP : PChar; // adres IP komputera
  public
  procedure GetIPAndName(var IPAddress, ACompName: PCHar);

    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);

VAR

dwConnection : DWORD;
begin
{ flagi }
  dwConnection := INTERNET_CONNECTION_MODEM + INTERNET_CONNECTION_LAN +
    INTERNET_CONNECTION_PROXY;

  { sprawd�, czy jest po��czenie }
  if not InternetGetConnectedState(@dwConnection, 0) then
    ShowMessage('Brak po��czenia')
  else ShowMessage('Jest po��czenie ;-)')
end;
procedure TForm1.Button2Click(Sender: TObject);
{var
ipp:PChar;
ACP:PChar; }
begin
 {TForm1.GetIPAndName(ipp,ACP);
 Edit1.Text:= IntToStr(ipp); }
end;

procedure TForm1.GetIPAndName(var IPAddress, ACompName: PCHar);
var
  Host : PHostEnt;
  CompName : array[0..MAX_PATH] of char;

  VER : WORD;
  Data : TWSAData;
begin
  // �adujemy bibliotek� Winsock
  VER := MAKEWORD(1, 0);
  WSAStartup(VER, Data);
  try
    // Pobieramy nazw� komputera i przypisujemy j� zmiennej "CompName"
    GetHostName(@CompName, MAX_PATH);
    Host := GetHostByName(@CompName); // uzyskanie nazwy u�ytkownika

    ACompName := Host^.h_name;// przypisanie zmiennej "ACompName" nazwy u�ytkownika
    // Pobieramy jego adres IP ( u�yte tu zosta�o rzutowanie )
    IP := iNet_ntoa(PInAddr(Host^.h_addr_list^)^);

    IPAddress := IP; // przypisanie zmiennej "IPAddress" nazwy IP
  finally
    WSACleanup; // zwolnij bibliotek� Winsock
  end;
end;
end.
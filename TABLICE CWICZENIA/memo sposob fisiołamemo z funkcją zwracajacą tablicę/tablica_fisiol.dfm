object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 201
  ClientWidth = 821
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btn1: TButton
    Left = 688
    Top = 159
    Width = 75
    Height = 25
    Caption = 'btn1'
    TabOrder = 0
    OnClick = btn1Click
  end
  object Button1: TButton
    Left = 453
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 104
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object Button2: TButton
    Left = 566
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 3
    OnClick = Button2Click
  end
  object btn2_prawdz_tabl_funkcja: TButton
    Left = 264
    Top = 8
    Width = 167
    Height = 25
    Caption = 'btn2_prawdz_tabl_funkcja'
    TabOrder = 4
    OnClick = btn2_prawdz_tabl_funkcjaClick
  end
  object btn2_uruchomienie_funkcji_tablicy: TButton
    Left = 8
    Top = 8
    Width = 250
    Height = 25
    Caption = 'btn2_uruchomienie_funkcji_tablicy'
    TabOrder = 5
    OnClick = btn2_uruchomienie_funkcji_tablicyClick
  end
  object Memo2: TMemo
    Left = 294
    Top = 104
    Width = 185
    Height = 89
    Lines.Strings = (
      'Memo2')
    TabOrder = 6
  end
  object btn5_MEMO_ASSIGN: TButton
    Left = 632
    Top = 64
    Width = 75
    Height = 25
    Caption = 'btn5_MEMO_ASSIGN'
    TabOrder = 7
    OnClick = btn5_MEMO_ASSIGNClick
  end
end

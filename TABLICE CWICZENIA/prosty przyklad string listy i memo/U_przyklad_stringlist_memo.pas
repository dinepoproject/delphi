unit U_przyklad_stringlist_memo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    list: TStringList;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
list.Add('a');
list.Add('b');
list.Add('c');
list.SaveToFile('abc');
end;

procedure TForm1.Button2Click(Sender: TObject);
var
 i : Integer;
begin
list.LoadFromFile('abc');
for I := 0 to list.Count -1 do
 Memo1.Lines.Add (list[i]);

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  list:= TStringList.Create;
end;

end.

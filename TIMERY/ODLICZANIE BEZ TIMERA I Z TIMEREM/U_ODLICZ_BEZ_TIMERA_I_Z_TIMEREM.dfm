object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 201
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btn_PIERWSZY_SPOSOB: TButton
    Left = 29
    Top = 25
    Width = 200
    Height = 25
    Caption = 'btn_PIERWSZY_SPOSOB'
    TabOrder = 0
    OnClick = btn_PIERWSZY_SPOSOBClick
  end
  object btn1_DRUGI_SPOSOB: TButton
    Left = 281
    Top = 25
    Width = 158
    Height = 25
    Caption = 'btn1_DRUGI_SPOSOB'
    TabOrder = 1
    OnClick = btn1_DRUGI_SPOSOBClick
  end
  object btn_ODLICZANIE_TIMEREM: TButton
    Left = 29
    Top = 102
    Width = 177
    Height = 25
    Caption = 'btn_ODLICZANIE_TIMEREM'
    TabOrder = 2
    OnClick = btn_ODLICZANIE_TIMEREMClick
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 334
    Top = 109
  end
end

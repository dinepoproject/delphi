unit U_ODLICZ_BEZ_TIMERA_I_Z_TIMEREM;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    btn_PIERWSZY_SPOSOB: TButton;
    btn1_DRUGI_SPOSOB: TButton;
    btn_ODLICZANIE_TIMEREM: TButton;
    Timer1: TTimer;
    procedure btn1_DRUGI_SPOSOBClick(Sender: TObject);
    procedure btn_ODLICZANIE_TIMEREMClick(Sender: TObject);
    procedure btn_PIERWSZY_SPOSOBClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
   var SecondsLeft: Integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1_DRUGI_SPOSOBClick(Sender: TObject);
var
i:integer;
begin
i := 2;
repeat
btn1_DRUGI_SPOSOB.caption := inttostr(i);
application.processmessages;
dec(i,1);
sleep(1000);
until i = 0;
//now you can do whatever you want ... like
showmessage('countdown done!');
end;

procedure TForm1.btn_ODLICZANIE_TIMEREMClick(Sender: TObject);
begin
// Seconds is the number of seconds the count down should last
SecondsLeft := 20;
Timer1.Enabled := True;
end;

procedure TForm1.btn_PIERWSZY_SPOSOBClick(Sender: TObject);

 var
i:integer;
begin
i := 3;
repeat
dec(i,1);
sleep(1000);
until i = 0;
//now you can do whatever you want ... like
showmessage('countdown done!');
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var Hours, Minutes, Seconds: Word;
begin
Dec(SecondsLeft);
Seconds := SecondsLeft;
// calculate the hours, minutes and seconds left
Hours := SecondsLeft div 3600;
Seconds := Seconds - (Hours * 3600);
Minutes := SecondsLeft div 60;
Seconds := Seconds - (Hours * 60);
// and display it in the caption of a panel
btn_ODLICZANIE_TIMEREM.Caption := FormatDateTime('hh:nn:ss', EncodeTime(Hours, Minutes, Seconds,0));
if (SecondsLeft = 0) then
begin
// call the function you want to execute at 0 here
Timer1.Enabled := False;
ShowMessage('Time is up');
end;
 end;

end.

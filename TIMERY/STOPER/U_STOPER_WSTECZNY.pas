unit U_STOPER_WSTECZNY;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    MINUTY: TLabel;
    SEKUNDY: TLabel;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    var
      X, Y, Z: INTEGER;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  X := 5;

  Timer1.Enabled := False;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin

  Button1.Caption := 'START';

  Timer1.Enabled := True;

end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin

  SEKUNDY.Caption := IntToStr(X);
  X := STRTOINT(SEKUNDY.Caption) - 1;
  if X = 0 then
  begin

    Close;
  end;

end;

end.


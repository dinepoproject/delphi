object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 351
  ClientWidth = 620
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Shape4: TShape
    Left = 90
    Top = 38
    Width = 139
    Height = 253
    Brush.Color = clMedGray
  end
  object Shape3: TShape
    Left = 126
    Top = 59
    Width = 65
    Height = 65
    Brush.Color = clMaroon
    Shape = stEllipse
  end
  object Shape2: TShape
    Left = 126
    Top = 130
    Width = 65
    Height = 65
    Brush.Color = clOlive
    Shape = stEllipse
  end
  object Shape1: TShape
    Left = 126
    Top = 201
    Width = 65
    Height = 65
    Brush.Color = clLime
    Shape = stEllipse
  end
  object Timer1: TTimer
    Interval = 5000
    OnTimer = Timer1Timer
    Left = 283
    Top = 139
  end
  object Timer2: TTimer
    Enabled = False
    OnTimer = Timer2Timer
    Left = 334
    Top = 141
  end
  object Timer3: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = Timer3Timer
    Left = 398
    Top = 145
  end
  object Timer4: TTimer
    Enabled = False
    OnTimer = Timer4Timer
    Left = 446
    Top = 147
  end
end

unit U_TRAFIC_LIGHTS_REAL;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Shape3: TShape;
    Timer1: TTimer;
    Shape2: TShape;
    Shape1: TShape;
    Shape4: TShape;
    Timer2: TTimer;
    Timer3: TTimer;
    Timer4: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure Timer4Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Timer1Timer(Sender: TObject);
begin
Shape1.Brush.Color:=clGreen;
 Shape2.Brush.Color:=clYellow;
 Timer1.Enabled:= False;
 Timer2.Enabled:= True;

end;

procedure TForm1.Timer2Timer(Sender: TObject);
BEGIN
Shape2.Brush.Color:=clOlive;
 Shape3.Brush.Color:=clRED;
 Timer2.Enabled:= False;
 Timer3.Enabled:= True;
 end;

procedure TForm1.Timer3Timer(Sender: TObject);
begin

 Shape2.Brush.Color:=clYellow;
 Timer3.Enabled:= False;
 Timer4.Enabled:= True;
end;

procedure TForm1.Timer4Timer(Sender: TObject);
begin
Shape2.Brush.Color:=clOLIVE;
 Shape1.Brush.Color:= clLime;
 Shape3.Brush.Color:=clMaroon;
 Timer4.Enabled:= False;
 Timer1.Enabled:= True;
end;

end.

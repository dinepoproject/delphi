unit GxTrainUtils;

interface

uses
  System.SysUtils;

    /// <summary>
    /// Funkcja zwraca ilo�� osi wagonu okre�lon� na podstawie jego numeru.
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test dla pustego �a�cucha: czy zostanie zwr�cony wynik 0
    ///  2. Test dla �a�cucha zaczynaj�cego si� od litery: czy zostanie zwr�cony wynik 0
    ///  3. Testy dla �a�cucha zaczynaj�cego si� od 0, 2, 4: czy zostanie zwr�cony wynik 2
    ///  4. Testy dla �a�cucha zaczynaj�cego si� od 1, 3, 8: czy zostanie zwr�cony wynik 4
    ///  5. Testy dla �a�cucha zaczynaj�cego si� od 5,6,7,9: czy zostanie zwr�cony wynik 0
    ///  Razem test�w: 12
    ///  SPRAWDZONA
  function  AxlesCount(const ATruckNum: String): Integer;

    /// <summary>
    /// Funkcja zwracaj�ca warto�� cyfry samokontroli dla numeru wagonu. Funkcja dzia�a dla ci�g�w sk�adaj�cych si�
    /// z co najmniej 11 cyfr. Numer wagonu mo�e zawiera� znaki formatowania.
    /// </summary>
    /// Testy do wykonania:
    /// 1. Test dla pustego �a�cucha: czy zostanie zwr�cony wynik: -1
    /// 2. Test dla �a�cucha zawieraj�cego od 1 do 10 cyfr. Test z u�yciem p�tli automatycznej. czy zostanie zwr�cony wynik: -1
    /// 3. Test dla �a�cucha zawieraj�cego 11 cyfr nr wagonu: czy zostanie zwr�cona prawid�owa cyfra samokontroli
    /// 4. Test dla �a�cucha zawieraj�cego 12 cyfr nr wagonu z nieprawid�ow� cyfr� samokontroli: czy zostanie zwr�cona
    ///    prawid�owa cyfra samokontroli
    /// 5. Test dla �a�cucha zawieraj�cego sformatowany nr wagonu z nieprawid�ow� cyfr� samokontroli: czy zostanie zwr�cona
    ///    prawid�owa cyfra samokontroli
    /// Razem test�w: 5
    ///  SPRAWDZONA
  function  ControlCipher(ATruckNum: String): SmallInt;

    /// <summary>
    /// Funkcja zwraca warto�� cyfry samokontroli dla ci�gu cyfr w �a�cuchu
    /// </summary>
    /// Testy do wykonania:
    /// 1. Test dla pustego �a�cucha: czy zostanie zwr�cony wynik 0
    /// 2. Test dla �a�cucha 00010: czy zostanie zwr�cona warto��: 9
    /// 3. Trzy testy dla �a�cucha z 11-toma cyframi nr wagonu - czy zostanie zwr�cona poprawna cyfra.
    /// 4. Test dla �a�cucha 55: czy zostanie zwr�cona warto��: 0
    /// 5. Testy dla �a�cuch�w 055, 0055, 00055: czy zostanie zwr�cona warto��: 0. Chodzi o wykazanie, �e
    ///    niezale�nie od ilo�ci zer poprzedzaj�cych liczb� uzyskujemy t� sam� liczb� samokontroli
    /// Razem test�w: 9
    ///  SPRAWDZONA


  function  CyfraOblicz(const ALancuch: String): Integer;


    /// <summary>
    /// Funkcja zwraca warto�� cyfry samokontroli - do oblicze� s� przekazywane wy��cznie cyfry z �a�cucha.
    /// </summary>
    /// Testy do wykonania:
    /// 1. Test dla pustego �a�cucha: czy zostanie zwr�cony wynik 0;
    /// 2. Test dla �a�cucha 00010: czy zostanie zwr�cona warto��: 9
    /// 3. Test dla �a�cucha 00-01-0: czy zostanie zwr�cona warto��: 9
    /// Razem test�w: 3
    ///  SPRAWDZONA
  function  CyfraObliczZCyfr(const ALancuch: String): Integer;

    /// <summary>
    /// Funkcja zwraca warto�� cyfry samokontroli - do oblicze� nie jest przekazywana ostatnia cyfra z �a�cucha
    /// </summary>
    ///  Testy do wykonania:
    ///  1. Test dla pustego �a�cucha: czy zostanie zwr�cony wynik 0;
    ///  2. Test dla �a�cucha, kt�ry nie zawiera cyfr: czy zostanie zwr�cony wynik 0;
    ///  3. Test dla �a�cucha: 000109: czy zostanie zwr�cony wynik 9;
    ///  4. Test dla �a�cucha: 00010-9: czy zostanie zwr�coy wynik 9;
    ///  Razem test�w: 4
    ///  SPRAWDZONA
  function  CyfraObliczBezOstatniej(const ALancuch: String): Integer;

    /// <summary>
    /// Funkcja zwraca True, gdy ostatnia cyfra podana w �a�cuchu jest poprawna.
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test dla pustego �a�cucha: czy zostanie zwr�cony wynik False
    ///  2. Test dla �a�cucha z jedn� cyfr� - w szczeg�lno�ci 5: czy zostanie zwr�cony wynik False
    ///  3. Test dla �a�cucha z cyframi poprawnego numeru wagonu bez formatowania: czy zostanie zwr�cony wynik True
    ///  4. Test dla �a�cucha z cyframi niepoprawnego numeru wagonu bez formatowania: czy zostanie zwr�cony wynik False
    ///  5. Test dla �a�cucha z cyframi poprawnego numeru wagonu z formatowaniem: czy zostanie zwr�cony wynik True
    ///  6. Test dla �a�cucha z cyframi niepoprawnego numeru wagonu z formatowaniem: czy zostanie zwr�cony wynik False
    ///  Razem test�w: 6
  function  CyfraPoprawna(const ALancuch: String): Boolean;

implementation

uses
  JclStrings;

function AxlesCount(const ATruckNum: String): Integer;
begin
  Result := 0;
  if Length(ATruckNum) > 0 then
    case ATruckNum[1] of
      '0', '2', '4':
        Result := 2;
      '1', '3', '8':
        Result := 4;
    end
end;

function ControlCipher(ATruckNum: String): SmallInt;
begin
  ATruckNum := StrKeepChars(ATruckNum, CharIsDigit);
    // Do obliczenia cyfry samokontroli potrzebne jest 11 cyfr
  case Length(ATruckNum) of
    11:
      Result := CyfraOblicz(ATruckNum);
    12:
      Result := CyfraOblicz(StrLeft(ATruckNum, 11));
    else
      Result := -1;
  end;
end;

function CyfraOblicz(const ALancuch: String): Integer;
var
  i, j: Integer;
begin
  Result := 0;
  for i := 1 to Length(ALancuch) do
  begin
    j := (Byte(ALancuch[i]) - 48) * (2 - (i + 1) mod 2);
    Inc(Result, (j div 10) + (j mod 10));
  end;
  Result := 10 - Result mod 10;
  if Result = 10 then Result := 0;
end;

function  CyfraObliczZCyfr(const ALancuch: String): Integer;
begin
  Result := CyfraOblicz(StrKeepChars(ALancuch, CharIsDigit));
end;

function CyfraObliczBezOstatniej(const ALancuch: String): Integer;
var
  lancuch: String;
begin
  lancuch := StrKeepChars(ALancuch, CharIsDigit);
  Result := CyfraOblicz(StrLeft(lancuch, Length(lancuch) - 1));
end;

function  CyfraPoprawna(const ALancuch: String): Boolean;
var
  lancuch: String;
begin
  lancuch := StrKeepChars(ALancuch, CharIsDigit);
  Result := IntToStr(CyfraOblicz(StrLeft(lancuch, Length(lancuch) - 1))) = StrRight(lancuch, 1);
end;

end.

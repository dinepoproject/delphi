unit uAxlesCount;

interface

uses
  System.SysUtils, TestFramework, Vcl.Dialogs;

type
  TtestAxlesCount = class(TTestCase)
  published
    procedure PUSTY_LANCUCH_CZY_ZWROCI_0;
    procedure LANCUCH_OD_LITERY_CZY_ZWROCI_0;
    procedure LANCUCH_OD_0_CZY_ZWROCI_2;
    procedure LANCUCH_OD_2_CZY_ZWROCI_2;
    procedure LANCUCH_OD_4_CZY_ZWROCI_2;
    procedure LANCUCH_OD_1_CZY_ZWROCI_4;
    procedure LANCUCH_OD_3_CZY_ZWROCI_4;
    procedure LANCUCH_OD_8_CZY_ZWROCI_4;
    procedure LANCUCH_OD_5_CZY_ZWROCI_0;
    procedure LANCUCH_OD_6_CZY_ZWROCI_0;
    procedure LANCUCH_OD_7_CZY_ZWROCI_0;
    procedure LANCUCH_OD_9_CZY_ZWROCI_0;
  end;

implementation

uses
  GxTrainUtils;

{ TtestAxlesCount }

procedure TtestAxlesCount.LANCUCH_OD_0_CZY_ZWROCI_2;
begin
  CheckEquals(2, AxlesCount('0234324'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_0_CZY_ZWROCI_2', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_1_CZY_ZWROCI_4;
begin
  CheckEquals(4, AxlesCount('1113'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_1_CZY_ZWROCI_4', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_2_CZY_ZWROCI_2;
begin
  CheckEquals(2, AxlesCount('24543'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_2_CZY_ZWROCI_2', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_3_CZY_ZWROCI_4;
begin
  CheckEquals(4, AxlesCount('3456ab5'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_3_CZY_ZWROCI_4', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_4_CZY_ZWROCI_2;
begin
  CheckEquals(2, AxlesCount('476865'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_4_CZY_ZWROCI_2', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_5_CZY_ZWROCI_0;
begin
  CheckEquals(0, AxlesCount('5675456-6'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_5CZY_ZWROCI_0', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_6_CZY_ZWROCI_0;
begin
  CheckEquals(0, AxlesCount('6-8787'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_6_CZY_ZWROCI_0', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_7_CZY_ZWROCI_0;
begin
  CheckEquals(0, AxlesCount('708768'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_7_CZY_ZWROCI_0', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_8_CZY_ZWROCI_4;
begin
  CheckEquals(4, AxlesCount('86757'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_8_CZY_ZWROCI_4', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_9_CZY_ZWROCI_0;
begin
  CheckEquals(0, AxlesCount('945676-5'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_9_CZY_ZWROCI_0', '']));
end;

procedure TtestAxlesCount.LANCUCH_OD_LITERY_CZY_ZWROCI_0;
begin
  CheckEquals(0, AxlesCount('A5467'), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_LITERY_CZY_ZWROCI_0', '']));
end;

procedure TtestAxlesCount.PUSTY_LANCUCH_CZY_ZWROCI_0;
begin
  CheckEquals(0, AxlesCount(''), Format('%s.%s: %s', [ClassName, 'PUSTY_LANCUCH_CZY_ZWROCI_0', '']));
end;

initialization
  RegisterTest(TtestAxlesCount.suite);

end.


unit uControlCipher;

interface

uses
  System.SysUtils, TestFramework, Vcl.Dialogs;

type
  TtestControlCipher = class(TTestCase)
  published
    procedure PUSTY_LANCUCH_CZY_ZWROCI_MINUS_1;
    procedure LANCUCH_OD_1_DO_10_CYFR_PETLA_AUTOMATYCZNA_CZY_ZWROCI_MINUS_1;
    procedure LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI;
    procedure LANCUCH_12_CYFR_Z_NIEPRAWIDLOWA_CYFRA_SAMOKONTROLI_CZY_ZWROCI_PRAWIDLOWA;
    procedure LANCUCH_SFORMATOWANY_NUMER_Z_NIEPRAWIDLOWA_CYFRA_SAMOKONTROLI_CZY_ZWROCI_PRAWIDLOWA;
    procedure LANCUCH_SFORMATOWANY_NUMER_Z_NIEPRAWIDLOWA_CYFRA_SAMOKONTROLI_CZY_ZWROCI_PRAWIDLOWA_WAGON_PASAZERSKI;
  end;

implementation

uses
  GxTrainUtils;

{ TtestControlCipher }

procedure TtestControlCipher.LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI;
begin
  CheckEquals(7, ControlCipher('31547986361'), Format('%s.%s: %s', [ClassName, 'LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI', '']));

end;

procedure TtestControlCipher.LANCUCH_12_CYFR_Z_NIEPRAWIDLOWA_CYFRA_SAMOKONTROLI_CZY_ZWROCI_PRAWIDLOWA;
begin
  CheckEquals(7, ControlCipher('835478546139'), Format('%s.%s: %s', [ClassName, 'LANCUCH_12_CYFR_Z_NIEPRAWIDLOWA_CYFRA_SAMOKONTROLI_CZY_ZWROCI_PRAWIDLOWA', '']));
end;

procedure TtestControlCipher.LANCUCH_OD_1_DO_10_CYFR_PETLA_AUTOMATYCZNA_CZY_ZWROCI_MINUS_1;
var
  i: Integer;
  cyfrawagon: array[1..10] of string;
begin

  cyfrawagon[1] := '3';
  cyfrawagon[2] := '3-1';
  cyfrawagon[3] := '31-5';
  cyfrawagon[4] := '3151';
  cyfrawagon[5] := '31-515';
  cyfrawagon[6] := '315153';
  cyfrawagon[7] := '3151-533';
  cyfrawagon[8] := '31515331';
  cyfrawagon[9] := '315153313';
  cyfrawagon[10] := '31515331-38';
  for I := 1 to 10 do

  begin
    CheckEquals(-1, ControlCipher(cyfrawagon[i]), Format('%s.%s: %s', [ClassName, 'LANCUCH_OD_1_DO_10_CYFR_PETLA_AUTOMATYCZNA_CZY_ZWROCI_MINUS_1', '']));
  end;
end;

procedure TtestControlCipher.LANCUCH_SFORMATOWANY_NUMER_Z_NIEPRAWIDLOWA_CYFRA_SAMOKONTROLI_CZY_ZWROCI_PRAWIDLOWA;
begin
  CheckEquals(8, ControlCipher('8451-5348679-7'), Format('%s.%s: %s', [ClassName, 'LANCUCH_SFORMATOWANY_NUMER_Z_NIEPRAWIDLOWA_CYFRA_SAMOKONTROLI_CZY_ZWROCI_PRAWIDLOWA', '']));
end;

procedure TtestControlCipher.LANCUCH_SFORMATOWANY_NUMER_Z_NIEPRAWIDLOWA_CYFRA_SAMOKONTROLI_CZY_ZWROCI_PRAWIDLOWA_WAGON_PASAZERSKI;
begin
  begin
    CheckEquals(1, ControlCipher('51 51 39-70 012-9'), Format('%s.%s: %s', [ClassName, 'LANCUCH_SFORMATOWANY_NUMER_Z_NIEPRAWIDLOWA_CYFRA_SAMOKONTROLI_CZY_ZWROCI_PRAWIDLOWA_WAGON_PASAZERSKI', '']));
  end;

end;

procedure TtestControlCipher.PUSTY_LANCUCH_CZY_ZWROCI_MINUS_1;
begin
  CheckEquals(-1, ControlCipher(''), Format('%s.%s: %s', [ClassName, 'PUSTY_LANCUCH_CZY_ZWROCI_MINUS_1', '']));
end;

initialization
  RegisterTest(TtestControlCipher.suite);

end.


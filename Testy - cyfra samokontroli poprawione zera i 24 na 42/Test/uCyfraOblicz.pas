unit uCyfraOblicz;

interface

uses
  System.SysUtils, TestFramework, Vcl.Dialogs;

type
  TtestCyfraOblicz = class(TTestCase)
  published
    procedure PUSTY_LANCUCH_CZY_ZWROCI_0;
    procedure LANCUCH_00010_CZY_ZWROCI_9;
    procedure LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI_1;
    procedure LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI_2;
    procedure LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI_3;
    procedure LANCUCH_42_CYFR_CZY_ZWROCI_0;
    procedure LANCUCH_420_CYFR_CZY_ZWROCI_0;
    procedure LANCUCH_4200_CYFR_CZY_ZWROCI_0;
    procedure LANCUCH_42000_CYFR_CZY_ZWROCI_0;
    procedure LANCUCH_420000_CYFR_CZY_ZWROCI_0;
  end;
 //spos�b obliczania cyfry kontrolnej opisany jest  na http://kolej.eu07.pl/serwis/targi/mlak/samo.html

implementation

uses
  GxTrainUtils;

  { TtestCyfraOblicz }

procedure TtestCyfraOblicz.LANCUCH_420000_CYFR_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraOblicz('420000'), Format('%s.%s: %s', [ClassName, 'LANCUCH_000024_CYFR_CZY_ZWROCI_0', '']));
end;

procedure TtestCyfraOblicz.LANCUCH_00010_CZY_ZWROCI_9;
begin
  CheckEquals(9, CyfraOblicz('00010'), Format('%s.%s: %s', [ClassName, 'LANCUCH_00010_CZY_ZWROCI_MINUS_9', '']));
end;

procedure TtestCyfraOblicz.LANCUCH_42000_CYFR_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraOblicz('42000'), Format('%s.%s: %s', [ClassName, 'LANCUCH_00055_CYFR_CZY_ZWROCI_MINUS_0', '']));
end;

procedure TtestCyfraOblicz.LANCUCH_4200_CYFR_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraOblicz('4200'), Format('%s.%s: %s', [ClassName, 'LANCUCH_0055_CYFR_CZY_ZWROCI_MINUS_0', '']));
end;

procedure TtestCyfraOblicz.LANCUCH_420_CYFR_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraOblicz('420'), Format('%s.%s: %s', [ClassName, 'LANCUCH_055_CYFR_CZY_ZWROCI_MINUS_0', '']));
end;

procedure TtestCyfraOblicz.LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI_1;
begin
  CheckEquals(9, CyfraOblicz('33517983847'), Format('%s.%s: %s', [ClassName, 'LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI_1', '']));
end;

procedure TtestCyfraOblicz.LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI_2;
begin
  CheckEquals(8, CyfraOblicz('33517880500'), Format('%s.%s: %s', [ClassName, 'LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI_2', '']));
end;

procedure TtestCyfraOblicz.LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI_3;
begin
  CheckEquals(2, CyfraOblicz('33517983817'), Format('%s.%s: %s', [ClassName, 'LANCUCH_11_CYFR_CZY_PRAWIDLOWA_CYFRA_SAMOKONTROLI_3', '']));
end;

procedure TtestCyfraOblicz.LANCUCH_42_CYFR_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraOblicz('42'), Format('%s.%s: %s', [ClassName, 'LANCUCH_55_CYFR_CZY_ZWROCI_MINUS_0', '']));
end;

procedure TtestCyfraOblicz.PUSTY_LANCUCH_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraOblicz(''), Format('%s.%s: %s', [ClassName, 'PUSTY_LANCUCH_CZY_ZWROCI_0', '']));
end;

initialization
  RegisterTest(TtestCyfraOblicz.suite);

end.


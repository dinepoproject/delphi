unit UCyfraObliczBezOstatniej;

interface

uses
  System.SysUtils, TestFramework, Vcl.Dialogs;

type
  TtestUCyfraObliczBezOstatniej = class(TTestCase)
  private
  published
    procedure PUSTY_LANCUCH_CZY_ZWROCI_0;
    procedure LANCUCH_BEZ_CYFR_LITERA_CZY_ZWROCI_0;
    procedure LANCUCH_BEZ_CYFR_MYSLNIK_CZY_ZWROCI_0;
    procedure LANCUCH_000109_CZY_ZWROCI_9;
    procedure LANCUCH_00010_9_CZY_ZWROCI_9;
  end;

implementation

uses
  GxTrainUtils;

{ TtestUCyfraObliczBezOstatniej }

procedure TtestUCyfraObliczBezOstatniej.LANCUCH_000109_CZY_ZWROCI_9;
begin
  CheckEquals(9, CyfraObliczBezOstatniej('000109'), Format('%s.%s: %s', [ClassName, 'LANCUCH_000109_CZY_ZWROCI_9', '']));
end;

procedure TtestUCyfraObliczBezOstatniej.LANCUCH_00010_9_CZY_ZWROCI_9;
begin
  CheckEquals(9, CyfraObliczBezOstatniej('00010-9'), Format('%s.%s: %s', [ClassName, 'LANCUCH_00010_9_CZY_ZWROCI_9', '']));
end;

procedure TtestUCyfraObliczBezOstatniej.LANCUCH_BEZ_CYFR_LITERA_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraObliczBezOstatniej('WAGON'), Format('%s.%s: %s', [ClassName, 'LANCUCH_BEZ_CYFR_LITERA_CZY_ZWROCI_0', '']));
end;

procedure TtestUCyfraObliczBezOstatniej.LANCUCH_BEZ_CYFR_MYSLNIK_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraObliczBezOstatniej('--'), Format('%s.%s: %s', [ClassName, 'LANCUCH_BEZ_CYFR_MYSLNIK_CZY_ZWROCI_0', '']));
end;

procedure TtestUCyfraObliczBezOstatniej.PUSTY_LANCUCH_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraObliczBezOstatniej(''), Format('%s.%s: %s', [ClassName, 'PUSTY_LANCUCH_CZY_ZWROCI_0', '']));
end;

initialization
  RegisterTest(TtestUCyfraObliczBezOstatniej.suite);

end.


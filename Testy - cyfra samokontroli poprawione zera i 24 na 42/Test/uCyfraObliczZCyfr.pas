unit UCyfraObliczZCyfr;

interface

uses
  System.SysUtils, TestFramework, Vcl.Dialogs;

type
  TtestUCyfraObliczZCyfr = class(TTestCase)
  published
    procedure PUSTY_LANCUCH_CZY_ZWROCI_0;
    procedure LANCUCH_00010_CZY_ZWROCI_9;
    procedure LANCUCH_00_01_0_CZY_ZWROCI_9;
  end;

implementation

uses
  GxTrainUtils;


{ TtestUCyfraObliczZCyfr }

procedure TtestUCyfraObliczZCyfr.LANCUCH_00010_CZY_ZWROCI_9;
begin
  CheckEquals(9, CyfraObliczZCyfr('00010'), Format('%s.%s: %s', [ClassName, 'LANCUCH_00010_CZY_ZWROCI_9', '']));
end;

procedure TtestUCyfraObliczZCyfr.LANCUCH_00_01_0_CZY_ZWROCI_9;
begin
  CheckEquals(9, CyfraObliczZCyfr('00-01-0'), Format('%s.%s: %s', [ClassName, 'LANCUCH_00_01_0_CZY_ZWROCI_9', '']));
end;

procedure TtestUCyfraObliczZCyfr.PUSTY_LANCUCH_CZY_ZWROCI_0;
begin
  CheckEquals(0, CyfraObliczZCyfr(''), Format('%s.%s: %s', [ClassName, 'PUSTY_LANCUCH_CZY_ZWROCI_0', '']));
end;

initialization
  RegisterTest(TtestUCyfraObliczZCyfr.suite);

end.


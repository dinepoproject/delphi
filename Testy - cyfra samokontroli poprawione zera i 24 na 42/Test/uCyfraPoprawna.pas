unit UCyfraPoprawna;

interface

uses
  System.SysUtils, TestFramework, Vcl.Dialogs;

type
  TtestUCyfraPoprawna = class(TTestCase)
  private
  published
    procedure PUSTY_LANCUCH_CZY_ZWROCI_FALSE;
    procedure LANCUCH_Z_JEDNA_CYFRA_SZCZEGOLNIE_5_CZY_ZWROCI_FALSE;
    procedure LANCUCH_POPRAWNY_NUMER_BEZ_FORMATOWANIA_CZY_ZWROCI_TRUE;
    procedure LANCUCH_NIEPOPRAWNY_NUMER_BEZ_FORMATOWANIA_CZY_ZWROCI_FALSE;
    procedure LANCUCH_POPRAWNY_NUMER_Z_FORMATOWANIEM_CZY_ZWROCI_TRUE;
    procedure LANCUCH_NIEPOPRAWNY_NUMER_Z_FORMATOWANIEM_CZY_ZWROCI_FALSE;
  end;

implementation

uses
  GxTrainUtils;

{ TtestUCyfraPoprawna }

procedure TtestUCyfraPoprawna.LANCUCH_NIEPOPRAWNY_NUMER_BEZ_FORMATOWANIA_CZY_ZWROCI_FALSE;
begin
  CheckEquals(FALSE, CyfraPoprawna('315166533480'), Format('%s.%s: %s', [ClassName, 'LANCUCH_NIEPOPRAWNY_NUMER_BEZ_FORMATOWANIA_CZY_ZWROCI_FALSE', '']));
end;

procedure TtestUCyfraPoprawna.LANCUCH_NIEPOPRAWNY_NUMER_Z_FORMATOWANIEM_CZY_ZWROCI_FALSE;
begin
  CheckEquals(FALSE, CyfraPoprawna('31-51-6637076-8'), Format('%s.%s: %s', [ClassName, 'LANCUCH_NIEPOPRAWNY_NUMER_Z_FORMATOWANIEM_CZY_ZWROCI_FALSE', '']));
end;

procedure TtestUCyfraPoprawna.LANCUCH_POPRAWNY_NUMER_BEZ_FORMATOWANIA_CZY_ZWROCI_TRUE;
begin
  CheckEquals(TRUE, CyfraPoprawna('315166380628'), Format('%s.%s: %s', [ClassName, 'LANCUCH_POPRAWNY_NUMER_BEZ_FORMATOWANIA_CZY_ZWROCI_TRUE', '']));
end;

procedure TtestUCyfraPoprawna.LANCUCH_POPRAWNY_NUMER_Z_FORMATOWANIEM_CZY_ZWROCI_TRUE;
begin
  CheckEquals(TRUE, CyfraPoprawna('31-51-6634825-2'), Format('%s.%s: %s', [ClassName, 'LANCUCH_POPRAWNY_NUMER_Z_FORMATOWANIEM_CZY_ZWROCI_TRUE', '']));
end;

procedure TtestUCyfraPoprawna.LANCUCH_Z_JEDNA_CYFRA_SZCZEGOLNIE_5_CZY_ZWROCI_FALSE;
VAR
  I: INTEGER;
begin
  for I := 1 to 9 do
  begin
    CheckEquals(FALSE, CyfraPoprawna('I'), Format('%s.%s: %s', [ClassName, 'LANCUCH_Z_JEDNA_CYFRA_SZCZEGOLNIE_5_CZY_ZWROCI_FALSE', '']));
  end;
end;

procedure TtestUCyfraPoprawna.PUSTY_LANCUCH_CZY_ZWROCI_FALSE;
begin
  CheckEquals(FALSE, CyfraPoprawna(''), Format('%s.%s: %s', [ClassName, 'PUSTY_LANCUCH_CZY_ZWROCI_FALSE', '']));
end;

initialization
  RegisterTest(TtestUCyfraPoprawna.suite);

end.


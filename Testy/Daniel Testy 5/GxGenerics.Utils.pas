﻿unit GxGenerics.Utils;

interface

uses
  System.TypInfo, System.Math;

  /// Tym razem do testów przesyłam klasę. Nie jest to typowa klasa bo:
  ///  1. jest generyczna - czyli działa dla różnych typów danych
  ///  2. zawiera wyłącznie metody klasowe - a więc nie jest konieczne tworzenie instancji klasy aby z niej korzystać.
  ///
  /// Jak to testować? Na potrzeby testów trzeba utworzyć jakiś typ enumeryczny np.:
  ///  TTestEnum = (teEnum1, teEnum2, teEnum3, teEnum4);
  /// Aby wywołać metodę klasy należy użyć następującego kodu: TGxEnumeration<typ>.funkcja czyli np.:
  ///  TGxEnumeration<TTestEnum>.IsOrdinal
  /// Oczywiście zamiast TTestEnum możemy podać dowolny inny typ enumeryczny albo klasowy albo prosty.
  ///
  /// Zakres testów:
  ///  1. Funkcja IsEnumeration:
  ///  - test sprawdzający czy funkcja zwróci True dla typu enumerycznego (np. naszego testowego)
  ///  - test sprawdzający czy funkcja zwróci False dla typu prostego (np. String albo Integer)
  ///  - test sprawdzający czy funkcja zwróci False dla typu klasowego (np. TObject albo TComponent)
  ///  2. Funkcja ToOrdinal - funkcja zwraca indeks elementu dla typu enumerycznego zaczynając o zera. Dla pierwszego
  ///    elementu zwróci 0, dla drugiego 1 itd.
  ///  - test sprawdzający czy funkcja zwróci 0 dla pierwszego elementu (teEnum1) testowego typu enumerycznego TTestEnum
  ///  - test sprawdzający czy funkcja zwróci 1 dla pierwszego elementu (teEnum2) testowego typu enumerycznego TTestEnum
  ///  - test sprawdzający czy funkcja zwróci 2 dla pierwszego elementu (teEnum3) testowego typu enumerycznego TTestEnum
  ///  - test sprawdzający czy funkcja zwróci 3 dla pierwszego elementu (teEnum4) testowego typu enumerycznego TTestEnum
  ///  3. Funkcja FromOrdinal - funkcja odwrotna do funkcji ToOrdinal czyli dla indeksu zwraca element typu enumerycznego
  ///  - test sprawdzający czy dla wartości 0 funkcja zwróci pierwszy element typu enumerycznego (teEnum1)
  ///  - test sprawdzający czy dla wartości 1 funkcja zwróci pierwszy element typu enumerycznego (teEnum2)
  ///  - test sprawdzający czy dla wartości 2 funkcja zwróci pierwszy element typu enumerycznego (teEnum3)
  ///  - test sprawdzający czy dla wartości 3 funkcja zwróci pierwszy element typu enumerycznego (teEnum4)
  ///  4. Funkcja MinValue - funkcja zwraca wartość indeksu dla pierwszego elementu typu enumerycznego
  ///  - test sprawdzający czy dla typu TTestEnum funkcja zwraca 0
  ///  5. Funkcja MaxValue - funkcja zwraca wartość indeksu dla ostatniego elementu typu enumerycznego
  ///  - test sprawdzający czy dla typu TTestEnum funkcja zwraca 3
  ///  6. Funkcja InRange - funkcja zwraca True, gdy podana wartość mieści się w zakresie indeksów dla elementów typu
  ///  enumerycznego
  ///  - test sprawdzający czy dla typu TTestEnum funkcja zwróci False dla wartości -1
  ///  - test sprawdzający czy dla typu TTestEnum funkcja zwróci False dla wartości 4
  ///  - test sprawdzający czy dla typu TTestEnum funkcja zwróci True dla wartości od 0 do 3 (test w pętli?)
  ///  7. Funkcja EnsureRange - funkcja zwraca wartość, która na pewno mieści się w zakresie indeksów elementów typu
  ///  enumerycznego. Dla wartości prawidłowych indeksów zwraca wartość tego indeksu, dla wartości przekraczającej
  ///  indeks ostatniego elementu indeks ostatniego elementu i dla wartości mniejszej od indeksu pierwszego elementu
  ///  indeks pierwszego elementu.
  ///  - test sprawdzający czy dla typu TTestEnum funkcja zwróci 0 dla wartości -1
  ///  - test sprawdzający czy dla typu TTestEnum funkcja zwróci 3 dla wartości 4 lub większej
  ///  - test sprawdzający czy dla typu TTestEnum funkcja zwróci podany indeks dla wartości od 0 do 3 (test w pętli?)
  ///  8. Funkcja NameOf - funkcja zwraca łańcuch z nazwą elementu typu enumerycznego
  ///  - test sprawdzający czy dla typu TTestEnum funkcje zwróci: 'teEnum1' dla wartości teEnum1.
  ///  - test sprawdzający czy dla typu TTestEnum funkcje zwróci: 'teEnum3' dla wartości teEnum3.
  ///  - test sprawdzający czy dla typu TTestEnum funkcje zwróci: 'teEnum4' dla wartości teEnum4.

type
    /// <summary>
    /// Klasa umożliwiająca obsługę typów enumerycznych w powiązaniu z generycznymi.
    /// Klasa udostępniona przez David Heffernan
    /// (http://stackoverflow.com/questions/27901990/how-can-i-call-getenumname-with-a-generic-enumerated-type)
    /// </summary>
  TGxEnumeration<T: record> = class
  strict private
    class function TypeInfo: PTypeInfo; inline; static;
    class function TypeData: PTypeData; inline; static;
  public
    class function IsEnumeration: Boolean; static;
    class function ToOrdinal(AEnum: T): Integer; static; inline;
    class function FromOrdinal(AValue: Integer): T; static; inline;
    class function MinValue: Integer; inline; static;
    class function MaxValue: Integer; inline; static;
    class function InRange(AValue: Integer): Boolean; static; inline;
    class function EnsureRange(AValue: Integer): Integer; static; inline;
    class function NameOf(AEnum: T): String; static; inline;
  end;

implementation

{ TGxEnumeration<T> }

class function TGxEnumeration<T>.TypeInfo: PTypeInfo;
begin
  Result := System.TypeInfo(T);
end;

class function TGxEnumeration<T>.TypeData: PTypeData;
begin
  Result := System.TypInfo.GetTypeData(TypeInfo);
end;

class function TGxEnumeration<T>.IsEnumeration: Boolean;
begin
  Result := TypeInfo.Kind = tkEnumeration;
end;

class function TGxEnumeration<T>.ToOrdinal(AEnum: T): Integer;
begin
  Assert(IsEnumeration);
  Assert(SizeOf(AEnum)<=SizeOf(Result));
  Result := 0; // needed when SizeOf(AEnum) < SizeOf(Result)
  Move(AEnum, Result, SizeOf(AEnum));
  Assert(InRange(Result));
end;

class function TGxEnumeration<T>.FromOrdinal(AValue: Integer): T;
begin
  Assert(IsEnumeration);
  Assert(InRange(AValue));
  Assert(SizeOf(Result)<=SizeOf(AValue));
  Move(AValue, Result, SizeOf(Result));
end;

class function TGxEnumeration<T>.MinValue: Integer;
begin
  Assert(IsEnumeration);
  Result := TypeData.MinValue;
end;

class function TGxEnumeration<T>.MaxValue: Integer;
begin
  Assert(IsEnumeration);
  Result := TypeData.MaxValue;
end;

class function TGxEnumeration<T>.InRange(AValue: Integer): Boolean;
var
  ptd: PTypeData;
begin
  Assert(IsEnumeration);
  ptd := TypeData;
  Result := System.Math.InRange(AValue, ptd.MinValue, ptd.MaxValue);
end;

class function TGxEnumeration<T>.EnsureRange(AValue: Integer): Integer;
var
  ptd: PTypeData;
begin
  Assert(IsEnumeration);
  ptd := TypeData;
  Result := System.Math.EnsureRange(AValue, ptd.MinValue, ptd.MaxValue);
end;

class function TGxEnumeration<T>.NameOf(AEnum: T): String;
begin
  Assert(IsEnumeration);
  Result := GetEnumName(TypeInfo, ToOrdinal(AEnum));
end;

end.

unit UIsEnumeration.test;

interface

uses
  System.SysUtils, TestFramework, System.DateUtils, Vcl.Dialogs;

type
  TIsEnumeration = class(TTestCase)
  private
  published
    procedure True_dla_typu_enumerycznego;
    //procedure False_dla_typu_string;
    //procedure False_dla_typu_Tobject;
    procedure False_dla_typu_integer;
  end;

  TDefault = class(TObject)
  end;

type
  TToOrdinal = class(TTestCase)
  private
  published
    procedure Wartosc_dla_TeEnum1;
    procedure Wartosc_dla_TeEnum2;
    procedure Wartosc_dla_TeEnum3;
    procedure Wartosc_dla_TeEnum4;
  end;

type
  TFromOrdinal = class(TGenericTestCase)
  private
  published
    procedure Dla_wartosci_0_czy_zwroci_TeEnum1;
    procedure Dla_wartosci_1_czy_zwroci_TeEnum2;
    procedure Dla_wartosci_2_czy_zwroci_TeEnum3;
    procedure Dla_wartosci_3_czy_zwroci_TeEnum4;
  end;

type
  TMinValue = class(TTestCase)
  private
  published
    procedure Czy_dla_typu_TTestEnum_funkcja_zwraca_0;
  end;

type
  TMaxValue = class(TTestCase)
  private
  published
    procedure Czy_dla_typu_TTestEnum_funkcja_zwraca_3;
  end;

type
  TInRange = class(TTestCase)
  private
  published
    procedure Czy_funkcja_zwroci_False_dla_wartosci_4;
    procedure Czy_funkcja_zwroci_False_dla_wartosci_minus_1;
    procedure Czy_funkcja_zwroci_True_dla_wartosci_od_0_do_3;
  end;

type
  TEnsureRange = class(TTestCase)
  private
  published
    procedure Czy_funkcja_zwroci_0_dla_wartosci_minus_1;
    procedure Czy_funkcja_zwroci_3_dla_wartosci_5;
    procedure Czy_funkcja_zwroci_podany_indeks_dla_wartosci_od_0_do_3;
  end;

type
  TEnsureRange1 = class(TTestCase)
  private
  published
    procedure Czy_funkcja_zwroci_0_dla_wartosci_minus_1;
    procedure Czy_funkcja_zwroci_3_dla_wartosci_5;
    procedure Czy_funkcja_zwroci_podany_indeks_dla_wartosci_od_0_do_3;
  end;

type
  TNameOf = class(TTestCase)
  private
  published
    procedure Czy_funkcja_zwroci_teEnum1_dla_wartosci_teEnum1;
    procedure Czy_funkcja_zwroci_teEnum2_dla_wartosci_teEnum2;
    procedure Czy_funkcja_zwroci_teEnum3_dla_wartosci_teEnum3;
  end;

type
  TTestEnum = (teEnum1, teEnum2, teEnum3, teEnum4);

{type
  TMyType = array of String; }
{Type
  TMojTyp = string;  } // przyk�ad z ksiazki jak zrobic typ, popatrz jak to robi Adam w integer
type
  TMojIntTyp = type Integer;

implementation

uses
  GxGenerics.Utils;





{ TIsEnumeration }
 {function ArrayFunc : TMyType;
  SetLength(Result, 2);
  Result[0] := 'Delphi';
  Result[1] := '2005';
end; }

{procedure TIsEnumeration.False_dla_typu_string;
var
  litera: TMojTyp;
begin
 {ArrayFunc; }
  {litera := 'litera';

  CheckEquals(false, TGxEnumeration<TMojTyp>.IsEnumeration, Format('%s.%s: %s', [ClassName, 'False_dla_typu_string', '']));
end; }

{{procedure TIsEnumeration.False_dla_typu_Tobject;
begin
  CheckEquals(false,TGxEnumeration<TDefault>.IsEnumeration , Format('%s.%s: %s', [ClassName, 'False_dla_typu_Tobject', '']));
end; }

procedure TIsEnumeration.False_dla_typu_integer;
begin
  CheckEquals(false, TGxEnumeration<TMojIntTyp>.IsEnumeration, Format('%s.%s: %s', [ClassName, 'False_dla_typu_integer', '']));
end;

procedure TIsEnumeration.True_dla_typu_enumerycznego;
begin
  CheckEquals(true, TGxEnumeration<TTestEnum>.IsEnumeration, Format('%s.%s: %s', [ClassName, 'True_dla_typu_enumerycznego', '']));
end;

procedure TToOrdinal.Wartosc_dla_TeEnum2;
begin
  CheckEquals(1, TGxEnumeration<TTestEnum>.ToOrdinal(teEnum2), Format('%s.%s: %s', [ClassName, 'Wartosc_dla_TeEnum2', '']));
end;

procedure TToOrdinal.Wartosc_dla_TeEnum1;
begin
  CheckEquals(0, TGxEnumeration<TTestEnum>.ToOrdinal(teEnum1), Format('%s.%s: %s', [ClassName, 'Wartosc_dla_enum1', '']));
end;

procedure TToOrdinal.Wartosc_dla_TeEnum3;
begin
  CheckEquals(2, TGxEnumeration<TTestEnum>.ToOrdinal(teEnum3), Format('%s.%s: %s', [ClassName, 'Wartosc_dla_TeEnum3', '']));
end;

procedure TToOrdinal.Wartosc_dla_TeEnum4;
begin
  CheckEquals(3, TGxEnumeration<TTestEnum>.ToOrdinal(teEnum4), Format('%s.%s: %s', [ClassName, 'Wartosc_dla_TeEnum4', '']));
end;

procedure TFromOrdinal.Dla_wartosci_1_czy_zwroci_TeEnum2;
begin
  CheckEquals<TTestEnum>(teEnum2, TGxEnumeration<TTestEnum>.FromOrdinal(1), Format('%s.%s: %s', [ClassName, 'Dla_wartosci_1_czy_zwroci_TeEnum2', '']));
end;

procedure TFromOrdinal.Dla_wartosci_0_czy_zwroci_TeEnum1;
begin
  CheckEquals<TTestEnum>(teEnum1, TGxEnumeration<TTestEnum>.FromOrdinal(0), Format('%s.%s: %s', [ClassName, 'Dla_wartosci_0_czy_zwroci_TeEnum1', '']));
end;

procedure TFromOrdinal.Dla_wartosci_2_czy_zwroci_TeEnum3;
begin
  CheckEquals<TTestEnum>(teEnum3, TGxEnumeration<TTestEnum>.FromOrdinal(2), Format('%s.%s: %s', [ClassName, 'Dla_wartosci_2_czy_zwroci_TeEnum3', '']));
end;

procedure TFromOrdinal.Dla_wartosci_3_czy_zwroci_TeEnum4;
begin
  CheckEquals(TeEnum4, TGxEnumeration<TTestEnum>.FromOrdinal(3), Format('%s.%s: %s', [ClassName, 'Dla_wartosci_3_czy_zwroci_TeEnum4', '']));
end;

procedure TMinValue.Czy_dla_typu_TTestEnum_funkcja_zwraca_0;
begin
  CheckEquals(0, TGxEnumeration<TTestEnum>.MinValue, Format('%s.%s: %s', [ClassName, 'Czy_dla_typu_TTestEnum_funkcja_zwraca_0', '']));
end;

procedure TMaxValue.Czy_dla_typu_TTestEnum_funkcja_zwraca_3;
begin
  CheckEquals(3, TGxEnumeration<TTestEnum>.MaxValue, Format('%s.%s: %s', [ClassName, 'Czy_dla_typu_TTestEnum_funkcja_zwraca_3', '']));
end;

procedure TInRange.Czy_funkcja_zwroci_False_dla_wartosci_4;
begin
  CheckEquals(False, TGxEnumeration<TTestEnum>.InRange(4), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_False_dla_wartosci_4', '']));
end;

procedure TInRange.Czy_funkcja_zwroci_False_dla_wartosci_minus_1;
begin
  CheckEquals(False, TGxEnumeration<TTestEnum>.InRange(-1), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_False_dla_wartosci_minus_1', '']));
end;

procedure TInRange.Czy_funkcja_zwroci_True_dla_wartosci_od_0_do_3;
var
  I: integer;
begin
  for I := 0 to 3 do
  begin
    CheckEquals(True, TGxEnumeration<TTestEnum>.InRange(I), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_True_dla_wartosci_od_0_do_3', '']));
  end;
end;

procedure TEnsureRange.Czy_funkcja_zwroci_0_dla_wartosci_minus_1;
begin
  CheckEquals(0, TGxEnumeration<TTestEnum>.EnsureRange(-1), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_0_dla_wartosci_minus_1', '']));
end;

procedure TEnsureRange.Czy_funkcja_zwroci_3_dla_wartosci_5;
begin
  CheckEquals(3, TGxEnumeration<TTestEnum>.EnsureRange(5), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_3_dla_wartosci_5', '']));
end;

procedure TEnsureRange.Czy_funkcja_zwroci_podany_indeks_dla_wartosci_od_0_do_3;
var
  I: integer;
begin
  for I := 0 to 3 do
  begin
    CheckEquals((I), TGxEnumeration<TTestEnum>.EnsureRange(I), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_podany_indeks_dla_wartosci_od_0_do_3', '']));
  end;
end;

procedure TEnsureRange1.Czy_funkcja_zwroci_0_dla_wartosci_minus_1;
begin
  CheckEquals(0, TGxEnumeration<TTestEnum>.EnsureRange(-1), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_0_dla_wartosci_minus_1', '']));
end;

procedure TEnsureRange1.Czy_funkcja_zwroci_3_dla_wartosci_5;
begin
  CheckEquals(3, TGxEnumeration<TTestEnum>.EnsureRange(5), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_3_dla_wartosci_5', '']));
end;

procedure TEnsureRange1.Czy_funkcja_zwroci_podany_indeks_dla_wartosci_od_0_do_3;
var
  I: integer;
begin
  for I := 0 to 3 do
  begin
    CheckEquals((I), TGxEnumeration<TTestEnum>.EnsureRange(I), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_podany_indeks_dla_wartosci_od_0_do_3', '']));
  end;
end;

procedure TNameOf.Czy_funkcja_zwroci_teEnum1_dla_wartosci_teEnum1;
begin
  CheckEquals('teEnum1', TGxEnumeration<TTestEnum>.NameOf(teEnum1), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_teEnum1_dla_wartosci_teEnum1', '']));
end;

procedure TNameOf.Czy_funkcja_zwroci_teEnum2_dla_wartosci_teEnum2;
begin
  CheckEquals('teEnum2', TGxEnumeration<TTestEnum>.NameOf(teEnum2), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_teEnum2_dla_wartosci_teEnum2', '']));
end;

procedure TNameOf.Czy_funkcja_zwroci_teEnum3_dla_wartosci_teEnum3;
begin
  CheckEquals('teEnum3', TGxEnumeration<TTestEnum>.NameOf(teEnum3), Format('%s.%s: %s', [ClassName, 'Czy_funkcja_zwroci_teEnum3_dla_wartosci_teEnum3', '']));
end;

initialization
  RegisterTests([TIsEnumeration.suite, TToOrdinal.suite, TFromOrdinal.suite, TMinValue.Suite, TMaxValue.Suite, TInRange.Suite, TEnsureRange.Suite, TNameOf.Suite]);

end.


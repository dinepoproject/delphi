   unit PeriodInPeriodTest;



interface
 uses
 System.SysUtils, TestFramework,system.DateUtils;
 type
TtestPeriodInPeriod = class(TTestCase)
    published

 procedure  Test_Zakres_Przed_Zakresem_AInFrom_I_AInTo;
 procedure  Test_Zakres_Za_Zakresem_AInFrom_I_AInTo;
 procedure  Test_Data_Czas_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_False;
 procedure  Test_Data_Czas_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_True;
 procedure  Test_Tylko_APeriodFrom_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_False;
 procedure  Test_Tylko_APeriodFrom_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_True;
 procedure  Test_Tylko_APeriodTo_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_False;
 procedure  Test_Tylko_APeriodTo_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_True;
 procedure  Test_APeriodFrom_APeriodTo_Rowne_AInFrom_I_AInTo_Partial_False;
 procedure  Test_APeriodFrom_APeriodTo_Rowne_AInFrom_I_AInTo_Partial_True;
 procedure  Test_Assercja_AInFrom_Pozniejsze_Od_AInTo;
 procedure  Test_Assercja_APeriodFrom_Pozniejsze_Od_APeriodTo;
end;
implementation
uses
GxDateUtils;



{ TtestPeriodInPeriod }

procedure TtestPeriodInPeriod.Test_APeriodFrom_APeriodTo_Rowne_AInFrom_I_AInTo_Partial_False;
begin
CheckEquals(True,PeriodInPeriod((EncodeDatetime(2015,04,01,23,59,59,999)),(EncodeDatetime(2015,05,02,23,59,59,999)),(EncodeDatetime(2015,04,01,23,59,59,999)),(EncodeDatetime(2015,05,02,23,59,59,999)),False), Format('%s.%s: %s', [ClassName, 'Test_APeriodFrom_APeriodTo_Rowne_AInFrom_I_AInTo_Partial_False', '']));
end;

procedure TtestPeriodInPeriod.Test_APeriodFrom_APeriodTo_Rowne_AInFrom_I_AInTo_Partial_True;
begin
CheckEquals(True,PeriodInPeriod((EncodeDatetime(2015,04,01,23,59,59,999)),(EncodeDatetime(2015,05,02,23,59,59,999)),(EncodeDatetime(2015,04,01,23,59,59,999)),(EncodeDatetime(2015,05,02,23,59,59,999)),True), Format('%s.%s: %s', [ClassName, 'Test_APeriodFrom_APeriodTo_Rowne_AInFrom_I_AInTo_Partial_True', '']));
end;

procedure TtestPeriodInPeriod.Test_Assercja_AInFrom_Pozniejsze_Od_AInTo;
begin
  {$IFOPT C+}
  ExpectedException := EAssertionFailed;
  PeriodInPeriod((EncodeDatetime(2015,04,01,23,59,59,999)),EncodeDatetime(2015,05,02,23,59,59,999),EncodeDatetime(2015,12,31,23,59,59,999),EncodeDatetime(2015,01,01,23,59,59,999), True);
  {$ELSE}
  Check(True, Format('%s.%s: %s', [ClassName, 'Test_Assercja_AInFrom_Pozniejsze_Od_AInTo', '']));
  {$ENDIF}
end;

procedure TtestPeriodInPeriod.Test_Assercja_APeriodFrom_Pozniejsze_Od_APeriodTo;
begin
 {$IFOPT C+}
 ExpectedException := EAssertionFailed;
 PeriodInPeriod((EncodeDatetime(2015,12,31,23,59,59,999)),(EncodeDatetime(2015,01,01,23,59,59,999)),(EncodeDatetime(2015,04,01,23,59,59,999)),(EncodeDatetime(2015,05,02,23,59,59,999)),True);
 {$ELSE}
 Check(True, Format('%s.%s: %s', [ClassName, 'Test_Assercja_APeriodFrom_Pozniejsze_Od_APeriodTo', '']));
 {$ENDIF}
 end;

procedure TtestPeriodInPeriod.Test_Data_Czas_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_False;
begin
CheckEquals(True,PeriodInPeriod((EncodeDatetime(2015,05,01,23,59,59,999)),(EncodeDatetime(2015,05,02,23,59,59,999)),(EncodeDatetime(2015,01,01,23,59,59,999)),(EncodeDatetime(2015,12,01,23,59,59,999)),False), Format('%s.%s: %s', [ClassName, 'Test_Data_Czas_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_False', '']));
end;

procedure TtestPeriodInPeriod.Test_Data_Czas_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_True;
begin
CheckEquals(True, PeriodInPeriod((EncodeDatetime(2015,05,01,23,59,59,999)),(EncodeDatetime(2015,05,02,23,59,59,999)),(EncodeDatetime(2015,01,01,23,59,59,999)),(EncodeDatetime(2015,12,01,23,59,59,999)),True), Format('%s.%s: %s', [ClassName, 'Test_Data_Czas_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_True', '']));
end;

procedure TtestPeriodInPeriod.Test_Tylko_APeriodFrom_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_False;
begin
CheckEquals(False, PeriodInPeriod((EncodeDatetime(2015,05,01,23,59,59,999)),(EncodeDatetime(2015,12,31,23,59,59,999)),(EncodeDatetime(2015,04,01,23,59,59,999)),(EncodeDatetime(2015,06,01,23,59,59,999)),False), Format('%s.%s: %s', [ClassName, 'Test_Tylko_APeriodFrom_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_False', '']));
end;

procedure TtestPeriodInPeriod.Test_Tylko_APeriodFrom_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_True;
begin
CheckEquals(True,PeriodInPeriod((EncodeDatetime(2015,05,01,23,59,59,999)),(EncodeDatetime(2015,12,31,23,59,59,999)),(EncodeDatetime(2015,04,01,23,59,59,999)),(EncodeDatetime(2015,06,01,23,59,59,999)),True), Format('%s.%s: %s', [ClassName, 'Test_Tylko_APeriodFrom_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_True', '']));
end;

procedure TtestPeriodInPeriod.Test_Tylko_APeriodTo_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_False;
begin
CheckEquals(False,PeriodInPeriod((EncodeDatetime(2015,01,01,23,59,59,999)),(EncodeDatetime(2015,05,31,23,59,59,999)),(EncodeDatetime(2015,04,01,23,59,59,999)),(EncodeDatetime(2015,06,01,23,59,59,999)),False), Format('%s.%s: %s', [ClassName, 'Test_Tylko_APeriodTo_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_False', '']));
end;

procedure TtestPeriodInPeriod.Test_Tylko_APeriodTo_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_True;
begin
CheckEquals(True,PeriodInPeriod((EncodeDatetime(2015,01,01,23,59,59,999)),(EncodeDatetime(2015,05,31,23,59,59,999)),(EncodeDatetime(2015,04,01,23,59,59,999)),(EncodeDatetime(2015,06,01,23,59,59,999)),True), Format('%s.%s: %s', [ClassName, 'Test_Tylko_APeriodTo_Pomiedzy_Wartosciami_AInFrom_I_AInTo_Partial_True', '']));
end;

procedure TtestPeriodInPeriod.Test_Zakres_Przed_Zakresem_AInFrom_I_AInTo;
begin
CheckEquals(False,PeriodInPeriod((EncodeDatetime(2015,01,01,23,59,59,999)),(EncodeDatetime(2015,01,02,23,59,59,999)),(EncodeDatetime(2015,12,01,23,59,59,999)),(EncodeDatetime(2015,12,31,23,59,59,999)),False), Format('%s.%s: %s', [ClassName, 'Test_Zakres_Przed_Zakresem_AInFrom_I_AInTo', '']));
end;

procedure TtestPeriodInPeriod.Test_Zakres_Za_Zakresem_AInFrom_I_AInTo;
begin
CheckEquals(False,PeriodInPeriod((EncodeDatetime(2015,10,01,23,59,59,999)),(EncodeDatetime(2015,10,31,23,59,59,999)),(EncodeDatetime(2015,01,01,23,59,59,999)),(EncodeDatetime(2015,01,31,23,59,59,999)),False), Format('%s.%s: %s', [ClassName, 'Test_Zakres_za_Zakresem_AInFrom_I_AInTo', '']));
end;

initialization
RegisterTest ( TtestPeriodInPeriod.suite);
end.

unit GxDateUtils;

interface

    /// <summary>
    /// Funkcja zwraca informacj�, czy dana data i czas (ADateTime) pochodzi z zakresu od (AFrom) do (ATo)
    /// </summary>
    /// Testy do wykonania:
    /// 1. ADateTime przed zakresem AInFrom i AInTo - wynik False
    /// 2. ADateTime za zakresem AInFrom i AInTo - wynik False
    /// 3. ADateTime pomi�dzy warto�ciami AInFrom i AInTo - wynik True
    /// 4. ADateTime r�wny warto�ci AInFrom - wynik True
    /// 5. ADateTime r�wny warto�ci AInTo - wynik True
    /// 6. ADateTime data r�wna warto�ci AInFrom, ale wcze�niejsza godzina - wynik False.
    /// 7. ADateTime data r�wna warto�ci AInTo, ale p�niejsza godzina - wynik False.
    /// 8. Test czy dzia�a assercja gdy AInFrom jest p�niejsze od AInTo.
  function DateTimeInPeriod(const ADateTime, AInFrom, AInTo: TDateTime): Boolean;

    /// <summary>
    /// Funkcja zwraca informacj� czy zakres czasu jest zawarty w innym zakresie.
    /// Mo�liwe jest okre�lenie, �e funkcja ma sprawdza� jedynie zaz�bienie zakres�w -
    /// czyli cz�ciowe zawieranie si� jednego zakresu w drugim.
    /// </summary>
    /// Testy do wykonania:
    /// 1. Zakres przed zakresem AInFrom i AInTo - wynik False
    /// 2. Zakres za zakresem AInFrom i AInTo - wynik False
    /// 3. Obie warto�ci daty i czasu zakresu pomi�dzy warto�ciami AInFrom i AInTo, parametr APartial=False - wynik True
    /// 4. Obie warto�ci daty i czasu zakresu pomi�dzy warto�ciami AInFrom i AInTo, parametr APartial=True - wynik True
    /// 5. Tylko warto�� APeriodFrom pomi�dzy warto�ciami AInFrom i AInTo, parametr APartial=False - wynik False
    /// 6. Tylko warto�� APeriodFrom pomi�dzy warto�ciami AInFrom i AInTo, parametr APartial=True - wynik True
    /// 7. Tylko warto�� APeriodTo pomi�dzy warto�ciami AInFrom i AInTo, parametr APartial=False - wynik False
    /// 8. Tylko warto�� APeriodTo pomi�dzy warto�ciami AInFrom i AInTo, parametr APartial=True - wynik True
    /// 9. Warto�ci APeriodFrom i APeriodTo r�wne odpowiednio AInFrom i AInTo, parametr APartial=False - wynik True
    /// 10. Warto�ci APeriodFrom i APeriodTo r�wne odpowiednio AInFrom i AInTo, parametr APartial=True - wynik True
    /// 11. Test czy dzia�a assercja gdy AInFrom jest p�niejsze od AInTo.
    /// 12. Test czy dzia�a assercja gdy APeriodFrom jest p�niejsze od APeriodTo.
  function PeriodInPeriod(const APeriodFrom, APeriodTo, AInFrom, AInTo: TDateTime;
    const APartial: Boolean = False): Boolean;

    /// <summary>
    /// Funkcja zwraca dat� ko�ca kwarta�u na podstawie numeru roku i kwarta�u.
    /// </summary>
    /// Testy do wykonania -
    ///  1. czy funkcja zwraca prawid�ow� dat� ko�ca kwarta�u dla kolejnych kwarta��w z bie��cego roku.
    ///  2. jak funkcja obs�u�y podanie warto�ci zero dla kwarta�u - powinna zwr�ci� dat� ko�ca 1. kwarta�u.
    ///  3. jak funkcja obs�u�y podanie warto�ci 5 dla kwarta�u - powinna zwr�ci� dat� ko�ca 4. kwarta�u.
  function EndOfAQuarter(const AYear, AQuarter: Word): TDateTime;

    /// <summary>
    /// Funkcja zwraca dat� ko�ca kwarta�u na podstawie warto�ci daty i czasu.
    /// </summary>
    ///  Testy do wykonania - czy funkcja zwraca prawid�ow� dat� ko�ca kwarta�u dla nast�puj�cych warto�ci
    ///  1. Data pierwszego dnia ka�dego kwarta�u w bie��cym roku
    ///  2. Data ostatniego dnia ka�dego kwarta�u w bie��cym roku
    ///  3. Dowolna data z kwarta�u.
  function EndOfTheQuarter(const ADate: TDateTime): TDateTime;

implementation

uses
  System.SysUtils, System.DateUtils;

function DateTimeInPeriod(const ADateTime, AInFrom, AInTo: TDateTime): Boolean;
begin
  Assert(AInFrom <= AInTo);
  Result := (ADateTime >= AInFrom) and (ADateTime <= AInTo);
end;

function EndOfAQuarter(const AYear, AQuarter: Word): TDateTime;
var
  month: Word;
begin
  case AQuarter of
    1:
      month := 3;
    2:
      month := 6;
    3:
      month := 9;
    else
      month := 12;
  end;
  Result := EndOfAMonth(AYear, month);
end;

function EndOfTheQuarter(const ADate: TDateTime): TDateTime;
var
  d, m, y: Word;
begin
  DecodeDate(ADate, y, m, d);
  case m of
    1..3:
      m := 3;
    4..6:
      m := 6;
    7..9:
      m := 9;
    else
      m := 12;
  end;
  Result := EndOfAMonth(y, m);
end;

function PeriodInPeriod(const APeriodFrom, APeriodTo, AInFrom, AInTo: TDateTime;
  const APartial: Boolean = False): Boolean;
begin
  Assert(APeriodFrom <= APeriodTo); // assert ang. - zapewnij
  Assert(AInFrom <= AInTo);
  if APartial then
    Result := DateTimeInPeriod(APeriodFrom, AInFrom, AInTo) or DateTimeInPeriod(APeriodTo, AInFrom, AInTo)
  else
    Result := DateTimeInPeriod(APeriodFrom, AInFrom, AInTo) and DateTimeInPeriod(APeriodTo, AInFrom, AInTo);
end;

end.

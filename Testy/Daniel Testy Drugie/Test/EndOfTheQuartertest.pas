unit EndOfTheQuarterTest;



interface
 uses
 System.SysUtils, TestFramework,System.DateUtils;
 type
TtestEndOfTheQuarter = class(TTestCase)
    published

procedure Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Pierwszy_kwartal;
procedure Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Drugi_kwartal;
procedure Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Trzeci_kwartal;
procedure Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Czwarty_kwartal;
procedure Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Pierwszy_kwartal;
procedure Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Drugi_kwartal;
procedure Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Trzeci_kwartal;
procedure Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Czwarty_kwartal;
procedure Test_Dowolna_Data_Z_Kwartalu;
end;
implementation
uses
GxDateUtils;


{ TtestEndOfTheQuarter }

procedure TtestEndOfTheQuarter.Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Czwarty_Kwartal;
begin
CheckEquals(EncodeDatetime(2015,12,31,23,59,59,999),EndOfTheQuarter (EncodeDatetime(2015,12,31,23,59,59,999)), Format('%s.%s: %s', [ClassName, 'Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Czwarty_Kwartal', '']));
end;

procedure TtestEndOfTheQuarter.Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Drugi_Kwartal;
begin
CheckEquals(EncodeDatetime(2015,06,30,23,59,59,999),EndOfTheQuarter(EncodeDatetime(2015,06,30,23,59,59,999)) , Format('%s.%s: %s', [ClassName, 'Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Drugi_Kwartal', '']));
end;

procedure TtestEndOfTheQuarter.Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Pierwszy_Kwartal;
begin
CheckEquals(EncodeDatetime(2015,03,31,23,59,59,999),EndOfTheQuarter(EncodeDatetime(2015,03,31,23,59,59,999)) , Format('%s.%s: %s', [ClassName, 'Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Pierwszy_Kwartal', '']));
end;

procedure TtestEndOfTheQuarter.Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Trzeci_Kwartal;
begin
CheckEquals(EncodeDatetime(2015,09,30,23,59,59,999),EndOfTheQuarter(EncodeDatetime(2015,09,30,23,59,59,999)) , Format('%s.%s: %s', [ClassName, 'Test_Data_Koniec_Kwartalu_Dla_Ostatni_Dzien_Trzeci_Kwartal', '']));
end;

procedure TtestEndOfTheQuarter.Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Czwarty_Kwartal;
begin
CheckEquals(EncodeDatetime(2015,12,31,23,59,59,999),EndOfTheQuarter(EncodeDatetime(2015,10,01,23,59,59,999)) , Format('%s.%s: %s', [ClassName, 'Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Czwarty_Kwartal', '']));
end;

procedure TtestEndOfTheQuarter.Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Drugi_kwartal;
begin
CheckEquals(EncodeDatetime(2015,06,30,23,59,59,999),EndOfTheQuarter(EncodeDatetime(2015,04,01,23,59,59,999)) , Format('%s.%s: %s', [ClassName, 'Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Drugi_Kwartal', '']));
end;

procedure TtestEndOfTheQuarter.Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Pierwszy_kwartal;
begin
CheckEquals(EncodeDatetime(2015,03,31,23,59,59,999),EndOfTheQuarter(EncodeDatetime(2015,01,01,23,59,59,999)) , Format('%s.%s: %s', [ClassName, 'Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Pierwszy_Kwartal', '']));
end;

procedure TtestEndOfTheQuarter.Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Trzeci_kwartal;
begin
CheckEquals(EncodeDatetime(2015,09,30,23,59,59,999),EndOfTheQuarter(EncodeDatetime(2015,07,01,23,59,59,999)) , Format('%s.%s: %s', [ClassName, 'Test_Data_Koniec_Kwartalu_Dla_Pierwszy_Dzien_Trzeci_Kwartal', '']));
end;

procedure TtestEndOfTheQuarter.Test_Dowolna_Data_Z_Kwartalu;
begin
CheckEquals(EncodeDatetime(2015,12,31,23,59,59,999),EndOfTheQuarter(EncodeDatetime(2015,11,20,23,59,59,999)) , Format('%s.%s: %s', [ClassName, 'Test_Dowolna_Data_Z_Kwartalu', '']));
end;

initialization
RegisterTest ( TtestEndOfTheQuarter.suite);
end.

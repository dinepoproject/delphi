program Project1Tests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  GxDateUtils in '..\GxDateUtils.pas',
  DateTimeInPeriodTest in '..\Gotowe testy wys�ane do Adama\DateTimeInPeriodTest.pas',
  PeriodInPeriodTest in '..\Gotowe testy wys�ane do Adama\PeriodInPeriodTest.pas',
  EndOfAQuarterTest in '..\Gotowe testy wys�ane do Adama\EndOfAQuarterTest.pas',
  EndOfTheQuartertest in '..\Gotowe testy wys�ane do Adama\EndOfTheQuartertest.pas';

{$R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.


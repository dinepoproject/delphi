unit FormatDateTime24TestBFormat;



interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestFormatDateTime24TestBFormat = class(TTestCase)

    published

 procedure  Test_Czy_Funkcja_Prawidlowo_Zmienia_Godziny_Petla;
 procedure  Test_Pelne_Formatowanie_Data_Czas_W_Godzinach;
 procedure  Test_Reakcja_Funkcji_Na_Znaki_Separatorow_W_Parametrze_AFormatSettings;
    /// <summary>
    /// Funkcja zwraca �a�cuch z dat� u�o�on� wg formatu podanego w parametrze Format.
    /// Funkcja formatuje godziny w warto�ciach 1-24, chyba �e format wymusza zastosowanie czasu 12 godzinnego.
    /// </summary>
    ///  Testy do wykonania:
    /// 1. Test sprawdzaj�cy czy funkcja prawid�owo zmienia godziny - p�tla od zero do 23 - czy otrzymujemy godziny 1-24   }
    /// 2. Test sprawdzaj�cy pe�ne formatowanie daty i czasu w godzinach
    /// 3. Test sprawdzaj�cy czy funkcja reaguje na znaki separator�w podawane w parametrze AFormatSettings.
end;
implementation
uses
GxDateUtils;


{ TtestEndOfAQuarter }





{ TtestEndOfAQuarter }

procedure TtestFormatDateTime24TestBFormat.Test_Czy_Funkcja_Prawidlowo_Zmienia_Godziny_Petla;
 Var
I : word;
  dataOczekiwana: string;
  dataSprawdzana: TDateTime;

  wylicz : string;

begin
for I := 0 to 23 do
    //dataOczekiwana:=  EncodeDatetime(2000,12,20,I+1, 00, 00, 000);
    begin
    dataSprawdzana:= Encodedatetime(2000,12,20,I,30,00,0);
      //dataSprawdzana:= Encodetime(I,30,00,0);

   // wylicz :=  inttostr(2000)+','+inttostr(12)+','+inttostr (20) + ',' + IntToStr(I+1)+',' + IntToStr(00) + ',' +IntToStr(00) + ',' + IntToStr(000);
    dataOczekiwana :=  '2000'+','+ '12'+','+ '20' + ',' + Format( '%.2d',[I+1])+',' + '00' + ',' + '00' + ',' + '000';
    //wylicz :=  Format( '%.2d',[I+1])+',' + '00' + ',' + '00' + ',' + '000';
   //CheckEquals((intToStr((2000,12,20,1,00,00,000)),(FormatDateTime24('yyyy,dd,mm,hh,nn,ss,zzz',dataSprawdzana)), Format('%s.%s: %s', [ClassName, 'Test_Czy_Funkcja_Prawid�owo_Zmienia_Godziny_Petla', '']));
    CheckEquals(dataOczekiwana,(FormatDateTime24('yyyy,mm,dd,hh,nn,ss,zzz',dataSprawdzana)), Format('%s.%s: %d', [ClassName, 'Test_Czy_Funkcja_Prawid�owo_Zmienia_Godziny_Petla', i]));
     //CheckEquals(wylicz,(FormatDateTime24('hh,nn,ss,zzz',dataSprawdzana)), Format('%s.%s: %d', [ClassName, 'Test_Czy_Funkcja_Prawid�owo_Zmienia_Godziny_Petla', i]));
      end;
   end;


procedure TtestFormatDateTime24TestBFormat.Test_Pelne_Formatowanie_Data_Czas_W_Godzinach;
var
 dataSprawdzana: TDateTime;
 dataOczekiwana : string;
begin
 dataOczekiwana :=  '2000' + ',' +'12'+ ','+'20'+ ',' + '00'+',' + '24' + ',' + '00' + ',' + '000';
 dataSprawdzana:= Encodedatetime(2000,12,20,23,30,30,300);
 CheckEquals(dataoczekiwana,(FormatDateTime24('yyyy,mm,dd,nn,hh,ss,zzz',dataSprawdzana)) , Format('%s.%s: %s', [ClassName, 'Test_Pelne_Formatowanie_Data_Czas_W_Godzinach', '']));
  { W te�cie zmieniono formatowanie czasu(ze zmiennej dataSprawdzana)
  zamieniono miejscami godzin� i minuty, dodatkowo sprawdzono czy nastepuje wyzerowanie minut,sekund i milisekund. }
 end;

procedure TtestFormatDateTime24TestBFormat.Test_Reakcja_Funkcji_Na_Znaki_Separatorow_W_Parametrze_AFormatSettings;
var
fs : TFormatSettings;
dataOczekiwana : string;
dataSprawdzana: TDateTime;
begin
fs.timeSeparator := '-';
dataSprawdzana := Encodedatetime(2000,12,20,23, 23, 24, 100);
 dataOczekiwana:='2000' + '-'+'12' + '-' + '20'+ '-'+'24'+'-' + '00' + '-' + '00' + '-' + '000';
  CheckEquals(dataOczekiwana,(FormatDateTime24('yyyy:mm:dd:hh:nn:ss:zzz', dataSprawdzana, fs)) , Format('%s.%s: %s', [ClassName, 'Test_Reakcja_Funkcji_Na_Znaki_Separator�w_W_Parametrze_AFormatS', '']));
end;

initialization
RegisterTest ( TtestFormatDateTime24TestBFormat.suite);
end.

unit GxEncodeDateSmartTest;



interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestGxEncodeDateSmartTest = class(TTestCase)

    published

 procedure  Test_Czy_Wyjatek_Gdy_Rok_Mniejszy_Niz_1;
 procedure  Test_Czy_Wyjatek_Gdy_Rok_Wiekszy_Niz_9999;
 procedure  Test_Czy_Wyjatek_Gdy_Miesiac_Mniejszy_Niz_1;
 procedure  Test_Czy_Wyjatek_Gdy_Miesiac_Wiekszy_Niz_12;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Mniejszy_Niz_1;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_31_Dla_Miesiac_31;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_30_Dla_Miesiac_30;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_28_Luty_Dla_Rok_Nie_Przestepny;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_29_Dla_Luty_Rok_Przestepny;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_29_Luty_Dla_Rok_Przestepny;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_28_Luty_Dla_Rok_Nie_Przestepny;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_31_Dla_Miesiac_31;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_30_Dla_Miesiac_30;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_1_Stycznia_Dla_Dowolny_Rok;

    /// <summary>
    /// Funkcja tworz�ca warto�� typu TDateTime na podstawie sk�adowych daty
    /// W przypadku b��du jest zg�aszany wyj�tek GxConvertError, a komunikat dok�adnie wskazuje,
    /// kt�ra ze sk�adowych zosta�a podana w nieprawid�owy spos�b.
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy rok b�dzie mniejszy ni� 1
    ///  2. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy rok b�dzie wi�kszy ni� 9999
    ///  3. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy miesi�c b�dzie mniejszy ni� 1
    ///  4. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy miesi�c b�dzie wi�kszy ni� 12
    ///  5. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie mniejszy ni� 1
    ///  6. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie wi�kszy ni� 31 dla miesi�cy, kt�re maj� 31 dni
    ///  7. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie wi�kszy ni� 30 dla miesi�cy, kt�re maj� 30 dni
    ///  8. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie wi�kszy ni� 28 dla lutego w roku nie przest�pnym
    ///  9. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie wi�kszy ni� 29 dla lutego w roku przest�pnym
    ///  10. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie wi�kszy r�wny 29 dla lutego w roku przest�pnym
    ///  11. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie wi�kszy r�wny 28 dla lutego w roku nieprzest�pnym
    ///  12. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie wi�kszy r�wny 31 dla miesi�cy, kt�re maj� 31 dni
    ///  13. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie wi�kszy r�wny 30 dla miesi�cy, kt�re maj� 30 dni
    ///  14. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie wi�kszy r�wny 1 stycznia dowolnego wybranego roku.
end;
implementation
uses
GxDateUtils;

    { SDataBladRok = 'Warto�� roku musi by� podana z zakresu: 1-9999.';
  SDataBladMiesiac = 'Warto�� miesi�ca musi by� podana z zakresu: 1-12.';
  SDataBladDzien = 'Warto�� dnia dla roku: %d i miesi�ca: %d musi by� podana z zakresu: 1-%d.';
    // Komunikaty konwersji daty i czasu
  SCzasBladGodzina = 'Warto�� godziny musi by� podana z zakresu: 0-23.';
  SCzasBladGodzina24 = 'Warto�� godziny musi by� podana z zakresu: 1-24.';
  SCzasBladMinuta = 'Warto�� minuty musi by� podana z zakresu: 0-59.';
  SCzasBladSekunda = 'Warto�� sekundy musi by� podana z zakresu: 0-59.';
  SCzasBladMiliSekunda = 'Warto�� milisekundy musi by� podana z zakresu: 0-999.'; }
 { TtestEndOfAQuarter }

  procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Mniejszy_Niz_1;
begin
   ExpectedException := GxConvertError;
   try
     GxEncodeDateSmart(2000,01,00);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2000, 01, 31]), E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Mniejszy_Niz_1', '']));
     raise;
     end;
   end;


end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_28_Luty_Dla_Rok_Nie_Przestepny;
begin
  CheckEquals(EncodeDate(2001,02,28), GxEncodeDateSmart(2001,02,28), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_28_Luty_Dla_Rok_Nie_Przestepny', '']));
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_29_Luty_Dla_Rok_Przestepny;
begin
  CheckEquals(EncodeDate(2000,02,29), GxEncodeDateSmart(2000,02,29), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_29_Luty_Dla_Rok_Przestepny', '']));
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_28_Luty_Dla_Rok_Nie_Przestepny;
begin
    ExpectedException := GxConvertError;

   try
     GxEncodeDateSmart(2001,02,29);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2001, 02, 28]),E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_28_Luty_Dla_Rok_Nie_Prze', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_29_Dla_Luty_Rok_Przestepny;
begin
 ExpectedException := GxConvertError;


 try
     GxEncodeDateSmart(2000,02,30);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2000, 02, 29]),E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_29_Dla_Luty_Rok_Przestep', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_30_Dla_Miesiac_30;
begin
  ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(2000,04,31);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2000, 04, 30]),E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_30_Dla_Miesiac_30', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_31_Dla_Miesiac_31;
begin
  ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(2000,01,32);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2000, 01,31]),E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_31_Dla_Miesiac_31', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_1_Stycznia_Dla_Dowolny_Rok;
{begin
 ExpectedException := GxConvertError;

  GxEncodeDateSmart(2015,01,01);
end; }

begin
  CheckEquals(EncodeDate(2015,01,01), GxEncodeDateSmart(2015,01,01), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_1_Stycznia_Dla_Dowolny_Rok', '']));
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_30_Dla_Miesiac_30;
{begin
  ExpectedException := GxConvertError;

  GxEncodeDateSmart(2000,04,30);
end;    }
  begin
  CheckEquals(EncodeDate(2000,04,30), GxEncodeDateSmart(2000,04,30), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_30_Dla_Miesiac_30', '']));
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_31_Dla_Miesiac_31;

 //ExpectedException := GxConvertError;

 // GxEncodeDateSmart(2000,01,31);
 begin
  CheckEquals(EncodeDate(2000,01,31), GxEncodeDateSmart(2000,01,31), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_31_Dla_Miesiac_31', '']));
end;


procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Miesiac_Mniejszy_Niz_1;
begin
   ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(2000,00,01);
   except
     on E: Exception do
     begin
     CheckEquals(SDataBladMiesiac,E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Miesiac_Mniejszy_Niz_1', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Miesiac_Wiekszy_Niz_12;
begin
  ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(2000,13,01);
   except
     on E: Exception do
     begin
     CheckEquals(SDataBladMiesiac,E.Message,Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Miesiac_Wiekszy_Niz_12', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Rok_Mniejszy_Niz_1;
begin
  ExpectedException := GxConvertError;

  try
     GxEncodeDateSmart(0000,01,01);
   except
     on E: Exception do
     begin
     CheckEquals(SDataBladRok,E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Rok_Mniejszy_Niz_1', '']));
     raise;
     end;
   end;

end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Rok_Wiekszy_Niz_9999;
begin
  ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(10000,01,01);
   except
     on E: Exception do
     begin
     CheckEquals(SDataBladRok,E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Rok_Wiekszy_Niz_9999', '']));
     raise;
     end;
   end;
end;

initialization
RegisterTest ( TtestGxEncodeDateSmartTest.suite);
end.

unit GxEncodeTime24SmartTest;



interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestGxEncodeTime24SmartTest = class(TTestCase)

    published
 procedure  Test_Czy_Wyjatek_Gdy_Godzina_Wieksza_Niz_24;
 procedure  Test_Czy_Wyjatek_Gdy_Godzina_Mniejsza_Niz_1;
 procedure  Test_Czy_Wyjatek_Gdy_Minuta_Wieksza_Niz_59;
 procedure  Test_Czy_Wyjatek_Gdy_Sekunda_Wieksza_Niz_59;
 procedure  Test_Czy_Wyjatek_Gdy_Milisekunda_Wieksza_Niz_999;
 procedure  Test_Wartosc_Dla_Poprawnych_Wartosci;

      // Funkcja zwraca warto�� czasu w formacie TDateTime. W parametrze AHour godziny s� podawane
    // z zakresu 1-24
    /// Testy do wykonania:
    ///  1. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy godzina b�dzie wi�ksza ni� 24
    ///  2. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy godzina b�dzie mniejsza ni� 1
    ///  3. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy minuta b�dzie wi�ksza ni� 59
    ///  4. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy sekunda b�dzie wi�ksza ni� 59
    ///  5. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy milisekunda b�dzie wi�ksza ni� 999
    ///  6. Test sprawdzaj�cy warto�� zwr�con� przez funkcj� dla poprawnych warto�ci.

end;
implementation
uses
GxDateUtils;


   { TtestEndOfAQuarter }

{ TtestGxEncodeTime24SmartTest }

procedure TtestGxEncodeTime24SmartTest.Test_Czy_Wyjatek_Gdy_Godzina_Mniejsza_Niz_1;
begin
   ExpectedException := GxConvertError;
   try
     GxEncodeTime24Smart(00,01,01,999);
   except
     on E: Exception do
     begin
     CheckEquals(SCzasBladGodzina24, E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyj�tek_Gdy_Godzina_Mniejsza_Niz_1', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeTime24SmartTest.Test_Czy_Wyjatek_Gdy_Godzina_Wieksza_Niz_24;
begin
   ExpectedException := GxConvertError;
   try
     GxEncodeTime24Smart(25,01,01,999);
   except
     on E: Exception do
     begin
     CheckEquals(SCzasBladGodzina24,E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyj�tek_Gdy_Godzina_Wieksza_Niz_24', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeTime24SmartTest.Test_Czy_Wyjatek_Gdy_Minuta_Wieksza_Niz_59;
begin
   ExpectedException := GxConvertError;
   try
     GxEncodeTime24Smart(24,60,01,999);
   except
     on E: Exception do
     begin
     CheckEquals(SCzasBladMinuta,E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyj�tek_Gdy_Minuta_Wieksza_Niz_59', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeTime24SmartTest.Test_Czy_Wyjatek_Gdy_Milisekunda_Wieksza_Niz_999;
begin
   ExpectedException := GxConvertError;
   try
     GxEncodeTime24Smart(24,01,59,1000);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SCzasBladMiliSekunda, [24,01,01,999]),E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Milisekunda_Wieksza_Niz_999', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeTime24SmartTest.Test_Czy_Wyjatek_Gdy_Sekunda_Wieksza_Niz_59;
begin
   ExpectedException := GxConvertError;
   try
     GxEncodeTime24Smart(24,01,60,999);
   except
     on E: Exception do
     begin
     CheckEquals(SCzasBladSekunda,E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyj�tek_Gdy_Sekunda_Wieksza_Niz_59', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeTime24SmartTest.Test_Wartosc_Dla_Poprawnych_Wartosci;
begin
CheckEquals((Encodetime(23,59,59,999)),(GxEncodeTimeSmart(23,59,59,999)), Format('%s.%s: %s', [ClassName, 'Test_Wartosc_Dla_Poprawnych_Wartosci', '']));
 end;

initialization
RegisterTest ( TtestGxEncodeTime24SmartTest.suite);
end.

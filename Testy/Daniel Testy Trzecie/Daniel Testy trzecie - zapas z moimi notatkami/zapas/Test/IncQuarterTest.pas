unit IncQuarterTest;



interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestIncQuarterTest = class(TTestCase)

    published

 procedure  Test_Otrzymana_Wartosc_Gdy_Parametr_ANumberOfQuarters_Zero;  // ile ma by�
 procedure  Test_Wynik_Data_Przesuniecie_Jeden_Kwartal;
 procedure  Test_Wynik_Data_Przesuniecie_Minus_Jeden_Kwartal;
 procedure  Test_Wynik_Data_Przesuniecie_Cztery_Kwartaly;
 procedure  Test_Wynik_Data_Przesuniecie_Minus_Cztery_Kwartaly;
    // Funkcja dodaje do daty ilo�� kwarta��w podan� jak� parametr
    // Testy do wykonania:
    // 1. Test sprawdzaj�cy warto�� otrzyman� gdy parametr ANumberOfQuarters ma warto�� zero.
    // 2. Test sprawdzaj�cy wynik daty przesuni�tej o jeden kwarta�.
    // 3. Test sprawdzaj�cy wynik daty przesuni�tej o minus jeden kwarta�.
    // 4. Test sprawdzaj�cy wynik daty przesuni�tej o cztery rok.
    // 5. Test sprawdzaj�cy wynik daty przesuni�tej o minus cztery kwarta�y.

end;
implementation
uses
GxDateUtils;

   { TtestEndOfAQuarter }

procedure TtestIncQuarterTest.Test_Otrzymana_Wartosc_Gdy_Parametr_ANumberOfQuarters_zero;
begin
   CheckEquals((EncodeDate(2000,01,01)),(IncQuarter(EncodeDate(2000,01,01),0)) , Format('%s.%s: %s', [ClassName, 'Test_Otrzymana_Wartosc_Gdy_Parametr_ANumberOfQuarters_zero', '']));
end;


procedure TtestIncQuarterTest.Test_Wynik_Data_Przesuniecie_Cztery_Kwartaly;
begin
   CheckEquals((EncodeDate(2001,01,01)),(IncQuarter(EncodeDate(2000,01,01),4)) , Format('%s.%s: %s', [ClassName, 'Test_Wynik_Data_Przesuniecie_Cztery_Kwartaly', '']));
end;

procedure TtestIncQuarterTest.Test_Wynik_Data_Przesuniecie_Jeden_Kwartal;
begin
   CheckEquals((EncodeDate(2000,04,01)),(IncQuarter(EncodeDate(2000,01,01),1)) , Format('%s.%s: %s', [ClassName, 'Test_Wynik_Data_Przesuniecie_Jeden_Kwartal', '']));
end;

procedure TtestIncQuarterTest.Test_Wynik_Data_Przesuniecie_Minus_Cztery_Kwartaly;
begin
   CheckEquals((EncodeDate(1999,01,01)),(IncQuarter(EncodeDate(2000,01,01),-4)) , Format('%s.%s: %s', [ClassName, 'Test_Wynik_Data_Przesuniecie_Minus_Cztery_Kwartaly', '']));
end;

procedure TtestIncQuarterTest.Test_Wynik_Data_Przesuniecie_Minus_Jeden_Kwartal;
begin
   CheckEquals((EncodeDate(1999,10,01)),(IncQuarter(EncodeDate(2000,01,01),-1)) , Format('%s.%s: %s', [ClassName, 'Test_Wynik_Data_Przesuniecie_Minus_Jeden_Kwartal', '']));
end;

initialization
RegisterTest ( TtestIncQuarterTest.suite);
end.

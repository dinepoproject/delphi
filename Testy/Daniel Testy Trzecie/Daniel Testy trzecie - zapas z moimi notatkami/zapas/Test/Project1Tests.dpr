program Project1Tests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  GxDateUtils in '..\GxDateUtils.pas',
  IncQuarterTest in 'IncQuarterTest.pas',
  FormatDateTime24TestBFormat in 'FormatDateTime24TestBFormat.pas',
  FormatDateTime24TestAFormat in 'FormatDateTime24TestAFormat.pas',
  GxEncodeDateSmartTest in 'GxEncodeDateSmartTest.pas',
  GxEncodeTimeSmartTest in 'GxEncodeTimeSmartTest.pas',
  GxEncodeTime24SmartTest in 'GxEncodeTime24SmartTest.pas';

{$R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.


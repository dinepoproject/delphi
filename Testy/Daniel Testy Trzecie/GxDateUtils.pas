unit GxDateUtils;

interface

uses
  System.SysUtils;

type
    // Specjalna klasa b��d�w umo�liwiaj�ca prawid�owe zg�aszanie komunikat�w o niepoprawnych
    // warto�ciach kierowanych do konwersji - zapewnia prawid�owe dzia�anie podczas rozszerzonej
    // obs�ugi b��d�w.
  GxConvertError = class(Exception);

    // Funkcja dodaje do daty ilo�� kwarta��w podan� jak� parametr
    // Testy do wykonania:
    // 1. Test sprawdzaj�cy warto�� otrzyman� gdy parametr ANumberOfQuarters ma warto�� zero.
    // 2. Test sprawdzaj�cy wynik daty przesuni�tej o jeden kwarta�.
    // 3. Test sprawdzaj�cy wynik daty przesuni�tej o minus jeden kwarta�.
    // 4. Test sprawdzaj�cy wynik daty przesuni�tej o cztery rok.
    // 5. Test sprawdzaj�cy wynik daty przesuni�tej o minus cztery kwarta�y.
  function IncQuarter(const ADate: TDateTime; const ANumberOfQuarters: Integer = 1): TDateTime;

    /// <summary>
    /// Funkcja zwraca �a�cuch z dat� u�o�on� wg formatu podanego w parametrze Format.
    /// Funkcja formatuje godziny w warto�ciach 1-24, chyba �e format wymusza zastosowanie czasu 12 godzinnego.
    /// </summary>
    ///  Testy do wykonania:
    /// 1. Test sprawdzaj�cy czy funkcja prawid�owo zmienia godziny - p�tla od zero do 23 - czy otrzymujemy godziny 1-24
    /// 2. Test sprawdzaj�cy pe�ne formatowanie daty i czasu w godzinach
  function FormatDateTime24(const Format: string; DateTime: TDateTime): string; overload;

    /// <summary>
    /// Funkcja zwraca �a�cuch z dat� u�o�on� wg formatu podanego w parametrze Format.
    /// Funkcja formatuje godziny w warto�ciach 1-24, chyba �e format wymusza zastosowanie czasu 12 godzinnego.
    /// </summary>
    ///  Testy do wykonania:
    /// 1. Test sprawdzaj�cy czy funkcja prawid�owo zmienia godziny - p�tla od zero do 23 - czy otrzymujemy godziny 1-24
    /// 2. Test sprawdzaj�cy pe�ne formatowanie daty i czasu w godzinach
    /// 3. Test sprawdzaj�cy czy funkcja reaguje na znaki separator�w podawane w parametrze AFormatSettings.
  function FormatDateTime24(const Format: string; DateTime: TDateTime;
    const AFormatSettings: TFormatSettings): String; overload;

    /// <summary>
    /// Funkcja tworz�ca warto�� typu TDateTime na podstawie sk�adowych daty
    /// W przypadku b��du jest zg�aszany wyj�tek GxConvertError, a komunikat dok�adnie wskazuje,
    /// kt�ra ze sk�adowych zosta�a podana w nieprawid�owy spos�b.
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy rok b�dzie mniejszy ni� 1
    ///  2. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy rok b�dzie wi�kszy ni� 9999
    ///  3. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy miesi�c b�dzie mniejszy ni� 1
    ///  4. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy miesi�c b�dzie wi�kszy ni� 12
    ///  5. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie mniejszy ni� 1
    ///  6. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie wi�kszy ni� 31 dla miesi�cy, kt�re maj� 31 dni
    ///  7. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie wi�kszy ni� 30 dla miesi�cy, kt�re maj� 30 dni
    ///  8. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie wi�kszy ni� 28 dla lutego w roku nie przest�pnym
    ///  9. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy dzie� b�dzie wi�kszy ni� 29 dla lutego w roku przest�pnym
    ///  10. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie r�wny 29 dla lutego w roku przest�pnym
    ///  11. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie r�wny 28 dla lutego w roku nieprzest�pnym
    ///  12. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie r�wny 31 dla miesi�cy, kt�re maj� 31 dni
    ///  13. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie r�wny 30 dla miesi�cy, kt�re maj� 30 dni
    ///  14. Test sprawdzaj�cy zwr�con� dat�, gdy dzie� b�dzie r�wny 1 stycznia dowolnego wybranego roku.
  function GxEncodeDateSmart(Year, Month, Day: Word): TDateTime;
     /// <summary>
    /// Funkcja tworz�ca warto�� typu TDateTime na podstawie sk�adowych czasu
    /// W przypadku b��du jest zg�aszany wyj�tek GxConvertError, a komunikat
    /// dok�adnie wskazuje, kt�ra ze sk�adowych zosta�a podana w nieprawid�owy
    /// spos�b.
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy godzina b�dzie wi�ksza ni� 23
    ///  2. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy minuta b�dzie wi�ksza ni� 59
    ///  3. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy sekunda b�dzie wi�ksza ni� 59
    ///  4. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy milisekunda b�dzie wi�ksza ni� 999
    ///  5. Test sprawdzaj�cy warto�� zwr�con� przez funkcj� dla poprawnych warto�ci.
  function GxEncodeTimeSmart(const Hour, Min, Sec, MSec: Word): TDateTime;
     /// <summary>
    /// Funkcja zwraca warto�� czasu w formacie TDateTime. W parametrze AHour godziny s� podawane
    /// z zakresu 1-24
     /// </summary>
    /// Testy do wykonania:
    ///  1. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy godzina b�dzie wi�ksza ni� 24
    ///  2. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy godzina b�dzie mniejsza ni� 1
    ///  3. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy minuta b�dzie wi�ksza ni� 59
    ///  4. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy sekunda b�dzie wi�ksza ni� 59
    ///  5. Test sprawdzaj�cy czy wyst�pi wyj�tek, gdy milisekunda b�dzie wi�ksza ni� 999
    ///  6. Test sprawdzaj�cy warto�� zwr�con� przez funkcj� dla poprawnych warto�ci.
  function GxEncodeTime24Smart(const AHour: Word; const AMin: Word = 0; const ASec: Word = 0;
    const AMSec: Word = 0): TDateTime;

resourcestring
  SDataBladRok = 'Warto�� roku musi by� podana z zakresu: 1-9999.';
  SDataBladMiesiac = 'Warto�� miesi�ca musi by� podana z zakresu: 1-12.';
  SDataBladDzien = 'Warto�� dnia dla roku: %d i miesi�ca: %d musi by� podana z zakresu: 1-%d.';
    // Komunikaty konwersji daty i czasu
  SCzasBladGodzina = 'Warto�� godziny musi by� podana z zakresu: 0-23.';
  SCzasBladGodzina24 = 'Warto�� godziny musi by� podana z zakresu: 1-24.';
  SCzasBladMinuta = 'Warto�� minuty musi by� podana z zakresu: 0-59.';
  SCzasBladSekunda = 'Warto�� sekundy musi by� podana z zakresu: 0-59.';
  SCzasBladMiliSekunda = 'Warto�� milisekundy musi by� podana z zakresu: 0-999.';

implementation

uses
  System.DateUtils, Winapi.Windows;

const
  cMonthsPerQuarter = 3;


  // W procedurze jest wykonywana konwersja daty na �a�cuch z przeniesieniem warto�ci godziny
  // o jedn� jednostk� do przodu. Operacja ta jest wykonywana tylko wtedy, gdy godzina
  // ma by� podana w formacie 24 godzinnym. Warto�ci minut i sekund s� w tym przypadku
  // ustawiane jako zera.
procedure DateTimeToString24(var Result: string; const Format: string; DateTime: TDateTime;
  const AFormatSettings: TFormatSettings); forward;


function IncQuarter(const ADate: TDateTime; const ANumberOfQuarters: Integer = 1): TDateTime;
begin
  Result := IncMonth(ADate, ANumberOfQuarters * cMonthsPerQuarter);
end;

function FormatDateTime24(const Format: string; DateTime: TDateTime): string;
begin
  DateTimeToString24(Result, Format, DateTime, FormatSettings);
end;

function FormatDateTime24(const Format: string; DateTime: TDateTime; const AFormatSettings: TFormatSettings): String;
begin
  DateTimeToString24(Result, Format, DateTime, AFormatSettings);
end;

function GxEncodeDateSmart(Year, Month, Day: Word): TDateTime;
var
  maxDay: Integer;
begin
  if not TryEncodeDate(Year, Month, Day, Result) then
  begin
    if (Year < 1) or (Year > 9999) then
      raise GxConvertError.Create(SDataBladRok);
    if (Month < 1) or (Month > 12) then
      raise GxConvertError.Create(SDataBladMiesiac);
    maxDay := MonthDays[IsLeapYear(Year), Month];
    if (Day < 1) or (Day > maxDay) then
      raise GxConvertError.CreateFmt(SDataBladDzien, [Year, Month, maxDay]);
  end;
end;

procedure DateTimeToString24(var Result: string; const Format: string;
  DateTime: TDateTime; const AFormatSettings: TFormatSettings);
var
  BufPos, AppendLevel: Integer;
  Buffer: array[0..255] of Char;
  DynBuffer: array of Char;
  Sb: TCharArray;

  procedure AppendChars(P: PChar; Count: Integer);
  var
    N, I: Integer;
    begin
    N := SizeOf(Buffer) div SizeOf(Char);
    N := N - BufPos;
    if Count > N then
      begin
      I := Length(DynBuffer);
      SetLength(DynBuffer, I + BufPos + Count);
      if BufPos > 0 then
      begin
        Move(Buffer[0], DynBuffer[I], BufPos * SizeOf(Char));
        Inc(I, BufPos);
      end;
      Move(P[0], DynBuffer[I], Count * SizeOf(Char));
      BufPos := 0;
    end
    else if Count > 0 then
    begin
      Move(P[0], Buffer[BufPos], Count * SizeOf(Char));
      Inc(BufPos, Count);
    end;
  end;

  procedure AppendString(const S: string);
  begin
    AppendChars(Pointer(S), Length(S));
  end;

  procedure AppendNumber(Number, Digits: Integer);
  const
    Format: array[0..3] of Char = '%.*d';
  var
    NumBuf: array[0..15] of Char;
  begin
    AppendChars(NumBuf, FormatBuf(NumBuf, Length(NumBuf), Format,
      Length(Format), [Digits, Number]));
  end;

  procedure AppendFormat(Format: PChar);
  var
    Starter, Token, LastToken: Char;
    DateDecoded, TimeDecoded, Use12HourClock,
    BetweenQuotes: Boolean;
    P: PChar;
    Count: Integer;
    Year, Month, Day, Hour, Min, Sec, MSec, H: Word;

    procedure GetCount;
    var
      P: PChar;
    begin
      P := Format;
      while Format^ = Starter do Inc(Format);
      Count := Format - P + 1;
    end;

    procedure GetDate;
    begin
      if not DateDecoded then
      begin
        DecodeDate(DateTime, Year, Month, Day);
        DateDecoded := True;
      end;
    end;

      // W procedurze jest wykonywane sprawdzenie, czy czas ma by� formatowany na 24 czy 12 godzin
    procedure CheckTimeFormatUse12Hour;
    var
      p: PChar;
      betweenQuotes: Boolean;
    begin
      Use12HourClock := False;
      betweenQuotes := False;
      p := Format;
      while p^ <> #0 do
      begin
        if AnsiChar(p^) in LeadBytes then
        begin
          p := StrNextChar(p);
          Continue;
        end;
        case p^ of
          'A', 'a':
            if not betweenQuotes then
            begin
              if ((StrLIComp(p, 'AM/PM', 5) = 0)
                or (StrLIComp(p, 'A/P',   3) = 0)
                or (StrLIComp(p, 'AMPM',  4) = 0) ) then
                Use12HourClock := True;
              Break;
            end;
          'H', 'h':
            Break;
          '''', '"': betweenQuotes := not betweenQuotes;
        end;
        Inc(p);
      end;
    end;

    procedure GetTime;
    begin
      if not TimeDecoded then
      begin
        CheckTimeFormatUse12Hour;
        DecodeTime(DateTime, Hour, Min, Sec, MSec);
          // Je�li nie jest u�ywany format 12 godzinny...
        if not Use12HourClock then
        begin
            // Zwi�kszamy warto�� godziny o jeden i zerujemy pozosta�e sk�adowe czasu
          Inc(Hour);
          Min := 0;
          Sec := 0;
          MSec := 0;
        end;
        TimeDecoded := True;
      end;
    end;

{$IFDEF MSWINDOWS}
    function ConvertEraString(const Count: Integer) : string;
    var
      FormatStr: string;
      SystemTime: TSystemTime;
      Buffer: array[Byte] of Char;
      P: PChar;
    begin
      Result := '';
      SystemTime.wYear  := Year;
      SystemTime.wMonth := Month;
      SystemTime.wDay   := Day;

      FormatStr := 'gg';
      if GetDateFormat(GetThreadLocale, DATE_USE_ALT_CALENDAR, @SystemTime,
        PChar(FormatStr), Buffer, SizeOf(Buffer)) <> 0 then
      begin
        Result := Buffer;
        if Count = 1 then
        begin
          case SysLocale.PriLangID of
            LANG_JAPANESE:
              Result := Copy(Result, 1, CharToElementLen(Result, 1));
            LANG_CHINESE:
              if (SysLocale.SubLangID = SUBLANG_CHINESE_TRADITIONAL)
                and (ElementToCharLen(Result, Length(Result)) = 4) then
              begin
                P := Buffer + CharToElementIndex(Result, 3) - 1;
                SetString(Result, P, CharToElementLen(P, 2));
              end;
          end;
        end;
      end;
    end;

    function ConvertYearString(const Count: Integer): string;
    var
      FormatStr: string;
      SystemTime: TSystemTime;
      Buffer: array[Byte] of Char;
    begin
      Result := '';
      SystemTime.wYear  := Year;
      SystemTime.wMonth := Month;
      SystemTime.wDay   := Day;

      if Count <= 2 then
        FormatStr := 'yy' // avoid Win95 bug.
      else
        FormatStr := 'yyyy';

      if GetDateFormat(GetThreadLocale, DATE_USE_ALT_CALENDAR, @SystemTime,
        PChar(FormatStr), Buffer, SizeOf(Buffer)) <> 0 then
      begin
        Result := Buffer;
        if (Count = 1) and (Result[Low(string)] = '0') then
          Result := Copy(Result, 2, Length(Result)-1);
      end;
    end;
{$ENDIF MSWINDOWS}

{$IFDEF POSIX}
    {$IFNDEF MACOS}
    function FindEra(Date: Integer): Byte;
    var
      I : Byte;
    begin
      Result := 0;
      for I := 1 to EraCount do
      begin
        if (EraRanges[I].StartDate <= Date) and
           (EraRanges[I].EndDate >= Date) then
          Exit(I);
      end;
    end;
    {$ENDIF !MACOS}

    function ConvertEraString(const Count: Integer) : String;
    var
      {$IFDEF MACOS}
      Formatter: CFDateFormatterRef;
      LDate: CFGregorianDate;
      LYear, LMonth, LDay: Word;
      FormatString: TCFString;
      DefaultTZ: CFTimeZoneRef;
      Locale: CFLocaleRef;
      {$ELSE !MACOS}
      I : Byte;
      {$ENDIF MACOS}
    begin
      Result := '';
      {$IFDEF MACOS}
      Locale := nil;
      DefaultTZ := nil;
      Formatter := nil;
      FormatString.Value := nil;

      try
        Locale := CFLocaleCopyCurrent;
        DefaultTZ := CFTimeZoneCopyDefault;
        Formatter := CFDateFormatterCreate(kCFAllocatorDefault, Locale,
                        kCFDateFormatterFullStyle, kCFDateFormatterNoStyle);
        FormatString := TCFString.Create('GG');
        CFDateFormatterSetFormat(Formatter, FormatString.Value);
        DecodeDate(DateTime, LYear, LMonth, LDay);
        LDate.year := LYear; LDate.month := ShortInt(LMonth); LDate.day := ShortInt(LDay);
        LDate.hour := 0; LDate.minute := 0; LDate.second := 0;
        Result := TCFString(CFDateFormatterCreateStringWithAbsoluteTime(
                              kCFAllocatorDefault, Formatter,
                              CFGregorianDateGetAbsoluteTime(LDate, DefaultTZ))
                           ).AsString(true);
      finally
        if FormatString.Value <> nil then
          CFRelease(FormatString.Value);
        if Formatter <> nil then
          CFRelease(Formatter);
        if DefaultTZ <> nil then
          CFRelease(DefaultTZ);
        if Locale <> nil then
          CFRelease(Locale);
      end;
      {$ELSE !MACOS}
      I := FindEra(Trunc(DateTime));
      if I > 0 then
        Result := EraNames[I];
      {$ENDIF MACOS}
    end;

    function ConvertYearString(const Count: Integer) : String;
    var
      S : string;
      function GetEraOffset: integer;
      {$IFDEF MACOS}
      var
        StartEra, TargetDate, LengthEra: CFAbsoluteTime;
        LDate: CFGregorianDate;
        LYear, LMonth, LDay: Word;
        Calendar, CurrentCalendar: CFCalendarRef;
        TimeZone: CFTimeZoneRef;
      {$ENDIF MACOS}
      begin
        {$IFDEF MACOS}
        Result := 0;
        TimeZone := nil;
        CurrentCalendar := nil;
        Calendar := nil;
        try
          DecodeDate(DateTime, LYear, LMonth, LDay);
          LDate.year := LYear; LDate.month := ShortInt(LMonth); LDate.day := ShortInt(LDay);
          LDate.hour := 0; LDate.minute := 0; LDate.second := 0;
          TimeZone := CFTimeZoneCopyDefault;
          TargetDate := CFGregorianDateGetAbsoluteTime(LDate, TimeZone);
          CurrentCalendar := CFCalendarCopyCurrent;
          Calendar := CFCalendarCreateWithIdentifier(kCFAllocatorDefault,
                                  CFCalendarGetIdentifier(CurrentCalendar));
          if CFCalendarGetTimeRangeOfUnit(Calendar, kCFCalendarUnitEra,
                                          TargetDate, @StartEra, @LengthEra) then
          begin
            LDate := CFAbsoluteTimeGetGregorianDate(StartEra, TimeZone);
            Result := LDate.Year - 1;
          end;
        finally
          if CurrentCalendar <> nil then
            CFRelease(CurrentCalendar);
          if Calendar <> nil then
            CFRelease(Calendar);
          if TimeZone <> nil then
            CFRelease(TimeZone);
        end;
        {$ELSE !MACOS}
        Result := FindEra(Trunc(DateTime));
        if Result > 0 then
          Result := EraYearOffsets[Result];
        {$ENDIF MACOS}
      end;
    begin
      S := IntToStr(Year - GetEraOffset);
      while Length(S) < Count do
        S := '0' + S;
      if Length(S) > Count then
        S := Copy(S, Length(S) - (Count - 1), Count);
      Result := S;
    end;
{$ENDIF POSIX}

  begin
    if (Format <> nil) and (AppendLevel < 2) then
    begin
      Inc(AppendLevel);
      LastToken := ' ';
      DateDecoded := False;
      TimeDecoded := False;
      Use12HourClock := False;
      while Format^ <> #0 do
      begin
        Starter := Format^;
        if IsLeadChar(Starter) then
        begin
          AppendChars(Format, StrCharLength(Format) div SizeOf(Char));
          Format := StrNextChar(Format);
          LastToken := ' ';
          Continue;
        end;
        Format := StrNextChar(Format);
        Token := Starter;
        if CharInSet(Token, ['a'..'z']) then Dec(Token, 32);
        if CharInSet(Token, ['A'..'Z']) then
        begin
          if (Token = 'M') and (LastToken = 'H') then Token := 'N';
          LastToken := Token;
        end;
        case Token of
          'Y':
            begin
              GetCount;
              GetDate;
              if Count <= 2 then
                AppendNumber(Year mod 100, 2) else
                AppendNumber(Year, 4);
            end;
          'G':
            begin
              GetCount;
              GetDate;
              AppendString(ConvertEraString(Count));
            end;
          'E':
            begin
              GetCount;
              GetDate;
              AppendString(ConvertYearString(Count));
            end;
          'M':
            begin
              GetCount;
              GetDate;
              case Count of
                1, 2: AppendNumber(Month, Count);
                3: AppendString(AFormatSettings.ShortMonthNames[Month]);
              else
                AppendString(AFormatSettings.LongMonthNames[Month]);
              end;
            end;
          'D':
            begin
              GetCount;
              case Count of
                1, 2:
                  begin
                    GetDate;
                    AppendNumber(Day, Count);
                  end;
                3: AppendString(AFormatSettings.ShortDayNames[DayOfWeek(DateTime)]);
                4: AppendString(AFormatSettings.LongDayNames[DayOfWeek(DateTime)]);
                5: AppendFormat(Pointer(AFormatSettings.ShortDateFormat));
              else
                AppendFormat(Pointer(AFormatSettings.LongDateFormat));
              end;
            end;
          'H':
            begin
              GetCount;
              GetTime;
              BetweenQuotes := False;
              P := Format;
              while P^ <> #0 do
              begin
                if IsLeadChar(P^) then
                begin
                  P := StrNextChar(P);
                  Continue;
                end;
                case P^ of
                  'A', 'a':
                    if not BetweenQuotes then
                    begin
                      if ( (StrLIComp(P, 'AM/PM', 5) = 0)
                        or (StrLIComp(P, 'A/P',   3) = 0)
                        or (StrLIComp(P, 'AMPM',  4) = 0) ) then
                        Use12HourClock := True;
                      Break;
                    end;
                  'H', 'h':
                    Break;
                  '''', '"': BetweenQuotes := not BetweenQuotes;
                end;
                Inc(P);
              end;
              H := Hour;
              if Use12HourClock then
                if H = 0 then H := 12 else if H > 12 then Dec(H, 12);
              if Count > 2 then Count := 2;
              AppendNumber(H, Count);
            end;
          'N':
            begin
              GetCount;
              GetTime;
              if Count > 2 then Count := 2;
              AppendNumber(Min, Count);
            end;
          'S':
            begin
              GetCount;
              GetTime;
              if Count > 2 then Count := 2;
              AppendNumber(Sec, Count);
            end;
          'T':
            begin
              GetCount;
              if Count = 1 then
                AppendFormat(Pointer(AFormatSettings.ShortTimeFormat)) else
                AppendFormat(Pointer(AFormatSettings.LongTimeFormat));
            end;
          'Z':
            begin
              GetCount;
              GetTime;
              if Count > 3 then Count := 3;
              AppendNumber(MSec, Count);
            end;
          'A':
            begin
              GetTime;
              P := Format - 1;
              if StrLIComp(P, 'AM/PM', 5) = 0 then
              begin
                if Hour >= 12 then Inc(P, 3);
                AppendChars(P, 2);
                Inc(Format, 4);
                Use12HourClock := TRUE;
              end else
              if StrLIComp(P, 'A/P', 3) = 0 then
              begin
                if Hour >= 12 then Inc(P, 2);
                AppendChars(P, 1);
                Inc(Format, 2);
                Use12HourClock := TRUE;
              end else
              if StrLIComp(P, 'AMPM', 4) = 0 then
              begin
                if Hour < 12 then
                  AppendString(AFormatSettings.TimeAMString) else
                  AppendString(AFormatSettings.TimePMString);
                Inc(Format, 3);
                Use12HourClock := TRUE;
              end else
              if StrLIComp(P, 'AAAA', 4) = 0 then
              begin
                GetDate;
                AppendString(AFormatSettings.LongDayNames[DayOfWeek(DateTime)]);
                Inc(Format, 3);
              end else
              if StrLIComp(P, 'AAA', 3) = 0 then
              begin
                GetDate;
                AppendString(AFormatSettings.ShortDayNames[DayOfWeek(DateTime)]);
                Inc(Format, 2);
              end else
              AppendChars(@Starter, 1);
            end;
          'C':
            begin
              GetCount;
              AppendFormat(Pointer(AFormatSettings.ShortDateFormat));
              GetTime;
              if (Hour <> 0) or (Min <> 0) or (Sec <> 0) or (MSec <> 0) then
              begin
                AppendChars(' ', 1);
                AppendFormat(Pointer(AFormatSettings.LongTimeFormat));
              end;
            end;
          '/':
            if AFormatSettings.DateSeparator <> #0 then
              AppendChars(@AFormatSettings.DateSeparator, 1);
          ':':
            if AFormatSettings.TimeSeparator <> #0 then
              AppendChars(@AFormatSettings.TimeSeparator, 1);
          '''', '"':
            begin
              P := Format;
              while (Format^ <> #0) and (Format^ <> Starter) do
              begin
                if IsLeadChar(Format^) then
                  Format := StrNextChar(Format)
                else
                  Inc(Format);
              end;
              AppendChars(P, Format - P);
              if Format^ <> #0 then Inc(Format);
            end;
        else
          AppendChars(@Starter, 1);
        end;
      end;
      Dec(AppendLevel);
    end;
  end;

begin
  BufPos := 0;
  AppendLevel := 0;
  if Format <> '' then AppendFormat(Pointer(Format)) else AppendFormat('C');
  if Length(DynBuffer) > 0 then
  begin
    SetLength(Sb, Length(DynBuffer) + BufPos);
    Move(DynBuffer[0], Sb[0], Length(DynBuffer) * SizeOf(Char));
    if BufPos > 0 then
      Move(Buffer[0], Sb[Length(DynBuffer)], BufPos * SizeOf(Char));
    Result := String.Create(Sb);
  end
  else
    Result := String.Create(Buffer, 0, BufPos);
end;

function GxEncodeTimeSmart(const Hour, Min, Sec, MSec: Word): TDateTime;
begin
  if not TryEncodeTime(Hour, Min, Sec, MSec, Result) then
  begin
      // Sprawdzamy dlaczego wyst�pi� b��d
    if Hour > 23 then
      raise GxConvertError.Create(SCzasBladGodzina);
    if Min > 59 then
      raise GxConvertError.Create(SCzasBladMinuta);
    if Sec > 59 then
      raise GxConvertError.Create(SCzasBladSekunda);
    if MSec > 999 then
      raise GxConvertError.Create(SCzasBladMiliSekunda);
  end;
end;

function GxEncodeTime24Smart(const AHour: Word; const AMin: Word = 0; const ASec: Word = 0;
  const AMSec: Word = 0): TDateTime;
begin
    // Sprawdzamy dlaczego wyst�pi� b��d
  if (AHour < 1) or (AHour > 24) then
    raise GxConvertError.Create(SCzasBladGodzina24);
  Result := GxEncodeTimeSmart(AHour - 1, AMin, ASec, AMSec);
end;



end.

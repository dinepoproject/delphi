unit FormatDateTime24testAFormat;



interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestFormatDateTime24AFormat = class(TTestCase)

    published

 procedure  Test_Czy_Funkcja_Prawidlowo_Zmienia_Godziny_Petla;
 procedure  Test_Pelne_Formatowanie_Data_Czas_W_Godzinach;

end;
implementation
uses
GxDateUtils;


{ TtestEndOfAQuarter }



 procedure TtestFormatDateTime24AFormat.Test_Czy_Funkcja_Prawidlowo_Zmienia_Godziny_Petla;
 Var
  I : word;
  dataOczekiwana: string;
  dataSprawdzana: TDateTime;

  wylicz : string;

begin
for I := 0 to 23 do

    begin
    dataSprawdzana:= Encodedatetime(2000,12,20,I,30,00,0);
    dataOczekiwana :=  '2000'+','+ '12'+','+ '20' + ',' + Format( '%.2d',[I+1])+',' + '00' + ',' + '00' + ',' + '000';
    CheckEquals(dataOczekiwana,FormatDateTime24('yyyy,mm,dd,hh,nn,ss,zzz',dataSprawdzana), Format('%s.%s: %d', [ClassName, 'Test_Czy_Funkcja_Prawidlowo_Zmienia_Godziny_Petla', i]));

      end;
   end;


procedure TtestFormatDateTime24AFormat.Test_Pelne_Formatowanie_Data_Czas_W_Godzinach;
var
 dataSprawdzana: TDateTime;
 dataOczekiwana : string;
begin
 dataOczekiwana :=  '20' + ',' +'12'+ ','+'2000'+ ',' + '24'+',' + '00' + ',' + '00' + ',' + '000';
 dataSprawdzana:= Encodedatetime(2000,12,20,23,30,30,300);
 CheckEquals(dataoczekiwana,FormatDateTime24('dd,mm,yyyy,hh,nn,ss,zzz',dataSprawdzana), Format('%s.%s: %s', [ClassName, 'Test_Pelne_Formatowanie_Data_Czas_W_Godzinach', '']));
  { W te�cie zmieniono formatowanie daty (ze zmiennej dataSprawdzana)
  z rok-miesiac-dzie� na dzie�-miesi�c-rok, dodatkowo sprawdzono czy nastepuje wyzerowanie minut, sekund i milisekund. }
 end;

initialization
RegisterTest ( TtestFormatDateTime24AFormat.suite);
end.

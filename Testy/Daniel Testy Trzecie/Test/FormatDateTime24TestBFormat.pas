unit FormatDateTime24TestBFormat;



interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestFormatDateTime24BFormat = class(TTestCase)

    published

 procedure  Test_Czy_Funkcja_Prawidlowo_Zmienia_Godziny_Petla;
 procedure  Test_Pelne_Formatowanie_Data_Czas_W_Godzinach;
 procedure  Test_Reakcja_Funkcji_Na_Znaki_Separatorow_W_Parametrze_AFormatSettings;

end;
implementation
uses
GxDateUtils;


{ TtestEndOfAQuarter }





{ TtestEndOfAQuarter }

procedure TtestFormatDateTime24BFormat.Test_Czy_Funkcja_Prawidlowo_Zmienia_Godziny_Petla;
 Var
  I : word;
  dataOczekiwana: string;
  dataSprawdzana: TDateTime;

  wylicz : string;

begin
for I := 0 to 23 do

    begin
    dataSprawdzana:= Encodedatetime(2000,12,20,I,30,00,0);
    dataOczekiwana :=  '2000'+','+ '12'+','+ '20' + ',' + Format( '%.2d',[I+1])+',' + '00' + ',' + '00' + ',' + '000';
    CheckEquals(dataOczekiwana,FormatDateTime24('yyyy,mm,dd,hh,nn,ss,zzz',dataSprawdzana), Format('%s.%s: %d', [ClassName, 'Test_Czy_Funkcja_Prawidlowo_Zmienia_Godziny_Petla', i]));

      end;
   end;


procedure TtestFormatDateTime24BFormat.Test_Pelne_Formatowanie_Data_Czas_W_Godzinach;
var
 dataSprawdzana: TDateTime;
 dataOczekiwana : string;
begin
 dataOczekiwana :=  '2000' + ',' +'12'+ ','+'20'+ ',' + '00'+',' + '24' + ',' + '00' + ',' + '000';
 dataSprawdzana:= Encodedatetime(2000,12,20,23,30,30,300);
 CheckEquals(dataoczekiwana,FormatDateTime24('yyyy,mm,dd,nn,hh,ss,zzz',dataSprawdzana) , Format('%s.%s: %s', [ClassName, 'Test_Pelne_Formatowanie_Data_Czas_W_Godzinach', '']));
  { W te�cie zmieniono formatowanie czasu(ze zmiennej dataSprawdzana)
  zamieniono miejscami godzin� i minuty, dodatkowo sprawdzono czy nastepuje wyzerowanie minut,sekund i milisekund. }
 end;

procedure TtestFormatDateTime24BFormat.Test_Reakcja_Funkcji_Na_Znaki_Separatorow_W_Parametrze_AFormatSettings;
var
fs : TFormatSettings;
dataSprawdzana: TDateTime;
dataOczekiwana : string;
begin
fs.TimeSeparator := '-';
dataSprawdzana := Encodedatetime(2000,12,20,23, 23, 24, 100);
dataOczekiwana:='2000' + '-'+'12' + '-' + '20'+ '-'+'24'+'-' + '00' + '-' + '00' + '-' + '000';
CheckEquals(dataOczekiwana,FormatDateTime24('yyyy:mm:dd:hh:nn:ss:zzz', dataSprawdzana, fs) , Format('%s.%s: %s', [ClassName, 'Test_Reakcja_Funkcji_Na_Znaki_Separatorow_W_Parametrze_AFormatSettings', '']));
end;

initialization
RegisterTest ( TtestFormatDateTime24BFormat.suite);
end.

unit GxEncodeTimeSmartTest;



interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestGxEncodeTimeSmart = class(TTestCase)

    published

 procedure  Test_Czy_Wyjatek_Gdy_Godzina_Wieksza_Niz_23;
 procedure  Test_Czy_Wyjatek_Gdy_Minuta_Wieksza_Niz_59;
 procedure  Test_Czy_Wyjatek_Gdy_Sekunda_Wieksza_Niz_59;
 procedure  Test_Czy_Wyjatek_Gdy_Milisekunda_Wieksza_Niz_999;
 procedure  Test_Wartosc_Dla_Poprawnych_Wartosci;

end;
implementation
uses
GxDateUtils;


   { TtestEndOfAQuarter }

procedure TtestGxEncodeTimeSmart.Test_Czy_Wyjatek_Gdy_Godzina_Wieksza_Niz_23;
begin
 ExpectedException := GxConvertError;
   try
     GxEncodeTimeSmart(24,01,01,999);
   except
     on E: Exception do
     begin
      CheckEquals(SCzasBladGodzina, E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Godzina_Wieksza_Niz_23', '']));
     raise;
     end;
   end;


end;

procedure TtestGxEncodeTimeSmart.Test_Czy_Wyjatek_Gdy_Milisekunda_Wieksza_Niz_999;
begin
   ExpectedException := GxConvertError;
   try
     GxEncodeTimeSmart(23,01,01,1000);
   except
     on E: Exception do
     begin
     CheckEquals(SCzasBladMiliSekunda, E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Milisekunda_Wieksza_Niz_999', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeTimeSmart.Test_Czy_Wyjatek_Gdy_Minuta_Wieksza_Niz_59;
begin
  ExpectedException := GxConvertError;

   try
     GxEncodeTimeSmart(23,60,01,999);
   except
     on E: Exception do
     begin
     CheckEquals(SCzasBladMinuta, E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Minuta_Wieksza_Niz_59', '']));
     raise;
     end;
   end;

end;

procedure TtestGxEncodeTimeSmart.Test_Czy_Wyjatek_Gdy_Sekunda_Wieksza_Niz_59;
begin
   ExpectedException := GxConvertError;
   try
     GxEncodeTimeSmart(23,01,60,999);
   except
     on E: Exception do
     begin
      CheckEquals(SCzasBladSekunda, E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Sekunda_Wieksza_Niz_59', '']));
     raise;
     end;
   end;

end;

procedure TtestGxEncodeTimeSmart.Test_Wartosc_Dla_Poprawnych_Wartosci;
begin
CheckEquals(Encodetime(23,59,59,999),GxEncodeTimeSmart(23,59,59,999), Format('%s.%s: %s', [ClassName, 'Test_Wartosc_Dla_Poprawnych_Wartosci', '']));
 end;

initialization
RegisterTest (TtestGxEncodeTimeSmart.suite);
end.

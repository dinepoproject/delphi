unit IncQuarterTest;



interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestIncQuarter = class(TTestCase)

    published

 procedure  Test_Otrzymana_Wartosc_Gdy_Parametr_ANumberOfQuarters_Zero;
 procedure  Test_Wynik_Data_Przesuniecie_Jeden_Kwartal;
 procedure  Test_Wynik_Data_Przesuniecie_Minus_Jeden_Kwartal;
 procedure  Test_Wynik_Data_Przesuniecie_Cztery_Kwartaly;
 procedure  Test_Wynik_Data_Przesuniecie_Minus_Cztery_Kwartaly;


end;
implementation
uses
GxDateUtils;

   { TtestEndOfAQuarter }

procedure TtestIncQuarter.Test_Otrzymana_Wartosc_Gdy_Parametr_ANumberOfQuarters_zero;
  var
 I:Integer;
 y, m, d: word;
 robot: Tdatetime;
begin
   for I := 1 to 200 do
   begin
   Randomize;
   robot:= Random (60000);
   DecodeDate(robot,y,m,d);
   CheckEquals(EncodeDate(y,m,d),IncQuarter(EncodeDate(y,m,d),0) , Format('%s.%s: %s', [ClassName, 'Test_Otrzymana_Wartosc_Gdy_Parametr_ANumberOfQuarters_zero', '']));
   end
  end;


procedure TtestIncQuarter.Test_Wynik_Data_Przesuniecie_Cztery_Kwartaly;
begin
  CheckEquals(EncodeDate(2001,01,01),IncQuarter(EncodeDate(2000,01,01),4) , Format('%s.%s: %s', [ClassName, 'Test_Wynik_Data_Przesuniecie_Cztery_Kwartaly', '']));
end;

procedure TtestIncQuarter.Test_Wynik_Data_Przesuniecie_Jeden_Kwartal;
begin
  CheckEquals(EncodeDate(2000,04,01),IncQuarter(EncodeDate(2000,01,01),1) , Format('%s.%s: %s', [ClassName, 'Test_Wynik_Data_Przesuniecie_Jeden_Kwartal', '']));
end;

procedure TtestIncQuarter.Test_Wynik_Data_Przesuniecie_Minus_Cztery_Kwartaly;
begin
  CheckEquals(EncodeDate(1999,01,01),IncQuarter(EncodeDate(2000,01,01),-4) , Format('%s.%s: %s', [ClassName, 'Test_Wynik_Data_Przesuniecie_Minus_Cztery_Kwartaly', '']));
end;

procedure TtestIncQuarter.Test_Wynik_Data_Przesuniecie_Minus_Jeden_Kwartal;
begin
  CheckEquals(EncodeDate(1999,10,01),IncQuarter(EncodeDate(2000,01,01),-1) , Format('%s.%s: %s', [ClassName, 'Test_Wynik_Data_Przesuniecie_Minus_Jeden_Kwartal', '']));
end;

initialization
RegisterTest ( TtestIncQuarter.suite);
end.

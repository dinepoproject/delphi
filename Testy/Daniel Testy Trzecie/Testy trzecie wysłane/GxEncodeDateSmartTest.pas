unit GxEncodeDateSmartTest;



interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestGxEncodeDateSmartTest = class(TTestCase)

    published

 procedure  Test_Czy_Wyjatek_Gdy_Rok_Mniejszy_Niz_1;
 procedure  Test_Czy_Wyjatek_Gdy_Rok_Wiekszy_Niz_9999;
 procedure  Test_Czy_Wyjatek_Gdy_Miesiac_Mniejszy_Niz_1;
 procedure  Test_Czy_Wyjatek_Gdy_Miesiac_Wiekszy_Niz_12;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Mniejszy_Niz_1;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_31_Dla_Miesiac_31;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_30_Dla_Miesiac_30;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_28_Luty_Dla_Rok_Nie_Przestepny;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_29_Dla_Luty_Rok_Przestepny;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_29_Luty_Dla_Rok_Przestepny;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_28_Luty_Dla_Rok_Nie_Przestepny;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_31_Dla_Miesiac_31;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_30_Dla_Miesiac_30;
 procedure  Test_Czy_Wyjatek_Gdy_Dzien_Rowny_1_Stycznia_Dla_Dowolny_Rok;


end;
implementation
uses
GxDateUtils;



  procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Mniejszy_Niz_1;
begin
   ExpectedException := GxConvertError;
   try
     GxEncodeDateSmart(2000,01,00);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2000, 01, 31]), E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Mniejszy_Niz_1', '']));
     raise;
     end;
   end;


end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_28_Luty_Dla_Rok_Nie_Przestepny;
begin
  CheckEquals(EncodeDate(2001,02,28), GxEncodeDateSmart(2001,02,28), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_28_Luty_Dla_Rok_Nie_Przestepny', '']));
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_29_Luty_Dla_Rok_Przestepny;
begin
  CheckEquals(EncodeDate(2000,02,29), GxEncodeDateSmart(2000,02,29), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_29_Luty_Dla_Rok_Przestepny', '']));
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_28_Luty_Dla_Rok_Nie_Przestepny;
begin
    ExpectedException := GxConvertError;

   try
     GxEncodeDateSmart(2001,02,29);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2001, 02, 28]),E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_28_Luty_Dla_Rok_Nie_Przestepny', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_29_Dla_Luty_Rok_Przestepny;
begin
   ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(2000,02,30);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2000, 02, 29]),E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_29_Dla_Luty_Rok_Przestepny', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_30_Dla_Miesiac_30;
begin
  ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(2000,04,31);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2000, 04, 30]),E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_30_Dla_Miesiac_30', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_31_Dla_Miesiac_31;
begin
  ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(2000,01,32);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2000, 01,31]),E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Wiekszy_Niz_31_Dla_Miesiac_31', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_1_Stycznia_Dla_Dowolny_Rok;

begin
  CheckEquals(EncodeDate(2015,01,01), GxEncodeDateSmart(2015,01,01), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_1_Stycznia_Dla_Dowolny_Rok', '']));
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_30_Dla_Miesiac_30;

  begin
  CheckEquals(EncodeDate(2000,04,30), GxEncodeDateSmart(2000,04,30), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_30_Dla_Miesiac_30', '']));
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Dzien_Rowny_31_Dla_Miesiac_31;

 begin
  CheckEquals(EncodeDate(2000,01,31), GxEncodeDateSmart(2000,01,31), Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Rowny_31_Dla_Miesiac_31', '']));
end;


procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Miesiac_Mniejszy_Niz_1;
begin
   ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(2000,00,01);
   except
     on E: Exception do
     begin
     CheckEquals(SDataBladMiesiac,E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Miesiac_Mniejszy_Niz_1', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Miesiac_Wiekszy_Niz_12;
begin
  ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(2000,13,01);
   except
     on E: Exception do
     begin
     CheckEquals(SDataBladMiesiac,E.Message,Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Miesiac_Wiekszy_Niz_12', '']));
     raise;
     end;
   end;
end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Rok_Mniejszy_Niz_1;
begin
  ExpectedException := GxConvertError;

  try
     GxEncodeDateSmart(0000,01,01);
   except
     on E: Exception do
     begin
     CheckEquals(SDataBladRok,E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Rok_Mniejszy_Niz_1', '']));
     raise;
     end;
   end;

end;

procedure TtestGxEncodeDateSmartTest.Test_Czy_Wyjatek_Gdy_Rok_Wiekszy_Niz_9999;
begin
  ExpectedException := GxConvertError;


   try
     GxEncodeDateSmart(10000,01,01);
   except
     on E: Exception do
     begin
     CheckEquals(SDataBladRok,E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Rok_Wiekszy_Niz_9999', '']));
     raise;
     end;
   end;
end;

initialization
RegisterTest ( TtestGxEncodeDateSmartTest.suite);
end.

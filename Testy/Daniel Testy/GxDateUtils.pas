unit GxDateUtils;

interface

    /// Funkcja zwraca dat� pocz�tku kwarta�u na podstawie dowolnej daty nale��cej do tego kwarta�u podanej w
    /// parametrze ADate.
    ///  Testy do wykonania - czy funkcja zwraca prawid�ow� dat� pocz�tku kwarta�u dla nast�puj�cych warto�ci
    ///  1. Data pierwszego dnia ka�dego kwarta�u w bie��cym roku
    ///  2. Data ostatniego dnia ka�dego kwarta�u w bie��cym roku
    ///  3. Dowolna data z kwarta�u.
  function StartOfTheQuarter(const ADate: TDateTime): TDateTime;
    /// Funkcja zwraca dat� pocz�tku kwarta�u na podstawie podanego roku i numeru kwarta�u w roku.
    /// Testy do wykonania -
    ///  1. czy funkcja zwraca prawid�ow� dat� pocz�tku kwarta�u dla kolejnych kwarta��w z bie��cego roku.
    ///  2. jak funkcja obs�u�y podanie warto�ci zero dla kwarta�u - powinna zwr�ci� pocz�tek daty 1. kwarta�u.
    ///  3. jak funkcja obs�u�y podanie warto�ci 5 dla kwarta�u - powinna zwr�ci� pocz�tek daty 4. kwarta�u.
  function StartOfAQuarter(const AYear, AQuarter: Word): TDateTime;

    /// Funkcja zwraca numer kwarta�u w roku dla daty podanej jako parametr. Czyli dla daty 10 stycznia powinien zwr�ci�
    ///  1 (pierwszy kwarta�) dla 10 marca 2 (drugi kwarta�) itd.
    ///  Testy do wykonania - czy funkcja zwraca prawid�ow� dat� pocz�tku kwarta�u dla nast�puj�cych warto�ci
    ///  1. Data pierwszego dnia ka�dego kwarta�u w bie��cym roku
    ///  2. Data ostatniego dnia ka�dego kwarta�u w bie��cym roku
    ///  3. Dowolna data z kwarta�u.
  function QuarterOf(const ADate: TDateTime): Integer; overload;
    ///  Funkcja zwraca numer kwarta�u w roku dla miesi�ca o podanym numerze.
    ///  Testy do wykonania -
    ///  1. Czy funkcja zwraca prawid�owe warto�ci dla miesi�cy od 1 do 12.
    ///  2. Co funkcja zwr�ci, gdy do parametru AMonth przeka�emy warto�� 0 lub mniejsz�. (powinna zwr�ci� warto�� 0)
    ///  3. Co funkcja zwr�ci, gdy do parametru AMonth przeka�emy warto�� wi�ksz� ni� 12. (powinna zwr�ci� warto�� 0)
  function QuarterOf(const AMonth: Integer): Integer; overload;

implementation

uses
  System.SysUtils, System.DateUtils;

function StartOfTheQuarter(const ADate: TDateTime): TDateTime;
var
  d, m, y: word;
begin
  DecodeDate(ADate, y, m, d);
  case m of
    1..3:
      m := 1;
    4..6:
      m := 4;
    7..9:
      m := 7;
    else
      m := 10;
  end;
  Result := EncodeDate( y,m,1);
end;

function StartOfAQuarter(const AYear, AQuarter: Word): TDateTime;
var
  month: Integer;
begin
  case AQuarter of
    1:
      month := 1;
    2:
      month := 4;
    3:
      month := 7;
    else
      month := 10;
  end;
  Result := EncodeDate(AYear, month, 1);
end;

function QuarterOf(const ADate: TDateTime): Integer;
begin
  Result := QuarterOf(MonthOf(ADate));
end;

function QuarterOf(const AMonth: Integer): Integer; overload;
begin
  case AMonth of
    1..3:
      Result := 1;
    4..6:
      Result := 2;
    7..9:
      Result := 3;
    10..12:
      Result := 4;
    else
      Result := 0;
  end;
end;



end.

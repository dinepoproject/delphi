unit Funkcja_StartOfAQuarter;

interface

uses
System.SysUtils, TestFramework;
type
 Ttest_StartOfAQuarter = class(TTestCase)
   published

 procedure  Test_Pierwszego_Kwarta�u;
 procedure  Test_Drugiego_Kwarta�u;
 procedure  Test_Trzeciego_Kwarta�u;
 procedure  Test_Czwartego_Kwarta�u;
 procedure  Test_Kwartal_Rowny_0;
 procedure  Test_Kwartal_Rowny_5;
 end;

implementation
uses
GxDateUtils;
 { Ttest_StartOfAQuarter }

 procedure Ttest_StartOfAQuarter.Test_Czwartego_Kwarta�u;
var
  dataOczekiwana: TDateTime;

 begin

    dataOczekiwana:=  EncodeDate(2015,10,01);

 CheckEquals(dataOczekiwana, StartOfAQuarter(2015,4),
 Format ('%s.%s: %s', [ClassName, 'Test_Czwartego_Kwarta�u', '']));
 end;




procedure Ttest_StartOfAQuarter.Test_Drugiego_Kwarta�u;
var

  dataOczekiwana: TDateTime;

begin

    dataOczekiwana:=  EncodeDate(2015,04,01);

CheckEquals(dataOczekiwana, StartOfAQuarter(2015,2),
Format ('%s.%s: %s', [ClassName, 'Test_Drugiego_Kwarta�u', '']));
end;



procedure Ttest_StartOfAQuarter.Test_Pierwszego_Kwarta�u;
var

  dataOczekiwana: TDateTime;

begin

    dataOczekiwana:=  EncodeDate(2015,01,01);

CheckEquals(dataOczekiwana, StartOfAQuarter(2015,1),
Format ('%s.%s: %s', [ClassName, 'Test_Pierwszego_Kwarta�u', '']));
end;



procedure Ttest_StartOfAQuarter.Test_Trzeciego_Kwarta�u;
var

  dataOczekiwana: TDateTime;

begin

   dataOczekiwana:=  EncodeDate(2015,07,01);

CheckEquals(dataOczekiwana, StartOfAQuarter(2015,3),
Format ('%s.%s: %s', [ClassName, 'Test_Trzeciego_Kwarta�u', '']));
end;



procedure Ttest_StartOfAQuarter.Test_Kwartal_Rowny_5;
var

  dataOczekiwana: TDateTime;

begin

    dataOczekiwana:=  EncodeDate(2015,10,01);

CheckEquals(dataOczekiwana, StartOfAQuarter(2015,5),
Format ('%s.%s: %s', [ClassName, 'Test_kwartal_rowny_piec', '']));
end;



procedure Ttest_StartOfAQuarter.Test_Kwartal_Rowny_0;
var

  dataOczekiwana: TDateTime;

begin

    dataOczekiwana:=  EncodeDate(2015,10,01);

CheckEquals(dataOczekiwana, StartOfAQuarter(2015,0),
Format ('%s.%s: %s', [ClassName, 'Test_kwartal_rowny_zero', '']));
end;

initialization
RegisterTest ( Ttest_StartOfAQuarter.suite);
end.

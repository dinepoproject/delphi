unit Funkcja_QuarterOf_AMonth;

interface

uses
System.SysUtils, TestFramework;
type
 Ttest_QuarterOf_AMonth = class(TTestCase)
      published

 procedure  Test_Miesiac_Od_1_Do_12;
 procedure  Test_AMonth_0;
 procedure  Test_AMonth_Ponizej_Zera;
 procedure  Test_AMonth_powyzej_12;
 end;
implementation


uses
GxDateUtils;
 { Ttest_QuarterOf_AMonth }

procedure Ttest_QuarterOf_AMonth.Test_AMonth_0;
begin
CheckEquals(0, QuarterOf(0),
Format ('%s.%s: %s', [ClassName, 'Ttest_QuarterOf_AMonth.Test_AMonth_0', '']));
end;

procedure Ttest_QuarterOf_AMonth.Test_AMonth_Ponizej_Zera;
begin
 CheckEquals(0, QuarterOf(-10),
 Format ('%s.%s: %s', [ClassName, 'Ttest_QuarterOf_AMonth.Test_AMonth_Ponizej_zera', '']));
end;

procedure Ttest_QuarterOf_AMonth.Test_AMonth_powyzej_12;
begin
 CheckEquals(0, QuarterOf(14),
 Format ('%s.%s: %s', [ClassName, 'Ttest_QuarterOf_AMonth.Test_AMonth_powyzej_12', '']));
end;

procedure Ttest_QuarterOf_AMonth.Test_Miesiac_Od_1_Do_12;
begin
 CheckEquals(4, QuarterOf(10),
 Format ('%s.%s: %s', [ClassName, 'Ttest_QuarterOf_AMonth.Test_Miesiac_Od_1_Do_12', '']));
end;

initialization
RegisterTest ( Ttest_QuarterOf_AMonth.suite);
end.

unit Funkcja_StartOfAQuarter;

interface

uses
System.SysUtils, TestFramework;
type
 Ttest_StartOfAQuarter = class(TTestCase)
   published

 procedure  Test_Pierwszego_Kwarta�u;
 procedure  Test_Drugiego_Kwarta�u;
 procedure  Test_Trzeciego_Kwarta�u;
 procedure  Test_Czwartego_Kwarta�u;
 procedure  Test_kwartal_rowny_zero;
 procedure  Test_kwartal_rowny_piec;
 end;


implementation
uses
GxDateUtils;
 { Ttest_StartOfAQuarter }


procedure Ttest_StartOfAQuarter.Test_Czwartego_Kwarta�u;
    var
  dataOczekiwana: TDateTime;

 begin
    dataOczekiwana:=  EncodeDate(2015,10,01);
  (*data oczekiwana musi miec format Tdatetime bo taki zwraca funkcja,
  natomiast do samej funckji mozna wpisywac word *)


  CheckEquals(dataOczekiwana, StartOfAQuarter(2015,4),
   Format ('%s.%s: %s', [ClassName, 'Test_Czwartego_Kwarta�u', '']));
 end;




procedure Ttest_StartOfAQuarter.Test_Drugiego_Kwarta�u;
    var
  dataOczekiwana: TDateTime;

 begin
    dataOczekiwana:=  EncodeDate(2015,04,01);

 CheckEquals(dataOczekiwana, StartOfAQuarter(2015,2),
 Format ('%s.%s: %s', [ClassName, 'Test_Drugiego_Kwarta�u', '']));
 end;



procedure Ttest_StartOfAQuarter.Test_Pierwszego_Kwarta�u;
       var
  dataOczekiwana: TDateTime;

 begin
    dataOczekiwana:=  EncodeDate(2015,01,01);

 CheckEquals(dataOczekiwana, StartOfAQuarter(2015,1),
 Format ('%s.%s: %s', [ClassName, 'Test_Pierwszego_Kwarta�u', '']));
 end;



procedure Ttest_StartOfAQuarter.Test_Trzeciego_Kwarta�u;
var
  dataOczekiwana: TDateTime;

begin
   dataOczekiwana:=  EncodeDate(2015,07,01);

CheckEquals(dataOczekiwana, StartOfAQuarter(2015,3),
Format ('%s.%s: %s', [ClassName, 'Test_Trzeciego_Kwarta�u', '']));
end;



procedure Ttest_StartOfAQuarter.Test_kwartal_rowny_piec;
    var
  dataOczekiwana: TDateTime;

 begin
    dataOczekiwana:=  EncodeDate(2015,10,01);

 CheckEquals(dataOczekiwana, StartOfAQuarter(2015,5),
 Format ('%s.%s: %s', [ClassName, 'Test_kwartal_rowny_piec', '']));
 end;



procedure Ttest_StartOfAQuarter.Test_kwartal_rowny_zero;
    var
  dataOczekiwana: TDateTime;

 begin
    dataOczekiwana:=  EncodeDate(2015,10,01);

 CheckEquals(dataOczekiwana, StartOfAQuarter(2015,0),
 Format ('%s.%s: %s', [ClassName, 'Test_kwartal_rowny_zero', '']));
 end;



initialization
RegisterTest ( Ttest_StartOfAQuarter.suite);
end.

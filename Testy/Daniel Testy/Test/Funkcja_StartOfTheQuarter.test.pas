unit Funkcja_StartOfTheQuarter.test;

interface

uses
 System.SysUtils, TestFramework;
type
Ttest_StartOfTheQuarter = class(TTestCase)
private
 function Losowanie_Daty_Pierwszy_Kwartal: Tdatetime;
 //Procedura podstawia do testu 100 losowych dat z pierwszego kwarta�u
 published
 procedure  Test_Data_Pierwszego_Dnia_Pierwszego_Kwarta�u;
 procedure  Test_Data_Ostaniego_Dnia_Pierwszego_Kwarta�u;
 procedure  Test_Data_Pierwszego_Dnia_Ostatniego_Kwarta�u;
 procedure  Test_Data_Ostatniego_Dnia_Ostatniego_Kwarta�u;
 procedure  Test_Dowolna_Data;
 procedure  Test_Data_Losowana;

end;

implementation

  uses
   GxDateUtils;

{ Ttest_StartOfTheQuarter }

procedure Ttest_StartOfTheQuarter.Test_Data_Ostatniego_Dnia_Ostatniego_Kwarta�u;
  var
  dataOczekiwana: TDateTime;
  dataSprawdzana: TDateTime;
begin
    dataOczekiwana:=  EncodeDate(2015,10,01);

    dataSprawdzana:= EncodeDate(2015,12,31);


 CheckEquals(dataOczekiwana, StartOfTheQuarter(dataSprawdzana),
  Format ('%s.%s: %s', [ClassName, 'Test_Data_Ostatniego_Dnia_Ostatniego_Kwarta�u', '']));
end;


procedure Ttest_StartOfTheQuarter.Test_Data_Pierwszego_Dnia_Ostatniego_Kwarta�u;
var
  dataOczekiwana: TDateTime;
  dataSprawdzana: TDateTime;
begin
    dataOczekiwana:=  EncodeDate(2015,10,01);

    dataSprawdzana:= EncodeDate(2015,10,01);

 CheckEquals(dataOczekiwana, StartOfTheQuarter(dataSprawdzana),
 Format ('%s.%s: %s', [ClassName, 'Test_Data_Pierwszego_Dnia_Ostatniego_Kwarta�u', '']));
end;

procedure Ttest_StartOfTheQuarter.Test_Data_Pierwszego_Dnia_Pierwszego_Kwarta�u;
var
  dataOczekiwana: TDateTime;
  dataSprawdzana: TDateTime;
begin
    dataOczekiwana:=  EncodeDate(2015,01,01);

    dataSprawdzana:= EncodeDate(2015,01,01);

 CheckEquals(dataOczekiwana, StartOfTheQuarter(dataSprawdzana),
 Format ('%s.%s: %s', [ClassName, 'Test_Data_Pierwszego_Dnia_Pierwszego_Kwarta�u', '']));
end;

procedure Ttest_StartOfTheQuarter.Test_Dowolna_Data;
var
  dataOczekiwana: TDateTime;
  dataSprawdzana: TDateTime;
begin
    dataOczekiwana:=  EncodeDate(1999,10,01);

    dataSprawdzana:= EncodeDate(1999,11,20);

 CheckEquals(dataOczekiwana, StartOfTheQuarter(dataSprawdzana),
  Format ('%s.%s: %s', [ClassName, 'Test_Dowolna_Data', '']));
end;

function Ttest_StartOfTheQuarter.Losowanie_Daty_Pierwszy_Kwartal: Tdatetime;
var
  ALicznikDat: Integer;
 d, m, y: word;
begin
      for ALicznikDat := 1 to 100 do

      repeat
         Randomize;    //losowanie
      Result := Random (60000);
      DecodeDate(Result, y, m, d);
      until

      ((Y = 2015)and ((m in [1..3])  )and ((d in [1..31]))) ;

end;



procedure Ttest_StartOfTheQuarter.Test_Data_Losowana;

  var
  dataOczekiwana: TDateTime;
  dataSprawdzana: TDateTime;

begin
    dataOczekiwana:=  EncodeDate(2015,01,01);

    dataSprawdzana := Losowanie_Daty_Pierwszy_Kwartal;

 CheckEquals(dataOczekiwana, StartOfTheQuarter(dataSprawdzana),
 Format ('%s.%s: %s', [ClassName, 'Test_Data_Losowana', '']));
end;


procedure Ttest_StartOfTheQuarter.Test_Data_Ostaniego_Dnia_Pierwszego_Kwarta�u;
var
  dataOczekiwana: TDateTime;
  dataSprawdzana: TDateTime;
begin
    dataOczekiwana:=  EncodeDate(2015,01,01);

    dataSprawdzana:= EncodeDate(2015,03,31);

 CheckEquals(dataOczekiwana, StartOfTheQuarter(dataSprawdzana),
 Format ('%s.%s: %s', [ClassName, 'Test_Data_Ostaniego_Dnia_Pierwszego_Kwarta�u', '']));
end;
initialization
RegisterTest ( Ttest_StartOfTheQuarter.suite);
end.

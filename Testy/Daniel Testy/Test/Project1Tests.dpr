program Project1Tests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  GxDateUtils in '..\GxDateUtils.pas',
  Funkcja_StartOfTheQuarter.test in 'Funkcja_StartOfTheQuarter.test.pas',
  Funkcja_StartOfAQuarter in 'Funkcja_StartOfAQuarter.pas',
  Funkcja_QuarterOf_Adate in 'Funkcja_QuarterOf_Adate.pas',
  Funkcja_QuarterOf_AMonth in 'Funkcja_QuarterOf_AMonth.pas';

{$R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.


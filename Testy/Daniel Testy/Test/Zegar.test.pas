unit Zegar.test;

interface

uses
 TestFramework, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;
type
Ttest_StartOfTheQuarter = class(TTestCase)
 published
 procedure  BiezacaData;
end;

implementation

  uses
   GxDateUtils;

{ Ttest_StartOfTheQuarter }
var
data1: Tdatetime;
data2: Tdatetime;
procedure Ttest_StartOfTheQuarter.BiezacaData;
var
  datus: TDateTime;
  datus1: TDateTime;
begin
  datus:= StrToDate('2015-07-01');
  datus1:= StrToDate('2015-08-23');
 CheckEquals(datus, StartOfTheQuarter(datus1));
end;
initialization
RegisterTest ( Ttest_StartOfTheQuarter.suite);
end.

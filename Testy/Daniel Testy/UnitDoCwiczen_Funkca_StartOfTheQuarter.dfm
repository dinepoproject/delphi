object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 500
  ClientWidth = 593
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 424
    Top = 13
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 272
    Top = 101
    Width = 31
    Height = 13
    Caption = 'Label2'
  end
  object Label3: TLabel
    Left = 257
    Top = 198
    Width = 31
    Height = 13
    Caption = 'Label3'
  end
  object Button1: TButton
    Left = 24
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 120
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Edit1: TEdit
    Left = 282
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Edit1'
  end
  object Button3: TButton
    Left = 201
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button3'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 164
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Encode date'
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 176
    Top = 193
    Width = 75
    Height = 25
    Caption = 'Decode date'
    TabOrder = 5
    OnClick = Button5Click
  end
  object Edit2: TEdit
    Left = 24
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 6
    Text = 'rok'
  end
  object Edit3: TEdit
    Left = 24
    Top = 99
    Width = 121
    Height = 21
    TabOrder = 7
    Text = 'miesiac'
  end
  object Edit4: TEdit
    Left = 24
    Top = 126
    Width = 121
    Height = 21
    TabOrder = 8
    Text = 'dzien'
  end
  object Edit5: TEdit
    Left = 24
    Top = 195
    Width = 121
    Height = 21
    TabOrder = 9
    Text = 'Edit5'
  end
  object Edit6: TEdit
    Left = 24
    Top = 232
    Width = 121
    Height = 21
    TabOrder = 10
    Text = 'Edit6'
  end
  object Button6: TButton
    Left = 164
    Top = 224
    Width = 105
    Height = 25
    Caption = 'losowanie datus'
    TabOrder = 11
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 24
    Top = 304
    Width = 123
    Height = 25
    Caption = 'Uruchom Form 2'
    TabOrder = 12
    OnClick = Button7Click
  end
end

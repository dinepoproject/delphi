unit woda_testy;

interface

uses TestFramework;

type
ttest_oblicz = class (ttestcase)
      published
 procedure woda_1000_smola2;
 procedure woda_minus_1000_smo�a_2;
 procedure woda_minus_1000_smo�a_2_wyjatek_komunikat;
 //ten test sprawdza, czy wyswietla sie prawid�owy wyj�tek
 procedure woda__1000_smo�a_minus_2;
 procedure woda__1000_smo�a_minus_2_wyj�tek_komunikat;
 procedure woda__pow_10000_smo�a__2;
 procedure woda__pow_10000_smo�a__2_wyj�tek_komunikat;  //ctl+shift+C dodaje procedur�
 procedure woda__1000_smo�a_100;
 procedure woda__1000_smo�a_100_wyj�tek_komunikat;
 end;

implementation
 uses
 Woda, System.SysUtils;
    procedure ttest_oblicz.woda_1000_smola2;
    begin
       CheckEquals(980,OBLICZ(1000,2));
    end;



procedure ttest_oblicz.woda_minus_1000_smo�a_2;
begin
  ExpectedException := Exception;
  OBLICZ(-1000,2);
end;



procedure ttest_oblicz.woda_minus_1000_smo�a_2_wyjatek_komunikat;
begin
  try
    OBLICZ(-1000,2);
  except
    on E: Exception do
      CheckEquals('Wprowad� poprawne dane, warto�� musi by� dodatnia', E.Message);
      //sprawdza czy w E.Message (komunikacie wyjatku z woda.pas) jest ta wpisana warto�c(kolor niebieski)
  end;
end;

procedure ttest_oblicz.woda__1000_smo�a_100;
begin
      ExpectedException := Exception;
  OBLICZ(1000,100);
end;

procedure ttest_oblicz.woda__1000_smo�a_100_wyj�tek_komunikat;
begin
   try
    OBLICZ(1000,100);
  except
    on E: Exception do
      CheckEquals('Wprowad� poprawne dane, nieprawid�owy procent', E.Message);
  end;
end;

procedure ttest_oblicz.woda__1000_smo�a_minus_2;
begin
      ExpectedException := Exception;
   OBLICZ(1000,-2);
end;

procedure ttest_oblicz.woda__1000_smo�a_minus_2_wyj�tek_komunikat;
begin
  try
    OBLICZ(1000,-2);
  except
    on E: Exception do
      CheckEquals('Wprowad� poprawne dane, warto�� musi by� dodatnia', E.Message);
  end;
end;

procedure ttest_oblicz.woda__pow_10000_smo�a__2;
begin
   ExpectedException := Exception;
   OBLICZ(10000,2);
end;

procedure ttest_oblicz.woda__pow_10000_smo�a__2_wyj�tek_komunikat;
begin
  try
    OBLICZ(10000,2);
  except
    on E: Exception do
      CheckEquals('Wprowad� poprawne dane, warto�� nie mo�e przekracza� 10000', E.Message);
  end;
end;

initialization
 RegisterTest(Ttest_oblicz.Suite);


end.

unit U_SAP_test;
 // supports jest w pomocy
 //  https://4programmers.net/Delphi/Typy_danych
 //   https://4programmers.net/Delphi/Zbiory

interface

uses
  System.SysUtils, TestFramework, System.DateUtils, Vcl.Dialogs, Unit2;
  // ctrl+alt+c, ctrl +alt+v - kopiowanie

type
  TTest_dla_recordu = class(TTestCase)
    procedure Czysc_dla_rekordu;
    procedure PrzepiszDo_dla_rekordu;
    procedure Zmiana_danych_dla_rekordu;
    procedure Zmiana_zamowienia_dla_rekordu;
  end;

type
  Ttest_dla_klasy_TBkSAPPrzybycieWagon = class(TTestCase)
    procedure Czysc_dla_klasy_TBkSAPPrzybycieWagon;
    procedure Czy_klasa_obslug_interf_dla_TBkSAPPrzybycieWagon;
    procedure Test_metody_dane_dla_TBkSAPPrzybycieWagon;
    procedure Test_metody_ustaw_funkcja_puste_dla_TBkSAPPrzybycieWagon;
    procedure Test_metody_ustaw_zmienione_dane_recordu_dla_TBkSAPPrzybycieWagon;
  end;

type
  TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane = class(TTestCase)
    procedure Czysc_dla_klasy_TBkSAPPrzybycieWagonyDane;
    procedure Czy_klasa_obslug_interf_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_Dane_dla_1_elementu_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_Dane_dla_3_elementow_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_Dane_dla_10_elementow_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_DanePodstawienie_pusta_struk_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_DanePodstawienie_1_record_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_DanePodstawienie_2_record_dla_TBkSAPPrzybycieWagonyDane1;
    procedure Metoda_GetOperacja_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_SetOperacja_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_Pusty_tworzenie_obiektu_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_Pusty_dodanie_wart_metoda_ustaw_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_Ustaw_funkcja_Dane_dla_TBkSAPPrzybycieWagonyDane1;
    procedure Metoda_Ustaw_dwukr_ta_sama_wartosc_czy_Dane_dla_TBkSAPPrzybycieWagonyDane;
    procedure Metoda_Ustaw_dwukr_ta_sama_wartosc_czy_Zmieniony_dla_TBkSAPPrzybycieWagonyDane;
  end;

implementation
 { TTest_dla_recordu }

procedure TTest_dla_recordu.Czysc_dla_rekordu;
{ Test metody Czysc - we wszystkich polach rekordu wpisa� losowe dane. Po wywo�aniu metody Czysc w polach powinny znajdowa�
   si� zale�nie od typu warto�ci: 0 (pola numeryczne i datowe), puste �a�cuchy, False (pola Boolean)  }
var
  lRekordTestowy: TBkSAPPrzybycieWagonDane;
begin
  lRekordTestowy.Dane.PodstawienieId := 10;
  lRekordTestowy.Dane.PobytId := 10;
  lRekordTestowy.Dane.Numer := '3';
  lRekordTestowy.Dane.DostawcaId := 4;
  lRekordTestowy.Dane.LadunekId := 5;
  lRekordTestowy.Dane.MasaNadawcy := 6;
  lRekordTestowy.Dane.MasaKolei := 7;
  lRekordTestowy.Dane.CzasPrzyjecia := strtodate('2016-10-11');
  lRekordTestowy.Dane.ListId := 8;
  lRekordTestowy.Dane.ListCzasNadania := strtodate('2016-10-11');
  lRekordTestowy.Dane.ZamowienieNumer := '9';
  lRekordTestowy.Dane.ZamowieniePozycja := 10;
  lRekordTestowy.Czysc;
  CheckEquals(0, lRekordTestowy.Dane.PodstawienieId, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals(0, lRekordTestowy.Dane.PobytId, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals('', lRekordTestowy.Dane.Numer, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals(0, lRekordTestowy.Dane.DostawcaId, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals(0, lRekordTestowy.Dane.LadunekId, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals(0, lRekordTestowy.Dane.MasaNadawcy, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals(0, lRekordTestowy.Dane.MasaKolei, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals(0, lRekordTestowy.Dane.CzasPrzyjecia, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals(0, lRekordTestowy.Dane.ListId, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals(0, lRekordTestowy.Dane.ListCzasNadania, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals('', lRekordTestowy.Dane.ZamowienieNumer, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
  CheckEquals(0, lRekordTestowy.Dane.ZamowieniePozycja, Format('%s.%s: %s', [ClassName, 'Czysc_dla_rekordu', '']));
end;

   { TTest_dla_recordu }

procedure TTest_dla_recordu.PrzepiszDo_dla_rekordu;
{we wszystkich polach rekordu wpisa� losowe dane.
Po wywo�aniu metody PrzepiszDo trzeba sprawdzi�
   czy do rekordu typu TBkWagonPodstawionyDane
   zosta�y przepisane odpowiednie pola z rekordu typu TBkSAPPrzybycieWagonDane
    Nale�y utworzy� 2 testy -
    dla parametru ZamowienieStare r�wnego false i true. }
var
  Rekord_SAP: TBkSAPPrzybycieWagonDane;
  Rekord_Wagon: TBkWagonPodstawionyDane;
begin
  Rekord_SAP.Dane.PodstawienieId := 10;
  Rekord_SAP.Dane.PobytId := 10;
  Rekord_SAP.Dane.Numer := '3';
  Rekord_SAP.Dane.DostawcaId := 4;
  Rekord_SAP.Dane.LadunekId := 5;
  Rekord_SAP.Dane.MasaNadawcy := 6;
  Rekord_SAP.Dane.MasaKolei := 7;
  Rekord_SAP.Dane.CzasPrzyjecia := strtodate('2016-10-11');
  Rekord_SAP.Dane.ListId := 8;
  Rekord_SAP.Dane.ListCzasNadania := strtodate('2016-10-11');
  Rekord_SAP.Dane.ZamowienieNumer := '9';
  Rekord_SAP.Dane.ZamowieniePozycja := 10;
   //Sprawdzamy dla True
  Rekord_SAP.PrzepiszDo(Rekord_Wagon, True);

  CheckEquals(Rekord_Wagon.PodstawienieId, Rekord_SAP.Dane.PodstawienieId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.PobytId, Rekord_SAP.Dane.PobytId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.Numer, Rekord_SAP.Dane.Numer, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.DostawcaId, Rekord_SAP.Dane.DostawcaId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.LadunekId, Rekord_SAP.Dane.LadunekId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.MasaNadawcy, Rekord_SAP.Dane.MasaNadawcy, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.CzasPrzyjecia, Rekord_SAP.Dane.CzasPrzyjecia, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.ListId, Rekord_SAP.Dane.ListId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.ListCzasNadania, Rekord_SAP.Dane.ListCzasNadania, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.ZamowienieNumer, Rekord_SAP.Dane.ZamowienieNumer, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.ZamowieniePozycja, Rekord_SAP.Dane.ZamowieniePozycja, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
    //Sprawdzamy dla False
  Rekord_SAP.PrzepiszDo(Rekord_Wagon, false);

  CheckEquals(Rekord_Wagon.PodstawienieId, Rekord_SAP.Dane.PodstawienieId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.PobytId, Rekord_SAP.Dane.PobytId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.Numer, Rekord_SAP.Dane.Numer, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.DostawcaId, Rekord_SAP.Dane.DostawcaId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.LadunekId, Rekord_SAP.Dane.LadunekId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.MasaNadawcy, Rekord_SAP.Dane.MasaNadawcy, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.CzasPrzyjecia, Rekord_SAP.Dane.CzasPrzyjecia, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.ListId, Rekord_SAP.Dane.ListId, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.ListCzasNadania, Rekord_SAP.Dane.ListCzasNadania, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.ZamowienieNumer, Rekord_SAP.Dane.ZamowienieNumer, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
  CheckEquals(Rekord_Wagon.ZamowieniePozycja, Rekord_SAP.Dane.ZamowieniePozycja, Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
 // Rekord_SAP.PrzepiszDo(Rekord_SAP.Dane,False);
 //CheckEquals(Rekord_SAP.StareZamowienieSAPNr,Rekord_Wagon.ZamowienieNumer , Format('%s.%s: %s', [ClassName, 'PrzepiszDo_dla_rekordu', '']));
end;

   { TTest_dla_recordu }

procedure TTest_dla_recordu.Zmiana_danych_dla_rekordu;
var
 { Testy metody ZmianaDanych - grupa test�w sprawdzaj�cych czy funkcja zmiana danych zwraca warto�� True gdy jest zmiana danych
   i False, gdy nie zmiany danych w okre�lonym polu. Dla ka�dego pola tworzymy oddzielny test.}
 Rekord_SAP_do_kontroli: TBkSAPPrzybycieWagonDane;
 Rekord_SAP_zmieniony: TBkSAPPrzybycieWagonDane;
begin
  Rekord_SAP_do_kontroli.Dane.PodstawienieId := 10;
  Rekord_SAP_do_kontroli.Dane.PobytId := 10;
  Rekord_SAP_do_kontroli.Dane.Numer := '3';
  Rekord_SAP_do_kontroli.Dane.DostawcaId := 4;
  Rekord_SAP_do_kontroli.Dane.LadunekId := 5;
  Rekord_SAP_do_kontroli.Dane.MasaNadawcy := 6;
  Rekord_SAP_do_kontroli.Dane.MasaKolei := 7;
  Rekord_SAP_do_kontroli.Dane.CzasPrzyjecia := strtodate('2016-10-11');
  Rekord_SAP_do_kontroli.Dane.ListId := 8;
  Rekord_SAP_do_kontroli.Dane.ListCzasNadania := strtodate('2016-10-11');
  Rekord_SAP_do_kontroli.Dane.ZamowienieNumer := '9';
  Rekord_SAP_do_kontroli.Dane.ZamowieniePozycja := 10;



  Rekord_SAP_zmieniony.Dane.PodstawienieId := 11;
  Rekord_SAP_zmieniony.Dane.PobytId := 11;
  Rekord_SAP_zmieniony.Dane.Numer := '4';
  Rekord_SAP_zmieniony.Dane.DostawcaId := 5;
  Rekord_SAP_zmieniony.Dane.LadunekId := 6;
  Rekord_SAP_zmieniony.Dane.MasaNadawcy := 7;
  Rekord_SAP_zmieniony.Dane.MasaKolei := 8;
  Rekord_SAP_zmieniony.Dane.CzasPrzyjecia := strtodate('2016-10-12');
  Rekord_SAP_zmieniony.Dane.ListId := 8;
  Rekord_SAP_zmieniony.Dane.ListCzasNadania := strtodate('2016-10-20');
  Rekord_SAP_zmieniony.Dane.ZamowienieNumer := '8';
  Rekord_SAP_zmieniony.Dane.ZamowieniePozycja := 11;

begin
 CheckEquals(True,Rekord_SAP_do_kontroli.ZmianaDanych(Rekord_SAP_zmieniony) , Format('%s.%s: %s', [ClassName, 'Zmiana_danych_dla_rekordu', '']));
 CheckEquals(False,Rekord_SAP_do_kontroli.ZmianaDanych(Rekord_SAP_do_kontroli) , Format('%s.%s: %s', [ClassName, 'Zmiana_danych_dla_rekordu', '']));
 end;
 end;
   { TTest_dla_recordu }

procedure TTest_dla_recordu.Zmiana_zamowienia_dla_rekordu;
var
 { Testy metody ZmianaZamowienia - testy sprawdzaj�ce poprawno�� wynik�w zwracanych przez funkcj�. Jeden test dla warto�ci zgodnych.
   Po jednym te�cie sprawdzaj�cym r�nice poszczeg�lnych warto�ci (numer i pozycja) i jeden test dla kompletnie niezgodnych warto�ci.}

  Rekord_SAP: TBkSAPPrzybycieWagonDane;
  Rekord_Wagon: TBkWagonPodstawionyDane;
  Rekord_SAP_nie_zmieniony:TBkSAPPrzybycieWagonDane;
  Rekord_SAP_zmieniony:TBkSAPPrzybycieWagonDane;
begin
// za pomoca procedury PrzepiszDo podstawiam dane do recordu  TBkWagonPodstawionyDane;
  Rekord_SAP.Dane.PodstawienieId := 10;
  Rekord_SAP.Dane.PobytId := 10;
  Rekord_SAP.Dane.Numer := '3';
  Rekord_SAP.Dane.DostawcaId := 4;
  Rekord_SAP.Dane.LadunekId := 5;
  Rekord_SAP.Dane.MasaNadawcy := 6;
  Rekord_SAP.Dane.MasaKolei := 7;
  Rekord_SAP.Dane.CzasPrzyjecia := strtodate('2016-10-11');
  Rekord_SAP.Dane.ListId := 8;
  Rekord_SAP.Dane.ListCzasNadania := strtodate('2016-10-11');
  Rekord_SAP.Dane.ZamowienieNumer := '9';
  Rekord_SAP.Dane.ZamowieniePozycja := 10;
   //Sprawdzamy dla True
  Rekord_SAP.PrzepiszDo(Rekord_Wagon, True);

  Rekord_SAP_nie_zmieniony.Dane.PodstawienieId := 10;
  Rekord_SAP_nie_zmieniony.Dane.PobytId := 10;
  Rekord_SAP_nie_zmieniony.Dane.Numer := '3';
  Rekord_SAP_nie_zmieniony.Dane.DostawcaId := 4;
  Rekord_SAP_nie_zmieniony.Dane.LadunekId := 5;
  Rekord_SAP_nie_zmieniony.Dane.MasaNadawcy := 6;
  Rekord_SAP_nie_zmieniony.Dane.MasaKolei := 7;
  Rekord_SAP_nie_zmieniony.Dane.CzasPrzyjecia := strtodate('2016-10-11');
  Rekord_SAP_nie_zmieniony.Dane.ListId := 8;
  Rekord_SAP_nie_zmieniony.Dane.ListCzasNadania := strtodate('2016-10-11');
  Rekord_SAP_nie_zmieniony.Dane.ZamowienieNumer := '9';
  Rekord_SAP_nie_zmieniony.Dane.ZamowieniePozycja := 10;

   Rekord_SAP_nie_zmieniony.ZmianaZamowienia;

  CheckEquals(Rekord_SAP_nie_zmieniony.Dane.ZamowienieNumer,Rekord_Wagon.ZamowienieNumer , Format('%s.%s: %s', [ClassName, 'Zmiana_zamowienia_dla_rekordu', '']));

  Rekord_SAP_zmieniony.Dane.PodstawienieId := 11;
  Rekord_SAP_zmieniony.Dane.PobytId := 11;
  Rekord_SAP_zmieniony.Dane.Numer := '4';
  Rekord_SAP_zmieniony.Dane.DostawcaId := 5;
  Rekord_SAP_zmieniony.Dane.LadunekId := 6;
  Rekord_SAP_zmieniony.Dane.MasaNadawcy := 7;
  Rekord_SAP_zmieniony.Dane.MasaKolei := 8;
  Rekord_SAP_zmieniony.Dane.CzasPrzyjecia := strtodate('2016-10-12');
  Rekord_SAP_zmieniony.Dane.ListId := 8;
  Rekord_SAP_zmieniony.Dane.ListCzasNadania := strtodate('2016-10-20');
  Rekord_SAP_zmieniony.Dane.ZamowienieNumer := '8';
  Rekord_SAP_zmieniony.Dane.ZamowieniePozycja := 11;

   Rekord_SAP_zmieniony.ZmianaZamowienia;
 CheckEquals(Rekord_SAP_zmieniony.Dane.ZamowienieNumer,Rekord_Wagon.ZamowienieNumer , Format('%s.%s: %s', [ClassName, 'Zmiana_zamowienia_dla_rekordu', '']));
end;


{ Ttest_dla_klasy_TBkSAPPrzybycieWagon }

procedure Ttest_dla_klasy_TBkSAPPrzybycieWagon.Czysc_dla_klasy_TBkSAPPrzybycieWagon;
{ Test sprawdzaj�cy czy klasa obs�uguje interfejs IBkSAPPrzybycieWagonDane (u�yj metod� Supports z unitu SysUtils)}
//  https://stackoverflow.com/questions/4418278/use-of-supports-function-with-generic-interface-type
//  http://docs.embarcadero.com/products/rad_studio/delphiAndcpp2009/HelpUpdate2/EN/html/delphivclwin32/SysUtils_Supports@IInterface@TGUID.html
var
 UCHWYT_DO_INTERFACE_IBkSAPPrzybycieWagonDane:IBkSAPPrzybycieWagonDane;
begin
 UCHWYT_DO_INTERFACE_IBkSAPPrzybycieWagonDane:=TBkSAPPrzybycieWagon.Create;
 UCHWYT_DO_INTERFACE_IBkSAPPrzybycieWagonDane.Czysc;
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Czysc_dla_klasy_TBkSAPPrzybycieWagon', '']));
end;

{ Ttest_dla_klasy_TBkSAPPrzybycieWagon }

procedure Ttest_dla_klasy_TBkSAPPrzybycieWagon.Czy_klasa_obslug_interf_dla_TBkSAPPrzybycieWagon;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Czy_klasa_obslug_interf_dla_TBkSAPPrzybycieWagon', '']));
end;

{ Ttest_dla_klasy_TBkSAPPrzybycieWagon }

procedure Ttest_dla_klasy_TBkSAPPrzybycieWagon.Test_metody_dane_dla_TBkSAPPrzybycieWagon;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Test_metody_dane_dla_TBkSAPPrzybycieWagon', '']));
end;

{ Ttest_dla_klasy_TBkSAPPrzybycieWagon }

procedure Ttest_dla_klasy_TBkSAPPrzybycieWagon.Test_metody_ustaw_funkcja_puste_dla_TBkSAPPrzybycieWagon;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Test_metody_ustaw_funkcja_puste_dla_TBkSAPPrzybycieWagon', '']));
end;

{ Ttest_dla_klasy_TBkSAPPrzybycieWagon }

procedure Ttest_dla_klasy_TBkSAPPrzybycieWagon.Test_metody_ustaw_zmienione_dane_recordu_dla_TBkSAPPrzybycieWagon;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Test_metody_ustaw_zmienione_dane_recordu_dla_TBkSAPPrzybycieWagon', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Czysc_dla_klasy_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Czysc_dla_klasy_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Czy_klasa_obslug_interf_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Czy_klasa_obslug_interf_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_Dane_dla_1_elementu_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_Dane_dla_1_elementu_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_Dane_dla_3_elementow_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_Dane_dla_3_elementow_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_Dane_dla_10_elementow_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_Dane_dla_10_elementow_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_DanePodstawienie_pusta_struk_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_DanePodstawienie_pusta_struk_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_DanePodstawienie_1_record_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_DanePodstawienie_1_record_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_DanePodstawienie_2_record_dla_TBkSAPPrzybycieWagonyDane1;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_DanePodstawienie_2_record_dla_TBkSAPPrzybycieWagonyDane1', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_GetOperacja_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_GetOperacja_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_SetOperacja_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_SetOperacja_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_Pusty_tworzenie_obiektu_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_Pusty_tworzenie_obiektu_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_Pusty_dodanie_wart_metoda_ustaw_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_Pusty_dodanie_wart_metoda_ustaw_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_Ustaw_funkcja_Dane_dla_TBkSAPPrzybycieWagonyDane1;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_Ustaw_funkcja_Dane_dla_TBkSAPPrzybycieWagonyDane1', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_Ustaw_dwukr_ta_sama_wartosc_czy_Dane_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_Ustaw_dwukr_ta_sama_wartosc_czy_Dane_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

 { TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane }

procedure TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.Metoda_Ustaw_dwukr_ta_sama_wartosc_czy_Zmieniony_dla_TBkSAPPrzybycieWagonyDane;
begin
//CheckEquals(, , Format('%s.%s: %s', [ClassName, 'Metoda_Ustaw_dwukr_ta_sama_wartosc_czy_Zmieniony_dla_TBkSAPPrzybycieWagonyDane', '']));
end;

initialization
  RegisterTests([TTest_dla_recordu.suite, Ttest_dla_klasy_TBkSAPPrzybycieWagon.suite, TTest_dla_dla_klasy_TBkSAPPrzybycieWagonyDane.suite]);

end.


{

  procedure RECORDIX;

begin
uchwyt_record.PodstawienieId:= 1;
uchwyt_record.PobytId:= 2;
uchwyt_record.Numer:= '3';
uchwyt_record.DostawcaId:= 4;
uchwyt_record.LadunekId:= 5;
uchwyt_record.MasaNadawcy:= 6;
uchwyt_record.MasaKolei:= 7;
uchwyt_record.CzasPrzyjecia:= strtodate('2016-10-11');
uchwyt_record.ListId:= 8;
uchwyt_record.ListCzasNadania:= strtodate('2016-10-11');
//uchwyt_record.ZamowienieNumer:= 9;
uchwyt_record.ZamowieniePozycja:= 10;  // TODO -cMM: RECORDIX default body inserted


 stringl.add(inttostr(uchwyt_record.PodstawienieId));
stringl.add(inttostr(uchwyt_record.PobytId));
stringl.add(uchwyt_record.Numer);
stringl.add(inttostr(uchwyt_record.DostawcaId));
stringl.add(inttostr(uchwyt_record.LadunekId));
stringl.add(inttostr(uchwyt_record.MasaNadawcy));
stringl.add(inttostr(uchwyt_record.MasaKolei));
stringl.add(DateToStr(uchwyt_record.CzasPrzyjecia));
stringl.add(inttostr(uchwyt_record.ListId));
stringl.add(DateToStr(uchwyt_record.ListCzasNadania));
stringl.add(inttostr(uchwyt_record.ZamowienieNumer));
stringl.add(inttostr(uchwyt_record.ZamowieniePozycja));
 Memo1.Lines.Assign(stringl);
end;    }



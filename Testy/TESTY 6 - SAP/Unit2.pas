﻿unit Unit2;

interface

uses
  SynCommons,
  mORMot,
   System.Generics.Collections;

type
  TBkWagonPodstawienieId = TID;

    /// <summary>
    /// Rekord z danymi podstawienia wagonu
    /// </summary>
  TBkWagonPodstawionyDane = record
    PodstawienieId: TID;
    PobytId: TID;
    Numer: String;
    DostawcaId: TID;
    LadunekId: TID;
    MasaNadawcy: Integer;
    MasaKolei: Integer;
    CzasPrzyjecia: TDateTime;
    ListId: TID;
    ListCzasNadania: TDateTime;
    ZamowienieNumer: RawUTF8;
    ZamowieniePozycja: Integer;
  end;

    /// <summary>
    /// Rekord z danymi wagonu z przybycia.
    /// </summary>
  TBkSAPPrzybycieWagonDane = record
    Dane: TBkWagonPodstawionyDane;
    StareZamowienieSAPNr: RawUTF8;
    StareZamowienieSAPPozycja: Integer;
      /// <summary>
      /// Wyczyszczenie danych w rekordzie.
      /// </summary>
    procedure Czysc;
      /// <summary>
      /// Procedura umożliwiająca przepisanie danych rekordu do rekordu używanego do wywołań funkcji serwisu
      /// aplikacyjnego.
      /// </summary>
    procedure PrzepiszDo(var ADane: TBkWagonPodstawionyDane; const AZamowienieStare: Boolean = False);
      /// <summary>
      /// Funkcja zwraca wartość True, gdy w rekordzie przekazanym jako parametr są różnice w porównaniu danych w
      /// rekordzie.
      /// </summary>
    function ZmianaDanych(const ADane: TBkSAPPrzybycieWagonDane): Boolean;
      /// <summary>
      /// Funkcja zwraca wartość True, gdy na podstawie pól można ustalić, że była wykonywana zmiana zamówienia.
      /// </summary>
    function ZmianaZamowienia: Boolean;
  end;

    /// <summary>
    /// Typ enumeryczny umożliwiający określenie rodzaju danych przechowywanych w strukturze.
    /// </summary>
  TBkSAPWagonyOperacja = (woPodstawienie, woZmiana, woUsun);

    /// <summary>
    /// Interfejs dla struktur danych umożliwiających
    /// </summary>
  IBkSAPPrzybycieWagonDane = interface(IInvokable)
    ['{6925A6BD-588D-41F5-ACEE-26CB734264A0}']
      /// <summary>
      /// Usunięcie danych.
      /// </summary>
    procedure Czysc;
      /// <summary>
      /// Funkcja zwraca rekord z danymi przechowywanymi w strukturze.
      /// </summary>
    function Dane: TBkWagonPodstawionyDane;
      /// <summary>
      /// Funkcja zwraca True, gdy struktura nie zawiera danych.
      /// </summary>
    function Pusty: Boolean;
      /// <summary>
      /// Procedura umożliwiająca wpisanie danych wagonu do struktury
      /// </summary>
    procedure Ustaw(const ADane: TBkSAPPrzybycieWagonDane);
      /// <summary>
      /// Funkcja zwraca wartość True, gdy dane zostały ustawione wielokrotnie i wystąpiła ich zmiana.
      /// Wywołanie funkcji czyść powinno wyczyścić flagę informującą o zmianie danych.
      /// </summary>
    function Zmienione: Boolean;
  end;

  IBkSAPPrzybycieWagonyDane = interface(IInvokable)
    ['{B6AFFB1E-EC0E-4999-9A59-CA344B2C3AE3}']
      /// <summary>
      /// Wyczyszczenie struktury danych.
      /// </summary>
    procedure Czysc;
      /// <summary>
      /// Funkcja zwraca referencję do instancji danych przechowywanej dla wagonu o identyfikatorze podanym w parametrze
      /// AId.
      /// </summary>
    function Dane(const AId: TBkWagonPodstawienieId): IBkSAPPrzybycieWagonDane;
      /// <summary>
      /// Funkcja zwraca referencję do pojedynczej instancji danych przechowywanej w strukturze.
      /// </summary>
    function DanePodstawienia: IBkSAPPrzybycieWagonDane;
      /// <summary>Obsługa własności Operacja</summary>
    function GetOperacja: TBkSAPWagonyOperacja;
      /// <summary>
      /// Funkcja zwraca wartość True, gdy struktura o wagonach zawiera jakieś dane
      /// </summary>
    function Pusty: Boolean;
      /// <summary>Obsługa własności Operacja</summary>
    procedure SetOperacja(const AValue: TBkSAPWagonyOperacja);
      /// <summary>
      /// Ustawienie wartości dla wagonu o identyfikatorze podstawienia podanym w parametrze AId.
      /// </summary>
    procedure Ustaw(const ADane: TBkSAPPrzybycieWagonDane);
      /// <summary>
      /// Własność z informacją o rodzaju wykonywanej operacji na danych (podstawienie/zmiana danych)
      /// </summary>
    property Operacja: TBkSAPWagonyOperacja read GetOperacja write SetOperacja;
  end;

    /// <summary>
    /// Klasa umożliwiająca obsługę struktury danych umożliwiającej przekazywanie danych o modyfikowanych wagonach.
    /// </summary>
  TBkSAPPrzybycieWagon = class(TInterfacedObject, IBkSAPPrzybycieWagonDane)
  strict private
    FDane: TBkSAPPrzybycieWagonDane;
    FDaneUstawione: Boolean;
    FDaneZmienione: Boolean;
  public
      /// <summary>
      /// Usunięcie danych.
      /// </summary>
    procedure Czysc;
      /// <summary>
      /// Funkcja zwraca rekord z danymi przechowywanymi w strukturze.
      /// </summary>
    function Dane: TBkWagonPodstawionyDane;
      /// <summary>
      /// Funkcja zwraca True, gdy struktura nie zawiera danych.
      /// </summary>
    function Pusty: Boolean;
      /// <summary>
      /// Procedura umożliwiająca wpisanie danych wagonu do struktury
      /// </summary>
    procedure Ustaw(const ADane: TBkSAPPrzybycieWagonDane);
      /// <summary>
      /// Funkcja zwraca wartość True, gdy dane zostały ustawione wielokrotnie i wystąpiła ich zmiana.
      /// Wywołanie funkcji czyść powinno wyczyścić flagę informującą o zmianie danych.
      /// </summary>
    function Zmienione: Boolean;
  end;

type
    /// <summary>
    /// Klasa ze strukturą danych umożliwiającą przechowywanie informacji o danych wagonów przekazywanych do systemu SAP.
    /// </summary>
  TBkSAPPrzybycieWagonyDane = class(TInterfacedObject, IBkSAPPrzybycieWagonyDane)
  strict private
    FOperacja: TBkSAPWagonyOperacja;
    FWagony: TDictionary<TBkWagonPodstawienieId, IBkSAPPrzybycieWagonDane>;
  public
    constructor Create;
    destructor Destroy; override;
      /// <summary>
      /// Wyczyszczenie struktury danych.
      /// </summary>
    procedure Czysc;
      /// <summary>
      /// Funkcja zwraca referencję do instancji danych przechowywanej dla wagonu o identyfikatorze podanym w parametrze
      /// AId.
      /// </summary>
    function Dane(const AId: TBkWagonPodstawienieId): IBkSAPPrzybycieWagonDane;
      /// <summary>
      /// Funkcja zwraca referencję do pojedynczej instancji danych przechowywanej w strukturze.
      /// </summary>
    function DanePodstawienia: IBkSAPPrzybycieWagonDane;
      /// <summary>Obsługa własności Operacja</summary>
    function GetOperacja: TBkSAPWagonyOperacja;
      /// <summary>
      /// Funkcja zwraca wartość True, gdy struktura o wagonach zawiera jakieś dane
      /// </summary>
    function Pusty: Boolean;
      /// <summary>Obsługa własności Operacja</summary>
    procedure SetOperacja(const AValue: TBkSAPWagonyOperacja);
      /// <summary>
      /// Ustawienie wartości dla wagonu o identyfikatorze podstawienia podanym w parametrze AId.
      /// </summary>
    procedure Ustaw(const ADane: TBkSAPPrzybycieWagonDane);
  end;



implementation

uses
  System.SysUtils;

{ TBkSAPPrzybycieWagonDane }

procedure TBkSAPPrzybycieWagonDane.Czysc;
begin
  FillChar(Self, SizeOf(Self), #0);
end;

procedure TBkSAPPrzybycieWagonDane.PrzepiszDo(var ADane: TBkWagonPodstawionyDane; const AZamowienieStare: Boolean);
begin
  ADane := Self.Dane;
  if AZamowienieStare then
  begin
    ADane.ZamowienieNumer := Self.StareZamowienieSAPNr;
    ADane.ZamowieniePozycja := Self.StareZamowienieSAPPozycja;
  end;
end;

function TBkSAPPrzybycieWagonDane.ZmianaDanych(const ADane: TBkSAPPrzybycieWagonDane): Boolean;
var
  lZamowienieNr: RawUTF8;
  lZamowieniePozycja: Integer;
begin
  Result := (ADane.Dane.PodstawienieId <> Self.Dane.PodstawienieId) or
    (ADane.Dane.PobytId <> Self.Dane.PobytId) or
    (ADane.Dane.Numer <> Self.Dane.Numer) or
    (ADane.Dane.DostawcaId <> Self.Dane.DostawcaId) or
    (ADane.Dane.LadunekId <> Self.Dane.LadunekId) or
    (ADane.Dane.MasaNadawcy <> Self.Dane.MasaNadawcy) or
    (ADane.Dane.MasaKolei <> Self.Dane.MasaKolei) or
    (ADane.Dane.CzasPrzyjecia <> Self.Dane.CzasPrzyjecia) or
    (ADane.Dane.ListId <> Self.Dane.ListId) or
    (ADane.Dane.ListCzasNadania <> Self.Dane.ListCzasNadania);
  if not Result then
  begin
    lZamowienieNr := Self.Dane.ZamowienieNumer;
    if lZamowienieNr = '' then
      lZamowienieNr := Self.StareZamowienieSAPNr;
    Result := lZamowienieNr <> ADane.Dane.ZamowienieNumer;
    if Result then
      Exit;
    lZamowieniePozycja := Self.Dane.ZamowieniePozycja;
    if lZamowieniePozycja = 0 then
      lZamowieniePozycja := Self.StareZamowienieSAPPozycja;
    Result := lZamowieniePozycja <> ADane.Dane.ZamowieniePozycja;
  end;
end;

function TBkSAPPrzybycieWagonDane.ZmianaZamowienia: Boolean;
begin
  Result := (Dane.ZamowienieNumer <> StareZamowienieSAPNr) or (Dane.ZamowieniePozycja <> StareZamowienieSAPPozycja);
end;

{ TBkSAPPrzybycieWagon }

procedure TBkSAPPrzybycieWagon.Czysc;
begin
  FillChar(FDane, SizeOf(FDane), #0);
  FDaneUstawione := False;
  FDaneZmienione := False;
end;

function TBkSAPPrzybycieWagon.Dane: TBkWagonPodstawionyDane;
begin
  FDane.PrzepiszDo(Result);
end;

function TBkSAPPrzybycieWagon.Pusty: Boolean;
begin
  Result := not FDaneUstawione;
end;

procedure TBkSAPPrzybycieWagon.Ustaw(const ADane: TBkSAPPrzybycieWagonDane);
var
  b: Boolean;
begin
  if FDaneUstawione then
  begin
    b := FDane.ZmianaDanych(ADane);
    if b then
      FDaneZmienione := True;
  end
  else
    FDaneUstawione := True;
  FDane := ADane;
end;

function TBkSAPPrzybycieWagon.Zmienione: Boolean;
begin
  Result := FDaneZmienione;
end;

{ TBkSAPPrzybycieWagonyDane }

constructor TBkSAPPrzybycieWagonyDane.Create;
begin
  inherited Create;
  FWagony := TDictionary<TBkWagonPodstawienieId, IBkSAPPrzybycieWagonDane>.Create;
  FOperacja := woPodstawienie;
end;

destructor TBkSAPPrzybycieWagonyDane.Destroy;
begin
  FWagony.Free;
  inherited;
end;

procedure TBkSAPPrzybycieWagonyDane.Czysc;
begin
  FWagony.Clear;
  FOperacja := woPodstawienie;
end;

function TBkSAPPrzybycieWagonyDane.Dane(const AId: TBkWagonPodstawienieId): IBkSAPPrzybycieWagonDane;
begin
  Result := FWagony[AId];
end;

function TBkSAPPrzybycieWagonyDane.DanePodstawienia: IBkSAPPrzybycieWagonDane;
var
  lPair: TPair<TBkWagonPodstawienieId, IBkSAPPrzybycieWagonDane>;
begin
  if FWagony.Count > 1 then
    raise Exception.Create('Dodawanie wielu podstawień wagonów nie jest obsługiwane.');
  for lPair in FWagony do
    Result := lPair.Value;
end;

function TBkSAPPrzybycieWagonyDane.GetOperacja: TBkSAPWagonyOperacja;
begin
  Result := FOperacja;
end;

function TBkSAPPrzybycieWagonyDane.Pusty: Boolean;
begin
  Result := FWagony.Count = 0;
end;

procedure TBkSAPPrzybycieWagonyDane.SetOperacja(const AValue: TBkSAPWagonyOperacja);
begin
  FOperacja := AValue;
end;

procedure TBkSAPPrzybycieWagonyDane.Ustaw(const ADane: TBkSAPPrzybycieWagonDane);
var
  lPodstawienieId: TBkWagonPodstawienieId;
  lDane: IBkSAPPrzybycieWagonDane;
begin
  lPodstawienieId := ADane.Dane.PodstawienieId;
  Assert(lPodstawienieId > 0);   // assert - upewnij sie ze
  if not FWagony.TryGetValue(lPodstawienieId, lDane) then
  begin
    lDane := TBkSAPPrzybycieWagon.Create;
    FWagony.Add(lPodstawienieId, lDane);
  end;
  lDane.Ustaw(ADane);
end;

end.

17:31
Traptak
Cze��
17:31
Ja
hej
17:33
Traptak
Pode�l� Ci troch� kodu do testowania
17:33
Ja
oki, fajnie.
jakbys mial czas, to na weekend moglibysmy zrobic sesje 
mam 2 nieduze tematy 
17:35
Traptak
Spr�bujmy
17:36
Ja
chyba w sobote byloby najlepiej jakby Ci pasowa�o
mam dzisiaj i jutro popoludniowke, a w sobote powinieniem miec wolne, jak nic nie wypadnie
17:37
Traptak
Ok. Ale po po�udniu
17:37
Ja
oki, to bede w sobote patrzy� na gg czy jestes
i sie odezwe popoludniu
17:43
Traptak
Jakby co to pu�� mi strza�k�, bo czesto jestem z dala od komputera
Tak po 15-tej powinienem by� w domu
17:44
Ja
oki 
17:58
Traptak
Tym razem do test�w przekazuj� rekord i dwie klasy.
17:58
Ja
obejrzymy 
18:01
zapowiada sie ciekawie
18:11
Traptak
B�dziesz musia� zainstalowa� jedn� bibliotek� potrzebn� do typ�w danych oraz drug� obs�uguj�c� elementy Mock i Stub potrzebne do test�w.
18:14
Ja
oki, 
18:17
Traptak
Chocia� mo�e bez tych stub�w i mock�w si� obejdzie.
18:29
Wys�a�em Ci opis i kod
18:30
Ja
czytam sobie o tym mock, co to jest, ale po angielsku dosc skomplikowane jest to 
18:30
Traptak
W sumie powinno si� obej�� nawet bez instalacji tej dodatkowej biblioteki no i mock�w i stub�w. To sobie zostawimy na przysz�o��
18:31
Ja
zrozumialem, ze to atrapa, �eby nie by�o potrzeby �aczyc sie z baza danych
18:31
Traptak
Mock to taki element kodu, kt�ry pozwala Ci zast�pi� prawdziwy obiekt lub interfejs w celu wykonania test�w jednostkowych
18:32
Ja
oki, zajrze do poczty cos przyszlo
18:32
Traptak
Je�li wi�c jeden obiekt wykorzystuje inny obiekt do dzia�ania, to testuj�c ten pierwszy mo�esz u�y� mocka, kt�ry zast�pi Ci ten drugi obiekt. Dzi�ki temu do test�w nie musisz u�ywa� rzeczywistych obiekt�w, co czasami jest skomplikowane
Chyba w tej ksi��ce nicka hodgesa jest rozdzia� o testowaniu - tam to jest opisane.
18:34
Ja
no mam wlasnie, znalazlem w necie:
http://www.nickhodges.com/post/Delphi-Mocks-The-Basics.aspx
zerkne w tej swojej ksiazce jutro czy cos jest
18:36
Traptak
A to co Ci wys�a�em to klasy, kt�re u�ywamy obecnie do integracji z SAPem w GA
18:39
Ja
fajnie, ciekawe rzeczy
18:47
Traptak
Sporo tego mam a pokrycie kodu testami jest niewielkie, wi�c ch�tnie si� robot� podziel�.
19:10
Ja
na razie dostaj� oczoplasu, ale powoli sobie wszystko usystematyzuj�
19:13
w tym miejsce mam dylemat :  FWagony: TDictionary<TBkWagonPodstawienieId, IBkSAPPrzybycieWagonDane>;
bo to wyglada, na pobranie danych z bazy
20:04
w sumie to sam juz nie wiem  nurtuje mnie ten wczesniejszy zapis: type
  TBkWagonPodstawienieId = TID;
20:06
Traptak
Ten TDictionary to struktura przechowuj�ca dane w pami�ci w uk�adzie klucz-warto��. Jak masz klucz to mo�esz szybko odnale�� powi�zan� z nim warto��.
Co do TBkWagonPodstawienieId to typ przechowuj�cy identyfikatory rekord�w z bazy danych. Dok�adnie z tabeli podstawie� wagon�w na bocznic�.
Zawiera 64-ro bitow� warto�� liczbow�
Jest to�samy z typem TID. Typ TID to r�wnie� 64-ro bitowa warto�� liczbowa identyfikatora rekordu ale z dowolnej tabeli.
Natomiast TBkWagonPodstawienieId okre�la dok�adnie, �e chodzi o tabel� podstawie� wagon�w.
20:08
Ja
no ja to rozumiem, typ enumeryczny, tylko nie kumam skad wezme te dane jak nie lacze sie z �adna baz�
20:09
Traptak
Nie to nie jest typ enumeryczny. Zobacz, �e w definicji nie masz wymienionych element�w typu. To normalna warto�� liczbowa
20:10
Mo�esz napisa� kod: 
var
  lId:  TBkWagonPodstawienieId;
begin
  lId := 15432;
...
i Delphi Ci to skompiluje. Z typem enumerycznym czego� takiego nie zrobisz - b�dzie si� domaga� elementu okre�lonego w definicji
20:11
Ja
aaaaaha
20:12
Traptak
On przechowuje warto�� liczbow�, kt�ra normalnie jest w bazie danych. Ale nic nie stoi na przeszkodzie, �eby wpisa� mu dowoln� warto�� w kodzie na potrzeby test�w. Ten kod, kt�ry masz do przetestowania nie wykonuje zapis�w do bazy danych. On tylko przetwarza w pami�ci dane, kt�re normalnie s� zapisane w bazie danych. Testujemy jedynie spos�b przetwarzania danych. To czy one wezm� si� bazy danych czy innego �r�d�a nie jest przedmiotem testu. Przyjmujemy �e po prostu s�.
20:14
Ja
oki, ja myslalem, ze zaciaga z bazy dane do TDictionary
i nimi potem operujemy
20:15
Traptak
No tak w rzeczywisto�ci si� dzieje, ale robi to inny kod ni� ten, kt�ry masz do test�w. 
20:16
Ja
no to juz mi lepiej, bo nie moglem tego zatrybic 
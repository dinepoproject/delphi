unit GxCurrUtils;

interface

const
  cMoneyFormat    = ',0.00';
  cMoneyIntFormat = ',0';
  cDecimalPlaces  = 2;

    /// <summary>
    /// Funkcja zaokr�glaj�ca warto�� kwoty do pe�nych groszy. Warto�� u�amka grosza 5 lub wi�ksza powinna zosta� 
    /// zaokr�glona do pe�nego grosza.
    /// </summary>
  function RoundMoney(const AValue: Currency): Currency; overload;

implementation

uses
  System.Math;
  //   http://4programmers.net/Delphi/Absolute
 //   http://4programmers.net/Delphi/Dec
// http://4programmers.net/Delphi/Mod

var
  RoundMove: Integer;
  TruncValue: Currency = 0.5;

function RoundMoney(const AValue: Currency): Currency;
var
  V64: Int64 absolute Result;
  Decimals: Integer;
begin
  Result := AValue;
  Decimals := V64 mod 100;
  Dec(V64, Decimals);
  case Decimals of
    -99 .. -50 : Dec(V64, 100);
    50 .. 99 : Inc(V64, 100);
  end;
end;

initialization
  RoundMove := Trunc(IntPower(10, cDecimalPlaces));
end.

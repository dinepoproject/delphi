unit URoundMoney.test;

interface

uses
  System.SysUtils, TestFramework, System.DateUtils, Vcl.Dialogs;

type
  TTest_RoundMoney = class(TTestCase)
  private
  published
    procedure kwota_1_01;
    procedure kwota_0_05_w_dol;
    procedure kwota_0_05_w_gore;
    procedure kwota_1_99_w_dol;
    procedure kwota_1_99_w_gore;
    procedure kwota_2_00_w_dol;
    procedure kwota_3_00_w_dol;
    procedure kwota_4_00_w_dol;
    procedure kwota_3_00_w_dol_nast_zero;
    procedure kwota_3_00_w_gore;
    procedure kwota_100_01_w_dol;
    procedure kwota_100_01_w_gore;
    procedure kwota_minus_100_01_w_gore;
    procedure kwota_minus_100_01_w_dol;
    procedure kwota_minus_3_00_w_dol;
  end;

implementation

uses
  GxCurrUtils;

{ TTest_RoundMoney }

procedure TTest_RoundMoney.kwota_0_05_w_dol;
var
  kwota: Currency;
begin
  kwota := 0.05;
  CheckEquals(kwota, RoundMoney(0.05004855), Format('%s.%s: %s', [ClassName, 'kwota_0_05', '']));
end;

procedure TTest_RoundMoney.kwota_0_05_w_gore;
var
  kwota: Currency;
begin
  kwota := 0.05;
  CheckEquals(kwota, RoundMoney(0.04504855), Format('%s.%s: %s', [ClassName, 'kwota_0_05_w_gore', '']));
end;

procedure TTest_RoundMoney.kwota_100_01_w_dol;
var
  kwota: Currency;
begin
  kwota := 100.01;
  CheckEquals(kwota, RoundMoney(100.0149), Format('%s.%s: %s', [ClassName, 'kwota_100_00_w_dol', '']));
end;

procedure TTest_RoundMoney.kwota_100_01_w_gore;
var
  kwota: Currency;
begin
  kwota := 100.01;
  CheckEquals(kwota, RoundMoney(100.0095), Format('%s.%s: %s', [ClassName, 'kwota_100_01_w_gore', '']));
end;

procedure TTest_RoundMoney.kwota_1_01;
var
  kwota: Currency;
begin
  kwota := 1.01;
  CheckEquals(kwota, RoundMoney(1.01004855), Format('%s.%s: %s', [ClassName, 'zaokraglij', '']));

end;

procedure TTest_RoundMoney.kwota_1_99_w_dol;
var
  kwota: Currency;
begin
  kwota := 1.99;
  CheckEquals(kwota, RoundMoney(1.9912342), Format('%s.%s: %s', [ClassName, 'kwota_1_99_w_dol', '']));
end;

procedure TTest_RoundMoney.kwota_1_99_w_gore;
var
  kwota: Currency;
begin
  kwota := 1.99;
  CheckEquals(2.00, RoundMoney(1.9992342), Format('%s.%s: %s', [ClassName, 'kwota_1_99_w_gore', '']));
end;

procedure TTest_RoundMoney.kwota_2_00_w_dol;
var
  kwota: Currency;
begin
  kwota := 2.00;
  CheckEquals(kwota, RoundMoney(2.0049), Format('%s.%s: %s', [ClassName, 'kwota_2_00_w_dol', '']));
end;

procedure TTest_RoundMoney.kwota_3_00_w_dol;
var
  kwota: Currency;
begin
  kwota := 3.00;
  CheckEquals(kwota, RoundMoney(3.0049), Format('%s.%s: %s', [ClassName, 'kwota_3_00_w_dol', '']));
end;

procedure TTest_RoundMoney.kwota_3_00_w_dol_nast_zero;
var
  kwota: Currency;
begin
  kwota := 3.00;
  CheckEquals(kwota, RoundMoney(3.00049), Format('%s.%s: %s', [ClassName, 'kwota_3_00_w_dol_nast_zero', '']));
end;

procedure TTest_RoundMoney.kwota_3_00_w_gore;
var
  kwota: Currency;
begin
  kwota := 3.00;
  CheckEquals(kwota, RoundMoney(2.9994), Format('%s.%s: %s', [ClassName, 'kwota_3_00_w_gore', '']));
end;

procedure TTest_RoundMoney.kwota_4_00_w_dol;
var
  kwota: Currency;
begin
  kwota := 4.00;
  CheckEquals(kwota, RoundMoney(4.0048), Format('%s.%s: %s', [ClassName, 'kwota_4_00_w_dol', '']));
end;

procedure TTest_RoundMoney.kwota_minus_100_01_w_dol;
var
  kwota: Currency;
begin
  kwota := -100.01;
  CheckEquals(kwota, RoundMoney(-100.0101), Format('%s.%s: %s', [ClassName, 'kwota_minus_100_01_w_dol', '']));
end;

procedure TTest_RoundMoney.kwota_minus_100_01_w_gore;
var
  kwota: Currency;
begin
  kwota := -100.01;
  CheckEquals(kwota, RoundMoney(-100.0095), Format('%s.%s: %s', [ClassName, 'kwota_minus_100_01', '']));
end;

procedure TTest_RoundMoney.kwota_minus_3_00_w_dol;
var
  kwota: Currency;
begin
  kwota := -3.00;
  CheckEquals(kwota, RoundMoney(-3.0029), Format('%s.%s: %s', [ClassName, 'kwota_minus_3_00_w_dol', '']));
end;

initialization
  RegisterTest(TTest_RoundMoney.suite);

end.


unit uGxStrToDateTime.Test;

interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestGxStrToDateTime = class(TTestCase)
   /// <summary>
    /// Konwersja �a�cucha na dat� i czas - poprawnie pracuj�ca z rozszerzon� obs�ug� b��d�w
    /// </summary>
    /// Testy do wykonania:
    ///  1. Przekazanie poprawnie sformatowanej daty i czasu a nast�pnie sprawdzenie czy zwr�cony wynik jest poprawny
    ///  2. Przekazanie niepoprawnej daty i czasu a nast�pnie sprawdzenie, czy funkcja wywo�a wyj�tek klasy GxConvertError.
    published

 procedure  Przekazanie_poprawnej_daty_czasu_sprawdz_wynik;
 procedure  Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_rok;
 procedure  Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_miesiac;
 procedure  Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_dzien;
 procedure  Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_godzina;
 procedure  Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_minuta;
 procedure  Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_sekunda;
 procedure  Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_milisekunda;


end;
implementation
uses
GxDateUtils;

 { TtestGxStrToDateTime }




procedure TtestGxStrToDateTime.Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_dzien;


begin
   ExpectedException := GxConvertError;


  GxStrToDateTime('2000-12-32-12:30:30.0');

end;

procedure TtestGxStrToDateTime.Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_godzina;
begin
   ExpectedException := GxConvertError;

  GxStrToDateTime('2000-12-30-25:30:30.0');

end;

procedure TtestGxStrToDateTime.Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_miesiac;
begin
   ExpectedException := GxConvertError;

  GxStrToDateTime('2000-14-30-12:30:30.0');

end;

procedure TtestGxStrToDateTime.Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_milisekunda;
begin
  ExpectedException := GxConvertError;

  GxStrToDateTime('2000-12-30-12:30:30.1000');

end;

procedure TtestGxStrToDateTime.Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_minuta;
begin
   ExpectedException := GxConvertError;

  GxStrToDateTime('2000-12-30-12:70:30.0');
end;

procedure TtestGxStrToDateTime.Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_rok;
begin
  ExpectedException := GxConvertError;

  GxStrToDateTime('10000-12-30-12:30:30.0');

end;

procedure TtestGxStrToDateTime.Przekazanie_niepoprawnej_daty_czasu_sprawdz_wyjatek_sekunda;
begin
   ExpectedException := GxConvertError;

  GxStrToDateTime('2000-12-30-12:30:70.0');
end;

procedure TtestGxStrToDateTime.Przekazanie_poprawnej_daty_czasu_sprawdz_wynik;
var
 dataSprawdzana: TDatetime;
 lDataTekst: String;
begin
dataSprawdzana:= Encodedatetime(2000,12,20,12,30,30,0);
   //dataSprawdzana:=strtodatetime('2000-12-20-12-30-30-0') ;
lDataTekst := DateTimeToStr(dataSprawdzana);
CheckEquals(dataSprawdzana,GxStrToDateTime(lDataTekst),Format('%s.%s: %s', [ClassName, 'Przekazanie_poprawnej_daty_czasu_sprawdz_wynik', '']));





end;

initialization
RegisterTest ( TtestGxStrToDateTime.suite);
end.


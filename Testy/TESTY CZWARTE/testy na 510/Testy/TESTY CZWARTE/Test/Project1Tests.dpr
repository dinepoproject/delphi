program Project1Tests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  GxDateUtils in '..\GxDateUtils.pas',
  uGxStrToDate.Test in 'uGxStrToDate.Test.pas',
  uGxStrToDateTime.Test in 'uGxStrToDateTime.Test.pas',
  uMinToText.test in 'uMinToText.test.pas',
  uMinDateTime.Test in 'uMinDateTime.Test.pas',
  uMaxDateTime.Test in 'uMaxDateTime.Test.pas',
  uMinToText_Tformatsettings.test in 'uMinToText_Tformatsettings.test.pas';

{$R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.


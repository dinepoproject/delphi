unit uGxStrToDate.Test;

interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestGxStrToDate = class(TTestCase)
  private
//    procedure Do_wpisania;
    /// <summary>
    /// Konwersja �a�cucha na dat� - poprawnie pracuj�ca z rozszerzon� obs�ug� b��d�w
    /// </summary>
    /// Testy do wykonania:
    ///  1. Przekazanie poprawnie sformatowanej daty i sprawdzenie czy zwr�cony wynik jest poprawny
    ///  2. Przekazanie niepoprawnej daty i sprawdzenie, czy funkcja wywo�a wyj�tek klasy GxConvertError.
    published

 procedure  Przekazanie_poprawnej_daty_sprawdz_wynik;
 procedure  Przekazanie_niepoprawnej_daty_sprawdz_wyjatek_rok;
 procedure  Przekazanie_niepoprawnej_daty_sprawdz_wyjatek_miesiac;
 procedure  Przekazanie_niepoprawnej_daty_sprawdz_wyjatek_dzien;
 procedure  Przekazanie_niepoprawnego_separatora;
 procedure  Przekazanie_daty_ujemnej;
 procedure  Przekazanie_podwojnego_separatora;
 procedure  Przekazanie_kropki_po_dacie;
//
//
end;
implementation
uses
GxDateUtils;

   { TtestGxStrToDate}






procedure TtestGxStrToDate.Przekazanie_daty_ujemnej;
begin
 ExpectedException := GxConvertError;

   GxStrToDate('-2000-02-20');
end;

procedure TtestGxStrToDate.Przekazanie_kropki_po_dacie;
begin
 ExpectedException := GxConvertError;

   GxStrToDate('2000-02-20.');
end;

procedure TtestGxStrToDate.Przekazanie_niepoprawnego_separatora;
begin
 ExpectedException := GxConvertError;

   GxStrToDate('2000:02:20');
end;

procedure TtestGxStrToDate.Przekazanie_niepoprawnej_daty_sprawdz_wyjatek_dzien;
begin
  ExpectedException := GxConvertError;

   GxStrToDate('2000-02-30');
end;

procedure TtestGxStrToDate.Przekazanie_niepoprawnej_daty_sprawdz_wyjatek_miesiac;
begin
   ExpectedException := GxConvertError;

   GxStrToDate('2000-13-15');
end;

procedure TtestGxStrToDate.Przekazanie_niepoprawnej_daty_sprawdz_wyjatek_rok;
begin
  ExpectedException := GxConvertError;

   GxStrToDate('10000-01-15');

end;

procedure TtestGxStrToDate.Przekazanie_podwojnego_separatora;
begin
 ExpectedException := GxConvertError;

   GxStrToDate('2000--02-20');
end;

procedure TtestGxStrToDate.Przekazanie_poprawnej_daty_sprawdz_wynik;
var
 dataSprawdzana: TDate;
begin
dataSprawdzana:= Encodedate(2000,12,20);
CheckEquals(dataSprawdzana,GxStrToDate('2000-12-20'), Format('%s.%s: %s', [ClassName, 'Przekazanie_poprawnej_daty_sprawdz_wynik', '']));
end;

initialization
RegisterTest ( TtestGxStrToDate.suite);
end.
   {
    ExpectedException := GxConvertError;
   try
     GxEncodeDateSmart(2000,01,00);
   except
     on E: Exception do
     begin
     CheckEquals(Format(SDataBladDzien, [2000, 01, 31]), E.Message, Format('%s.%s: %s', [ClassName, 'Test_Czy_Wyjatek_Gdy_Dzien_Mniejszy_Niz_1', '']));
     raise;
     end;
   end;

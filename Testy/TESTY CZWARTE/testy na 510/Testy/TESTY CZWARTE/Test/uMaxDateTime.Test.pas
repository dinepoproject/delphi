unit uMaxDateTime.Test;

interface
 uses
 System.SysUtils, TestFramework, System.DateUtils;
 type
TtestMaxDateTime = class(TTestCase)
  private

    /// <summary>
    /// Funkcja zwraca wi�ksz� z dat podanych jako argumenty.
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test dla r�wnych warto�ci daty i czasu - funkcja powinna zwr�ci� jedn� z nich.
    ///  2. Test gdy ADate1 jest wi�ksza od ADate2 - funkcja powinna zwr�ci� ADate1.
    ///  3. Test gdy ADate2 jest wi�ksza od ADate1 - funkcja powinna zwr�ci� ADate2.
    published

 procedure  Rowne_wartosci_daty_czasu;
 procedure ADate1_wieksza_od_Adate2_zwraca_Adate1_Rok;
 procedure ADate2_wieksza_od_ADate1_zwraca_ADate2_Rok;
 procedure ADate1_wieksza_od_Adate2_zwraca_Adate1_Miesiac;
 procedure ADate2_wieksza_od_ADate1_zwraca_ADate2_Miesiac;
 procedure ADate1_wieksza_od_Adate2_zwraca_Adate1_Dzien;
 procedure ADate2_wieksza_od_ADate1_zwraca_ADate2_Dzien;
 procedure ADate1_wieksza_od_Adate2_zwraca_Adate1_Godzina;
 procedure ADate2_wieksza_od_ADate1_zwraca_ADate2_Godzina;
 procedure ADate1_wieksza_od_Adate2_zwraca_Adate1_Minuta;
 procedure ADate2_wieksza_od_ADate1_zwraca_ADate2_Minuta;
 procedure ADate1_wieksza_od_Adate2_zwraca_Adate1_Sekunda;
 procedure ADate2_wieksza_od_ADate1_zwraca_ADate2_Sekunda;
 procedure ADate1_wieksza_od_Adate2_zwraca_Adate1_Setna;
 procedure ADate2_wieksza_od_ADate1_zwraca_ADate2_Setna;
end;
implementation
uses
GxDateUtils;

{ TtestMaxDateTime }



procedure TtestMaxDateTime.ADate1_wieksza_od_Adate2_zwraca_Adate1_Dzien;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
 dataSprawdzana1:= Encodedatetime(2015,12,31,12,30,30,0);

 dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,30,0);
 CheckEquals(dataSprawdzana1,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate1_wieksza_od_Adate2_zwraca_Adate1_Dzien', '']));
end;

procedure TtestMaxDateTime.ADate1_wieksza_od_Adate2_zwraca_Adate1_Godzina;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
 dataSprawdzana1:= Encodedatetime(2015,12,20,13,30,30,0);

 dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,30,0);
CheckEquals(dataSprawdzana1,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate1_wieksza_od_Adate2_zwraca_Adate1_Godzina', '']));
end;

procedure TtestMaxDateTime.ADate1_wieksza_od_Adate2_zwraca_Adate1_Miesiac;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
 dataSprawdzana1:= Encodedatetime(2015,12,20,12,30,30,0);

 dataSprawdzana2:= Encodedatetime(2015,11,20,12,30,30,0);
 CheckEquals(dataSprawdzana1,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate1_wieksza_od_Adate2_zwraca_Adate1_Miesiac', '']));
end;

procedure TtestMaxDateTime.ADate1_wieksza_od_Adate2_zwraca_Adate1_Minuta;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
 dataSprawdzana1:= Encodedatetime(2015,12,20,12,40,30,0);

 dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,30,0);
 CheckEquals(dataSprawdzana1,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate1_wieksza_od_Adate2_zwraca_Adate1_Minuta', '']));
end;

procedure TtestMaxDateTime.ADate1_wieksza_od_Adate2_zwraca_Adate1_Rok;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
 dataSprawdzana1:= Encodedatetime(2016,12,20,12,30,30,0);

 dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,30,0);
 CheckEquals(dataSprawdzana1,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate1_wieksza_od_Adate2_zwraca_Adate1_Rok', '']));
end;

procedure TtestMaxDateTime.ADate1_wieksza_od_Adate2_zwraca_Adate1_Sekunda;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
 dataSprawdzana1:= Encodedatetime(2015,12,20,12,30,50,0);

 dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,30,0);
 CheckEquals(dataSprawdzana1,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate1_wieksza_od_Adate2_zwraca_Adate1_Sekunda', '']));
end;

procedure TtestMaxDateTime.ADate1_wieksza_od_Adate2_zwraca_Adate1_Setna;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
 dataSprawdzana1:= Encodedatetime(2015,12,20,12,30,30,100);

 dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,30,50);
 CheckEquals(dataSprawdzana1,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate1_wieksza_od_Adate2_zwraca_Adate1_Setna', '']));
end;

procedure TtestMaxDateTime.ADate2_wieksza_od_ADate1_zwraca_ADate2_Rok;
 var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
  dataSprawdzana1:= Encodedatetime(2015,12,20,12,30,30,0);

  dataSprawdzana2:= Encodedatetime(2016,12,20,12,30,30,0);
 CheckEquals(dataSprawdzana2,MaxDateTime(dataSprawdzana1,dataSprawdzana2),Format('%s.%s: %s', [ClassName, 'ADate2_wieksza_od_ADate1_zwraca_ADate2', '']));
end;

procedure TtestMaxDateTime.ADate2_wieksza_od_ADate1_zwraca_ADate2_Dzien;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
  dataSprawdzana1:= Encodedatetime(2015,12,20,12,30,30,0);

  dataSprawdzana2:= Encodedatetime(2015,12,30,12,30,30,0);
 CheckEquals(dataSprawdzana2,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate2_wieksza_od_ADate1_zwraca_ADate2_Dzien', '']));
end;

procedure TtestMaxDateTime.ADate2_wieksza_od_ADate1_zwraca_ADate2_Godzina;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
  dataSprawdzana1:= Encodedatetime(2015,12,20,10,30,30,0);

  dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,30,0);
 CheckEquals(dataSprawdzana2,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate2_wieksza_od_ADate1_zwraca_ADate2_Godzina', '']));
end;

procedure TtestMaxDateTime.ADate2_wieksza_od_ADate1_zwraca_ADate2_Miesiac;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
  dataSprawdzana1:= Encodedatetime(2015,11,20,12,30,30,0);

  dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,30,0);
 CheckEquals(dataSprawdzana2,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate2_wieksza_od_ADate1_zwraca_ADate2_Miesiac', '']));
end;

procedure TtestMaxDateTime.ADate2_wieksza_od_ADate1_zwraca_ADate2_Minuta;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
  dataSprawdzana1:= Encodedatetime(2015,12,20,12,30,30,0);

  dataSprawdzana2:= Encodedatetime(2015,12,20,12,40,30,0);
 CheckEquals(dataSprawdzana2,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate2_wieksza_od_ADate1_zwraca_ADate2_Minuta', '']));
end;



procedure TtestMaxDateTime.ADate2_wieksza_od_ADate1_zwraca_ADate2_Sekunda;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
  dataSprawdzana1:= Encodedatetime(2015,12,20,12,30,30,0);

  dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,40,0);
 CheckEquals(dataSprawdzana2,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate2_wieksza_od_ADate1_zwraca_ADate2_Sekunda', '']));
end;

procedure TtestMaxDateTime.ADate2_wieksza_od_ADate1_zwraca_ADate2_Setna;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;
begin
  dataSprawdzana1:= Encodedatetime(2015,12,20,12,30,30,50);

  dataSprawdzana2:= Encodedatetime(2015,12,20,12,30,30,100);
 CheckEquals(dataSprawdzana2,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'ADate2_wieksza_od_ADate1_zwraca_ADate2_Setna', '']));
end;

procedure TtestMaxDateTime.Rowne_wartosci_daty_czasu;
var
 dataSprawdzana1: TDatetime;
 dataSprawdzana2: TDatetime;

begin
  dataSprawdzana1:= Encodedatetime(2015,12,20,12,30,30,0);

  dataSprawdzana2:= Encodedatetime(2016,12,20,12,30,30,0);


 CheckEquals(dataSprawdzana2,MaxDateTime(dataSprawdzana1,dataSprawdzana2) , Format('%s.%s: %s', [ClassName, 'Rowne_wartosci_daty_czasu', '']));
end;

initialization
RegisterTest ( TtestMaxDateTime.suite);
end.


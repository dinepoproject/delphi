unit uMinToText_Tformatsettings.test;

interface
 uses
 WinApi.Windows,System.SysUtils, TestFramework, System.DateUtils,Winapi.Messages;
 type
TtestMinToText_Tformatsettings = class(TTestCase)
/// <summary>
    /// Funkcja konwertuje czas podany w minutach na �a�cuch tekstowy o formacie: hhh:nn. Wersja u�ywaj�ca podanego
    /// formatu czasu.
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test dla zera minut: czy zostanie zwr�cony wynik: 0:00
    ///  2. Test dla 59 minut: czy zostanie zwr�cony wynik: 0:59
    ///  3. Test dla 60 minut: czy zostanie zwr�cony wynik: 1:00
    ///  4. Test dla 92 minut: czy zostanie zwr�cony wynik: 1:32
    ///  5. Test dla ilo�ci minut, dla kt�rej ilo�� godzin b�dzie wi�ksza od 1000: sprawdzamy czy liczba godzin
    ///     b�dzie sformatowana z separatorem tysi�cznym innym ni� standardowy - separator tysi�czny
    ///     nale�y poda� w strukturze w parametrze AFormatSettings.
    ///  6. Test sprawdzaj�cy czy funkcja prawid�owo obs�uguje zmian� systemowego separatora czasu - separator czasu
    ///     nale�y poda� w strukturze w parametrze AFormatSettings.
    published

 procedure  Zero_minut;
 procedure  Piecdziesiat_dziewiec_minut;
 procedure  Szescdziesiat_minut;
 procedure  Dziewiecdziesiat_dwie_minuty;
 procedure  Ilosc_minut_gdy_ilosc_godzin_wieksza_od_1000;
 procedure  Czy_funkcja_prawidlowo_obsluguje_separator_czasu;




end;
implementation
uses
GxDateUtils;






{ TtestMinToText }

procedure TtestMinToText_Tformatsettings.Czy_funkcja_prawidlowo_obsluguje_separator_czasu;
 var
FS:Tformatsettings;
{tmpTS: String;
newTS: String; }
begin
FS.TimeSeparator := '#';
 {tmpTS := GetLocaleStr(GetThreadLocale, LOCALE_STIME, ':');
  try
    newTS := '#';
    SetLocaleInfo(GetThreadLocale, LOCALE_STIME, PWideChar(newTS));  }

     //Tformatsettings.Create;
       //FS.Create;

  CheckEquals('11#40' ,MinToText (700,FS) , Format('%s.%s: %s', [ClassName, 'Czy_funkcja_prawidlowo_obsluguje_separator_czasu', '']));
  {finally
   SetLocaleInfo(GetThreadLocale, LOCALE_STIME, PWideChar(tmpTS));
  end;
  {var
FS:Tformatsettings;
begin
 //Tformatsettings.Create;
 //FS.Create;
 FS.TimeSeparator := '#';

     CheckEquals('1�166#40' ,MinToText (70000,fs) , Format('%s.%s: %s', [ClassName, 'Czy_funkcja_prawidlowo_obsluguje_separator_czasu', '']));
     }
  end;



procedure TtestMinToText_Tformatsettings.Dziewiecdziesiat_dwie_minuty;
var
FS:Tformatsettings;

begin
 FS.TimeSeparator := '#';
 CheckEquals('1#32',MinToText(92,fs), Format('%s.%s: %s', [ClassName, 'Dziewiecdziesiat_dwie_minuty', '']));
end;

procedure TtestMinToText_Tformatsettings.Ilosc_minut_gdy_ilosc_godzin_wieksza_od_1000;
var
FS:Tformatsettings;
  lExpected: string;
begin
  FS := TFormatSettings.Create;
 FS.TimeSeparator := '#';
 FS.ThousandSeparator := '.';
 lExpected := '1.166#40';
 CheckEquals(lExpected,MinToText(70000,fs), Format('%s.%s: %s', [ClassName, 'Ilosc_minut_gdy_ilosc_godzin_wieksza_od_1000', '']));
end;

procedure TtestMinToText_Tformatsettings.Piecdziesiat_dziewiec_minut;
var
FS:Tformatsettings;

begin
 FS.TimeSeparator := '#';
 CheckEquals('0#59',MinToText(59,fs), Format('%s.%s: %s', [ClassName, 'Piecdziesiat_dziewiec_minut', '']));
end;

procedure TtestMinToText_Tformatsettings.Szescdziesiat_minut;
var
FS:Tformatsettings;

begin
 FS.TimeSeparator := '#';
 CheckEquals('1#00',MinToText(60,fs), Format('%s.%s: %s', [ClassName, 'Szescdziesiat_minut', '']));
end;

procedure TtestMinToText_Tformatsettings.Zero_minut;
var
FS:Tformatsettings;

begin
 FS.TimeSeparator := '#';
 CheckEquals('0#00',MinToText(0,fs) , Format('%s.%s: %s', [ClassName, 'Zero_minut', '']));
end;

initialization
RegisterTest ( TtestMinToText_Tformatsettings.suite);
end.
 {var
FS:Tformatsettings;
minuty: string;
tmpTS: String;
newTS: String;
begin
  getlocaleformatsettings (1045,fs) ;
  //FS.TimeSeparator := '#';
  tmpTS := GetLocaleStr(GetThreadLocale, LOCALE_STIME, ':');
  try
      // Ustawienie # jako separatora czasu
    newTS := '#';
       //newTS := ';';
    SetLocaleInfo(GetThreadLocale, LOCALE_STIME, PWideChar(newTS));
      // Od tej linii masz zmienione ustawienia
    //FS.Create;
      Tformatsettings.Create;
     minuty:=  MinToText (70000);
      //getlocaleformatsettings (1045,fs) ;
  finally
      // Przywr�cenie domy�lnego separatora daty
    SetLocaleInfo(GetThreadLocale, LOCALE_STIME, PWideChar(tmpTS));
  end;}

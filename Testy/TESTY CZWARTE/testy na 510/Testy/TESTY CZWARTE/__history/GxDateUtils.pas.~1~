unit GxDateUtils;

interface

uses
  System.SysUtils;

type
    // Specjalna klasa b��d�w umo�liwiaj�ca prawid�owe zg�aszanie komunikat�w o niepoprawnych
    // warto�ciach kierowanych do konwersji - zapewnia prawid�owe dzia�anie podczas rozszerzonej
    // obs�ugi b��d�w.
  GxConvertError = class(Exception);

    /// <summary>
    /// Konwersja �a�cucha na dat� - poprawnie pracuj�ca z rozszerzon� obs�ug� b��d�w
    /// </summary>
    /// Testy do wykonania:
    ///  1. Przekazanie poprawnie sformatowanej daty i sprawdzenie czy zwr�cony wynik jest poprawny
    ///  2. Przekazanie niepoprawnej daty i sprawdzenie, czy funkcja wywo�a wyj�tek klasy GxConvertError.
  function GxStrToDate(const AValue: String): TDate;

    /// <summary>
    /// Konwersja �a�cucha na dat� i czas - poprawnie pracuj�ca z rozszerzon� obs�ug� b��d�w
    /// </summary>
    /// Testy do wykonania:
    ///  1. Przekazanie poprawnie sformatowanej daty i czasu a nast�pnie sprawdzenie czy zwr�cony wynik jest poprawny
    ///  2. Przekazanie niepoprawnej daty i czasu a nast�pnie sprawdzenie, czy funkcja wywo�a wyj�tek klasy GxConvertError.
  function GxStrToDateTime(const AValue: String): TDateTime;

    /// <summary>
    /// Funkcja konwertuje czas podany w minutach na �a�cuch tekstowy o formacie: hhh:nn
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test dla zera minut: czy zostanie zwr�cony wynik: 0:00
    ///  2. Test dla 59 minut: czy zostanie zwr�cony wynik: 0:59
    ///  3. Test dla 60 minut: czy zostanie zwr�cony wynik: 1:00
    ///  4. Test dla 92 minut: czy zostanie zwr�cony wynik: 1:32
    ///  5. Test dla ilo�ci minut, dla kt�rej ilo�� godzin b�dzie wi�ksza od 1000: sprawdzamy czy liczba godzin
    ///     b�dzie sformatowana z separatorem tysi�cznym.
    ///  6. Test sprawdzaj�cy czy funkcja prawid�owo obs�uguje zmian� systemowego separatora czasu. (to troch� skomplikowany
    ///     test - b�dziemy musieli zrobi� go razem, bo nie wiem czy dasz rad� dobra� si� do funkcji, kt�ra umo�liwia
    ///     zmian� domy�lnego separatora czasu)
  function  MinToText(const AMin: Integer): String;

    /// <summary>
    /// Funkcja zwraca mniejsz� z dat podanych jako argumenty.
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test dla r�wnych warto�ci daty i czasu - funkcja powinna zwr�ci� jedn� z nich.
    ///  2. Test gdy ADate1 jest wi�ksza od ADate2 - funkcja powinna zwr�ci� ADate2.
    ///  3. Test gdy ADate2 jest wi�ksza od ADate1 - funkcja powinna zwr�ci� ADate1.
  function  MinDateTime(const ADate1, ADate2: TDateTime): TDateTime;

    /// <summary>
    /// Funkcja zwraca wi�ksz� z dat podanych jako argumenty.
    /// </summary>
    /// Testy do wykonania:
    ///  1. Test dla r�wnych warto�ci daty i czasu - funkcja powinna zwr�ci� jedn� z nich.
    ///  2. Test gdy ADate1 jest wi�ksza od ADate2 - funkcja powinna zwr�ci� ADate1.
    ///  3. Test gdy ADate2 jest wi�ksza od ADate1 - funkcja powinna zwr�ci� ADate2.
  function  MaxDateTime(const ADate1, ADate2: TDateTime): TDateTime;

type
    /// <summary>
    /// Typ wyliczeniowy umo�liwiaj�cy okre�lenie jednostki czasu, w kt�rej jest obliczany interwa� czasu.
    /// </summary>
  TGxIntervalUnit = (iuSecond, iuMinute, iuHour);

    /// <summary>
    /// Rekord danych umo�liwiaj�cy przechowywanie danych interwa�u czasu.
    /// </summary>
  TGxInterval = record
  private
      // Warto�� interwa�u. Wewn�trznie interwa� jest zawsze przechowywany w sekundach.
    FValue: Int64;
    FValueUnit: TGxIntervalUnit;
  public
      /// <summary>
      /// Obs�uga operatora por�wnania
      /// </summary>
    class operator Equal(AValue1: TGxInterval; AValue2: Integer): Boolean;
      /// <summary>
      /// Operator umo�liwiaj�cy przepisanie warto�ci typu TDateTime do warto�ci interwa�u.
      /// </summary>
    class operator Implicit(AValue: TDateTime): TGxInterval;
      /// <summary>
      /// Operator umo�liwiaj�cy przepisanie warto�ci typu Integer do warto�ci interwa�u.
      /// </summary>
    class operator Implicit(AValue: Integer): TGxInterval;
      /// <summary>
      /// Operator umo�liwiaj�cy przepisanie warto�ci interwa�u do warto�ci typu Integer.
      /// </summary>
    class operator Implicit(AValue: TGxInterval): Integer;
      /// <summary>
      /// Operator umo�liwiaj�cy przepisanie warto�ci interwa�u do warto�ci typu TDateTime.
      /// </summary>
    class operator Implicit(AValue: TGxInterval): TDateTime;
      /// <summary>
      /// Funkcja umo�liwiaj�ca konwersj� interwa�u z warto�ci typu Integer
      /// </summary>
    procedure FromInteger(const AValue: Integer; const AUnit: TGxIntervalUnit);
      /// <summary>
      /// Procedura umo�liwiaj�ca konwersj� interwa�u z warto�ci typu String
      /// </summary>
    procedure FromString(AValue: String; const AMaxUnit, AMinUnit: TGxIntervalUnit);
      /// <summary>
      /// Funkcja umo�liwiaj�ca konwersj� interwa�u do warto�ci typu Integer
      /// </summary>
    function ToInteger(const AUnit: TGxIntervalUnit): Integer;
      /// <summary>
      /// Funkcja umo�liwiaj�ca konwersj� interwa�u do warto�ci typu Integer
      /// </summary>
    function ToString(const AMaxUnit: TGxIntervalUnit = iuHour; const AMinUnit: TGxIntervalUnit = iuMinute;
      const AUseThousandSep: Boolean = True): String;
      /// <summary>
      /// W�asno�� umo�liwiaj�ca uzyskanie dost�pu do wewn�trznej warto�ci interwa�u - g��wnie w celach testowych.
      /// </summary>
    property InternalValue: Int64 read FValue;
      /// <summary>
      /// Flaga umo�liwiaj�ca okre�lenie w jakich jednostkach interwa� ma by� widziany na zewn�trz.
      /// </summary>
    property ValueUnit: TGxIntervalUnit read FValueUnit write FValueUnit;
  end;

  procedure IntervalUnitNotSupported(const AUnit: TGxIntervalUnit);

    /// <summary>
    /// Funkcja umo�liwia przeliczenie interwa�u w formacie TDateTime do interwa�u podanego w okre�lonych jednostkach
    /// czasu.
    /// </summary>
  function IntervalFromDateTime(ADate: TDateTime; const AUnit: TGxIntervalUnit = iuMinute): Int64;

    /// <summary>
    /// Funkcja umo�liwia przeliczenie interwa�u podanego w okre�lonych jednostkach czasu do warto�ci typu TDateTime.
    /// </summary>
  function  IntervalToDateTime(AValue: Integer; const AUnit: TGxIntervalUnit = iuMinute): TDateTime;

implementation

uses
  System.DateUtils, Winapi.Windows, System.TypInfo, JclStrings, System.SysConst, System.Character, System.Math,
  System.StrUtils;

const
  cMinPHour = 60;
  cHourPDay = 24;


function GxStrToDateTime(const AValue: String): TDateTime;
begin
  try
    Result := StrToDateTime(AValue)
  except
    on E: EConvertError do
      raise GxConvertError.Create(E.Message);
  end;
end;

function GxStrToDate(const AValue: String): TDate;
begin
  try
    Result := StrToDate(AValue)
  except
    on E: EConvertError do
      raise GxConvertError.Create(E.Message);
  end;
end;

function MinToText(const AMin: Integer): String;
var
  lInterval: TGxInterval;
begin
  lInterval.FromInteger(AMin, iuMinute);
  Result := lInterval.ToString;
end;

function MinDateTime(const ADate1, ADate2: TDateTime): TDateTime;
begin
  Result := ADate1;
  if Result > ADate2 then
    Result := ADate2;
end;

function  MaxDateTime(const ADate1, ADate2: TDateTime): TDateTime;
begin
  Result := ADate1;
  if Result < ADate2 then
    Result := ADate2;
end;

procedure IntervalUnitNotSupported(const AUnit: TGxIntervalUnit);
begin
  raise Exception.CreateFmt('Interval unit: %s not supported.', [GetEnumName(TypeInfo(TGxIntervalUnit), Integer(AUnit))]);
end;

function GxStrToInt(const AText: String): Integer;
var
  E: Integer;
begin
  Val(AText, Result, E);
  if E <> 0 then
    raise GxConvertError.CreateFmt(SInvalidInteger, [AText]);
end;

function StripNumberThousandSep(const ANumber: string; const AFormatSettings: TFormatSettings;
  AExtraThousandSepChars: string = ''): String; overload;

  function CheckCiphers(var ACurrPos: Integer; const ASepPos: Integer; const ACheckStr: String): Boolean;
  begin
    repeat
      if ACurrPos <= 0 then
        Break;
      if not ACheckStr[ACurrPos].IsDigit then
      begin
        Result := False;
        Exit;
      end;
      Dec(ACurrPos);
    until ACurrPos <= ASepPos;
    Result := True;
  end;

var
  i, k, decimalPos: Integer;
  thousandSep: Char;
begin
  Result := ANumber;
  if Pos(AFormatSettings.ThousandSeparator, AExtraThousandSepChars) = 0 then
    AExtraThousandSepChars := AExtraThousandSepChars + AFormatSettings.ThousandSeparator;
    // Odszukanie pozycji separatora dziesi�tnego
  decimalPos := Length(ANumber) + 1;
  for i := Length(ANumber) downto 1 do
    if ANumber[i] = AFormatSettings.DecimalSeparator then
    begin
      decimalPos := i;
      Break;
    end;
  k := Min(decimalPos - 1, Length(ANumber));

    // Odszukanie hipotetycznej pozycji pierwszego od prawej separatora tysi�cznego
  Dec(decimalPos, 4);
  if decimalPos > 0 then
  begin
    thousandSep := ANumber[decimalPos];
      // Gdy znak odczytany z pozycji separatora nie jest na li�cie dopuszczalnych separator�w...
    if Pos(thousandSep, AExtraThousandSepChars) = 0 then
      Exit;
  end
  else
    Exit;

    // Przej�cie i usuni�cie separator�w z wyniku
  repeat
      // Kontrola czy pomi�dzy separatorami znajduj� si� wy��cznie cyfry
    if not CheckCiphers(k, decimalPos, Result) then
    begin
      Result := ANumber;
      Exit;
    end;

      // Usuni�cie separatora
    if Result[decimalPos] = thousandSep then
    begin
      Delete(Result, decimalPos, 1);
      Dec(k);
    end
    else
    begin
        // Gdy nie ma okre�lonego separatora w okre�lonym miejscu anulujemy wprowadzone zmiany i ko�czymy
        // dzia�anie funkcji
      Result := ANumber;
      Exit;
    end;
      // Przej�cie do kolejnej pozycji z separatorem.
    Dec(decimalPos, 4);
  until decimalPos <= 0;

  if not CheckCiphers(k, decimalPos, Result) then
    Result := ANumber;
end;

function StripNumberThousandSep(const ANumber: string; AExtraThousandSepChars: string = ''): String; overload;
var
  localFormatSettings: TFormatSettings;
begin
  localFormatSettings := TFormatSettings.Create;
  Result := StripNumberThousandSep(ANumber, localFormatSettings, AExtraThousandSepChars);
end;

function IntervalFromDateTime(ADate: TDateTime; const AUnit: TGxIntervalUnit = iuMinute): Int64;
var
  d: LongWord;
  h, m, s, ms: Word;
  negative: Boolean;
begin
  negative := ADate < 0;
  if negative then
    ADate := Abs(ADate);
  d := Trunc(ADate);
  DecodeTime(ADate, h, m, s, ms);

  Result := 0;
  case AUnit of
    iuSecond:
      Result := s + (m * 60) + (h * cMinPHour * 60) + (d * cHourPDay * cMinPHour * 60);
    iuMinute:
      Result := m + (h * cMinPHour) + (d * cHourPDay * cMinPHour);
    iuHour:
      Result := h + (d * cHourPDay);
    else
      IntervalUnitNotSupported(AUnit);
  end;
    // Negacja warto�ci
  if negative then
    Result := 0 - Result;
end;

function IntervalToDateTime(AValue: Integer; const AUnit: TGxIntervalUnit = iuMinute): TDateTime;
var
  d, h, m, s: Word;
  negative: Boolean;
begin
  d := 0;
  h := 0;
  s := 0;
  m := 0;
    // Obs�uga warto�ci ujemnych - dla interwa��w jest to mo�liwe.
  negative := AValue < 0;
  if negative then
    AValue := 0 - AValue;

  case AUnit of
    iuSecond:
    begin
      s := AValue mod 60;
      AValue := AValue div 60;
      m := AValue mod cMinPHour;
      AValue := AValue div cMinPHour;
      h := AValue mod cHourPDay;
      d := AValue div cHourPDay;
    end;
    iuMinute:
    begin
      m := AValue mod cMinPHour;
      AValue := AValue div cMinPHour;
      h := AValue mod cHourPDay;
      d := AValue div cHourPDay;
    end;
    iuHour:
    begin
      h := AValue mod cHourPDay;
      d := AValue div cHourPDay;
    end
    else
      IntervalUnitNotSupported(AUnit);
  end;
  Result := d + EncodeTime(h, m, s, 0);
  if negative then
    Result := 0 - Result;
end;

{ TGxInterval }

class operator TGxInterval.Equal(AValue1: TGxInterval; AValue2: Integer): Boolean;
begin
  Result := AValue1.ToInteger(AValue1.ValueUnit) = AValue2;
end;

class operator TGxInterval.Implicit(AValue: TGxInterval): Integer;
begin
  Result := AValue.ToInteger(AValue.ValueUnit);
end;

procedure TGxInterval.FromInteger(const AValue: Integer; const AUnit: TGxIntervalUnit);
begin
  case AUnit of
    iuSecond:
      FValue := AValue;
    iuMinute:
      FValue := AValue * 60;
    iuHour:
      FValue := AValue * cMinPHour * 60;
    else
      IntervalUnitNotSupported(AUnit);
  end;
end;

procedure TGxInterval.FromString(AValue: String; const AMaxUnit, AMinUnit: TGxIntervalUnit);
var
  text: String;
  tmpValue: Int64;
  i: TGxIntervalUnit;
begin
  if AMaxUnit = AMinUnit then
    FromInteger(GxStrToInt(StripNumberThousandSep(AValue)), AMaxUnit)
  else
  begin
    i := AMaxUnit;
    tmpValue := 0;
    while True do
    begin
      if i > AMinUnit then
        text := StrBefore(FormatSettings.TimeSeparator, AValue)
      else
        text := AValue;
      FromInteger(GxStrToInt(StripNumberThousandSep(text)), i);
      Inc(tmpValue, FValue);
      AValue := StrAfter(FormatSettings.TimeSeparator, AValue);
      if i <= AMinUnit then
        Break
      else
        Dec(i);
    end;
    FValue := tmpValue;
  end;
end;

class operator TGxInterval.Implicit(AValue: TDateTime): TGxInterval;
begin
  Result.FromInteger(IntervalFromDateTime(AValue, iuSecond), iuSecond);
end;

class operator TGxInterval.Implicit(AValue: TGxInterval): TDateTime;
begin
  Result := IntervalToDateTime(AValue.FValue, iuSecond);
end;

function TGxInterval.ToInteger(const AUnit: TGxIntervalUnit): Integer;
begin
  Result := 0;
  case AUnit of
    iuSecond:
      Result := FValue;
    iuMinute:
      Result := FValue div 60;
    iuHour:
      Result := FValue div (cMinPHour * 60);
    else
      IntervalUnitNotSupported(AUnit);
  end;
end;

function TGxInterval.ToString(const AMaxUnit, AMinUnit: TGxIntervalUnit; const AUseThousandSep: Boolean): String;
var
  format, formatMax: string;
  i: TGxIntervalUnit;
  j: Integer;
begin
  if AUseThousandSep then
    formatMax := ',0'
  else
    formatMax := '0';
    // Je�li interwa� ma by� wy�wietlony w pojedynczych jednostkach
  if AMinUnit = AMaxUnit then
    Result := FormatFloat(formatMax, ToInteger(AMaxUnit))
  else
  begin
    i := AMinUnit;
    format := '';
    while i < AMaxUnit do
    begin
      case i of
        iuSecond:
          format := 'ss' + IfThen(Length(format) > 0, FormatSettings.TimeSeparator) + format;
        iuMinute:
          format := 'nn' + IfThen(Length(format) > 0, FormatSettings.TimeSeparator) + format;
        iuHour:
          format := 'hh' + IfThen(Length(format) > 0, FormatSettings.TimeSeparator) + format;
        else
          IntervalUnitNotSupported(i);
      end;
      Inc(i);
    end;
      // Sformatowanie ko�c�wki warto�ci
    Result := FormatDateTime(format, Self);
      // Sformatowanie g��wnej warto�ci interwa�u
    j := ToInteger(AMaxUnit);
    Result := FormatFloat(formatMax, j) + FormatSettings.TimeSeparator + Result;
  end;
end;

class operator TGxInterval.Implicit(AValue: Integer): TGxInterval;
begin
  Result.FromInteger(AValue, Result.ValueUnit);
end;




end.

unit GxCurrUtils;

interface

const
  cMoneyFormat    = ',0.00';
  cMoneyIntFormat = ',0';
  cDecimalPlaces  = 2;

    /// <summary>
    /// Funkcja zaokr�glaj�ca warto�� kwoty do pe�nych groszy. Warto�� u�amka grosza 5 lub wi�ksza powinna zosta� 
    /// zaokr�glona do pe�nego grosza.
    /// </summary>
  function RoundMoney(const AValue: Currency): Currency; overload;

implementation

uses
  System.Math;

var
  RoundMove: Integer;
  TruncValue: Currency = 0.5;

function RoundMoney(const AValue: Currency): Currency;
begin
  Result := Trunc(AValue * RoundMove + TruncValue) / RoundMove;
end;

initialization
  RoundMove := Trunc(IntPower(10, cDecimalPlaces));
end.

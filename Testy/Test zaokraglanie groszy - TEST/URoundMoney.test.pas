unit URoundMoney.test;

interface
 uses
 System.SysUtils, TestFramework, System.DateUtils, Vcl.Dialogs;
 type
TRoundMoney = class(TTestCase)
  private

    published


 procedure  zaokraglij;
  //http://4programmers.net/Delphi/StrToIntDef

 procedure  kwota_1_01;
 procedure  kwota_0_05_w_dol;
 procedure  kwota_0_05_w_gore;
 procedure  kwota_1_99_w_dol;
 procedure  kwota_1_99_w_gore;
 procedure  kwota_2_00_w_dol;
 procedure  kwota_3_00_w_dol;
 procedure  kwota_4_00_w_dol;
 procedure  kwota_3_00_w_dol_nast_zero;
 procedure  kwota_3_00_w_gore;
 procedure  kwota_100_01_w_dol;
 procedure  kwota_100_01_w_gore;
 procedure  kwota_minus_100_01_w_gore;
 procedure  kwota_minus_100_01_w_dol;
 procedure  kwota_minus_3_00_w_dol;
 procedure zakrog_funkcja;


 function losuj (wej:Integer):string;
 var


end;
implementation
uses
GxCurrUtils;

{ TRoundMoney }


//thousendseparator :=
function TRoundMoney.losuj(wej: Integer): string;
  var
a:Integer;
b:Integer;

begin
Randomize;


 begin
 b:= Random(wej);
 result:= ( '00241,14' + IntToStr(b))
 end;
 end;
procedure TRoundMoney.zakrog_funkcja;
var
kwota_po_zaokr:Currency;
a:Integer;
los:currency;
fs:TFormatSettings;

begin
 fs.CurrencyDecimals:= 2;
 fs.DecimalSeparator := ',';
for a := 1 to 2 do


begin
kwota_po_zaokr:= 00241.14;
los:= StrToCurr(losuj(4));
CheckEquals(kwota_po_zaokr,(RoundMoney((los))) , Format('%s.%s: %s', [ClassName, 'zakrog_funckja', '']));
//showmessage (currtostr(los));
end;
end;

procedure TRoundMoney.kwota_0_05_w_dol;
var
kwota:Currency;
begin
kwota:= 0.05;
CheckEquals(kwota,(RoundMoney(0.05004855))  , Format('%s.%s: %s', [ClassName, 'kwota_0_05', '']));
end;

procedure TRoundMoney.kwota_0_05_w_gore;
var
kwota:Currency;
begin
kwota:= 0.05;
CheckEquals(kwota,(RoundMoney(0.04504855)) , Format('%s.%s: %s', [ClassName, 'kwota_0_05_w_gore', '']));
end;

procedure TRoundMoney.kwota_100_01_w_dol;
var
kwota:Currency;
begin
kwota:= 100.01;
CheckEquals(kwota,(RoundMoney(100.014988855)) , Format('%s.%s: %s', [ClassName, 'kwota_100_00_w_dol', '']));
end;

procedure TRoundMoney.kwota_100_01_w_gore;
var
kwota:Currency;
begin
kwota:= 100.01;
CheckEquals(kwota,(RoundMoney(100.009504855))  , Format('%s.%s: %s', [ClassName, 'kwota_100_01_w_gore', '']));
end;

procedure TRoundMoney.kwota_1_01;
var
kwota:Currency;
begin
kwota:= 1.01;
    CheckEquals(kwota,(RoundMoney(1.01004855)) , Format('%s.%s: %s', [ClassName, 'zaokraglij', '']));

    end;

procedure TRoundMoney.kwota_1_99_w_dol;
var
kwota:Currency;
begin
kwota:= 1.99;
CheckEquals(kwota,(RoundMoney(1.9912342)) , Format('%s.%s: %s', [ClassName, 'kwota_1_99_w_dol', '']));
end;

procedure TRoundMoney.kwota_1_99_w_gore;
var
kwota:Currency;
begin
kwota:= 1.99;
CheckEquals(2.00,(RoundMoney(1.9992342))  , Format('%s.%s: %s', [ClassName, 'kwota_1_99_w_gore', '']));
end;

procedure TRoundMoney.kwota_2_00_w_dol;
var
kwota:Currency;
begin
kwota:= 2.00;
CheckEquals(kwota,(RoundMoney(2.0049992342))  , Format('%s.%s: %s', [ClassName, 'kwota_2_00_w_dol', '']));
end;

procedure TRoundMoney.kwota_3_00_w_dol;
var
kwota:Currency;
begin
kwota:= 3.01;
CheckEquals(kwota,(RoundMoney(3.0049992342))  , Format('%s.%s: %s', [ClassName, 'kwota_3_00_w_dol', '']));
end;

procedure TRoundMoney.kwota_3_00_w_dol_nast_zero;
var
kwota:Currency;
begin
kwota:= 3.00;
CheckEquals(kwota,(RoundMoney(3.00049992342)) , Format('%s.%s: %s', [ClassName, 'kwota_3_00_w_dol_nast_zero', '']));
end;

procedure TRoundMoney.kwota_3_00_w_gore;
var
kwota:Currency;
begin
kwota:= 3.00;
CheckEquals(kwota,(RoundMoney(2.99949992342))  , Format('%s.%s: %s', [ClassName, 'kwota_3_00_w_gore', '']));
end;

procedure TRoundMoney.kwota_4_00_w_dol;
var
kwota:Currency;
begin
kwota:= 4.00;
CheckEquals(kwota,(RoundMoney(4.0048992342)) , Format('%s.%s: %s', [ClassName, 'kwota_4_00_w_dol', '']));
end;

procedure TRoundMoney.kwota_minus_100_01_w_dol;
var
kwota:Currency;
begin
kwota:= -100.01;
CheckEquals(kwota,(RoundMoney(-100.010104855))  , Format('%s.%s: %s', [ClassName, 'kwota_minus_100_01_w_dol', '']));
end;

procedure TRoundMoney.kwota_minus_100_01_w_gore;
var
kwota:Currency;
begin
kwota:= -100.01;
CheckEquals(kwota,(RoundMoney(-100.009504855))  , Format('%s.%s: %s', [ClassName, 'kwota_minus_100_01', '']));
end;

procedure TRoundMoney.kwota_minus_3_00_w_dol;
var
kwota:Currency;
begin
kwota:= -3.00;
 CheckEquals(kwota,(RoundMoney(-3.004904855)) , Format('%s.%s: %s', [ClassName, 'kwota_minus_3_00_w_dol', '']));
end;

initialization
RegisterTest ( TRoundMoney.suite);

end.
  //http:4programmers.net/Delphi/StrToIntDef
  // rozwiazaniem jest zastosowanie - 0,5 przy liczbach ujemnych.
  // b�ad 49 w ujemnych nie wystepuje.

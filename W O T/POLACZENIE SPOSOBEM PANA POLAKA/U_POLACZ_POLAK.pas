unit U_POLACZ_POLAK;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Data.DB, Vcl.Grids, Vcl.DBGrids, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.StdCtrls, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  Vcl.Imaging.jpeg, Vcl.ExtCtrls;

type
  TForm2 = class(TForm)
    FDConnection1: TFDConnection;
    FDQuery1: TFDQuery;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Button1: TButton;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    POKAZ_STACJE: TButton;
    Button2: TButton;
    DELETE1111: TButton;
    Image1: TImage;
    btnZDZIESZOWICE_NA_BOCZNICA: TButton;
    FDUpdateSQL1: TFDUpdateSQL;
    procedure btnZDZIESZOWICE_NA_BOCZNICAClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure DELETE1111Click(Sender: TObject);
    procedure POKAZ_STACJEClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}
  //  http://docwiki.embarcadero.com/RADStudio/Tokyo/en/Executing_Commands_(FireDAC)


procedure TForm2.btnZDZIESZOWICE_NA_BOCZNICAClick(Sender: TObject);
begin
//FDUpdateSQL1.InsertSQL := 'insert into t_STACJE (ST_NAZWA) values (:ST_NAZWA)';
//FDUpdateSQL1.Commands[arInsert].ParamByName('workstation_time').AsDateTime := Now();
  FDConnection1.ExecSQL('insert into T_STACJE (ST_NAZWA,ST_ID,ST_DATA_OD,ST_TYP,ST_NUMER,ST_KR_KR_ID,ST_ST_ID) values (:ST_NAZWA,:ST_ID,:ST_DATA_OD,:ST_TYP,:ST_NUMER,:ST_KR_KR_ID,:ST_ST_ID)',['Lichynia',99990,'12.12.2018',0,12345,1,25970]);
  FDQuery1.Refresh;
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
 FDConnection1.ExecSQL('INSERT INTO T_CZYNNOSCI VALUES (:CZ_ID, :CZ_SYMBOL, :CZ_OPIS, :CZ_TYP)', [1,1,1,1]);
 FDQuery1.Refresh;
 end;

procedure TForm2.Button2Click(Sender: TObject);
begin
 FDQuery1.SQL.Text:= 'SELECT * FROM T_CZYNNOSCI' ;
FDQuery1.Open;

end;

procedure TForm2.DELETE1111Click(Sender: TObject);
begin
FDConnection1.ExecSQL('DELETE FROM T_CZYNNOSCI WHERE CZ_ID = (:CZ_ID)', [1]);
 FDQuery1.Refresh;
end;

procedure TForm2.POKAZ_STACJEClick(Sender: TObject);
begin
FDQuery1.SQL.Text:= 'SELECT * FROM T_STACJE' ;
FDQuery1.Open;

end;

end.

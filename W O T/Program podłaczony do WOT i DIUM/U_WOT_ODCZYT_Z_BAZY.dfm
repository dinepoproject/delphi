﻿object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 466
  ClientWidth = 745
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 24
    Top = 24
    Width = 697
    Height = 169
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object btn1_polacz_z_baza: TButton
    Left = 24
    Top = 381
    Width = 179
    Height = 25
    Caption = 'btn1_polacz_z_baza'
    TabOrder = 1
    OnClick = btn1_polacz_z_bazaClick
  end
  object btn1_Wybierz_Zdzieszowice: TButton
    Left = 344
    Top = 433
    Width = 225
    Height = 25
    Caption = 'btn1_Wybierz_Zdzieszowice'
    TabOrder = 2
    OnClick = btn1_Wybierz_ZdzieszowiceClick
  end
  object edt1_nazwa_stacji: TEdit
    Left = 296
    Top = 383
    Width = 201
    Height = 21
    TabOrder = 3
    Text = 'ZDZIESZOWICE'
  end
  object btn1_połącz_bazą_Szczecin: TButton
    Left = 8
    Top = 416
    Width = 179
    Height = 25
    Caption = 'btn1_po'#322#261'cz_baz'#261'_Szczecin'
    TabOrder = 4
  end
  object DBGrid2: TDBGrid
    Left = 24
    Top = 240
    Width = 697
    Height = 120
    DataSource = DataSource2
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object ListBox1: TListBox
    Left = 48
    Top = 208
    Width = 121
    Height = 26
    ItemHeight = 13
    TabOrder = 6
  end
  object FDConnection1: TFDConnection
    ConnectionName = 'Po'#322#261'cz z FB'
    Params.Strings = (
      'Database=C:\Users\Lenovo\Desktop\Z A S i m\WOK.FDB'
      'CharacterSet=UTF8'
      'User_Name=sysdba'
      'Password=masterkey'
      'DriverID=FB')
    LoginPrompt = False
    Left = 632
    Top = 40
  end
  object FDQuery1: TFDQuery
    ConnectionName = 'Po'#322#261'cz z FB'
    SQL.Strings = (
      ''
      'select * from T_stacje')
    Left = 688
    Top = 208
  end
  object DataSource1: TDataSource
    DataSet = FDQuery1
    Left = 624
    Top = 200
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 640
    Top = 280
  end
  object FDQuery2: TFDQuery
    ConnectionName = 'Po'#322#261'cz z FB'
    SQL.Strings = (
      'select * from T_stacje')
    Left = 688
    Top = 160
  end
  object DataSource2: TDataSource
    DataSet = FDQuery2
    Left = 640
    Top = 360
  end
end

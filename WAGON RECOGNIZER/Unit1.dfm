object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 337
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 200
    Top = 149
    Width = 9
    Height = 25
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 104
    Top = 88
    Width = 289
    Height = 25
    Caption = 'WPROWADZ NUMER WAGONU'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 120
    Top = 289
    Width = 31
    Height = 13
    Caption = 'Label3'
  end
  object Label4: TLabel
    Left = 120
    Top = 250
    Width = 31
    Height = 13
    Caption = 'Label4'
  end
  object Label5: TLabel
    Left = 287
    Top = 289
    Width = 186
    Height = 13
    Caption = 'Label5'
  end
  object Button1: TButton
    Left = 104
    Top = 200
    Width = 225
    Height = 25
    Caption = 'SPRAWDZ'
    TabOrder = 0
    OnClick = Button1Click
    OnMouseEnter = Button1MouseEnter
    OnMouseLeave = Button1MouseLeave
  end
  object Edit1: TEdit
    Left = 105
    Top = 156
    Width = 89
    Height = 21
    NumbersOnly = True
    TabOrder = 1
    OnChange = Edit1Change
  end
  object Edit2: TEdit
    Left = 215
    Top = 156
    Width = 217
    Height = 21
    MaxLength = 8
    NumbersOnly = True
    TabOrder = 2
    OnChange = Edit2Change
    OnKeyUp = Edit2KeyUp
  end
end

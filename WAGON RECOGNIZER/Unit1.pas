﻿unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.StrUtils;
  type
  DANE = record
    PIEWRSZY_CIAG : string;
    DRUGI_CIAG : string;
    TRZECI_CIAG : string;

  end;
type
  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    Label1: TLabel;
    Edit2: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Button1MouseEnter(Sender: TObject);
    procedure Button1MouseLeave(Sender: TObject);
    procedure Button1Click(Sender: TObject);

    procedure Edit2KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    function dwie_cyfry: string;
    function cztery_cyfry: string;
    function SIEDEMCYFR:string;

    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  Unit2;

procedure TForm1.FormCreate(Sender: TObject);
begin
Edit1.Text:='';
end;

function TForm1.SIEDEMCYFR: string;
var
  cyfra: string;
  r: string;

begin
  cyfra := (Edit2.Text);
  r := LeftStr(cyfra, 7);

  case StrToInt(r) of


    5940000..5945912 : label5.caption:= 'eamos';
    5993000..5994045 : label5.caption:=  'EAMS' ;

end;
end;
procedure TForm1.Button1Click(Sender: TObject);
begin
  dwie_cyfry;
  cztery_cyfry;
  SIEDEMCYFR;
end;

procedure TForm1.Button1MouseEnter(Sender: TObject);
begin
  Button1.Caption := 'Wyświetla informację o wagonie';
end;

procedure TForm1.Button1MouseLeave(Sender: TObject);
begin
  Button1.Caption := 'Sprawdź';
end;



function TForm1.cztery_cyfry: string;
var
  cyfra: string;
  r: string;

begin
  cyfra := (Edit1.Text);
  r := rightstr(cyfra, 2);



  case StrToInt(r) of


    10 : label3.caption:= 'Fińskie Koleje Państwowe (VR)';
    20 : label3.caption:=  'Koleje Rosyjskie (RŻD)' ;
    21 : label3.caption:=  'Koleje Białoruskie (BC)' ;
    22 : label3.caption:=  'Koleje Ukraińskie (UZ)';
    23 : label3.caption:=  'Koleje Moładawskie (CFM)' ;
    24 : label3.caption:=  'Koleje Litewskie (LG)' ;
    25 : label3.caption:=  'Koleje Łotewskie (LDZ)' ;
    26 : label3.caption:=  'Koleje Estońskie (EVR)' ;
    27 : label3.caption:= 'Koleje Kazachskie (KZH)' ;
    28 : label3.caption:=  'Koleje Gruzińskie (GR)';
    29 : label3.caption:=  'Koleje Uzbeckie (SAZ)' ;
    30 : label3.caption:=  'Koleje Koreańskiej Republiki Ludowej (KRZ)';
    31 : label3.caption:=  'Koleje Mongolii (MTZ)';
    32 : label3.caption:=  'Koleje Wietnamskiej Republiki Ludowej (DSVN)';
    33 : label3.caption:=  'Koleje Chińskiej Republiki Ludowej (KZD)';
    40 : label3.caption:=  'Koleje Kubańskie (FC)' ;
    41 : label3.caption:=  'Albańskie Koleje Żelazne (HSH)';
    42 : label3.caption:=  'Koleje Japońskie (JR)';
    43 : label3.caption:=  'Kolej Żelazna Raab-Ebenfurt (GySEV)';
    44 : label3.caption:=  'Budapesztańskie Przedsiębiorstwo Transportowe (BHEV)';
    51 : label3.caption:=  'Polskie Koleje Państwowe (PKP)' ;
    52 : label3.caption:=  'Bułgarskie Koleje Państwowe (BDŻ)' ;
    53 : label3.caption:=  'Rumuńskie Koleje Żelazne (CFR)';
    54 : label3.caption:=  'Koleje Czeskie (CD)';
    55 : label3.caption:=  'Węgierskie Koleje Państwowe (MAV)';
    56 : label3.caption:=  'Koleje Republiki Słowacji (ZSR)';
    57 : label3.caption:=  'Koleje Azerbejżańskie (AZ)';
    58 : label3.caption:=  'Koleje Ormiańskie (ARM)';
    59 : label3.caption:=  'Koleje Kirgijskie (KRG)' ;
    60 : label3.caption:=  'Koleje Irlandzkie (CIE)';
    61 : label3.caption:=  'Koleje Koreańskie (KNR)' ;
    62 : label3.caption:=  'Prywatne Koleje Szwajcarskie (SP)' ;
    63 : label3.caption:=  'Kolej Bern-Lotschberg-Simpol (BLS)';
    64 : label3.caption:=  'Koleje Nord-Milano-Escercizio (FNME)';
    65 : label3.caption:=  'Kolej Żelazna Republiki Jugosławi i Macedoni (MŻ)';
    66 : label3.caption:=  'Koleje Tadżyckie (TZD)';
    68 : label3.caption:=  'Koleje Ahaus-Alstate (AAE)';
    69 : label3.caption:=  'Towarzystwo Eurotunel (Eurotunel)';
    70 : label3.caption:=  'Brytyjskie Towarzystwo Transportowe (RFD)';
    71 : label3.caption:=  'Państwowa Sieć Kolei Hiszpańskich (RENFE)';
    72 : label3.caption:=  'Wspólnota Kolei Jugosłowiańskich (JŻ)';
    73 : label3.caption:=  'Koleje Greckie (CH)';
    74 : label3.caption:=  'Szwedzkie Koleje Państwowe (SJ)';
    75 : label3.caption:=  'Tureckie Koleje Państwowe (TCDD)';
    76 : label3.caption:=  'Norweskie Koleje Państwowe (NSB)';
    78 : label3.caption:=  'Chorwackie Koleje Żelazne (HŻ)';
    79 : label3.caption:=  'Słoweńskie Koleje Żelazne (SŻ)';
    80 : label3.caption:=  'Niemiecka Kolej Federalna (DB)' ;
    81 : label3.caption:=  'Austriackie Koleje Federalne (OBB)';
    82 : label3.caption:=  'Narodowe Towarzystwo Kolei Luksemburskich (CFL)';
    83 : label3.caption:=  'Włoskie Koleje Państwowe(FS)';
    84 : label3.caption:=  'Holenderskie Koleje Żelazne (NS)';
    85 : label3.caption:=  'Szwajcarskie Koleje Federalne (SBB)';
    86 : label3.caption:=  'Duńskie Koleje Państwowe (DSB)' ;
    87 : label3.caption:=  'Towarzystwo Narodowe Dróg Żelaznych we Francji (SNCF)';
    88 : label3.caption:=  'Towarzystwo Narodowe Dróg Żelaznych w Belgii (B)';
    89 : label3.caption:=  'Koleje Bośni i Hercegowiny (ZBH)';
    90 : label3.caption:=  'Koleje Egipskie (ER)';
    91 : label3.caption:=  'Towarzystwo Narodowe Dróg Żelaznych w Tunezji (SNCFT)';
    92 : label3.caption:=  'Towarzystwo Narodowe Transportu Kolejowego w Algierii';
    93 : label3.caption:=  'Marokańskie Koleje Państwowe (ONCFM)';
    94 : label3.caption:=  'Koleje Prortugalskie (CP)';
    95 : label3.caption:=  'Izraelskie Koleje Państwowe (IR)';
    96 : label3.caption:=  'Irańskie Koleje Państwowe (RAI)';
    97 : label3.caption:=  'Syryjskie Koleje Żelazne (CFS)';
    98 : label3.caption:=  'Libańskie Koleje Państwowe (CEL)';
    99 : label3.caption:=  'Irackie Koleje Żelazne (IRR)';

  end;


end;

function TForm1.dwie_cyfry: string;
var
  cyfra: string;
  p: string;
begin
  cyfra := (Edit1.Text);
  p := leftstr(cyfra, 2);
  if Edit1.Text = '' then
    ShowMessage('wpisz numer wagonu')
  else
   case StrToInt(p) of
    01:  label4.caption:= 'Tor normalny / szeroki , ruch międzynarodowy w systemie RIV';
    20:  label4.caption:= 'Tor normalny, tylko komunikacja wewnętrzna';
    21:  label4.caption:= 'Tor normalny, ruch międzynarodowy w systemie RIV i MC';
    22:  label4.caption:= 'Tor normalny/szeroki, ruch międzynarodowy w systemie RIV i MC';
    30:  label4.caption:= 'Wagon dla potrzeb wewnętrznych kolei';
    31:  label4.caption:= 'Tor normalny, ładowność 40t, ruch międzynarodowy w systemie RIV i MC';
       else
       Label4.Caption:= '';
       end


end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  if Length(edit1.Text) > 3 then
    Edit2.SetFocus;

end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
  //Edit2.SelLength:= 8;
end;

procedure TForm1.Edit2KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 if Key = VK_RETURN then
    Button1.Click;
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
Edit1.SetFocus;
end;

initialization
  //MessageBox(0, 'Proszę o zapoznanie się z instrukcją przed rozpoczęciem korzystania z programu', 'Uwaga!', MB_OK);
finalization


end.


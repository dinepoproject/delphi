unit Raport_SAP_2017;

interface

uses
  System.SysUtils, System.Classes, frxClass, frxExportPDF;

type
  TDataModule1 = class(TDataModule)
    U_SAP_RAPORT: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    procedure U_SAP_RAPORTNewGetValue(Sender: TObject; const VarName: string; var
        Value: Variant);
  private
    { Private declarations }
    FDOSTAWASAP: string;
    FTONAZSMOLY : string;
    FSMOLABEZWODNA: string;
    FZAWARTOSCWODY: string;
    FSMOLAUWODNIONA: string;


  public
    { Public declarations }
    property dostawa_sap:STRING read FDOSTAWASAP write FDOSTAWASAP;
    property tonaz_smoly:STRING read FTONAZSMOLY write FTONAZSMOLY;
    property tonaz_bezwodny:STRING read FSMOLABEZWODNA write FSMOLABEZWODNA;
    property zawartosc_wody:STRING read FZAWARTOSCWODY write FZAWARTOSCWODY;
    property smola_uwodniona:STRING read FSMOLAUWODNIONA write FSMOLAUWODNIONA;
    procedure PODGLAD;
  end;

var
  DataModule1: TDataModule1;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDataModule1.PODGLAD;
begin
U_SAP_RAPORT.ShowReport;
end;

procedure TDataModule1.U_SAP_RAPORTNewGetValue(Sender: TObject; const VarName:
    string; var Value: Variant);
begin
    if VarName = 'dostawa_sap' then  Value :=dostawa_sap;
    if VarName = 'tonaz_smoly' then  Value :=tonaz_smoly;
    if VarName = 'tonaz_bezwodny' then  Value :=tonaz_bezwodny;
    if VarName = 'zawartosc_wody' then  Value :=zawartosc_wody;
    if VarName = 'smola_uwodniona' then  Value :=smola_uwodniona;
end;

end.

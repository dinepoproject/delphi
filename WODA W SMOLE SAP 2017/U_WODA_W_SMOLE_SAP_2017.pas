unit U_WODA_W_SMOLE_SAP_2017;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Menus,
  Vcl.Imaging.jpeg, Vcl.StdCtrls;

type
  TSMO�A_BEZWODNA_SAP_2017 = class(TForm)
    MainMenu1: TMainMenu;
    DRUKUJ1: TMenuItem;
    Panel1: TPanel;
    Panel2: TPanel;
    WYCZYSC: TMenuItem;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Splitter2: TSplitter;
    Panel4: TPanel;
    Image1: TImage;
    Label1: TLabel;
    DRUKUJ3: TMenuItem;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Label3: TLabel;
    Edit3: TEdit;
    Image2: TImage;
    Label4: TLabel;
    Label5: TLabel;
    Image3: TImage;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Timer1: TTimer;
    PopupMenu1: TPopupMenu;
    WKLEJ1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure DRUKUJ1Click(Sender: TObject);
    procedure DRUKUJ3Click(Sender: TObject);
    procedure WYCZYSCClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure WKLEJ1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  I_Oblicz = interface(IInterface)
    ['{31163D39-68A2-494E-8EE7-BF897EB23F91}']
    function OBLICZ(aTonaz: Single; aprocentwody: single): single;
  end;

  T_Oblicz = class(TInterfacedObject, I_Oblicz)
    function OBLICZ(aTonaz: Single; aprocentwody: single): single;
  end;

var
  SMO�A_BEZWODNA_SAP_2017: TSMO�A_BEZWODNA_SAP_2017;
  Uchwyt_do_interface: I_Oblicz;

implementation

{$R *.dfm}
uses
Raport_SAP_2017;

{ T_Oblicz }

function T_Oblicz.OBLICZ(aTonaz, aprocentwody: single): single;
begin

  if (aTonaz <= 0) or (aprocentwody <= 0) then
  begin
    raise Exception.Create('Wprowad� poprawne dane, warto�� musi by� dodatnia');
  end;
     // ten wyjatek wyswietla tak�e okno w .exe
  if aTonaz >= 10000 then
  begin
    raise Exception.Create('Wprowad� poprawne dane, warto�� nie mo�e przekracza� 10000');
  end;

  if aprocentwody >= 100 then
  begin
    raise Exception.Create('Wprowad� poprawne dane, nieprawid�owy procent');
  end;
  if aprocentwody > 5 then
  begin
    MessageDlg('PROGRAM DOKONA OBLICZE�, ALE PROCENT WODY POWINIEN BY� MNIEJSZY NI� 5.', mtInformation, [mbOK], 0);
  end;

  RESULT := aTonaz - (aTonaz * aprocentwody / 100);
end;

procedure TSMO�A_BEZWODNA_SAP_2017.FormCreate(Sender: TObject);
begin
  Label8.Caption := DateTimeToStr(NOW);
  PopupMenu1.AutoPopup := True;
  // uruchamianie popup prawym przyciskiem
  Edit1.PopupMenu := PopupMenu1;
  //przypisanie do komponentu pop up'a
  Edit2.PopupMenu := PopupMenu1;
  Edit3.PopupMenu := PopupMenu1;
end;

procedure TSMO�A_BEZWODNA_SAP_2017.DRUKUJ1Click(Sender: TObject);
begin
  if (edit1.text = '') or (edit2.text = '') then
  begin
    raise Exception.Create('Wprowad� warto��');
  end;
  try

    Uchwyt_do_interface := T_Oblicz.Create;
    Label4.Caption := formatfloat('0.000', (Uchwyt_do_interface.OBLICZ(Strtofloat(edit1.text), Strtofloat(edit2.text))));
    Label5.Caption := Edit1.Text;
    Label6.Caption := 'ZAWARTO�� WODY:' + ' ' + Edit2.Text + ' ' + '%';
    Label7.Caption := 'SMO�A UWODNIONA:' + ' ' + Edit1.Text + ' ' + 'TO';

  except
//on E:Exception do
//MessageDlg('NIE WPISA�E� LICZB', mtInformation, [mbOK], 0) ;
    on e: EConvertError do
      MessageDlg('NIEPRAWID�OWY FORMAT WPROWADZONEGO TEKSTU', mtInformation, [mbOK], 0);

    on EDivByZero do
      MessageDlg('nie dziel przez zero!', mtInformation, [mbOK], 0);
 // tu daje swoj wyjatek
  end;
  Panel2.Color := clSilver;
end;

procedure TSMO�A_BEZWODNA_SAP_2017.DRUKUJ3Click(Sender: TObject);
begin
  DataModule1.DOSTAWA_SAP:= Edit3.Text;
  DataModule1.tonaz_bezwodny:= Label4.Caption;
  DataModule1.tonaz_smoly:= Edit1.Text;
  DataModule1.zawartosc_wody:= Label6.Caption;
  DataModule1.smola_uwodniona:= Label7.Caption;
  DataModule1.PODGLAD;
end;

procedure TSMO�A_BEZWODNA_SAP_2017.WYCZYSCClick(Sender: TObject);
begin
  Edit1.Text := '';
  Edit2.Text := '';
  Edit3.Text := '';

  Label4.Caption := '';
  Label5.Caption := '';
  Label6.Caption := '';
  Label7.Caption := '';
  Panel2.Color := clMoneyGreen;
end;

procedure TSMO�A_BEZWODNA_SAP_2017.Timer1Timer(Sender: TObject);
begin
  Label8.Caption := DateTimeToStr(NOW);
end;

procedure TSMO�A_BEZWODNA_SAP_2017.WKLEJ1Click(Sender: TObject);
begin
  if PopupMenu1.PopupComponent = Edit1 then
    Edit1.PasteFromClipboard
  else if PopupMenu1.PopupComponent = Edit2 then
    Edit2.PasteFromClipboard
  else if PopupMenu1.PopupComponent = Edit3 then
    Edit3.PasteFromClipboard
end;

end.


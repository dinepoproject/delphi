unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;
   type
  Tmaxkm_h= record
  Polonez: Integer;
  Fiat:Integer;
  Volkswagen: Integer;
  Citroen: Integer;
  Opel:Integer;

    end;
      Pmaxkm_h =  ^Tmaxkm_h;

      type
      Tkubki= record
      kawa: string;
      mleko: string;
      woda: string;

      end;
      Pkubki = ^Tkubki;
       // @ - przydzielam
      // ^ - wskazuje na...
      type
      tzelazka = record
      // mialem tu blad bo tak samo nazwalem buttona i record ale juz ok
      parowe_temp: Integer;
      weglowe_temp: Integer;
      metalowe_temp: Integer;
      end;
      Pzelazka = ^tzelazka;
type
  TForm1 = class(TForm)
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    Button1: TButton;
    Button2: TButton;
    zelazka: TButton;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ZELAZKAClick(Sender: TObject);

    procedure zmien_temp_metal_zelazka(zmiana_temp:Pzelazka);

  private
  procedure dodajdowskaznika(zmianapredkosci: Pmaxkm_h);
  procedure zmienkolor(innyKolor:Pkubki);
    { Private declarations }
  public
    { Public declarations }

  end;

  PInteger = ^Integer;  //przydzia� do button2.click
//jak robic-
//1.deklarujemy typ wskaznikowy(powyzej) WAZNE !!! TERAZ CZYTAJ:
// 14.04.2017 dopisa�em (ktory zawiera typ np. integere, string)
//2. przydzielamy zmiennej typ wskaznik-     w1,w2,w3,w4: PInteger;
//3.pokazujemy wskaznik malpa-    w1 := @zmienna_zrodlo;
//3.przydzielamy wskaznikiem wartosc dla zmienej- w3^ := zmienna zrodlo+ 6;
//przyk�ad  z ksiazki
{
program Pointers;

{$APPTYPE CONSOLE}
 {
var
  S : String;
  P : ^String;

begin
  S := 'Delphi'; // przypisanie danych do zwyk�ej zmiennej
  P := @S; // uzyskanie adresu zmiennej
  P^ := 'Delphi jest fajne'; // modyfikacja danych

  Writeln(S);
  Readln;
end. }

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
var
  i1, i2: Integer;
  i3:PInteger;
begin
  i1 := 5;
  i3:= @i1;
  i3^ :=  7;  // przerobilem ;)
  //i2 := i1;
  //i2 := 7;
  btn1.Caption := i1.ToString;
end;

procedure TForm1.btn2Click(Sender: TObject);
var
  zmienna_zrodlo: Integer;
  a:Integer;
  w1,w2,w3,w4: PInteger;   //chyba Pinteger zastepuje ^integer (tak jak jest w ksiazce)??
  // 14.04.17 chyba nie bo wczesniej zadeklarowales  PInteger = ^Integer;
begin

  a:=20;
  zmienna_zrodlo := 5;
  w1 := @zmienna_zrodlo;
  w2 := @zmienna_zrodlo;
  w3 := @zmienna_zrodlo;
  w4 := @zmienna_zrodlo;
  w2^ := zmienna_zrodlo + a;
  w3^ := zmienna_zrodlo+ 6;
  w1^ := zmienna_zrodlo -30;
  btn2.Caption := w4^.ToString;
   //zmieni�em sobie kod, wiem juz o co chodzi, zmieniasz i jednym wskaznikiem a reszta wskaznikow ma taka sama wartosc
   //A widzisz, jak doda�em I3^ to doda� mi i mia�em 31
   // mozna sobie regulowac zmienna :)
end;

procedure TForm1.btn3Click(Sender: TObject);
var
  tablica: array [1..10] of Integer;
  i: PInteger;
  j: Integer;
  a:PInteger;
  l:PInteger; //faktyczna zawartosc tablicy
  c:PInteger;

begin
  for j := 1 to 10 do tablica[j] := j;

  i := @(tablica[3]);
  //Inc(i,7);
  i^:= (tablica[3] +10);
  c:= @(tablica[3]);
  //Inc(c,10); //
   btn3.Caption := i^.ToString; //przenioslem to bo zle pokazywal
   //tyle samo co c jak bylo na koncu
  C^:=(tablica[3]+11);

  //manipuluje drugi raz zmienna i za pomoca drugiego wskaznika c


  // jak widac po dodaniu ^ trzeba sie nim
  //poslugiwac caly czas, ma�pa tylko raz wskazuje.
  showMessage(c^.ToString);
  //teraz dziala i ma wartosc 24.
end;

procedure TForm1.Button1Click(Sender: TObject);
  var
  predkosc:Tmaxkm_h;
  zmiennarec: Pmaxkm_h;


begin
predkosc.Polonez := 100;
predkosc.Fiat:= 140;
//predkosc.Volkswagen := 100;
//predkosc.Citroen:= 140;
//predkosc.Opel:= 140;
//zmiennarec := @predkosc;
//zmiennarec.Polonez:= 200;  14.04.2017 TAK TEZ DZIA�A
dodajdowskaznika(@predkosc);  //obojetnie ktora dasz to musi dzialac
//dodajdowskaznika(zmiennarec);
ShowMessage('predkosc samochodu Polonez to:' + predkosc.Polonez.ToString + 'predkosc samochodu Fiat to:' + predkosc.Fiat.tostring);
end;

procedure TForm1.Button2Click(Sender: TObject);
var
kolor :Tkubki;
kolorwsk: Pkubki;
begin
kolor.kawa:= 'bia�y';
kolor.mleko := 'czarny';
kolor.woda := 'zolty';
kolorwsk:= @kolor;
zmienkolor(kolorwsk);// zwroc uwage, ze inaczej jest tu to zrobione
// a inaczej w polonezie
ShowMessage(kolor.woda);

end;

procedure TForm1.dodajdowskaznika(zmianapredkosci: Pmaxkm_h);
begin
zmianapredkosci.polonez := zmianapredkosci.polonez + 100;
zmianapredkosci.Fiat:= zmianapredkosci.Fiat-50;
end;

procedure TForm1.ZELAZKAClick(Sender: TObject);
var
PRASUJ: tzelazka;
 zmiana_temp_prasow:Pzelazka;
begin
PRASUJ.parowe_temp:= 5;
PRASUJ.weglowe_temp:=5;
PRASUJ.metalowe_temp:=5;      // NIE DZIA�A POKI CO JAK POWINNO
                             // BO NIE ZMIENIA PRASUJ.METALOWE
                             // A JEDYNIE TWORZY NOWY WSKAZNIK
                             // DOPRACUJ TO
 New(zmiana_temp_prasow);        // M E T O D A  Z   N E W
 {zmien_temp_metal_zelazka(@PRASUJ);
 ShowMessage (IntToStr(prasuj.metalowe_temp)); } // to bylo ok, ale niepotrzebne
 zmiana_temp_prasow^.weglowe_temp:= 200;
 zmiana_temp_prasow^.parowe_temp:= 100;
 zmiana_temp_prasow^.metalowe_temp:= 700;
 // TERAZ SOBIE ZMIENIAJ:

 //ShowMessage (IntToStr(PRASUJ.metalowe_temp));
  ShowMessage (IntToStr(ZMIANA_TEMP_PRASOW^.metalowe_temp));
 Dispose(zmiana_temp_prasow);
 end;



procedure TForm1.zmienkolor(innyKolor: Pkubki);
begin
innyKolor.woda := 'czerwony';
end;

procedure TForm1.zmien_temp_metal_zelazka(zmiana_temp:Pzelazka);
begin

 zmiana_temp.metalowe_temp:= 300; // TODO -cMM: TForm1.zmien_temp_metal_zelazka default body inserted
end;

end.

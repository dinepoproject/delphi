object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 447
  ClientWidth = 738
  Color = clBtnFace
  TransparentColor = True
  TransparentColorValue = clBackground
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 24
    Top = 88
    Width = 689
    Height = 233
    Color = clHighlight
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 24
    Top = 61
    Width = 673
    Height = 21
    TabOrder = 1
    Text = 'Edit1'
  end
  object Button2: TButton
    Left = 536
    Top = 22
    Width = 153
    Height = 25
    Caption = 'ZAPISZ DO PLIKU'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Panel1: TPanel
    Left = 0
    Top = 376
    Width = 738
    Height = 71
    Align = alBottom
    Color = clGray
    ParentBackground = False
    TabOrder = 3
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 736
      Height = 69
      Align = alClient
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      WordWrap = True
      ExplicitWidth = 4
      ExplicitHeight = 16
    end
  end
  object Button1: TButton
    Left = 40
    Top = 16
    Width = 75
    Height = 25
    Caption = 'OTW'#211'RZ'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Panel2: TPanel
    Left = 0
    Top = 335
    Width = 738
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel2'
    Color = clBackground
    ParentBackground = False
    TabOrder = 5
    ExplicitLeft = 24
    ExplicitTop = 344
    ExplicitWidth = 185
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.TXT'
    Filter = 'TEKSTOWE|*.TXT'
    Left = 240
    Top = 8
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.TXT'
    Filter = 'TEKSTOWE|*.TXT'
    Left = 328
    Top = 8
  end
end

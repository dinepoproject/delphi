unit U_INI_FILES;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, INIFILES, Vcl.StdCtrls ; // CTRL + SPACJA - KONTEKSTOWA PODPOWIE

type
  TForm1 = class(TForm)
    btn1_POKAZ_DRUGA_FORMATKE: TButton;
    procedure btn1_POKAZ_DRUGA_FORMATKEClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  Form1: TForm1;
 // zeby panel zapamietywal kolory trzeba zmienic parent background na false
 myIni: TIniFile;
 procedure saveforplacement(frm:TForm);
 procedure readforplacement(frm:TForm);

implementation

{$R *.dfm}

uses Unit2;
procedure saveforplacement(frm:TForm);
var
key:string;
begin
key:= frm.Name;
myIni.WriteInteger('Placement', key + 'left', Frm.Left);
myIni.WriteInteger('Placement', key + 'top', Frm.Top);
myIni.WriteInteger('Placement', key + 'width', Frm.Width);
myIni.WriteInteger('Placement', key + 'height', Frm.Height);
end;

procedure readforplacement(frm:TForm);
var
key:string;
begin
key:= frm.Name;
Frm.Left:=   myIni.ReadInteger('Placement', key + 'left',(Screen.Width - frm.Width) div 2);
Frm.Top :=   myIni.ReadInteger('Placement', key + 'top',(Screen.Height - frm.Height) div 2 );
Frm.Width:=  myIni.ReadInteger('Placement', key + 'width',Frm.Width );
Frm.Height:= myIni.ReadInteger('Placement', key + 'height',Frm.Height );
end;

procedure TForm1.btn1_POKAZ_DRUGA_FORMATKEClick(Sender: TObject);
begin
FRM_ustawienia.ShowModal;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  saveforplacement(Form1);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
FreeAndNil(myIni);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
myIni:= TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
 readforplacement(Form1)
end;
 // stop na 20.41
end.

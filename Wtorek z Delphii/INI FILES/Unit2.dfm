object FRM_ustawienia: TFRM_ustawienia
  Left = 0
  Top = 0
  Caption = 'USTAWIENIA'
  ClientHeight = 217
  ClientWidth = 586
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Edit1: TEdit
    Left = 16
    Top = 16
    Width = 169
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object btn1_USTAW: TButton
    Left = 304
    Top = 14
    Width = 137
    Height = 25
    Caption = 'btn1_USTAW'
    TabOrder = 1
    OnClick = btn1_USTAWClick
  end
  object Panel1: TPanel
    Left = 56
    Top = 111
    Width = 185
    Height = 41
    Caption = 'Panel1'
    ParentBackground = False
    TabOrder = 2
  end
  object btn1_ZMIEN_KOLOR: TButton
    Left = 304
    Top = 120
    Width = 137
    Height = 25
    Caption = 'btn1_ZMIEN_KOLOR'
    TabOrder = 3
    OnClick = btn1_ZMIEN_KOLORClick
  end
  object Edit2: TEdit
    Left = 304
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'FF0000'
  end
end

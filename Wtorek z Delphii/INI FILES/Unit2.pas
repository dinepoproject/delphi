unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFRM_ustawienia = class(TForm)
    Edit1: TEdit;
    btn1_USTAW: TButton;
    Panel1: TPanel;
    btn1_ZMIEN_KOLOR: TButton;
    Edit2: TEdit;
    procedure btn1_USTAWClick(Sender: TObject);
    procedure btn1_ZMIEN_KOLORClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRM_ustawienia: TFRM_ustawienia;

implementation

{$R *.dfm}
USES U_INI_FILES;

procedure TFRM_ustawienia.btn1_USTAWClick(Sender: TObject);
begin
myIni.WriteString('USTAWIENIA','NAZWA', Edit1.Text);
end;

procedure TFRM_ustawienia.btn1_ZMIEN_KOLORClick(Sender: TObject);
begin
myIni.WriteString('KOLORY', 'PANEL_COLOR', Edit2.Text);
Panel1.Color:= (StrToIntDef('$' + Edit2.Text, $FF0000));
end;

procedure TFRM_ustawienia.FormCreate(Sender: TObject);
begin
readforplacement(FRM_ustawienia);
Edit1.Text:= myIni.ReadString('USTAWIENIA','NAZWA', 'DOMYSLNA WARTOSC');
Edit2.Text:= myIni.ReadString('KOLORY', 'PANEL_COLOR','$FF0000');
Panel1.Color:= (StrToIntDef('$' +EDIT2.Text , $FF0000));

end;

procedure TFRM_ustawienia.FormClose(Sender: TObject; var Action: TCloseAction);
begin
saveforplacement(FRM_ustawienia);
end;

end.

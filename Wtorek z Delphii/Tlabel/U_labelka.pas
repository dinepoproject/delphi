unit U_labelka;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage,Winapi.ShellAPI;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    Panel1: TPanel;
    Button2: TButton;
    Label2: TLabel;
    Image1: TImage;
    Label3: TLabel;
    Label4: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label4MouseDown(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
    procedure Label4MouseEnter(Sender: TObject);
    procedure Label4MouseLeave(Sender: TObject);
    procedure Label4MouseUp(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  // PRAWY KLAWISZ, VIEW AS TEXT, WPISUJEMY W NAPISIE LABELKI '#13'
  //I DZIELI NAM TO ZDANIE NA 2 RZEDY

  // ABY ZMIENIC KOLOR LABELA TRANSPARENT USTAWIAMY NA FALSE


implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
Label1.Caption:= 'DINEPO'#13'DANIEL'#13'PALUBSKI';
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
Label2.Caption:= Label1.Caption;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
Label3.Caption:= 'ZDARZENIE'#13'WYWOLANE'#13'ON_SHOW'#13'FORMATKI';
end;

procedure TForm1.Label4Click(Sender: TObject);
begin
//   https://4programmers.net/Delphi/FAQ/W_jaki_spos%C3%B3b_otworzy%C4%87_klienta_pocztowego_lub_przegl%C4%85dark%C4%99_internetow%C4%85
SHELLEXECUTE(HandlE, 'OPEN',PChar ('HTTP://WWW.ATNEL.PL'),nil,nil, SW_SHOW);
end;

procedure TForm1.Label4MouseDown(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
begin
Label4.Top:= Label4.Top+2;
Label4.Left:= Label4.Left+2;
end;

procedure TForm1.Label4MouseEnter(Sender: TObject);
begin
Label4.Font.Style:=[fsBold,fsUnderline];
Label4.Font.Color:= clBlue;
end;

procedure TForm1.Label4MouseLeave(Sender: TObject);
begin
 Label4.Font.Style:=[];
Label4.Font.Color:= clBlack;
end;

procedure TForm1.Label4MouseUp(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
begin
Label4.Top:= Label4.Top-2;
Label4.Left:= Label4.Left-2;
end;

end.

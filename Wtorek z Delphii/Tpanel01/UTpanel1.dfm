object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 438
  ClientWidth = 779
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 185
    Top = 29
    Height = 409
    ExplicitLeft = 328
    ExplicitTop = 160
    ExplicitHeight = 100
  end
  object Splitter3: TSplitter
    Left = 591
    Top = 29
    Height = 409
    Align = alRight
    ExplicitLeft = 480
    ExplicitTop = 192
    ExplicitHeight = 100
  end
  object Panel1: TPanel
    Left = 0
    Top = 29
    Width = 185
    Height = 409
    Align = alLeft
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 1
      Top = 321
      Width = 183
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitLeft = 0
      ExplicitTop = 225
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 183
      Height = 320
      Align = alTop
      Constraints.MinWidth = 50
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 1
      Top = 324
      Width = 183
      Height = 84
      Align = alClient
      Constraints.MinWidth = 50
      TabOrder = 1
    end
  end
  object Panel4: TPanel
    Left = 594
    Top = 29
    Width = 185
    Height = 409
    Align = alRight
    TabOrder = 1
    object Splitter4: TSplitter
      Left = 1
      Top = 324
      Width = 183
      Height = 1
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 224
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 183
      Height = 323
      Align = alTop
      Constraints.MinWidth = 50
      TabOrder = 0
    end
    object Panel6: TPanel
      Left = 1
      Top = 325
      Width = 183
      Height = 83
      Align = alClient
      Constraints.MinWidth = 50
      TabOrder = 1
    end
  end
  object Panel7: TPanel
    Left = 188
    Top = 29
    Width = 403
    Height = 409
    Align = alClient
    TabOrder = 2
    object Splitter5: TSplitter
      Left = 1
      Top = 330
      Width = 401
      Height = 2
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 219
    end
    object Panel8: TPanel
      Left = 1
      Top = 1
      Width = 401
      Height = 329
      Align = alClient
      TabOrder = 0
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 399
        Height = 327
        Align = alClient
        Constraints.MinHeight = 50
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object Panel9: TPanel
      Left = 1
      Top = 332
      Width = 401
      Height = 76
      Align = alBottom
      TabOrder = 1
      object DBGrid2: TDBGrid
        Left = 1
        Top = 1
        Width = 399
        Height = 74
        Align = alClient
        Constraints.MinHeight = 50
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 779
    Height = 29
    Caption = 'ToolBar1'
    TabOrder = 3
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Caption = 'ToolButton1'
      ImageIndex = 0
    end
    object ToolButton2: TToolButton
      Left = 23
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 31
      Top = 0
      Caption = 'ToolButton3'
      ImageIndex = 1
    end
    object ToolButton4: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 62
      Top = 0
      Caption = 'ToolButton5'
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 85
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 3
      Style = tbsSeparator
    end
  end
  object MainMenu1: TMainMenu
    Left = 713
    Top = 105
    object menugwne1: TMenuItem
      Caption = 'menu g'#322#243'wne'
    end
  end
end

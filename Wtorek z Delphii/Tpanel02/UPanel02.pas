unit UPanel02;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
if Panel1.Color = clMaroon then  Panel1.Color := clRed else
Panel1.Color := clMaroon;

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
 if Panel2.Color = clBlue then  Panel2.Color :=clAqua else
Panel2.Color := clblue;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
   if Panel3.Color = clgreen then  Panel3.Color :=clLime else
Panel3.Color := clgreen;
end;

end.

unit wlasciwosci;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  Tklasa = class
  private // pola wlasciwosci sa prywatne
    wartosc: Integer;
    function odczyt: integer;
    procedure zapis(const Value: Integer);
  public
    property liczba: Integer read odczyt write zapis;
    constructor Create;
    destructor destroy; override;
  end;

  TForm1 = class(TForm)
    btn1_zapisz_edit1_do_wlasciwosci: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    btn2_pokaz_wartosc_wlasciwosci: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1_zapisz_edit1_do_wlasciwosciClick(Sender: TObject);
    procedure btn2_pokaz_wartosc_wlasciwosciClick(Sender: TObject);
  private
    { Private declarations }
    Iklasa: Tklasa;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor destroy; override;
  end;




var
  Form1: TForm1;


implementation

{$R *.dfm}

procedure TForm1.btn1_zapisz_edit1_do_wlasciwosciClick(Sender: TObject);
begin
  Iklasa.liczba := StrToInt(form1.Edit1.Text); // do wlasciwosci przypisujemy liczbe
  form1.Edit1.Text := '';
end;

constructor TForm1.Create(AOwner: TComponent);
begin
  Iklasa := Tklasa.Create;


  inherited;
end;

destructor TForm1.destroy;
begin
  iklasa.free;
  inherited;
end;

procedure TForm1.btn2_pokaz_wartosc_wlasciwosciClick(Sender: TObject);
begin
  Form1.Edit2.Text := IntToStr(Iklasa.wartosc);  // pokazuje przypisana wartosc
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
form1.Edit1.Text := '';
form1.Edit2.Text := '';
end;

constructor Tklasa.Create;
begin

end;

destructor Tklasa.destroy;
begin
  inherited;
end;

function Tklasa.odczyt: integer;
begin
  result := wartosc; // funkcja odczytuje z pola wartosc
end;

procedure Tklasa.zapis(const Value: Integer);
begin
  wartosc := Value;  // funkcja zapisuje do pola
end;

end.


object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 662
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    AlignWithMargins = True
    Left = 296
    Top = 192
    Width = 417
    Height = 377
    Enabled = False
    ParentShowHint = False
    ShowHint = False
    Stretch = True
  end
  object Panel1: TPanel
    Left = 48
    Top = 17
    Width = 625
    Height = 41
    Caption = 'Panel1'
    TabOrder = 0
  end
  object DriveComboBox1: TDriveComboBox
    Left = 49
    Top = 28
    Width = 201
    Height = 19
    DirList = DirectoryListBox1
    TabOrder = 1
  end
  object FilterComboBox1: TFilterComboBox
    Left = 272
    Top = 28
    Width = 201
    Height = 21
    FileList = FileListBox1
    Filter = 'Bitmapa (*.bmp)|*.bmp|zdjecia (*.jpg)|*.jpg'
    TabOrder = 2
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 49
    Top = 88
    Width = 201
    Height = 385
    FileList = FileListBox1
    TabOrder = 3
  end
  object FileListBox1: TFileListBox
    Left = 352
    Top = 64
    Width = 305
    Height = 97
    ItemHeight = 13
    Mask = '*.bmp'
    TabOrder = 4
    OnChange = FileListBox1Change
  end
end
